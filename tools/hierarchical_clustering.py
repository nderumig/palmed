import pickle
from functools import wraps
import datetime
from collections import defaultdict
from typing import Sequence, NamedTuple, List, Mapping, Dict, Callable, Optional, Tuple
import numpy as np  # type:ignore
from scipy.spatial.distance import pdist  # type:ignore
from scipy.cluster import hierarchy  # type:ignore
from scipy.optimize import curve_fit  # type:ignore
from scipy import signal  # type:ignore
from matplotlib import pyplot as plt  # type:ignore
from sklearn.metrics import silhouette_score  # type:ignore

from palmed.messages import ProgressBar
from palmed.instructions import Instruction
from pipedream.asm.x86.instructions_xed import ISA  # type:ignore
import pipedream.asm.ir as pipeasm

pipearch = pipeasm.Architecture.for_name("x86")
pipe_instr_set = pipearch.instruction_set()


def timed(fct):
    @wraps(fct)
    def wrapped(*args, **kwargs):
        start = datetime.datetime.now()
        out = fct(*args, **kwargs)
        delta = datetime.datetime.now() - start
        print("[Executed in {}]".format(delta))
        return out

    return wrapped


def load_from_checkpoint_quadra_dense(handle):
    """Load the QUADRA_DENSE checkpoint file passed as parameter, and return the tuple
    (bench_matrix, ipc_matrix, instruction_list)"""

    bench_matr = pickle.load(handle).reconstructed_matr
    ipc_matr = np.array([[bench.IPC() for bench in row] for row in bench_matr])
    instructions = []
    for pos in range(len(bench_matr)):
        bench = bench_matr[pos][pos]
        instrs = bench.instrs()
        if len(instrs) != 1:
            raise Exception(
                "Diagonal bench with more than one instruction: {}".format(bench)
            )
        instructions.append(instrs[0])

    return bench_matr, ipc_matr, instructions


def clustering(ipc_matr: np.array, method: str = "average") -> np.array:
    return hierarchy.linkage(ipc_matr, method)


def get_confidences(clusters: np.array) -> Sequence[float]:
    return [merge[2] for merge in clusters]


def smooth_exp(data: np.array) -> np.array:
    """Smooth a dataset with exponential decay"""
    DECAY_COEF = 0.2
    fwd = [data[0]]
    bwd = [data[-1]]
    for elt in data[1:]:
        fwd.append(DECAY_COEF * fwd[-1] + (1 - DECAY_COEF) * elt)
    for elt in data[:-1:-1]:
        bwd.append(DECAY_COEF * bwd[-1] + (1 - DECAY_COEF) * elt)
    bwd = bwd[::-1]

    flat = (np.array(fwd) + np.array(bwd)) / 2
    return flat


def smooth_avg(data: np.array, window_size: int = 5) -> np.array:
    """Smooth a dataset by taking the average value on a window"""
    assert window_size % 2 == 1
    half_window = window_size // 2
    out = np.zeros(len(data))
    for pos in range(half_window):
        out[pos] = data[pos]
        out[len(data) - pos - 1] = data[len(data) - pos - 1]

    avg = sum(data[:window_size]) / window_size
    for pos, val in enumerate(data[window_size:]):
        out[pos + half_window] = avg
        avg += val / window_size
        avg -= data[pos] / window_size
    out[-half_window - 1] = avg
    return out


def plot_smooth(data: np.array, smooth_func=smooth_avg, **smooth_args):
    X = range(len(data))
    plt.plot(X, data, "bx")
    plt.plot(X, smooth_func(data, **smooth_args), "r-")
    plt.show()


def trybound(bound: float, clusters: np.array, show: bool = True):
    """Show the effect of cutting clusters at a given bound"""
    confidences = get_confidences(clusters)
    idx = 0
    for pos, confidence in enumerate(confidences):
        if confidence > bound:
            idx = pos - 1
            break
    plt.plot(range(len(clusters)), confidences, "bx")
    slope = (confidences[idx + 1] - confidences[idx - 1]) / 2
    y0 = confidences[idx] - slope * idx
    plt.plot([0, len(clusters)], [y0, len(clusters) * slope + y0], "r--")
    plt.plot([0, len(clusters)], [confidences[idx], confidences[idx]], "r--")
    plt.ylim(bottom=0)
    print(
        "Last merge: {} -- remains {} groups. Slope: {}".format(
            idx, len(confidences) - idx, slope
        )
    )
    if show:
        plt.show()


class CutLinkage(NamedTuple):
    clusters: np.array
    original_clusters: np.array
    node_map: List[int]
    rev_node_map: Mapping[int, int]
    leaves_of: List[List[int]]
    instr_of_id: List[Instruction]
    ipc_matr: np.array

    def __repr__(self):
        return "<CutLinkage: first {} splits out of {}>".format(
            len(self.clusters), len(self.original_clusters)
        )

    def silhouette(self) -> float:
        labels = [0] * len(self.ipc_matr)
        cluster_size = [-1] * len(self.ipc_matr)
        for label, cls in enumerate(self.leaves_of):
            for leaf in cls:
                if cluster_size[leaf] < 0 or cluster_size[leaf] > len(cls):
                    labels[leaf] = label
                    cluster_size[leaf] = len(cls)
        # DEBUG
        labelset = set()
        for elt in labels:
            labelset.add(elt)
        if len(labelset) != len(self.clusters):
            print(
                ">>> Expected {} clusters, got {}".format(
                    len(self.clusters), len(labelset)
                )
            )
        return silhouette_score(self.ipc_matr, labels)


def cut_tree(
    clusters: np.array,
    cut_value: float,
    ipc_matr: np.array,
    instr_of_id: List[Instruction],
) -> CutLinkage:
    """Cut the cluster tree at a given value, keeping only the most significant links"""
    out = {
        "original_clusters": clusters,
        "instr_of_id": instr_of_id,
        "ipc_matr": ipc_matr,
    }

    # Prune clusters
    new_clusters = []
    for row in clusters[::-1]:
        if row[2] < cut_value:
            break
        new_clusters.append(np.array(row))
    new_clusters = new_clusters[::-1]

    # Reindex clusters
    present_ids = set()
    for row in new_clusters:
        for pos in (0, 1):
            present_ids.add(row[pos])
    present_ids.add(2 * len(clusters))  # root node
    id_map = {}
    for pos, elt in enumerate(sorted(present_ids)):
        id_map[int(elt)] = pos
    for row in new_clusters:
        for pos in (0, 1):
            row[pos] = id_map[row[pos]]

    node_map = [0] * len(id_map)
    for key, val in id_map.items():
        node_map[val] = key
    out["clusters"] = np.array(new_clusters)
    out["node_map"] = node_map
    out["rev_node_map"] = id_map

    # Fill leaves_of
    leaves_of: List[List[int]] = [[] for _ in range(len(node_map))]

    def dfs(node):
        if node.is_leaf():
            return [node.id]
        sub_nodes = dfs(node.left) + dfs(node.right)
        if node.id in id_map:
            sub_nodes.sort()  # efficient enough -- we don't have many iterations
            leaves_of[id_map[node.id]] = sub_nodes
        return sub_nodes

    dfs(hierarchy.to_tree(clusters))
    out["leaves_of"] = leaves_of

    return CutLinkage(**out)


cluster_annotation = Callable[[int, CutLinkage], str]


def cluster_id_of_value(linkage: np.array, value: float):
    """Find the cluster ID that has the given distance value in the linkage list.
    This is O(N) although it could be bisected -- but should be way enough for what we
    want."""

    eps = 1e-6

    for pos, row in enumerate(linkage):
        curValue = row[2]
        if curValue - eps < value < curValue + eps:
            return len(linkage) + 1 + pos

    raise KeyError("Value {} not found in linkage".format(value))


def cut_dendrogram(
    linkage: CutLinkage,
    show: bool = False,
    annotate_cluster: Optional[cluster_annotation] = None,
):
    """Plot the dendogram related to a cut linkage"""

    def annot(instr: int) -> str:
        cluster_annot = ""
        if annotate_cluster is not None:
            cluster_annot = "\n" + annotate_cluster(instr, linkage)
        return "#{}{}".format(len(linkage.leaves_of[instr]), cluster_annot)

    ddata = hierarchy.dendrogram(
        linkage.clusters, leaf_label_func=annot, leaf_font_size=8.0
    )

    for i, d, color in zip(ddata["icoord"], ddata["dcoord"], ddata["color_list"]):
        x = 0.5 * sum(i[1:3])
        y = d[1]
        plt.plot(x, y, "o", c=color)
        cluster_id = cluster_id_of_value(linkage.clusters, y)
        plt.annotate(
            annot(cluster_id),
            (x, y),
            xytext=(0, -5),
            textcoords="offset points",
            va="top",
            ha="center",
            fontsize=8.0,
        )

    if show:
        plt.show()


# ========== Instruction properties ==========
def instr_has_port(instr, port: str) -> bool:
    ports_str = instr._ports_str
    if not ports_str:
        return False
    ports = list(map(lambda x: x.strip().split("*p")[1], ports_str.split("+")))
    return any(map(lambda x: port in x, ports))


def instr_isa(instr) -> ISA:
    pipe_instr = pipe_instr_set.instruction_for_name(instr.name())
    if not pipe_instr:
        raise KeyError("No such pipedream instruction: {}".format(instr.name()))
    return pipe_instr.isa_set


def is_mem_instr(instr):
    return "MEM" in instr.name()


def is_ipc4(instr):
    return instr.IPC() >= 4 - 1e-3


def is_alu(instr):
    return any(map(lambda x: instr_has_port(instr, x), ["0156", "01"]))


def is_avx(instr):
    return instr_isa(instr) == ISA.AVX


def is_avx2(instr):
    return instr_isa(instr) == ISA.AVX2


def is_avx512(instr):
    return "AVX512" in str(instr_isa(instr))


def is_sse(instr):
    return "SSE" in str(instr_isa(instr))


def annotate_with_instr_prop(
    instr_props: Mapping[str, Callable[[Instruction], bool]],
    linkage: np.array,
) -> cluster_annotation:
    parent_cluster: Dict[int, int] = {}
    for pos, row in enumerate(linkage):
        cur_id = pos + len(linkage) + 1
        for child in (0, 1):
            parent_cluster[int(row[child])] = cur_id

    def get_prop_count(cluster, linkage) -> Tuple[Mapping[str, int], int]:
        prop_count: Dict[str, int] = defaultdict(int)
        leaves = linkage.leaves_of[cluster]
        for instr in map(lambda x: linkage.instr_of_id[x], leaves):
            for name, prop in instr_props.items():
                if prop(instr):
                    prop_count[name] += 1
        return prop_count, len(leaves)

    def out(cluster, linkage):
        prop_count, instr_count = get_prop_count(cluster, linkage)
        parent = parent_cluster.get(cluster, None)
        if parent is not None:
            parent_prop_count, parent_instr_count = get_prop_count(parent, linkage)

        out_chunks = []
        for prop in instr_props.keys():
            chunk = "{}: {:.01f}%".format(prop, prop_count[prop] * 100 / instr_count)
            if parent is not None and parent_prop_count[prop]:
                chunk += " [{:.01f}%P]".format(
                    prop_count[prop] * 100 / parent_prop_count[prop]
                )
            out_chunks.append(chunk)
        return "\n".join(out_chunks)

    return out


# ========== Experiments on cutting at various thresholds ==========
def clusters_for_cut_at(ipc_matr: np.array, linkage: np.array, cut_value: float):
    label_for: List[int] = [0] * len(ipc_matr)

    def dfs(node, cur_label=None):
        if cur_label is None and node.dist <= cut_value:
            cur_label = node.id
        if node.is_leaf():
            label_for[node.id] = cur_label
            return
        dfs(node.left, cur_label)
        dfs(node.right, cur_label)

    dfs(hierarchy.to_tree(linkage))
    #    clusters = defaultdict(list)
    #    for elt, label in enumerate(label_for):
    #        clusters[label].append(elt)
    #    print(
    #        "Clustering at {:.02f}: {:d} clusters".format(cut_value, len(clusters.keys()))
    #    )

    return label_for


def clusters_for_cut_count(ipc_matr: np.array, linkage: np.array, count: int):
    parent_for: List[int] = [i for i in range(len(ipc_matr) + len(linkage))]

    def union(i, j, merged):
        parent_for[i] = merged
        parent_for[j] = merged

    def find(i):
        if parent_for[i] == i:
            return i
        parent = find(parent_for[i])
        parent_for[i] = parent
        return parent

    for merge_id, merge in enumerate(linkage[:count]):
        union(int(merge[0]), int(merge[1]), merge_id + len(ipc_matr))

    label_for: List[int] = [find(i) for i, _ in enumerate(ipc_matr)]
    labelset = list(set(label_for))
    labelset.sort()
    relabel = {old_label: new_label for new_label, old_label in enumerate(labelset)}
    for pos, old_label in enumerate(label_for):
        label_for[pos] = relabel[old_label]

    return label_for


def silhouette_for_cut_at(
    ipc_matr: np.array, linkage: np.array, cut_value: float
) -> float:
    clusters = clusters_for_cut_at(ipc_matr, linkage, cut_value)
    return silhouette_score(ipc_matr, clusters)


def silhouette_for_cut_count(
    ipc_matr: np.array, linkage: np.array, count: int
) -> float:
    clusters = clusters_for_cut_count(ipc_matr, linkage, count)
    return silhouette_score(ipc_matr, clusters)


def get_silhouette_metrics(
    ipc_matr: np.array,
    linkage: np.array,
    min_classes: Optional[int] = None,
    max_classes: Optional[int] = None,
) -> List[Optional[float]]:
    """Returns a partially-filled list of `len(linkage)` of the silhouette score of the
    clustering if cut at this merge"""
    silhouettes: List[Optional[float]] = [None] * len(linkage)

    start_bound = len(linkage) // 2
    end_bound = len(linkage) - 1
    if min_classes is not None:
        end_bound = len(ipc_matr) - min_classes
    if max_classes is not None:
        start_bound = len(ipc_matr) - max_classes

    # Partially fill the silhouette
    for pos in ProgressBar(range(start_bound, end_bound, 3)):
        silhouettes[pos] = silhouette_for_cut_count(ipc_matr, linkage, pos)

    # Get the amplitude
    filled_silhouettes = list(filter(lambda x: x is not None, silhouettes))
    min_val = min(filled_silhouettes)
    max_val = max(filled_silhouettes)
    amplitude = max_val - min_val
    top_threshold = min_val + 0.8 * amplitude

    # Gather relevant ranges
    intervals = []
    cur_start = None
    for pos, elt in enumerate(silhouettes):
        if elt is not None:
            if cur_start is None and elt >= top_threshold:
                cur_start = pos
            elif cur_start is not None and elt < top_threshold:
                intervals.append(range(cur_start, pos + 1))
                cur_start = None
    if cur_start is not None:
        intervals.append(range(cur_start, len(linkage)))

    # Fill the selected ranges
    for relevant_range in intervals:
        for pos in relevant_range:
            if silhouettes[pos] is None:
                silhouettes[pos] = silhouette_for_cut_count(ipc_matr, linkage, pos)

    return silhouettes


def graph_silhouette_metrics(
    ipc_matr: np.array, linkage: np.array, show: bool = False, **kwargs
) -> List[Optional[float]]:
    """Plot the results of `get_silhouette_metrics`"""
    silhouettes = get_silhouette_metrics(ipc_matr, linkage, **kwargs)
    X_val = [pos for pos, val in enumerate(silhouettes) if val is not None]
    Y_val = [silhouettes[pos] for pos in X_val]
    plt.plot(X_val, Y_val, "rx")
    plt.plot(X_val, Y_val, "b-")

    x_max = X_val[np.argmax(Y_val)]
    print(
        "Max silhouette: {} @ {} merges -> {} clusters".format(
            silhouettes[x_max], x_max, len(ipc_matr) - x_max
        )
    )

    if show:
        plt.show()
    return silhouettes
