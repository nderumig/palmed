#!/usr/bin/env python3

import argparse
from pathlib import Path
import pickle
import sys

from palmed.comparative_bench.entry import EvaluationResult, BenchResult


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--out", type=Path, help="Path to the output pickled file")
    parser.add_argument(
        "--in", dest="in_benchs", type=Path, action="append", help="Pickled input files"
    )
    parser.add_argument(
        "--columns",
        action="append",
        help=(
            "comma-separated list of column names, one per input file. "
            "Pass an empty argument for no renaming."
        ),
    )
    args = parser.parse_args()

    columns = args.columns
    if not columns:
        columns = len(args.in_benchs) * [""]
    if len(args.in_benchs) != len(columns):
        print(
            (
                f"Bad number of --columns provided: found {len(args.in_benchs)} "
                f"in_pickle files, but {len(columns)} --columns"
            ),
            file=sys.stderr,
        )
        sys.exit(2)

    in_data: list[EvaluationResult] = []
    col_rename: list[list[str]] = []
    for col_str, path in zip(columns, args.in_benchs):
        with path.open("rb") as h:
            cur_data: EvaluationResult = pickle.load(h)
            in_data.append(cur_data)
        if col_str:
            renames = list(map(lambda x: x.strip(), col_str.split(",")))
            if len(renames) != len(cur_data.selected_benchers):
                print(
                    (
                        f"Column count mismatch in {path}: found "
                        f"{len(cur_data.selected_benchers)} columns, "
                        f"provided {len(renames)}."
                    ),
                    file=sys.stderr,
                )
                sys.exit(2)
            col_rename.append(renames)
        else:
            col_rename.append([])

    concat_benchers: list[str] = []
    col_subst: list[dict[str, str]] = []
    for renames, dat in zip(col_rename, in_data):
        if not renames:
            concat_benchers += dat.selected_benchers
            col_subst.append({k: k for k in dat.selected_benchers})
        else:
            concat_benchers += renames
            col_subst.append({k: v for (k, v) in zip(dat.selected_benchers, renames)})
    assert len(set(concat_benchers)) == len(concat_benchers)

    concat_benchs: list[BenchResult] = []
    for bench_id in range(len(in_data[0].benchs)):
        assert len(set(dat.benchs[bench_id].bench for dat in in_data)) == 1

        results: dict[str, t.Optional[float]] = {}
        for subst, dat in zip(col_subst, in_data):
            for source_bencher, res in dat.benchs[bench_id].results.items():
                results[subst[source_bencher]] = res

        bench_dict = in_data[0].benchs[bench_id]._asdict()
        bench_dict["results"] = results
        reconst_bench = BenchResult(**bench_dict)
        concat_benchs.append(reconst_bench)

    concat_dat = EvaluationResult(
        benchs=concat_benchs, selected_benchers=concat_benchers
    )
    with args.out.open("wb") as h:
        pickle.dump(concat_dat, h)


if __name__ == "__main__":
    main()
