""" Evaluates a mapping on the benchmarks that are stored in DB -- that is, single
instructions, quad benchmarks, (ab⁴), etc.

Supposedly these benchmarks were fed into Gurobi in order to obtain the model, so this
could be seen as both a check that the model is decently correct, and a sanity check
against bugs in what Gurobi is asked to solve. """

import argparse
import pickle
from pathlib import Path
import logging
import typing as t
import sqlalchemy as sa
from collections import defaultdict
import tqdm

from palmed.config import Config
from palmed.messages import setup_logging
from palmed import db

import palmed_model

logger = logging.getLogger(__name__)


class CheckResult(t.NamedTuple):
    bench_descr: str
    bench_ipc: float
    model_pred_ipc: float
    bench_id: int
    bench_measure_id: int


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--isa")
    parser.add_argument("--arch")
    parser.add_argument(
        "-m", "--machine", help="The machine that ran the benchmarks", required=True
    )
    parser.add_argument(
        "--model", help="Path to the Palmed model file", type=Path, required=True
    )
    parser.add_argument(
        "-o",
        "--out",
        type=Path,
        help="Path at which the output pickle file should be saved",
        required=True,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        action="store_const",
        const=logging.INFO,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-g",
        "--debug",
        dest="loglevel",
        action="store_const",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    return parser.parse_args()


def basic_palmed_config(args):
    setup_logging(args.loglevel, False)
    Config.SELECTED_MACHINE = args.machine or None
    Config.ISA = args.isa
    Config.MU_ARCH = args.arch
    Config.init_pipedream()


def check_bench(
    model: palmed_model.PalmedModel,
    bench_db: db.models.Benchmark,
    measure: db.models.BenchmarkMeasure,
) -> t.Optional[CheckResult]:
    """Check that a single measure matches the DB result"""
    res_usage: t.Dict[palmed_model.Resource, float] = defaultdict(float)
    instr_count = 0
    for chunk in bench_db.descr.strip().split(";"):
        _rep, instr_str = chunk.split("*")
        rep = float(_rep)
        instr_count += rep

        try:
            instr = model.get_instr(instr_str)
        except KeyError:
            logger.error(
                "Unsupported instruction: %s. Skipping %s.", instr_str, bench_db.descr
            )
            return None

        cur_res_usage: palmed_model.ResourceUsage = model.resource_usage(instr)
        for res, load in cur_res_usage.items():
            res_usage[res] += load * rep

    predicted_ipc = instr_count / max(res_usage.values())

    return CheckResult(
        bench_descr=bench_db.descr,
        bench_ipc=measure.ipc,
        model_pred_ipc=predicted_ipc,
        bench_id=bench_db.id,
        bench_measure_id=measure.id,
    )


def check_benchs(
    model: palmed_model.PalmedModel,
    benchs: t.List[t.Tuple[db.models.Benchmark, db.models.BenchmarkMeasure]],
) -> t.List[CheckResult]:
    results: t.List[CheckResult] = []
    for bench, bench_measure in tqdm.tqdm(benchs, desc="Checking benchs"):
        res = check_bench(model, bench, bench_measure)
        if res is not None:
            results.append(res)

    return results


def main():
    args = parse_args()
    basic_palmed_config(args)
    model: palmed_model.PalmedModel = palmed_model.PalmedModel.from_path(args.model)
    db.init_engine()

    logger.info("Querying DB…")
    with db.Session() as session:
        machine_id = db.current_machine.get_pk()
        query = (
            sa.select(db.models.Benchmark, db.models.BenchmarkMeasure)
            .join(db.models.BenchmarkMeasure)
            .where(db.models.BenchmarkMeasure.machine_id == machine_id)
        )
        db_benchs_res = session.execute(query).all()
        db_benchs = list(db_benchs_res)  # `scalars` is broken on join results

    logger.info("Found %d DB entries. Checking measures.", len(db_benchs))

    results = check_benchs(model, db_benchs)
    out = {
        # Convert to dict; if not, it would be harder to unpickle the results
        "results": list(map(lambda x: x._asdict(), results)),
        "entries_count": len(db_benchs),
        "machine": args.machine,
        "model": args.model,
    }
    with args.out.open("wb") as h:
        pickle.dump(out, h)


if __name__ == "__main__":
    main()
