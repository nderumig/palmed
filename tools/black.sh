#!/usr/bin/env bash
#
# Run black on all sources
#
set -euo pipefail
cd "$(dirname "$0")"/..

r=0
git ls-files '*.py' | xargs black --check || r=1

[ "$r" = 0 ] || { echo "ERROR: reindent with: git ls-files '*.py' | xargs black [--check|--diff]" >&2; exit 1; }


