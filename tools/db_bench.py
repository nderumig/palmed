""" Database benchmarking

File intended to check how fast/slow is the database interface to fetch data """

import logging
import asyncio
import argparse
import datetime
import functools
from concurrent import futures
import os
import math
from queue import SimpleQueue
from typing import Iterable, Optional
import sqlalchemy as sa
from enum import Enum

from palmed import db
from palmed.config import Config
from palmed import messages
from palmed import benchmarks
from palmed import instructions as ins


logger = logging.getLogger(__name__)


class ConnTypes(Enum):
    sequential = "sequential"
    asyncio = "asyncio"
    threaded = "threaded"
    verythreaded = "verythreaded"


def timed(time_storage: Optional[SimpleQueue]):
    """Decorator that measures the time spent in a function"""

    def out_wrapper(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            start_time = datetime.datetime.now()
            out = func(*args, **kwargs)
            delta = datetime.datetime.now() - start_time
            if time_storage is not None:
                time_storage.put(delta)
            return out, delta

        return wrapper

    return out_wrapper


def parse_args():
    parser = argparse.ArgumentParser("database benchmarking")
    parser.add_argument(
        "-d",
        "--db",
        help="database URL, sqlalchemy format",
        default=Config.DATABASE_URL,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-n",
        "--nb",
        type=int,
        default=1000,
        help="number of benches to fetch",
        dest="nb_bench",
    )
    parser.add_argument(
        "--warmup",
        type=int,
        default=2,
        help="Number of warmup iterations of the experiment (discarded)",
    )
    parser.add_argument(
        "--iter", type=int, default=10, help="Number of iterations of the experiment"
    )

    parser.add_argument("conn_type", type=ConnTypes, choices=ConnTypes)

    return parser.parse_args()


measures_queue = SimpleQueue()


@timed(measures_queue)
def sequential(benchs: Iterable[benchmarks.Benchmark]):
    for bench in benchs:
        bench.db_fetch_IPC()


def k_threaded(nb_thread: int, benchs: Iterable[benchmarks.Benchmark]):
    with futures.ThreadPoolExecutor(max_workers=nb_thread) as executor:
        future_to_bench = {
            executor.submit(benchmarks.Benchmark.db_fetch_IPC, bench): bench
            for bench in benchs
        }
        for _ in futures.as_completed(future_to_bench):
            pass


@timed(measures_queue)
def threaded(benchs: Iterable[benchmarks.Benchmark]):
    return k_threaded(os.cpu_count(), benchs)


@timed(measures_queue)
def verythreaded(benchs: Iterable[benchmarks.Benchmark]):
    return k_threaded(10 * os.cpu_count(), benchs)


@timed(measures_queue)
def do_asyncio(benchs: Iterable[benchmarks.Benchmark]):
    async def async_transactions():
        engine = sa.ext.asyncio.create_async_engine(Config.DATABASE_URL)

        async with sa.ext.asyncio.AsyncSession(engine) as session:
            for bench in benchs:
                bench.db_fetch_IPC(session)

    asyncio.run(async_transactions())


def gen_benchs(nb: int) -> Iterable[benchmarks.Benchmark]:
    for _ in range(nb):
        yield benchmarks.Benchmark(  # Some random benchmark. We don't care.
            ins.Instruction_Pipedream("MOVAPD_MEM64f64x2_VR128f64x2"),
            ins.Instruction_Pipedream("IMUL_GPR32i32_GPR32i32_IMMi8"),
        )


def do_bench(compute_func, args, exp_name):
    # warmup
    for _ in range(args.warmup):
        compute_func(gen_benchs(args.nb_bench))
    while not measures_queue.empty():
        measures_queue.get()

    deltas = []
    for _ in range(args.iter):
        compute_func(gen_benchs(args.nb_bench))
    while not measures_queue.empty():
        deltas.append(measures_queue.get())

    microsecond_deltas = list(map(lambda x: x.microseconds + 1e6 * x.seconds, deltas))
    microsecond_deltas.sort()
    avg = sum(microsecond_deltas) / len(microsecond_deltas)
    med = microsecond_deltas[len(microsecond_deltas) // 2]
    std_dev = math.sqrt(
        sum(map(lambda x: (x - avg) ** 2, microsecond_deltas)) / len(microsecond_deltas)
    )

    print(
        (
            "=== {name} ===\n"
            "Avg:     {avg:>10} μs\n"
            "Med:     {med:>10} μs\n"
            "Std dev: {std_dev:>10} μs [{dev_prop} %]\n"
        ).format(
            name=exp_name,
            avg=int(avg / args.nb_bench),
            med=int(med / args.nb_bench),
            std_dev=int(std_dev / args.nb_bench),
            dev_prop=int(100 * std_dev / avg),
        )
    )


def main():
    """Entrypoint"""
    args = parse_args()
    messages.setup_logging(args.loglevel)

    compute_funcs = {
        ConnTypes.sequential: sequential,
        ConnTypes.asyncio: do_asyncio,
        ConnTypes.threaded: threaded,
        ConnTypes.verythreaded: verythreaded,
    }
    compute_func = compute_funcs[args.conn_type]

    Config.DATABASE_URL = args.db
    db.init_engine()

    typical_bench = next(gen_benchs(1))
    with db.Session() as session:
        bench = (
            session.query(db.models.Benchmark)
            .where(db.models.Benchmark.descr == typical_bench.db_str())
            .one_or_none()
        )
        if bench is not None:
            session.delete(bench)
            session.commit()
    do_bench(compute_func, args, "Empty database")
    typical_bench.get_db_entry_or_create()
    do_bench(compute_func, args, "Bench in database")
    typical_bench.IPC()
    do_bench(compute_func, args, "Measure in database")


if __name__ == "__main__":
    main()
