#!/usr/bin/env python3

""" Generate results table (Fig 3.b in paper) from multiple benchmark results' pickle
files """

import pickle
import argparse
import datetime
import os
import socket
from pathlib import Path
from palmed.comparative_bench.analyze import entry as analyze


class BenchSuite:
    def __init__(self, file):
        self.path = Path(file)
        with open(file, "rb") as handle:
            self.benchs = pickle.load(handle)

        self._rms_error = analyze.compute_rms(
            self.benchs, analyze.RmsWeightingChoice.OCCURENCES_BY_FILE, "native", False
        )
        self._tau = analyze.compute_tau(self.benchs, "native", False)

    def rms_error(self, bencher):
        val = self._rms_error.get(bencher, None)
        if val is None:
            return None
        return val[1] * 100

    def coverage(self, bencher):
        if bencher == "palmed":  # N/A: coverage is wrt. Palmed
            return None
        val = self._rms_error.get(bencher, None)
        if val is None:
            return None
        return val[0] * 100 / len(self.benchs.benchs)

    def tau(self, bencher):
        val = self._tau.get(bencher, None)
        if val is None:
            return None
        return val[1]


def fmt_cov(cov):
    if cov:
        return "{:.01f}".format(cov)
    return "\\na"


def fmt_err(err):
    if err:
        return "{:.01f}".format(err)
    return "\\na"


def fmt_tau(tau):
    if tau:
        return "{:.02f}".format(tau)
    return "\\na"


def print_table(benchs, bencher_names, platform_names, suite_names):
    benchers = ["palmed", "UOPS", "PMEvo", "IACA", "LLVM_MCA"]
    benchers_print = list(map(lambda x: bencher_names.get(x, x), benchers))

    out = r"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table generated automatically using palmed's `tools/gen_results_table.py`
%   on {whengenerated}
%   by {username}@{machine}
\label{{tab:realcpu_results}}\begin{{tabular}}{{c c | {tab_headers}}}
    \toprule
""".format(
        tab_headers=" | ".join(["C E K"] * len(benchers)),
        whengenerated=datetime.datetime.now(),
        username=os.getlogin(),
        machine=socket.gethostname(),
    )
    out += (
        "    & & "
        + " & ".join(
            [
                r"\multicolumn{{3}}{{c}}{{{}}}".format(bencher)
                for bencher in benchers_print
            ]
        )
        + r"\\"
        + "\n"
    )
    out += (
        "    & & "
        + " & ".join([r"\tabcoverage & \taberror & \tabkendall"] * len(benchers))
        + r"\\"
        + "\n"
    )
    out += (
        "    & Unit & "
        + " & ".join(
            [r"\tabcoverageunit & \taberrorunit & \tabkendallunit"] * len(benchers)
        )
        + r"\\"
        + "\n"
    )
    out += "    \\midrule\n"

    for platform, series in benchs.items():
        platform_label = (
            r"\multirow{2}{*}{\textbf{" + platform_names.get(platform, platform) + "}}"
        )
        for suite, suite_bench in series.items():
            row = "    % Generated from {}\n".format(suite_bench.path.resolve())
            row += r"    {platform} & \textbf{{{suite}}} & ".format(
                platform=platform_label, suite=suite_names.get(suite, suite)
            )
            platform_label = ""

            row += (
                " & ".join(
                    [
                        "{} & {} & {}".format(
                            fmt_cov(suite_bench.coverage(bencher)),
                            fmt_err(suite_bench.rms_error(bencher)),
                            fmt_tau(suite_bench.tau(bencher)),
                        )
                        for bencher in benchers
                    ]
                )
                + "\\\\\n"
            )
            out += row

    out += r"""    \bottomrule
\end{tabular}
% End automatically generated table
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"""

    print(out)


def main():
    parser = argparse.ArgumentParser("gen_results_table")
    parser.add_argument("skx_spec")
    parser.add_argument("skx_polybench")
    parser.add_argument("zen_spec")
    parser.add_argument("zen_polybench")
    parser.add_argument("--tool-name", type=str, default="palmed")
    args = parser.parse_args()

    bencher_names = {
        "palmed": args.tool_name,
        "LLVM_MCA": "llvm-mca",
        "UOPS": "uops.info",
    }
    platform_names = {
        "skx": "SKL-SP",
        "zen": "ZEN1",
    }
    suite_names = {
        "spec": "SPEC2017",
        "polybench": "Polybench",
    }

    benchs = {
        "skx": {
            "spec": BenchSuite(args.skx_spec),
            "polybench": BenchSuite(args.skx_polybench),
        },
        "zen": {
            "spec": BenchSuite(args.zen_spec),
            "polybench": BenchSuite(args.zen_polybench),
        },
    }

    print_table(benchs, bencher_names, platform_names, suite_names)


if __name__ == "__main__":
    main()
