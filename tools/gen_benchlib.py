#!/usr/bin/env python3

import argparse
import tempfile
from shutil import copy
from palmed import load_kernel
import palmed.instructions
from palmed.config import Config

import pipedream.asm.ir as pipeasm
import pipedream.benchmark as pipebench
from pipedream.benchmark.common import _Benchmark_Runner


def cat(path):
    with open(path, "r") as handle:
        text = handle.read()
    print(text)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("kernel_file", help="Yaml file describing the kernel")
    parser.add_argument(
        "--asm",
        "-s",
        help="Save the ASM at this location. Print to stdout if '-'",
        required=True,
    )
    parser.add_argument(
        "--iaca",
        "-i",
        action="store_true",
        help="Generate IACA markers",
        dest="iaca_markers",
    )
    parser.add_argument("--reg-pools", action="store_true")
    parser.add_argument("--arch", "-a", default="SKL")
    parser.add_argument("--isa", default="x86")
    parser.add_argument("--debug", "-g", default=False, type=bool)
    parser.add_argument(
        "--no-unroll",
        action="store_true",
        help="Do not unroll the kernel for ease of reading",
    )
    parser.add_argument(
        "--run",
        action="store_true",
        help="Actually run the benchmark after generating it",
    )
    args = parser.parse_args()

    Config.CHECKPOINT.set_suffix(f".pipedream.{Config.SELECTED_MACHINE}")
    Config.UOPS = False
    Config.PIPEDREAM = True
    Config.X86_ARCH = args.arch
    Config.ISA = args.isa

    arch = pipeasm.Architecture.for_name(args.isa)
    kind = pipebench.Benchmark_Kind.THROUGHPUT
    instrs = load_kernel.load_pipedream_kernel_description(args.kernel_file)

    inst_set = arch.instruction_set()
    pipedream_instructions = [
        inst_set.instruction_for_name(inst.name()) for inst in instrs
    ]

    register_pools = None
    if args.reg_pools:
        register_pools = {x: 1 for x in pipedream_instructions}
    unroll_length = 500
    if args.no_unroll:
        unroll_length = len(pipedream_instructions)
    pipedream_benchmark = pipebench.Benchmark_Spec.from_instructions(
        arch=arch,
        kind=kind,
        instructions=pipedream_instructions,
        align_kernel=False,
        register_pools=register_pools,
        num_dynamic_instructions=100_000,
        unrolled_length=unroll_length,
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        runner = _Benchmark_Runner(verbose=True)
        benchmark_lib = runner._gen_benchmark_lib(
            benchmark_specs=[pipedream_benchmark],
            architecture=arch,
            num_iterations=50,
            gen_iaca_markers=args.iaca_markers,
            perflib=None,
            tmp_dir=tmp_dir,
            debug=args.debug,
            is_lib_tmp=False,
        )

        asm_path = tmp_dir + "/benchmarks.s"
        if args.asm == "-":
            cat(asm_path)
        else:
            copy(asm_path, args.asm)

        if args.run:
            print(">> Running the benchmark…")
            runner._run_benchmarks(
                arch=arch,
                bench_lib=benchmark_lib,
                perf_counters=None,
                num_iterations=50,
                num_warmup_iterations=10,
                keep_raw_data=False,
                outlier_low=0,
                outlier_high=100,
                tmp_dir=tmp_dir,
            )


if __name__ == "__main__":
    main()
