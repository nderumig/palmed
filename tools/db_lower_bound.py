#!/usr/bin/env python3

import argparse
from palmed import db
from palmed import benchmarks
from palmed import instructions
from palmed.config import Config


def parse_arguments():
    parser = argparse.ArgumentParser("db_lower_bound")
    parser.add_argument("--arch", "-a")
    parser.add_argument("--dry-run", "-n", action="store_true")
    parser.add_argument("--verbose", "-v", action="store_true")
    parser.add_argument("--machine", "-m")

    return parser.parse_args()


def init(args):
    db.init_engine()
    if args.machine:
        Config.SELECTED_MACHINE = args.machine
    Config.CHECKPOINT.set_suffix(f".pipedream.{Config.SELECTED_MACHINE}")
    Config.UOPS = False
    Config.PIPEDREAM = True
    Config.X86_ARCH = args.arch
    instructions.load_pipedream()


def apply_lower_bounds(dry_run=False, verbose=False):
    benchs = (
        db.Session()
        .query(db.models.Benchmark, db.models.BenchmarkMeasure)
        .where(db.models.BenchmarkMeasure.machine_id == db.current_machine.get_pk())
        .join(db.models.Benchmark)
        .all()
    )

    print(
        "Got {} measures. Proceeding… [{}]".format(
            len(benchs), "DRY RUN" if dry_run else "APPLYING"
        )
    )

    capped = 0
    for bench, measure in benchs:
        palmed_bench = benchmarks.Benchmark.from_db_entry(bench)
        min_IPC = palmed_bench.min_IPC()
        if measure.ipc < min_IPC:
            if measure.ipc + 1e-5 < min_IPC:
                capped += 1
                if verbose:
                    print(
                        "Capping benchmark {}: IPC {} -> {}".format(
                            bench.descr, measure.ipc, min_IPC
                        )
                    )
            if not dry_run:
                measure.ipc = min_IPC

    if not dry_run:
        db.Session().commit()

    print(
        "Capped {}/{} measures [{:.01f}%]".format(
            capped, len(benchs), 100 * capped / len(benchs)
        )
    )


def main():
    args = parse_arguments()
    init(args)
    apply_lower_bounds(dry_run=args.dry_run, verbose=args.verbose)


if __name__ == "__main__":
    main()
