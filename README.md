# PALMED

PALMED is a framework aiming to automatically provide a precise performance
model for a processor. It works by inferring a port mapping of a given
processor and deducing the resource usage of each relevant instruction.

## Authors

* Nicolas DERUMIGNY
* Théophile BASTIAN
* Fabrice RASTELLO

## Dependencies

* python dependencies: listed in `requirements.txt`
* (development) python dependencies: listed in `requirements-dev.txt`
* [PAPI](http://icl.cs.utk.edu/papi/): this project depends on PAPI being
  installed on your machine. An
  [AUR package](https://aur.archlinux.org/packages/papi/) is available for
  Archlinux.

  An alternative using solely `perf` can be used, see `~/Pipedream/README.md`
  for more details.
* Any database capable of supporting *huge* requests; postgresql is
  recommended.
* All `pipedream` dependencies: `graphviz`, `cmake`, `gcc`
* `aarch64-linux-gnu-gcc` for Arm build

## Installing

### Palmed

If you are using git, clone this repository **recursively**, that is,
```bash
git clone --recurse-submodules $REPO_URL
```

Then, compile the dependencies and generate the instruction set using
```bash
./install.sh
```

Alternatively, if you already installed PALMED on another machine and don't
want to run the instruction set generation again, you can run all the steps
from `install.sh`, omitting `./extract-XXX-instructions […]`, and copy
over from your other machine the files
`pipedream/src/pipedream/asm/x86/instructions_xed.py`.
`pipedream/src/pipedream/asm/armv8a/instructions_binutils.py`.


Then, it is recommended to install the python dependencies and modules inside a
virtualenv:

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt # Only if you intend to contribute
pip install -e pipedream/
pip install -e .
```

**BEWARE!** The `/` in `pipedream/` is *essential*, since there is another
project named [`pipedream`](https://pypi.org/project/pipedream/) on
[pypi](https://pypi.org), which has nothing to do with our pipedream. Not
including the `/` will install the other pipedream from pypi.

Alternatively, you can simply run the last line to install those dependencies
directly to your system.

### Setting up the database

Any database handled by SQLAlchemy will do. We officially support PostgreSQL,
and will describe here how to use it, but feel free to use anything else.

To use some other database, use the `--db-url` flag.

In the default configuration, this URL connects to a local PostgreSQL through a
Unix socket in `/var/run/postgresql` using `psycopg2` to the `palmed` database,
with no authentication.  The Unix socket setup should work out of the box on
most Linux distributions.

To make the database and authentication work, assuming
[peer authentication](https://www.postgresql.org/docs/current/auth-peer.html)
is enabled and your **local** user is `<USERNAME>`,
```bash
sudo -u postgres psql
CREATE DATABASE palmed;
CREATE USER <USERNAME>;  # or skip if you already have a user
GRANT ALL PRIVILEGES ON DATABASE palmed TO <USERNAME>;
ALTER DATABASE palmed OWNER TO <USERNAME>;
\q
```

Then, create the database tables:
```
alembic upgrade head
```
**Note:** if you use a custom database URL, you will need to modify it in
`alembic.ini` for this step, as this command takes no `--db-url`.

To benchmark locally, add your local machine to the database:
```
palmed-machines add
```
and follow the script.

To list available machines (if the database is imported from elsewhere),
```
palmed-machines list
```

## Running

### Requirements

Always source the virtualenv before running anything:
`source venv/bin/activate`.

PALMED (and thus, the python interpreter) must be granted `CAP_SYS_NICE` in
order to run:
```bash
sudo setcap "CAP_SYS_NICE=+eip" path/to/your/python
```

You must also ensure that `/proc/sys/kernel/perf_event_paranoid` is set to `1`:
```bash
echo 1 | sudo tee /proc/sys/kernel/perf_event_paranoid
```

To start postgresql and set the corresponding environement variable for
a local PerfPipedream installation, the `set-env.sh` script can be executed for
quicker setup.

### Running PALMED

The main entrypoint of PALMED is the command `palmed`. See `palmed --help` for the
execution parameters.

Other entrypoints include:
* `palmed-benchmarks`: tool to run specificcaly custom microbenchmarks with several
available tools (IACA, PMEvo, native, PALMED-mapping, ...)
* `palmed-analyze-benchmarks`: tool to perform analysis and graph generation from
results
* `palmed-machines`: manage PALMED available benching machines
* `palmed-export-mapping`: export a previously computed mapping as a standalone,
palmed-independent file
* `palmed-export-uops-mapping`: same as above, but specific to mappings deduced
from UOPS
* `palmed-export-gus`: export the `instructions_info.hpp` and `.cpp` file needed
for GUS for a mapping

#### Export a mapping to GUS

Exporting a mapping to GUS can either from reverseed-engineer mappings (both from
an actual mapping or from UOPS' simulation) or directly from uops.info file.
The steps for the reverse engineered one are as follows:
1. Create the desired mapping, e.g. `palmed --uops --arch SKX`. It creates a mapping
description file in `~/out/mapping.uops.SKX` by default.
2. Use `palmed-export-gus` using the generated mapping to create the GUS file
`Instruction_Info.cpp`, e.g. `palmed-export-gus out/mapping.uops.SKX`.

For the direct uops.info one:
1. Create the desired mapping using `palmed-export-uops-mapping` with standard format,
e.g. `palmed-export-uops-mapping --keep-free-insn --std-mapping --arch SKX my_mapping.uops.skx`.
It create a mapping description file in `~/my_mapping.uops.skx`.
2. Use `palmed-export-gus` using the generated mapping to create the GUS file `Instruction_Info.cpp`
e.g. `palmed-export-gus my_mapping.uops.skx`.

## Contributing

### Formatting guidelines
All commits must be `mypy`-validated. Use the `pre-commit` hook to ensure it at
commit time, and `make mypy` to check it beforehand. All files are formatted
using `black`. `make test-black` can be used to test it.

### Unit tests
`make test` *should* succeed after each commit, and *must* pass on `master`
(i.e. after all PRs).

For the mapping test to succeed, the following (uops) mapping must have been
pre-generated: `SKL`, `CON` using the command `palmed --uops --arch [SLK | CON]`
