.PHONY:
.SUFFIXES:

help:
	@echo ""
	@echo "usage: make TARGET"
	@echo "  where TARGET is one of"
	@echo "    test: test all"
	@echo "    test-mypy: test all mypy tests"
	@echo "    test-pytest: test all pytest tests"
	@echo "    test-nouops: test all tests except uops' (faster)"
	@echo "    test-coverage: show the coverage metrics of all unit tests"
	@echo "    test-examples: test all example tests"

all: test

test: test-mypy test-pytest test-black

test-nouops: test-mypy test-pytest-nouops test-black

test-mypy:
	mypy palmed tests

test-pytest:
	pytest tests

test-pytest-nouops:
	pytest tests --ignore test/uops

test-coverage:
	pytest --cov=palmed tests

test-examples:
	$(MAKE) -C tests test-examples

test-black:
	tools/black.sh
