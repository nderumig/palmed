#!/bin/sh
source venv/bin/activate
eval "$(register-python-argcomplete palmed)"
eval "$(register-python-argcomplete palmed-analyze-benchmarks)"
eval "$(register-python-argcomplete palmed-benchmarks)"
eval "$(register-python-argcomplete palmed-export-gus)"
eval "$(register-python-argcomplete palmed-export-mapping)"
eval "$(register-python-argcomplete palmed-export-uops-mapping)"
eval "$(register-python-argcomplete palmed-machines)"
sudo systemctl start postgresql.service
export LD_LIBRARY_PATH=/usr/local/lib
