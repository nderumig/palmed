# Evaluation results

This directory consigns our evaluation results, from running
`plamed-benchmarks` with produced mappings.

The evaluations are performed on basic blocks extracted from the Polybench and
Spec2017 testsuites, at different optimisation levels.

The different columns are:

* `native`: actually run the basic block and benchmark its IPC
* `IACA`: [Intel Architecture Code Analyzer](https://software.intel.com/content/www/us/en/develop/articles/intel-architecture-code-analyzer-download.html)
* `UOPS`: [uops.info](https://uops.info/)
* `UOPS_4`: `UOPS`, upper-capped at 4 (because the frontends of the considered
  CPUs can only decode 4 instructions per cycle)
* `palmed`: results extracted from our mapping

The various CPUs/machines used to benchmark are:

* `SCW-SKX`: the baremetal `HC-BM1-XS` offer from
  [scaleway](https://scaleway.com) (as of 2020-02): SCW-PAR2 datacenter,
  2xIntel Xeon Silver 4114, 20 cores.
* `SCW-ZEN`: the baremetal `GP-BM1-L` offer from
  [scaleway](https://scaleway.com) (as of 2020-02): SCW-PAR2 datacenter,
  1xAMD EPYC 7281, 16 cores.
* `Motoko`: personal computer of @nderumig. 1xIntel Core i9-7940X, 14 cores.


## 2021-02-23 @ SCW-SKX

Mapping file: scw-skx.2021-02-23.mapping
Max. number of resources: 15

**Spec2017**

Evaluation file: scw-skx.2021-02-23.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                          11109) |              13742 |              13742 |              11109 |              11109 |              13742|
TOT  RMS error                                             |             11.1 % |              0.0 % |             72.5 % |             13.0 % |             12.1 %|
TOT  RMS error/clean rows                                  |             11.4 % |              0.0 % |             72.5 % |             13.0 % |             12.9 %|
```

**Polybench**

Evaluation file: scw-skx.2021-02-23.benchs.polybench
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                           2287) |               2664 |               2664 |               2287 |               2287 |               2664|
TOT  RMS error                                             |              8.4 % |              0.0 % |             48.4 % |             27.6 % |             26.6 %|
TOT  RMS error/clean rows                                  |              8.8 % |              0.0 % |             48.4 % |             27.6 % |             28.4 %|
```

## 2021-02-25 @ SCW-ZEN

WARNING: `IACA`, `UOPS` and `UOPS_4` are based on SKX port informations, and should therefore **not** be taken into account.

Mapping file: scw-zen.2021-02-25.mapping
Max. number of resources: 15

**Spec2017**

Evaluation file: scw-zen.2021-02-25.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                          11109) |              13742 |              13742 |              11109 |              11109 |              13742|
TOT  RMS error                                             |             19.7 % |              0.0 % |             57.0 % |             19.7 % |             14.5 %|
TOT  RMS error/clean rows                                  |             18.2 % |              0.0 % |             57.0 % |             19.7 % |             15.4 %|
```

**Polybench**

Evaluation file: scw-zen.2021-02-23.benchs.polybench
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                           2264) |               2664 |               2664 |               2287 |               2287 |               2641|
TOT  RMS error                                             |             15.7 % |              0.0 % |             46.3 % |             33.1 % |             31.3 %|
TOT  RMS error/clean rows                                  |             16.5 % |              0.0 % |             39.9 % |             27.1 % |             33.4 %|
```

## 2021-03-01 @ SCW-SKX

Mapping file: scw-skx.2021-03-01.mapping
Max. number of resources: 25

**Spec2017**

Evaluation file: scw-skx.2021-03-01.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                          11109) |              13742 |              13742 |              11109 |              11109 |              13742|
TOT  RMS error                                             |             11.6 % |              0.0 % |             72.5 % |             13.5 % |             26.3 %|
TOT  RMS error/clean rows                                  |             11.9 % |              0.0 % |             72.5 % |             13.5 % |             28.0 %|
```

**Polybench**

Evaluation file: scw-skx-2021-03-01.benchs.polybench
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                           2287) |               2664 |               2664 |               2287 |               2287 |               2664|
TOT  RMS error                                             |              8.2 % |              0.0 % |             48.3 % |             27.5 % |             26.9 %|
TOT  RMS error/clean rows                                  |              8.7 % |              0.0 % |             48.3 % |             27.5 % |             28.7 %|
```

## 2021-03-01 @ SCW-ZEN

Mapping file: scw-zen.2021-03-01.mapping
Max. number of resources: 25

**Spec2017**

Evaluation file: scw-zen.2021-03-01.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                          11109) |              13742 |              13742 |              11109 |              11109 |              13742|
TOT  RMS error                                             |             18.5 % |              0.0 % |             56.0 % |             18.4 % |             17.8 %|
TOT  RMS error/clean rows                                  |             16.8 % |              0.0 % |             56.0 % |             18.4 % |             18.8 %|
```

**Polybench**

Evaluation file: scw-zen-2021-03-01.benchs.polybench
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                           2264) |               2664 |               2664 |               2287 |               2287 |               2641|
TOT  RMS error                                             |             15.7 % |              0.0 % |             45.9 % |             33.1 % |             20.0 %|
TOT  RMS error/clean rows                                  |             16.6 % |              0.0 % |             39.4 % |             27.1 % |             21.3 %|
```

## 2021-03-23 @ SCW-SKX

Mapping file: scw-skx-2021-03-01.mapping
Max. number of resources: 15
Basic Instructions: X86\_BASE Max Clique + 4 Min Order / SSE Max Clique
LP Time: 900 seconds

**Spec2017**

Evaluation file: scw-skx-2021-03-23.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                          11109) |              13742 |              13742 |              11109 |              11109 |              13742|
TOT  RMS error                                             |             11.2 % |              0.0 % |             72.5 % |             13.0 % |             12.9 %|
TOT  RMS error/clean rows                                  |             11.4 % |              0.0 % |             72.5 % |             13.0 % |             13.7 %|
```

**Polybench**

Evaluation file: scw-skx-2021-03-23.benchs.polybenchs
```
Type Source                                                |               IACA |             native |               UOPS |             UOPS_4 |             palmed|
TOT  benchs passed (clean:                           2287) |               2664 |               2664 |               2287 |               2287 |               2664|
TOT  RMS error                                             |             12.9 % |              0.0 % |             48.3 % |             27.5 % |             19.1 %|
TOT  RMS error/clean rows                                  |             13.7 % |              0.0 % |             48.3 % |             27.5 % |             20.4 %|
```

## 2021-04-16 @ SCW-SKX -- result for initial MICRO21 submission

Mapping file: scw-skx-2021-04-16.MICRO21.mapping
Max. number of resources: 10
Basic Instructions: X86\_BASE + SSE + AVX,  Max Clique + 3 Min Order
LP Time: 1800 seconds

**Spec2017**

Evaluation file: scw-skx-2021-04-16.MICRO21.benchs.spec17
```
Type Source                                                |               IACA |             native |               UOPS |           LLVM_MCA |              PMEvo |             palmed|
TOT  benchs passed (clean:                           7895) |              13778 |              13778 |              11131 |              13358 |               9690 |              13778|
TOT  RMS error                                             |              8.5 % |              0.0 % |             71.8 % |               ?? % |             26.7 % |             23.7 %|
TOT  RMS error/clean rows                                  |              8.4 % |              0.0 % |             42.6 % |               ?? % |             27.9 % |             25.4 %|
```

**Polybench**

Evaluation file: scw-skx-2021-04-16.MICRO21.benchs.polybench
```
Type Source                                                |               IACA |             native |               UOPS |           LLVM_MCA |              PMEvo |             palmed|
TOT  benchs passed (clean:                           1664) |               2664 |               2664 |               2287 |               2650 |               1760 |               2664|
TOT  RMS error                                             |             13.2 % |              0.0 % |             52.3 % |               ?? % |             30.4 % |             24.0 %|
TOT  RMS error/clean rows                                  |             13.4 % |              0.0 % |             55.5 % |               ?? % |             30.5 % |             22.9 %|
```


## 2021-04-16 @ SCW-ZEN -- result for initial MICRO21 submission -- mapping from PLDI

Mapping file: scw-zen-2021-04-16.MICRO21-fromPLDI.mapping
Max. number of resources: 5
Basic Instructions: ??
LP Time: ??

Here the mapping wasn't recomputed on the spot, but taken from PLDI evaluation.
The only thing that matters here is the evaluation of llvm-mca and PMEvo.

**Spec2017**

Evaluation file: scw-zen-2021-04-16.MICRO21-fromPLDI.mapping
```
Type Source                                                |             native |           LLVM_MCA |              PMEvo |             palmed|
TOT  benchs passed (clean:                           9270) |              13778 |              13358 |               9690 |              13778|
TOT  RMS error                                             |              0.0 % |             16.3 % |             35.3 % |             20.2 %|
TOT  RMS error/clean rows                                  |              0.0 % |             16.6 % |             35.2 % |             18.9 %|
```

**Polybench**

Evaluation file: scw-zen-2021-04-16.MICRO21-fromPLDI.mapping
```
Type Source                                                |             native |           LLVM_MCA |              PMEvo |             palmed|
TOT  benchs passed (clean:                           1746) |               2664 |               2650 |               1760 |               2664|
TOT  RMS error                                             |              0.0 % |             23.6 % |             30.7 % |             29.0 %|
TOT  RMS error/clean rows                                  |              0.0 % |             25.8 % |             30.7 % |             32.2 %|
```

## 2021-08-24 @ SCW-ZEN -- trying new LP for CGO

Mapping file: scw-zen-2021-08-24.mapping
Max. number of resources: ??
Basic Instructions: ??
LP Time: ??

**Spec2017**

Evaluation file: scw-zen-2021-08-24.benchs.spec17
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         9383 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │      441 │      33.16 % │      27.64 % │
│ PMEvo                │     3954 │      36.63 % │      36.52 % │
│ palmed               │        0 │      34.16 % │      32.80 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │  p-val │    Clean │  p-val │
│                      │          │          │        │     9383 │        │
├──────────────────────┼──────────┼──────────┼────────┼──────────┼────────┤
│ native               │        0 │     1.00 │   0.00 │     1.00 │   0.00 │
│ LLVM_MCA             │      441 │     0.75 │   0.00 │     0.77 │   0.00 │
│ PMEvo                │     3954 │     0.43 │   0.00 │     0.43 │   0.00 │
│ palmed               │        0 │     0.61 │   0.00 │     0.60 │   0.00 │
```

**Polybench**

Evaluation file: scw-zen-2021-08-24.benchs.polybench
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         1766 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │       14 │      28.56 % │      30.87 % │
│ PMEvo                │      884 │      38.49 % │      38.49 % │
│ palmed               │        0 │      41.50 % │      43.19 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │  p-val │    Clean │  p-val │
│                      │          │          │        │     1766 │        │
├──────────────────────┼──────────┼──────────┼────────┼──────────┼────────┤
│ native               │        0 │     1.00 │   0.00 │     1.00 │   0.00 │
│ LLVM_MCA             │       14 │     0.40 │   0.00 │     0.33 │   0.00 │
│ PMEvo                │      884 │     0.11 │   0.00 │     0.11 │   0.00 │
│ palmed               │        0 │     0.36 │   0.00 │     0.29 │   0.00 │
```

## 2021-08-24 @ SCW-SKX -- trying new LP for CGO

Mapping file: scw-skx-2021-08-24.mapping
Max. number of resources: ??
Basic Instructions: ??
LP Time: ??

**Spec2017**

Evaluation file: scw-skx-2021-08-24.benchs.spec17
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         9383 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ IACA                 │        0 │       9.36 % │       8.58 % │
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │      441 │      20.47 % │      20.33 % │
│ PMEvo                │     3954 │      28.17 % │      28.42 % │
│ palmed               │        0 │      14.75 % │      15.54 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │  p-val │    Clean │  p-val │
│                      │          │          │        │     9383 │        │
├──────────────────────┼──────────┼──────────┼────────┼──────────┼────────┤
│ IACA                 │        0 │     0.80 │   0.00 │     0.84 │   0.00 │
│ native               │        0 │     1.00 │   0.00 │     1.00 │   0.00 │
│ LLVM_MCA             │      441 │     0.73 │   0.00 │     0.78 │   0.00 │
│ PMEvo                │     3954 │     0.47 │   0.00 │     0.48 │   0.00 │
│ palmed               │        0 │     0.74 │   0.00 │     0.75 │   0.00 │
```

**Polybench**

Evaluation file: scw-skx-2021-08-24.benchs.polybench
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         1766 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ IACA                 │        0 │      16.28 % │      19.77 % │
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │       14 │      16.59 % │      21.79 % │
│ PMEvo                │      884 │      46.74 % │      46.74 % │
│ palmed               │        0 │      25.61 % │      23.84 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │  p-val │    Clean │  p-val │
│                      │          │          │        │     1766 │        │
├──────────────────────┼──────────┼──────────┼────────┼──────────┼────────┤
│ IACA                 │        0 │     0.67 │   0.00 │     0.68 │   0.00 │
│ native               │        0 │     1.00 │   0.00 │     1.00 │   0.00 │
│ LLVM_MCA             │       14 │     0.65 │   0.00 │     0.67 │   0.00 │
│ PMEvo                │      884 │     0.14 │   0.00 │     0.14 │   0.00 │
│ palmed               │        0 │     0.65 │   0.00 │     0.61 │   0.00 │
```

## 2021-11-25 @ SCW-ZEN -- Artifact trial for CGO

Mapping file: scw-zen-2021-11-25.mapping
Max. number of resources: 25
Basic Instructions: ??
LP Time: GUROBI_TIME=1800

**Spec2017**

Evaluation file: scw-skx-2021-11-25.benchs.spec17
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         9824 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │        0 │      36.52 % │      32.50 % │
│ PMEvo                │     3954 │      36.48 % │      36.48 % │
│ palmed               │        0 │      28.12 % │      25.44 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │    Clean │
│                      │          │          │     9824 │
├──────────────────────┼──────────┼──────────┼──────────┤
│ native               │        0 │     1.00 │     1.00 │
│ LLVM_MCA             │        0 │     0.68 │     0.68 │
│ PMEvo                │     3954 │     0.43 │     0.43 │
│ palmed               │        0 │     0.74 │     0.72 │
```

**Polybench**

Evaluation file: scw-skx-2021-11-25.benchs.polybench
```
=== RMS errors ===
│ Bencher              │ # errors │     All rows │   Clean rows │
│                      │          │              │         1780 │
├──────────────────────┼──────────┼──────────────┼──────────────┤
│ native               │        0 │       0.00 % │       0.00 % │
│ LLVM_MCA             │        0 │      28.88 % │      32.62 % │
│ PMEvo                │      884 │      40.32 % │      40.32 % │
│ palmed               │        0 │      33.43 % │      35.07 % │
=== Kendall's Tau ===
│ Bencher              │ # errors │      All │    Clean │
│                      │          │          │     1780 │
├──────────────────────┼──────────┼──────────┼──────────┤
│ native               │        0 │     1.00 │     1.00 │
│ LLVM_MCA             │        0 │     0.33 │     0.28 │
│ PMEvo                │      884 │     0.11 │     0.11 │
│ palmed               │        0 │     0.49 │     0.42 │
```
