#!/usr/bin/env python3

from pathlib import Path
from setuptools import setup, find_packages
import pkg_resources
import sys


def parse_requirements():
    with Path("requirements.txt").open() as requirements_txt:
        install_requires = [
            str(requirement)
            for requirement in pkg_resources.parse_requirements(requirements_txt)
        ]
        return install_requires
    return []


setup(
    name="palmed",
    version="0.0.1",
    description="Automatic inference of processor performance model",
    author="CORSE",
    license="LICENSE",
    url="https://gitlab.inria.fr/nderumig/palmed",
    packages=find_packages(),
    include_package_data=True,
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={
        "console_scripts": [
            ("palmed = palmed.palmed:main"),
            ("palmed-benchmarks = palmed.comparative_bench.entry:main"),
            ("palmed-analyze-benchmarks = palmed.comparative_bench.analyze.entry:main"),
            ("palmed-machines = palmed.db.entry:entry_palmed_machines"),
            ("palmed-export-mapping = palmed.entrypoints:export_independant_mapping"),
            ("palmed-export-uops-mapping = palmed.entrypoints:export_uops_mapping"),
            ("palmed-export-gus = palmed.entrypoints:export_gus_files"),
        ]
    },
)
