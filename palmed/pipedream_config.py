## \file pipedream_remote.py
## \brief Pipedream-specific code for setting up and using remote benchmarking

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import typing as t
import inspect
from palmed.config import Config
from pipedream.benchmark.common import set_remote_connexion, unset_remote_connexion
from pipedream.benchmark.ssh_raw import SSH_Session
from pipedream.utils import pyperfs

logger = logging.getLogger(__name__)


class BadlyConfiguredPipedreamRemote(Exception):
    """Raised when the remote pipedream bencher is badly configured yet used"""


class PipedreamNotRemote(Exception):
    """Raised when trying to call a method only valid when Pipedream is configured as
    remote"""


class RemoteError(Exception):
    """Raised when an error occurs on the remote end of a pipedream remote
    execution"""


def online_cpus() -> t.List[int]:
    """This function can be either executed directly, or piped through an SSH channel
    -- hence its weird source code: its body must be working fine if pasted in a fresh
    shell"""

    from pathlib import Path

    base_path = Path("/sys/devices/system/cpu/")
    online = []
    for cpudir in base_path.glob("cpu*"):
        if not cpudir.is_dir():
            continue
        try:
            cpu_id = int(cpudir.name[3:])
        except ValueError:
            continue  # Not a cpuXX dir where XX is integer

        online_file = cpudir / "online"
        if online_file.exists():
            content = online_file.read_text().strip()
            assert content in ["0", "1"]
            if int(content):
                online.append(cpu_id)
        else:
            online.append(cpu_id)
    online.sort()
    return online


def online_cpus_src() -> str:
    """Get the source code of online_cpus, ready to be pasted in a python shell and
    print out online CPUs."""

    fullsrc = "import typing as t\n"

    oldsrc = ""
    src = inspect.getsource(online_cpus)
    while oldsrc != src:  # until fixpoint
        oldsrc = src
        src = src.replace("\n\n", "\n")

    fullsrc += src + "\n\n"
    fullsrc += "print(','.join(map(str, online_cpus())))\n"
    return fullsrc


class RemotePipedream:
    is_remote: bool = False
    remote_core: t.Optional[int] = None
    _online_cpus: t.Optional[t.List[int]] = None

    @classmethod
    def init_remote_pipedream_bencher(
        cls, core_id: int, noremote_ok: bool = True
    ) -> None:
        """Initialize, if required, the remote pipedream bencher using data from Config.
        If `noremote_ok` is False, raise `BadlyConfiguredPipedreamRemote` exception if
        the remote is not configured.
        """
        cls._online_cpus = None  # Reset cached value
        pipedream_args = Config.remote_pipedream_args()
        if pipedream_args is not None:
            set_remote_connexion(
                pinned_core=core_id,
                **pipedream_args,
            )
            cls.is_remote = True
            cls.remote_core = core_id

            if Config.NUM_CPU is None:
                Config.NUM_CPU = len(cls.get_online_cpus())

        elif not noremote_ok:
            raise BadlyConfiguredPipedreamRemote(
                "Remote pipedream host is not configured"
            )

    @classmethod
    def get_ssh_session(cls) -> SSH_Session:
        args = Config.remote_pipedream_args()
        if not cls.is_remote or args is None:
            raise PipedreamNotRemote("Cannot initialize an SSH session")
        del args["wrapper"]
        return SSH_Session(**args)

    @classmethod
    def get_online_cpus(cls) -> t.List[int]:
        def do_get() -> t.List[int]:
            if not cls.is_remote:
                return online_cpus()
            else:
                session = cls.get_ssh_session()
                pysrc = online_cpus_src()
                cmd = f"/usr/bin/env python3 << EOF\n{pysrc}\nEOF"
                ret, out, err = session.remote_exec(cmd, shell=True)
                if ret != 0 or err:
                    raise RemoteError(f"Cannot get online CPUs: got <{ret}> {err}")
                session.disconnect()
                return list(map(lambda x: int(x), out.strip().split(",")))

        if cls._online_cpus is None:
            cls._online_cpus = do_get()
        return cls._online_cpus

    @classmethod
    def reset_remote_pipedream_bencher(cls) -> None:
        """Deinitialize the remote pipedream bencher."""
        if cls.is_remote:
            unset_remote_connexion()
            cls.is_remote = False
            cls.remote_core = None


_PERFLIB_FOR_ISA = {
    "ARMv8a": "perf-pipedream",
    "x86": "papi",
}


def configure_pipedream() -> None:
    """Initializes all benchmark-related options that require an initialisation based
    on the Config values in Pipedream"""

    # Select the perflib
    perflib = Config.PERFLIB
    if perflib == "auto":
        if Config.REMOTE_BENCH:
            perflib = _PERFLIB_FOR_ISA[Config.ISA]
    pyperfs.set_perf_lib(perflib)
