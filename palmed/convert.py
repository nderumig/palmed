## \file convert.py
## \brief Convertions between uops.info, pipedream and gus naming conventions
"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
import logging
import sys
from typing import Set, List, Dict, Tuple, Optional


logger = logging.getLogger(__name__)


UNSUPPORTED_INSTRUCTIONS_X86 = [
    "BND",
    "BNDCL",
    "BNDCN",
    "BNDCU",
    "BNDLDX",
    "BNDMOV",
    "BNDSTX",
    "CMOVNB",
    "CMOVNL",
    "CMOVZ",
    "CMOVNZ",
    "CMOVNBE",
    "FCOMIP",
    "FUCOMIP",
    "FXSAVE",
    "FXSAVE64",
    "FXRSTOR",
    "FXRSTOR64",
    "FWAIT",
    "OUT",
    "PCMPESTRI64",
    "PCMPESTRM64",
    "REP",
    "REPE",
    "REPNE",
    "SERIALIZE",
    "SETNB",
    "SETNL",
    "SETNLE",
    "SETNZ",
    "SETNBE",
    "VBROADCASTI128",
    "VCVTSD2SH",
    "VCVTSH2SD",
    "VCVTSI2SH",
    "VCVTSS2SH",
    "VCVTUSI2SH",
    "VDIVPH",
    "VGATHERPD",
    "VGATHERDPD",
    "VGATHERPDQ",
    "VPCMPESTRI64",
    "VPCMPESTRM64",
    "VP2INTERSECTD",
    "VP2INTERSECTQ",
    "VSQRTPH",
    "VREDUCESH",
    "VFMADD132PH",
    "VFMADD132SH",
    "VFMADD213PH",
    "VFMADD213SH",
    "VFMADD231PH",
    "VFMADD231SH",
    "VFMADDSUB132PH",
    "VFMADDSUB213PH",
    "VFMADDSUB231PH",
    "VFMSUB132PH",
    "VFMSUB132SH",
    "VFMSUB213PH",
    "VFMSUB213SH",
    "VFMSUB231PH",
    "VFMSUB231SH",
    "VFMSUBADD132PH",
    "VFMSUBADD213PH",
    "VFMSUBADD231PH",
    "VFNMADD132PH",
    "VFNMADD132SH",
    "VFNMADD213PH",
    "VFNMADD213SH",
    "VFNMADD231PH",
    "VFNMADD231SH",
    "VFNMSUB132PH",
    "VFNMSUB132SH",
    "VFNMSUB213PH",
    "VFNMSUB213SH",
    "VFNMSUB231PH",
    "VFNMSUB231SH",
    "VADDPH",
    "VADDSH",
    "VCVTPH2UW",
    "VCVTPH2W",
    "VCVTSH2SS",
    "VCVTTPH2UW",
    "VCVTTPH2W",
    "VCVTUW2PH",
    "VCVTW2PH",
    "VGETEXPPH",
    "VGETEXPSH",
    "VGETMANTPH",
    "VGETMANTSH",
    "VMAXPH",
    "VMAXSH",
    "VMINPH",
    "VMINSH",
    "VMULPH",
    "VMULSH",
    "VSUBPH",
    "VSUBSH",
    "VFCMADDCPH",
    "VFCMADDCSH",
    "VFMADDCPH",
    "VFMADDCSH",
    "VFCMULCPH",
    "VFCMULCSH",
    "VFMULCPH",
    "VFMULCSH",
    "VRNDSCALEPH",
    "VRNDSCALESH",
    "VSCALEFPH",
    "VSCALEFSH",
    "VMOVSH",
    "VMOVW",
    "VDIVSH",
    "VRSQRTPH",
    "VRCPPH",
    "VRSQRTSH",
    "VCOMISH",
    "VRCPSH",
    "VUCOMISH",
    "VSQRTSH",
    "VCVTPH2DQ",
    "VCVTPH2PSX",
    "VCVTPH2UDQ",
    "VCVTPS2PHX",
    "VCVTTPH2DQ",
    "VCVTTPH2UDQ",
    "VCVTSH2SI",
    "VCVTSH2USI",
    "VCVTTSH2SI",
    "VCVTTSH2USI",
    "VCMPPH",
    "VCMPSH",
    "VFPCLASSPH",
    "VFPCLASSSH",
    "VREDUCEPH",
    "VCVTDQ2PH",
    "VCVTNEPS2BF16",
    "VCVTUDQ2PH",
    "VCVTNE2PS2BF16",
    "VDPBF16PS",
    "VCVTPD2PH",
    "VCVTPH2PD",
    "VCVTPH2QQ",
    "VCVTPH2UQQ",
    "VCVTTPH2QQ",
    "VCVTTPH2UQQ",
    "VCVTQQ2PH",
    "VCVTUQQ2PH",
    "XCHG",
    "XLAT",
]
NON_OPERANDS_X86 = [
    # 1 as implicit operand"
    "ONE",
    # Base, Displacement, Index
    "B",
    "I",
    "D",
    "BD",
    "BI",
    "ID",
    "BID",
    # Several opcodes for the same instructions
    "08",
    "09",
    "0A",
    "0B",
    "1A",
    "1B",
    "2A",
    "2B",
    "3A",
    "3B",
    "7E",
    "8A",
    "8B",
    "C5",
    "D6",
    "0F6F",
    "0F7F",
    "0F10",
    "0F11",
    "0FD6",
    "0F7E",
    # Extension as a prefix of the instruction name
    "SSE",
    "SSE4",
    # Others
    "NEAR",  # CALL_NEAR
    "VSIB",
    "W",
    "EXCLUSIVE",
]

UNSUPPORTED_REGISTERS_X86 = [
    # Segment & Control Registers
    "SEG",
    "FG",
    "FS",
    "GS",
    "CR",
    "DR",
    "R",
    "RD",
    # Specialised register
    # as the generic version only
    # is detected as of now
    "ALi8",
    "AXi16",
    "EAXi32",
    "RAXi64",
]

REPLACE_OPERANDS_X86 = {
    "ADDR64i64": "M64",
    "RELBR8i8": "I8",
    "IMMi8": "I8",
    "IMMu8": "I8",
    "IMMi16": "I16",
    # Difference Capstone / XED
    "RELBR32i32": "I64",
    "IMMi32": "I32",
    "IMMi64": "I64",
    "IMMZ": "IMM",
    "IMMB": "IMM",
    "FPSTf80": "FP_REG",
    "ST0OTHER": "FP_REG",
}


REPLACE_INSTRUCTION_X86 = {
    "JNZ": "JNE",
    "JZ": "JE",
    "JNB": "JA",
    "JNBE": "JG",
    "JNLE": "JAE",
    "JNL": "JGE",
    "CMOVNLE": "CMOVNE",
    "SETZ": "SETE",
    "SETNZ": "SETNE",
}

REMPLACE_COMPLETE_X86 = {
    "RET_NEAR_IMMi16": "RET, NORMAL",
    "ADD_GPR64i64_IMMi32": "ADD, NORMAL, R64, I64",
}


# FIXME LD and ST are NOT SUPPORTED
def translate_instr_to_gus_x86(name: str) -> Optional[str]:
    splitted = name.split("_")
    isn_name = splitted.pop(0)
    # keep only explicit operands
    splitted = [v for v in splitted if v not in NON_OPERANDS_X86]
    # early exit if Instructions using unsupported regs
    unsupported = next(
        (True for r in splitted if r in UNSUPPORTED_REGISTERS_X86), False
    )
    if unsupported:
        return None

    if name in REMPLACE_COMPLETE_X86.keys():
        return REMPLACE_COMPLETE_X86[name]

    sizes = re.compile("[uif]([0-9]+)(x([0-9]+))?")
    for idx, val in enumerate(splitted):
        if val in REPLACE_OPERANDS_X86:
            splitted[idx] = REPLACE_OPERANDS_X86[val]

        # Special treatement for this LEA, as UOPS recognise the second argument as
        # "ADDR64i64"
        if val == "ADDR64i64" or val == "ADDR32i32":
            if splitted[0] == "R16":
                return "LEA, NORMAL, R16, M16"
            elif splitted[0] == "R32":
                return "LEA, NORMAL, R32, M32"
            elif splitted[0] == "R64":
                return "LEA, NORMAL, R64, M64"
            else:
                logger.error("Unknown LEA found: %s", name)
                sys.exit(1)

        if val.startswith("VR64"):
            splitted[idx] = "MMX"
        if val.startswith("VR128") or val.startswith("VRX128"):
            splitted[idx] = "XMM"
        if val.startswith("VR256") or val.startswith("VRX256"):
            splitted[idx] = "YMM"
        if val.startswith("VRX512") or val.startswith("VRX512"):
            splitted[idx] = "ZMM"

        if val.startswith("MEM64") or val.startswith("GPR"):
            vals = sizes.search(val)
            if not vals:
                continue
            op_size = int(vals.group(1))
            reps = int(vals.group(3)) if vals.group(3) else 1
            # No MEM8
            if val.startswith("MEM"):
                tot_size = max(16, op_size * reps)
            else:
                tot_size = op_size * reps
            if val[0] == "M":
                splitted[idx] = f"M{tot_size}"
            elif val[0] == "G":
                splitted[idx] = f"R{tot_size}"

    swap_names = {
        "VPCMPISTRI64": "VPCMPISTRI",
        "PCMPISTRI64": "PCMPISTRI",
    }

    if isn_name in swap_names.keys():
        isn_name = swap_names[isn_name]

    # Unsupported instructions
    if isn_name in UNSUPPORTED_INSTRUCTIONS_X86:
        return None

    if isn_name in REPLACE_INSTRUCTION_X86:
        isn_name = REPLACE_INSTRUCTION_X86[isn_name]

    if len(splitted) > 0 and splitted[0] != "LOCK":
        splitted = ["NORMAL"] + splitted
    rest = ", ".join(splitted)
    if rest != "":
        out = f"{isn_name}, {rest}"
    else:
        out = isn_name

    return out


def translate_instr_to_gus_arm(name: str) -> Optional[str]:
    raise NotImplementedError("todo translate_instr_gus_arm")
