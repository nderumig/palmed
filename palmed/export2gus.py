## \file export2gus.py
## \brief Convert python's internal representation to hpp/cpp files usable by GUS

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from io import TextIOWrapper
import logging
import math
from collections import defaultdict
from pathlib import Path
from itertools import repeat
from palmed.convert import translate_instr_to_gus_x86
from typing import Set, List, Dict, Tuple, Optional, DefaultDict
from palmed.instructions import Instruction
from palmed.messages import ProgressBar
from palmed.utils import get_frac
from palmed.config import Config

logger = logging.getLogger(__name__)

# Maximum number of uses of a resource
MAX_NB_RES = 100


def lcm(a: int, b: int) -> int:
    return abs(a * b) // math.gcd(a, b)


def get_thrp(
    mapping: Dict[Instruction, List[Tuple[float, str]]], label: Dict[str, str]
) -> Dict[str, int]:
    ret: Dict[str, int] = {}
    for _, list_res in mapping.items():
        for use, port in list_res:
            thrp = 1
            if ret.get(label[port]):
                thrp = ret[label[port]]
            ret[label[port]] = min(lcm(thrp, get_frac(use, max_denum=20)[1]), 20)
    return ret


def get_enum_name():
    uop_ext = "_UOPS" if Config.UOPS else ""
    return f"{Config.ISA}_{Config.MU_ARCH.replace('-', '_')}{uop_ext}"


def export_Instruction_Info_declarations(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    instruction_file,
):
    max_idx = 0
    resources = label.values()
    res_thrp = get_thrp(mapping, label)
    for idx, res in enumerate(resources):
        max_idx = max(max_idx, idx)

    instruction_file.write("\n/// START TO BE COPIED IN constants.hpp file\n")
    instruction_file.write(f"constexpr const size_t MAX_NUM = {max_idx+1};\n")
    instruction_file.write("constexpr const Id ALL[] = {")
    for idx in range(max_idx):
        instruction_file.write(" Id{" + str(idx) + "},")
    instruction_file.write(" Id{" + str(max_idx) + "} };\n")
    instruction_file.write("constexpr const Throughput THR[] = {")
    to_write = ""
    for res in resources:
        to_write += " " + str(res_thrp[res]) + ","
    instruction_file.write(to_write[:-1] + " };\n")
    instruction_file.write("/// END - TO BE COPIED IN constants.hpp file\n\n")

    instruction_file.write(f"enum {get_enum_name()} : uint8_t {{\n")
    for idx, res in enumerate(resources):
        instruction_file.write(f"  {res} = {idx},\n")
    instruction_file.write("};\n\n")


def export_Port_get_name(instruction_file: TextIOWrapper, label: Dict[str, str]):
    instruction_file.write("std::string get_name(const Port::Id port){\n")
    instruction_file.write(f"switch (({get_enum_name()}) port) {{\n")
    for name in label.values():
        instruction_file.write(f'    case Port::{name}:\n      return "{name}";\n')
    instruction_file.write(
        """
    default:
      std::cerr << "[ERROR] Unknown port ("<<(int) port<<\")\\n\";
      exit(-1);
  }
}
} // Namespace Port
"""
    )


def export_Instruction_Info_cpp(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    class_to_instr: Dict[Instruction, List[Instruction]],
    folder: Path,
):
    path = folder / Path("Instruction_Info.cpp")

    with path.open("w") as instruction_file:
        instruction_file.write('#include "arch/Instruction_Info.hpp"\n')
        instruction_file.write(f'#include "arch/{Config.ISA}_common.hpp"\n\n')
        instruction_file.write("#include <iostream>\n")
        instruction_file.write("namespace gus {\nnamespace Port{\n")
        export_Instruction_Info_declarations(mapping, label, instruction_file)
        export_Port_get_name(instruction_file, label)

        if Config.ISA == Config.SUPPORTED_ISA.X86.value:
            export_lookup_x86(instruction_file, mapping, label, class_to_instr)
        elif Config.ISA == Config.SUPPORTED_ISA.ARMv8a.value:
            export_lookup_arm(instruction_file, mapping, label, class_to_instr)
        else:
            raise NotImplementedError(f"Not supported ISA {Config.ISA}")

        instruction_file.write(
            """
} // Namespace gus"""
        )


def export_lookup_x86(
    instruction_file: TextIOWrapper,
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    class_to_instr: Dict[Instruction, List[Instruction]],
):
    instruction_file.write(
        "InstructionInfo::Info InstructionInfo::lookup(const cs_insn *insn, bool print_error) {\n"
        "  constexpr const Latency MISSING_LATENCY = 1;\n"
    )

    res_thrp = get_thrp(mapping, label)

    added_res: Set[str] = set()
    for idx, (instr, list_res) in enumerate(mapping.items()):
        nb_use = 0
        res_with_mult: List[str] = []
        for use, res in list_res:
            thrp = res_thrp[label[res]]
            nb_use = round(use * thrp)
            # WARNING: rounding happens here
            # According to the model, the number of uses should be a multiple of
            # the throughput i.e. `use == nb_use/thrp`. This is in practice not the
            # case as `thrp` is capped as 20.
            if Config.UOPS:
                frac = get_frac(use, max_denum=thrp)
                assert thrp / frac[1] - (thrp // frac[1]) == 0
            # Avoid flooding file with huge resource list (`R_Other` for example)
            if nb_use > MAX_NB_RES:
                logger.warning(
                    f"Instruction {instr.name()} uses too much {label[res]}, capped to {MAX_NB_RES}",
                )
                res_with_mult.extend(repeat(label[res], MAX_NB_RES))
            else:
                res_with_mult.extend(repeat(label[res], nb_use))

        if res_with_mult:
            added_res.add(idx)
            instruction_file.write(
                f"  const std::vector<Port::Id> table{str(idx)} = {{ "
                + ", ".join(
                    [
                        f"(Port::Id(Port::{get_enum_name()}::{res_name}))"
                        for res_name in res_with_mult
                    ]
                )
                + " };\n"
            )

    instruction_file.write("  switch (matcher(insn)) {\n")
    # Special handeling of NOP: add compound NOP and mov reg to reg
    if "NOP" not in mapping:
        if not Config.UOPS:
            instruction_file.write("    case match(X86_INS_NOP, NORMAL, M16):\n")
            instruction_file.write("    case match(X86_INS_NOP, NORMAL, M32):\n")
            instruction_file.write("      return Info{MISSING_LATENCY};\n\n")

    glob_done: Dict[str, Instruction] = {}
    tot_iter = sum(len(class_to_instr[ins]) for ins in mapping)
    bar = ProgressBar(total=tot_iter)

    for idx, (instr, list_res) in enumerate(mapping.items()):
        latency_classes: DefaultDict[float | str, List[Instruction]] = defaultdict(list)
        for instr_of_class in class_to_instr[instr]:
            latency: float | str
            try:
                latency = instr_of_class.latency()
            except ValueError:
                latency = "MISSING_LATENCY"
            latency_classes[latency].append(instr_of_class)

        for lat, lat_class in latency_classes.items():
            added = False
            done: Set[str] = set()
            for instr_of_class in lat_class:
                bar.update()
                ret = translate_instr_to_gus_x86(instr_of_class.name())
                # If GUS cannot tell the difference, but the used resources
                # are identical, then we do not care
                if not ret or ret in done:
                    continue
                added = True
                done.add(ret)
                if glob_done.get(ret):
                    logger.warning(
                        f"GUS cannot distinguish {instr_of_class.name()} "
                        f"and {glob_done[ret].name()}, keeping the latter only "
                        f'(common output: "{ret}")'
                    )
                    continue
                if ret.count(",") >= 7:
                    logger.warning(
                        "GUS does not support instructions with "
                        f"{ret.count(',')-1} operands, ignoring {ret}",
                    )
                    continue
                glob_done[ret] = instr
                instruction_file.write(f"    case match(X86_INS_{ret}):\n")

            if added:
                lat_str: str
                if isinstance(lat, str):
                    lat_str = lat
                else:
                    lat_str = (
                        str(round(lat))
                        if lat is not None and lat >= 0 and lat != float("inf")
                        else "MISSING_LATENCY"
                    )

                if idx in added_res:
                    instruction_file.write(
                        f"      return Info{{{lat_str}, table{idx}}};\n\n"
                    )
                else:
                    instruction_file.write(f"      return Info{{{lat_str}}};\n\n")
    bar.close()
    instruction_file.write(
        """  }

  if (print_error) { 
    todo(insn);
  }
  return Info{MISSING_LATENCY};
}
"""
    )


def export_lookup_arm(
    instruction_file: TextIOWrapper,
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    class_to_instr: Dict[Instruction, List[Instruction]],
):
    # gather ports and latencies information
    # dict garanties uniqueness of instr
    infos: Dict[str, Tuple[int, List[str]]] = dict()
    res_thrp = get_thrp(mapping, label)
    for instr, list_res in mapping.items():
        ports: List[str] = []
        for use, res in list_res:
            thrp = res_thrp[label[res]]
            nb_use = round(use * thrp)
            # Avoid flooding file with huge resource list (`R_Other` for example)
            if nb_use > MAX_NB_RES:
                logger.warning(
                    f"Instruction {instr.name()} uses too much {label[res]}, {nb_use=} capped at {MAX_NB_RES}",
                )
                nb_use = MAX_NB_RES
            port = f"Port::Id(Port::{get_enum_name()}::{label[res]})"
            ports.extend(repeat(port, nb_use))

        if len(ports) == 0:
            logger.warning(f"Instructions {instr.name()} has no resource")

        infos[instr.name()] = (round(instr.latency()), ports)
        # also add the instruction in the class
        infos.update(
            (i.name(), (round(i.latency()), ports)) for i in class_to_instr[instr]
        )

    # writes everything to file

    instruction_file.write(
        """
InstructionInfo::Info InstructionInfo::lookup(const cs_insn *insn, bool print_error) {
    constexpr const Latency MISSING_LATENCY = 1;
    static const std::unordered_map<PipeDreamInstrID, InstructionInfo::Info> M = {
"""
    )
    for instr_name, (latency, ports) in infos.items():
        if ports:
            args = f"{latency}, {{{', '.join(ports)}}}"
        else:
            args = str(latency)
        instruction_file.write(f'\t\t{{"{instr_name}", Info{{{args}}}}},\n')

    instruction_file.write(
        """
    }; // end unordered_map M
    auto pipe_id = get_pipedream_id(insn, print_error);
    auto sentinel = Info{(uint16_t) -1u};
    InstructionInfo::Info i = map_get_or_default(M, pipe_id, sentinel);
    if (i.latency == sentinel.latency) {
        if (print_error) {
          todo(insn);
        }
        return Info{MISSING_LATENCY};
    }
    return i;
}
"""
    )
