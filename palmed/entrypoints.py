"""Various entrypoints for auxiliary tools"""

# PYTHON_ARGCOMPLETE_OK

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argcomplete, argparse
import logging
import pickle
from collections import defaultdict
from pathlib import Path
from typing import Dict, Any, List, Tuple, FrozenSet, Iterator

import palmed.instructions
from palmed.ports import Port
from palmed.config import Config
from palmed.utils import get_frac
from palmed.messages import setup_logging
import palmed.sol2output as s2o
import palmed.export2gus as e2g

logger = logging.getLogger(__name__)


def fill_base_parser(parser):
    parser.add_argument(
        "-m",
        "--machine",
        default=Config.SELECTED_MACHINE,
        dest="SELECTED_MACHINE",
        help=(
            "Machine to use as a source of benchmarks. If 'localhost', the local "
            "machine will be used and benchmarks will be computed on the go; else, "
            "results will only be fetched from DB, and the program will fail if "
            "benches are missing."
        ),
    )
    parser.add_argument(
        "--db-url",
        default=Config.DATABASE_URL,
        dest="DATABASE_URL",
        help="SQLAlchemy URL used to connect to the database",
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="LOGLEVEL",
        default=logging.INFO,
        const=logging.DEBUG,
        help="Print debug output",
    )
    parser.add_argument(
        "--exp-reconstruct",
        help=(
            "Experimental reconstruct feature. If enabled, the instructions "
            "checkpoint file and the database are unnecessary. This might not be as "
            "accurate as using those files, and might be buggy — if possible, do not "
            "use this flag"
        ),
        action="store_true",
        dest="experimental_reconstruct",
    )

    parser.add_argument("mapping_file", help="mapping file, in its pickled form")


def base_config(args):
    """Export the CLI options set in `fill_base_parser` to :Config:"""
    setup_logging(args.LOGLEVEL, False)
    Config.DEBUG = args.LOGLEVEL <= logging.DEBUG
    Config.SELECTED_MACHINE = args.SELECTED_MACHINE
    Config.DATABASE_URL = args.DATABASE_URL
    Config.CHECKPOINT.set_suffix(".pipedream.{}".format(Config.SELECTED_MACHINE))

    with open(args.mapping_file, "rb") as handle:
        palmed_mapping = pickle.load(handle)
    Config.ISA = palmed_mapping["ISA"]
    Config.MU_ARCH = palmed_mapping["MU_ARCH"]
    check_and_load_uops(palmed_mapping)

    if Config.UOPS:
        return palmed_mapping

    if args.experimental_reconstruct:
        instr_from_mapping = {}
        for _, instrs in palmed_mapping["class_to_instrs"].items():
            for instr in instrs:
                if instr.orig_IPC is None or instr._lat is None:
                    logger.error(
                        "Cannot restore IPC or latency of instruction %s", instr.name()
                    )
                    exit(1)
                num, denum = get_frac(instr.orig_IPC, precision=Config.MIN_BENCH_COEF)
                ipc = num / denum
                instr_from_mapping[instr.name()] = (
                    ipc,
                    instr._lat,
                )
        palmed.instructions.sideload_pipedream(instr_from_mapping)
    else:
        palmed.instructions.load_pipedream()
    return palmed_mapping


def check_and_load_uops(palmed_mapping: Dict[str, Any]):
    if any(
        [
            isinstance(instr, palmed.instructions.Instruction_UOPS)
            for instr in palmed_mapping["class_to_instrs"].keys()
        ]
    ):
        Config.PIPEDREAM = False
        Config.UOPS = True
        palmed.instructions.load_uops(Config.MU_ARCH)
    else:
        Config.PIPEDREAM = True
        Config.UOPS = False


def export_uops_mapping() -> None:
    """Export a UOPS mapping in the same format as `palmed-export-mapping`"""

    parser = argparse.ArgumentParser(
        prog="palmed-export-uops-mapping",
        description=(
            "export a UOPS mapping in the same format as `palmed-export-mapping`"
        ),
    )
    parser.add_argument(
        "out_path", help="file in which the exported mapping will be stored, pickled"
    )
    parser.add_argument(
        "--std-mapping",
        dest="independant_mapping",
        action="store_false",
        help="Export a standard palmed mapping instead of a palmed-independant one",
    )
    parser.add_argument(
        "--keep-free-insn",
        action="store_true",
        help="Do not prune instructions that use no ports",
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="LOGLEVEL",
        default=logging.INFO,
        const=logging.DEBUG,
        help="Print debug output",
    )
    parser.add_argument(
        "--arch", "-a", dest="ARCH", help="Architecture to run", required=True
    )

    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    setup_logging(args.LOGLEVEL, False)
    Config.DEBUG = args.LOGLEVEL <= logging.DEBUG
    Config.ISA = Config.SUPPORTED_ISA.X86.value
    Config.MU_ARCH = args.ARCH

    instr_isa = palmed.instructions.load_uops(
        args.ARCH, prune_nores=not args.keep_free_insn
    )
    instrs = []
    for isa in instr_isa.values():
        instrs += isa
    logger.info("Found %d instructions", len(instrs))

    # Determine equivalence classes
    classes: Dict[
        FrozenSet[Tuple[int, Port]], List[palmed.instructions.Instruction]
    ] = defaultdict(list)
    for instr in instrs:
        # `instr.ports()` returns a list of *coalesced* ports: there is no two
        # occurrences (n_1, p), (n_2, p) in the list
        classes[frozenset(instr.ports())].append(instr)

    out: Dict[str, Any] = {}

    def normalize_ports(
        ports: FrozenSet[Tuple[int, Port]],
    ) -> Iterator[Tuple[float, Port]]:
        for rep, port in ports:
            yield (rep / len(port), port)

    # Labelling function is the identity — uops resource names are straightforward
    out["label"] = {}
    for port in Port.all_ports():
        pt_name = port.name()
        out["label"][pt_name] = pt_name

    if args.independant_mapping:
        out["mapping"] = {
            cls_ins[0].name(): {
                resource.name(): weight for (weight, resource) in normalize_ports(ports)
            }
            for ports, cls_ins in classes.items()
        }

        out["instructions"] = {}
        for ins_cls in classes.values():
            if not ins_cls:
                continue
            cls_repr = ins_cls[0]
            for instr_generic in ins_cls:
                out["instructions"][instr_generic.name()] = {
                    "repr": cls_repr.name(),
                    "IPC": instr_generic.IPC(),
                    "latency": instr_generic.latency() or -1.0,
                }
        with open(args.out_path, "wb") as handle:
            pickle.dump(out, handle)
    else:
        mapping: Dict[palmed.instructions.Instruction, List[Tuple[float, str]]] = {
            cls_ins[0]: [
                (weight, res.name()) for (weight, res) in normalize_ports(ports)
            ]
            for ports, cls_ins in classes.items()
        }
        class_to_instrs: Dict[
            palmed.instructions.Instruction, List[palmed.instructions.Instruction]
        ] = {cls[0]: cls for cls in classes.values()}
        s2o.save_mapping(
            mapping=mapping,
            label=out["label"],
            class_to_instrs=class_to_instrs,
            filename=args.out_path,
        )


def export_independant_mapping():
    """Export a mapping file that does not require Palmed to be interpreted"""
    parser = argparse.ArgumentParser(
        prog="palmed-export-mapping",
        description=(
            "export a mapping file that does not require Palmed to be interpreted"
        ),
    )
    parser.add_argument(
        "out_path", help="file in which the exported mapping will be stored, pickled"
    )
    fill_base_parser(parser)

    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    palmed_mapping = base_config(args)

    all_instructions = {}
    for rep, cls in palmed_mapping["class_to_instrs"].items():
        for instr in cls:
            all_instructions[instr] = rep
            if (
                isinstance(instr, palmed.instructions.Instruction_Pipedream)
                and not instr.has_IPC()
            ):
                raise Exception("Instruction {} has no IPC".format(instr))

    out = {}

    out["label"] = palmed_mapping["label"]
    out["mapping"] = {
        canonical_instr.name(): {
            resource: weight for (weight, resource) in instr_mapping
        }
        for canonical_instr, instr_mapping in palmed_mapping["mapping"].items()
    }
    out["instructions"] = {
        instr.name(): {
            "repr": rep.name(),
            "IPC": instr.IPC(),
            "latency": instr.latency(),
        }
        for instr, rep in all_instructions.items()
    }

    with open(args.out_path, "wb") as handle:
        pickle.dump(out, handle)


def export_gus_files():
    """Export GUS' `Instruction_Info.cpp` file derived from the computed mapping"""
    parser = argparse.ArgumentParser(
        prog="palmed-export-gus",
        description=("export files needed for GUS simulation"),
    )

    parser.add_argument(
        "--out-path",
        type=str,
        help="folder in which GUS files will be saved",
        default=".",
    )
    fill_base_parser(parser)

    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    palmed_mapping = base_config(args)

    e2g.export_Instruction_Info_cpp(
        palmed_mapping["mapping"],
        palmed_mapping["label"],
        palmed_mapping["class_to_instrs"],
        Path(args.out_path),
    )
