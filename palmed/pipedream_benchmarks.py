## \file pipedream_benchmarks.py
## \brief Pipedream-specific code for benchmarking

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import enum
import logging
import numpy as np
import tempfile
import pipedream.asm.ir as pipeasm  # type:ignore
import pipedream.benchmark as pipebench  # type:ignore
from typing import List, Dict, Tuple, Optional, Iterable, Any, NamedTuple
from shutil import copy
from palmed.config import Config
from palmed.utils import BenchmarkScheduler, retry_on
from palmed.pinned_pool import select_multiprocessing_pool

logger = logging.getLogger(__name__)


class IPC_bench_stats(NamedTuple):
    """Statistics on the way this IPC measure was made — if made on the spot"""

    variance: float
    num_iterations: int
    num_warmup_iterations: int
    num_dynamic_instructions: int
    unrolled_length: int
    core: int
    parallel_cores_used: int
    use_register_pools: bool
    align_kernel: bool


class UnknownMeasuringError(Exception):
    pass


class PipedreamKernel:
    """Small interface to link with Pipedream's measurement subsystem"""

    _instructions: List[pipeasm.Instruction]

    def __init__(self, list_instr: List[str]):
        if not list_instr:
            raise Exception("Empty benchmark: no instructions to be benchmarked.")
        arch = pipeasm.Architecture.for_name(Config.ISA)
        inst_set = arch.instruction_set()
        self._instructions = [
            inst_set.instruction_for_name(inst) for inst in list_instr
        ]

    def get_pipedream_benchmark(
        self,
        with_mapping: bool,
        num_dynamic_instructions: int,
        unrolled_length: int,
        kind: pipebench.types.Benchmark_Kind,
    ) -> pipebench.Benchmark_Spec:
        arch = pipeasm.Architecture.for_name(Config.ISA)
        instrs: pipeasm.Instruction = []

        instr_distrib: Dict[pipeasm.Instruction, int] = {}
        for instr in self._instructions:
            instr_distrib[instr] = 1

        benchmark: pipebench.Benchmark_Spec = (
            pipebench.Benchmark_Spec.from_instructions(
                arch=arch,
                kind=kind,
                # kernel will contain these instructions, in this order
                instructions=self._instructions,
                register_pools=instr_distrib if with_mapping else None,
                # benchmark will execute this many instructions in total (~approximate)
                num_dynamic_instructions=num_dynamic_instructions,
                # benchmark kernel will be unrolled to be this many instructions long
                unrolled_length=unrolled_length,
            )
        )
        return benchmark

    def measure_CPI(
        self,
        num_dynamic_instructions: int,
        unrolled_length: int,
        num_iterations: int,
        kind: pipebench.types.Benchmark_Kind,
        keep_raw_values=False,
        with_mapping=True,
        align_kernel=False,
        core: Optional[int] = None,
        asm_copyfile: Optional[str] = None,
    ) -> Tuple[float, Optional[List[float]], IPC_bench_stats]:
        logger.debug(
            "Benching IPC for instruction(s) %s",
            ",".join([ins.name for ins in self._instructions]),
        )
        arch = pipeasm.Architecture.for_name(Config.ISA)
        perf_counters = pipebench.Perf_Counter_Spec.make_throughput_counters()

        @retry_on(times=5, exn_types=(RuntimeError,))
        def get_results(
            with_mapping: bool,
        ) -> Tuple[pipebench.Benchmark_Run, Dict, int]:
            bench_stats = {
                "num_iterations": num_iterations,
                "num_warmup_iterations": 10,
                "num_dynamic_instructions": num_dynamic_instructions,
                "unrolled_length": unrolled_length,
                "use_register_pools": with_mapping,
                "align_kernel": align_kernel,
                "parallel_cores_used": select_multiprocessing_pool().current_process_count(),
            }
            benchmark: pipebench.Benchmark_Spec = self.get_pipedream_benchmark(
                with_mapping, num_dynamic_instructions, unrolled_length, kind
            )
            static_instr_count = (
                benchmark.unroll_factor
                * len(self._instructions)
                * benchmark.kernel_iterations
            )
            with tempfile.TemporaryDirectory() as tmp_dir:
                with BenchmarkScheduler(core=core) as scheduler:
                    # `run_benchmarks` return a generator, and do not - as its
                    # name may suggest - compute right away the value. Casting
                    # as a list forces it to do so. We return the firs value as
                    # we know only one benchmark is ever measure (i.e. `[benchmark]`)
                    res: pipebench.Benchmark_Run = list(
                        pipebench.run_benchmarks(
                            arch=arch,
                            benchmarks=[benchmark],
                            perf_counters=perf_counters,
                            ## how often is each benchmark run?
                            num_iterations=bench_stats["num_iterations"],
                            num_warmup_iterations=bench_stats["num_warmup_iterations"],
                            ## optional:
                            ##   Integers between 0 and 100.
                            ##   Results have *cycles* below/above the given percentiles are dropped.
                            ##   Helps filter out noise, like runs where the OS scheduler did something weird.
                            outlier_low=0,
                            outlier_high=100 if keep_raw_values else 25,
                            ## where should we store the temporay files we generate?
                            tmp_dir=tmp_dir,
                            verbose=False,
                            keep_raw_data=True,
                            ## extra debug messages
                            debug=False,
                            is_lib_tmp=False,
                        )
                    )[0]
                    bench_stats["core"] = scheduler.get_exec_core()
                if asm_copyfile is not None:
                    copy(
                        tmp_dir + "/benchmarks.s",
                        asm_copyfile + "/" + self.name() + ".s",
                    )
            logger.debug(
                "End of bench for instruction(s) %s",
                ",".join([ins.name for ins in self._instructions]),
            )
            return (res, bench_stats, static_instr_count)

        results: Optional[pipebench.Benchmark_Run] = None
        if Config.PIPEDREAM_NO_REGISTER_POOLS:
            results, bench_stats, instr_count = get_results(False)
        else:
            try:
                results, bench_stats, instr_count = get_results(with_mapping)
            except (pipeasm.Architecture.NotEnoughRegisters, pipeasm.Allocation_Error):
                print("Cannot use register pools: Falling back to usual allocator")
                assert with_mapping
                results, bench_stats, instr_count = get_results(False)

        ret = results.cycles.mean / instr_count
        if ret < 0.01:
            # more than 100 instructions per cycle is for sure an
            # error
            raise UnknownMeasuringError

        # Compute IPC variance
        ipc_data = instr_count / np.array(results.cycles.raw_data)
        bench_stats["variance"] = float(np.var(ipc_data))
        raw_data = list(ipc_data) if keep_raw_values else None
        return ret, raw_data, IPC_bench_stats(**bench_stats)

    def name(self):
        return "__".join(
            map(
                lambda instr: f"{instr.name}",
                self._instructions,
            )
        )


def is_valid_instruction(name: str) -> bool:
    """Get the corresponding `pipeasm.Instruction`"""
    arch = pipeasm.Architecture.for_name(Config.ISA)
    inst_set = arch.instruction_set()
    try:
        return inst_set.instruction_for_name(name)
    except KeyError:
        return False
    return True
