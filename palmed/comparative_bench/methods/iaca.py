## \file iaca.py
## \brief Code to benchmark translation blocks using IACA

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import subprocess
import logging
import traceback
from pathlib import Path
from .generic import IpcMeasure, MeasureFailed
from palmed.config import Config

logger = logging.getLogger(__name__)


class IacaIpcMeasure(IpcMeasure):
    def iaca_report(self, tmp_dir) -> str:
        if Config.ISA != Config.SUPPORTED_ISA.X86.value:
            logger.error("IACA bencher cannot be use with non-x86 ISA")
            exit()
        self.generate_benchmark_lib(tmp_dir, iaca_markers=True)
        try:
            out = subprocess.run(
                [
                    "iaca",
                    "--arch",
                    Config.MU_ARCH,
                    (tmp_dir / "benchmarks.o").as_posix(),
                ],
                stdout=subprocess.PIPE,
                check=True,
            )
        except subprocess.CalledProcessError as exn:
            raise MeasureFailed(
                "IACA error: command returned {status}".format(status=exn.returncode)
            ) from exn
        ret = out.stdout.decode("utf-8")
        if self.report_savedir is not None:
            with (Path(self.report_savedir) / Path(self.name + ".iaca.log")).open(
                "w"
            ) as f:
                f.write(ret)
        return ret

    def _do_get_ipc(self, tmp_dir):
        parsed = self.iaca_report(tmp_dir).split("\n")
        num_instructions = 0
        for line_std in parsed:
            if line_std[:17] == "Block Throughput:":
                idx = 0
                for idx, ch in enumerate(line_std):
                    if ch == "C":
                        break
                num_cycles = float(line_std[18 : idx - 1])
            if line_std[:4] == "|   ":
                num_instructions += 1
        return num_instructions / num_cycles
