## \file uops_capped.py
## \brief Code to benchmark translation blocks using UOPS data, capped to IPC=4

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from .generic import IpcMeasure
from . import uops


class CappedUopsIpcMeasure(uops.UopsIpcMeasure):
    def _do_get_ipc(self, tmp_dir) -> float:
        uops_result = super()._do_get_ipc(tmp_dir)
        return min(uops_result, 4.0)
