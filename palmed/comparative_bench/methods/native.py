## \file native.py
## \brief Code to benchmark translation blocks by actually running it on the machine

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List, Optional
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from .generic import IpcMeasure


class NativeIpcMeasure(IpcMeasure):
    REQUIRE_SCHEDULER = True
    _raw_values: Optional[List[float]] = None

    def _do_get_ipc(self, tmp_dir) -> float:
        ipc, raw_values = self.benchmark.IPC_no_db(
            keep_raw_values=True, asm_copyfile=self.asm_savedir
        )
        self._raw_values = raw_values
        return ipc

    @property
    def raw_values(self) -> List[float]:
        if self._raw_values is None:
            raise Exception(
                "NativeIpcMeasure has no raw_value before its measure: "
                "(_do_get_ipc() should probably be called before this)"
            )
        else:
            return self._raw_values
