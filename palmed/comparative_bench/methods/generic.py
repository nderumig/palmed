## \file generic.py
## \brief Common benchmarking code

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import tempfile
import logging
import traceback
from pathlib import Path
from palmed.utils import BenchmarkScheduler
from palmed.config import Config
from .. import generic
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from pipedream.asm.ir import Architecture

logger = logging.getLogger(__name__)


class MeasureFailed(Exception):
    """An error occurring during a benchmark"""

    def __init__(self, what):
        super().__init__()
        self.what = what

    def __str__(self):
        return self.what


class IpcMeasure:
    """Abstract base class for an IPC measure"""

    REQUIRE_SCHEDULER = False
    NUM_DYNAMIC_INSTRUCTIONS = 50_000_000

    def __init__(self, benchmark, inspect_crashes, asm_savedir, report_savedir):
        self.benchmark = benchmark
        self.inspect_crashes = inspect_crashes
        self.asm_savedir = asm_savedir
        self.report_savedir = report_savedir

    def generate_benchmark_lib(self, tmp_dir, iaca_markers=False, papi_calls=True):
        def do_gen(with_mapping):
            pipedream_bench = self.benchmark.to_pipedream_benchmark(
                with_mapping=with_mapping,
                num_dynamic_instructions=self.NUM_DYNAMIC_INSTRUCTIONS,
            )
            arch = pipeasm.Architecture.for_name(Config.ISA)
            pipebench.gen_benchmark_lib(
                arch=arch,
                benchmarks=[pipedream_bench],
                # how often is each benchmark run?
                num_iterations=50,
                gen_iaca_markers=iaca_markers,
                gen_papi_calls=papi_calls,
                verbose=False,
                # where should we store the temporay files we generate?
                dst_dir=tmp_dir.as_posix(),
                # extra debug messages
                debug=False,
                is_lib_tmp=False,
            )

        try:
            logger.debug("Trying with mapping")
            do_gen(True)
        except Architecture.NotEnoughRegisters:
            try:
                logger.debug("Not enough registers. Falling back to no mapping.")
                do_gen(False)
            except Architecture.NotEnoughRegisters as exn:
                raise MeasureFailed("Not enough registers") from exn

    def get_ipc(self) -> float:
        """Compute and get the IPC"""
        try:
            with tempfile.TemporaryDirectory(prefix="palmed_bench") as tmp_dir:
                with BenchmarkScheduler(nop=not self.REQUIRE_SCHEDULER):
                    return self._do_get_ipc(Path(tmp_dir))
        except MeasureFailed as exn:
            raise exn from exn
        except Exception as exn:
            # Uncaught exception in specific class. Catch to avoid crash.
            if self.inspect_crashes:
                logging.error(
                    "Recovered from unhandled exception in benchmark: [%s] %s. "
                    "Traceback:\n%s",
                    exn.__class__.__name__,
                    exn,
                    traceback.format_exc(),
                )
            raise MeasureFailed("Recover from unhandled crash") from exn

    @property
    def name(self):
        return "__".join(
            map(
                lambda instr: f"{instr}",
                self.benchmark.mingled_instructions,
            )
        )

    def _do_get_ipc(self, tmp_dir):
        pass

    @classmethod
    def pool_initargs(cls):
        return ()

    @classmethod
    def pool_initfunc(cls, *args):
        pass
