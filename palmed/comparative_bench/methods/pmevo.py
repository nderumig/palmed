## \file pmevo.py
## \brief Code to benchmark translation blocks using PMEvo

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import Optional, List, Set, Tuple, Dict
import logging
from palmed import instructions
from palmed.config import Config
from .generic import IpcMeasure, MeasureFailed

from pmevo_eval.eval import PmevoMapping
from pmevo_eval.utils.architecture import Insn as PmInsn

logger = logging.getLogger(__name__)


class PmevoIpcMeasure(IpcMeasure):
    _pmevo_mapping: Optional[PmevoMapping] = None
    _insn_mapping: Optional[Dict[instructions.Instruction_Pipedream, PmInsn]] = None

    class Uninitialized(Exception):
        def __str__(self):
            return "PMEvo evaluator is not initialized. Call `init_mapping` before."

    @classmethod
    def init_mapping(cls, insn_list: List[instructions.Instruction_Pipedream]):
        """Initialize the mapping between Palmed and PMEvo instructions"""
        arch = Config.MU_ARCH
        if arch in ["SKL", "SKX", "SKL-SP"]:
            arch = "SKL"
        cls._pmevo_mapping = PmevoMapping(arch)
        cls._insn_mapping = cls._pmevo_mapping.map_instructions(
            list(map(lambda x: x.name(), insn_list))
        )

    @property
    def pmevo_mapping(self) -> PmevoMapping:
        if self._pmevo_mapping is None:
            raise self.Uninitialized()
        return self._pmevo_mapping

    @property
    def insn_mapping(self) -> Dict[instructions.Instruction_Pipedream, PmInsn]:
        if self._insn_mapping is None:
            raise self.Uninitialized()
        return self._insn_mapping

    def _get_bench_pmevo_instrs(self) -> List[PmInsn]:
        return list(
            filter(
                lambda x: x is not None,
                map(
                    lambda palmed_insn: self.insn_mapping.get(palmed_insn, None),
                    self.benchmark.mingled_instructions,
                ),
            )
        )

    def _do_get_ipc(self, tmp_dir) -> float:
        try:
            instrs = self._get_bench_pmevo_instrs()
            if instrs:
                return self.pmevo_mapping.ipc_for(instrs)
            logger.info("PMEvo: no supported instructions, empty bench")
            raise MeasureFailed("PMEvo: no supported instructions, empty bench")
        except KeyError as exn:
            logger.info("PMEvo cannot bench instruction %s", str(exn))
            raise MeasureFailed(
                "PMEvo error: cannot bench instruction {}".format(str(exn))
            ) from exn
