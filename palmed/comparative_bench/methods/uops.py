## \file uops.py
## \brief Code to benchmark translation blocks using UOPS data

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List, Set, Tuple
import logging
import multiprocessing as mp
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from palmed import instructions
from .generic import IpcMeasure, MeasureFailed

logger = logging.getLogger(__name__)


class UopsIpcMeasure(IpcMeasure):
    _unsupported_instr_set: Set[instructions.Instruction_Pipedream] = set()
    _rec_pipes: Set[mp.connection.Connection] = set()
    _send_pipe: mp.connection.Connection

    def __init__(self, *args, **kwargs):
        # class member `_send_pipe` must be initialised (by `pool_args()` and
        # `pool_init()`) before using an instance of `UopsIpcMeasure`
        assert hasattr(self.__class__, "_send_pipe")
        super().__init__(*args, **kwargs)

    def _do_get_ipc(self, tmp_dir) -> float:
        try:
            return self.benchmark.IPC_UOPS_from_pipedream()
        except instructions.InstructionNotFound as exn:
            logger.info("μOPs cannot bench instruction %s", exn.instr_name)
            self.__class__._send_pipe.send(exn.instr_name)
            raise MeasureFailed(
                "UOPS error: cannot bench instruction {}".format(exn.instr_name)
            ) from exn

    @classmethod
    def unsupported_instr(cls) -> Set[instructions.Instruction_Pipedream]:
        for rec_pipe in cls._rec_pipes:
            while rec_pipe.poll():  # Beware! This returns `True` on a closed pipe...
                try:
                    unsupported = rec_pipe.recv()
                    cls._unsupported_instr_set.add(unsupported)
                except EOFError:  # ...hence the exception handling anyway
                    break
        return cls._unsupported_instr_set

    @classmethod
    def pool_initargs(cls) -> Tuple:
        rec_pipe, sendpipe = mp.Pipe(duplex=False)
        cls._rec_pipes.add(rec_pipe)
        return (sendpipe,)

    @classmethod
    def pool_initfunc(cls, *args) -> None:
        cls._send_pipe = args[0]
