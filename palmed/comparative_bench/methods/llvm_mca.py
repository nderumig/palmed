## \file llvm_mca.py
## \brief Code to benchmark translation blocks using llvm-mca

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import subprocess
import logging
import typing as t
import re
from pathlib import Path
from palmed.config import Config
from .generic import IpcMeasure, MeasureFailed

logger = logging.getLogger(__name__)


class LlvmMcaIpcMeasure(IpcMeasure):
    MCPU: t.Optional[str] = None  # Value for --mcpu llvm switch if remote
    _ISA_MAP = {  # Maps Palmed ISAs to --march llvm switch values
        "ARMv8a": "aarch64",
        "x86": "x86-64",
    }

    class BadConfiguration(Exception):
        """Raised when some required parameters are left unconfigured"""

    @classmethod
    def base_cmd(cls):
        """Get the base template for command line invocation"""
        out = [
            "llvm-mca",
        ]
        if Config.REMOTE_BENCH:
            if cls.MCPU is None:
                raise cls.BadConfiguration(
                    "LLVM MCA's MCPU must be configured for remote benchmarking"
                )
            out += [
                "--march",
                cls._ISA_MAP[Config.ISA],
                "--mcpu",
                cls.MCPU,
            ]
        return out

    @classmethod
    def check_configuration(cls):
        """Check whether llvm-mca is correctly set up and able to produce results"""
        cmd = cls.base_cmd()
        cmd.append("/dev/null")
        out = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,  # This command will fail anyway -- no input
        )
        assert out.returncode == 1
        stderr = out.stderr.decode("utf-8")

        bad_arch = re.search(r"error: invalid target '(?P<arch>[^']*)'", stderr)
        if bad_arch:
            raise cls.BadConfiguration(
                f"Bad architecture for llvm-mca: --march={bad_arch.group('arch')}"
            )

        bad_cpu = re.search(r"'(?P<cpu>[^']*)' is not a recognized processor", stderr)
        if bad_cpu:
            arch = "(native)"
            if Config.REMOTE_BENCH:
                arch = cls._ISA_MAP[Config.ISA]
            raise cls.BadConfiguration(
                f"Bad CPU for llvm-mca for arch {arch}: --mcpu={bad_cpu.group('cpu')}"
            )

        if "no assembly instructions found" in stderr:
            return  # all went well
        raise cls.BadConfiguration(f"Bad configuration for llvm-mca: {stderr}")

    def _prepare_asm(self, tmp_dir):
        in_path = tmp_dir / "benchmarks.s"
        out_path = tmp_dir / "benchmarks_sed.s"
        with out_path.open("w") as out_handle:
            try:
                subprocess.run(
                    ["sed", "s/movsxd/movsx/g", in_path.as_posix()],
                    check=True,
                    stdout=out_handle,
                )
            except subprocess.CalledProcessError as exn:
                raise MeasureFailed(
                    "sed: could not perform regex-replacement on asm"
                ) from exn
        return out_path

    def _do_get_ipc(self, tmp_dir):
        self.generate_benchmark_lib(tmp_dir)
        prepared_asm_path = self._prepare_asm(tmp_dir)
        args = self.base_cmd()
        args.append(prepared_asm_path.as_posix())
        try:
            out = subprocess.run(
                args,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                check=True,
            )
        except subprocess.CalledProcessError as exn:
            raise MeasureFailed(
                "llvm-mca error: command returned {status}".format(
                    status=exn.returncode
                )
            ) from exn
        stdout = out.stdout.decode("utf-8")
        if self.report_savedir is not None:
            with (Path(self.report_savedir) / Path(self.name + ".llvm-mca.log")).open(
                "w"
            ) as f:
                f.write(stdout)

        for line in stdout.split("\n"):
            if line.startswith("IPC:"):
                return float(line.split(":", 1)[1].strip())

        raise MeasureFailed("llvm-mca did not return an IPC.")
