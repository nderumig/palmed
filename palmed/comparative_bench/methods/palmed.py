## \file palmed.py
## \brief Code to benchmark translation blocks using a Palmed-generated mapping

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List, Dict, Tuple
import logging
import pickle
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from palmed import instructions as ins
from .generic import IpcMeasure, MeasureFailed

logger = logging.getLogger(__name__)


class PalmedIpcMeasure(IpcMeasure):
    class_to_instrs: Dict[ins.Instruction, List[ins.Instruction]] = {}
    instr_to_class: Dict[ins.Instruction, ins.Instruction] = {}
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]] = {}

    @classmethod
    def read_mapping(cls, handle):
        mapping_dict = pickle.load(handle)
        cls.class_to_instrs = mapping_dict["class_to_instrs"]
        cls.mapping = mapping_dict["mapping"]

        cls.instr_to_class = {}
        for repr_instr, instrs in cls.class_to_instrs.items():
            for instr in instrs:
                cls.instr_to_class[instr] = repr_instr

    def _do_get_ipc(self, tmp_dir) -> float:
        try:
            return self.benchmark.IPC_with_mapping(self.mapping, self.instr_to_class)
        except ins.InstructionNotFound as exn:
            logger.info("Palmed mapping cannot bench instruction %s", exn.instr_name)
            raise MeasureFailed(
                "Palmed mapping error: cannot bench instruction {}".format(
                    exn.instr_name
                )
            ) from exn

    @classmethod
    def pool_initargs(cls) -> Tuple:
        return (cls.class_to_instrs, cls.instr_to_class, cls.mapping)

    @classmethod
    def pool_initfunc(cls, class_to_instrs, instr_to_class, mapping):
        cls.class_to_instrs = class_to_instrs
        cls.instr_to_class = instr_to_class
        cls.mapping = mapping
