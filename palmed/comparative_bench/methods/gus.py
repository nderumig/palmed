## \file gus.py
## \brief Code to benchmark translation blocks using GUS

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import subprocess
import os
import logging
from typing import List
from pathlib import Path
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from .generic import IpcMeasure, MeasureFailed
from ..generic import format_command

logger = logging.getLogger(__name__)


class GusIpcMeasure(IpcMeasure):
    NUM_DYNAMIC_INSTRUCTIONS = 1000

    def _do_get_ipc(self, tmp_dir) -> float:
        self.generate_benchmark_lib(tmp_dir, papi_calls=False)

        base_palmed_dir = Path(os.path.dirname(os.path.abspath(__file__))) / "../../.."
        gcc_command = [
            "gcc",
            (base_palmed_dir / "tools/benchmark_loader.c").as_posix(),
            (tmp_dir / "benchmarks.o").as_posix(),
            "-o",
            (tmp_dir / "benchmark.gus").as_posix(),
        ]

        gus_command = [
            "gus",
            "--kernel",
            "benchmark_kernel_0",
            (tmp_dir / "benchmark.gus").as_posix(),
        ]
        try:
            subprocess.run(gcc_command, check=True)
        except subprocess.CalledProcessError as exn:
            raise MeasureFailed(
                "GCC error: command returned {status} while running `{cmd}`".format(
                    status=exn.returncode, cmd=format_command(gcc_command)
                )
            ) from exn

        try:
            out = subprocess.run(gus_command, stdout=subprocess.PIPE, check=True)
        except subprocess.CalledProcessError as exn:
            raise MeasureFailed(
                "Gus error: command returned {status} while running `{cmd}`".format(
                    status=exn.returncode, cmd=format_command(gus_command)
                )
            ) from exn

        parsed = out.stdout.decode("ascii").split("\n")
        logger.debug("<< Full GUS output : >>")
        logger.debug(out.stdout.decode("ascii"))
        for line_std in parsed:
            if line_std[:5] == "# IPC":
                return float(line_std[7:-1])
        return -1
