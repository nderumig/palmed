## \file palmed_front.py
## \brief Code to benchmark translation blocks using a Palmed-generated mapping

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import List, Dict, Tuple
import logging
import pickle
import pipedream.benchmark as pipebench
import pipedream.asm.x86 as pipeasm
from palmed import instructions as ins
from .generic import IpcMeasure, MeasureFailed
from .palmed import PalmedIpcMeasure
import a72_frontend

logger = logging.getLogger(__name__)


class PalmedFrontIpcMeasure(PalmedIpcMeasure):
    FRONTEND_MODEL: type[a72_frontend.generic.BaseFrontend]

    def _do_get_ipc(self, tmp_dir) -> float:
        palmed_ipc = super()._do_get_ipc(tmp_dir)
        try:
            frontend_carac = self.__class__.FRONTEND_MODEL(
                self.benchmark.mingled_instructions
            )
            frontend_cycles = frontend_carac.cycles()
        except a72_frontend.generic.InstructionNotFound as exn:
            logger.info("Palmed frontend cannot bench instruction %s", exn)
            raise MeasureFailed(
                f"Palmed frontend error: cannot bench instruction {exn}"
            ) from exn
        frontend_ipc = float(self.benchmark.card() / frontend_cycles)
        return min(palmed_ipc, frontend_ipc)

    @classmethod
    def pool_initfunc(cls, *args):
        super().pool_initfunc(*args)
        a72_frontend.uop_nocross.Instruction.load()


class PalmedFrontIpcMeasure_DispTense(PalmedFrontIpcMeasure):
    FRONTEND_MODEL = a72_frontend.dispatch_model.TenseDispatchModel


class PalmedFrontIpcMeasure_DispSlack(PalmedFrontIpcMeasure):
    FRONTEND_MODEL = a72_frontend.dispatch_model.SlackDispatchModel


class PalmedFrontIpcMeasure_UopLinear(PalmedFrontIpcMeasure):
    FRONTEND_MODEL = a72_frontend.uop_nocross.PureLinearModel


class PalmedFrontIpcMeasure_UopNoCross(PalmedFrontIpcMeasure):
    FRONTEND_MODEL = a72_frontend.uop_nocross.UopNoCrossModel
