## \file entry.py
## \brief Entrypoint functions for benchmark analysis

# PYTHON_ARGCOMPLETE_OK

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import pickle
import argcomplete, argparse
import logging
import math
import re
import itertools
import numpy as np
from scipy.stats import kendalltau
from enum import Enum
from ruamel import yaml
from typing import Dict, List, Sequence, Callable, Any, Iterable, Tuple, Optional
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from palmed.messages import setup_logging
from palmed.comparative_bench.entry import (
    AnnotatedBench,
    BenchResult,
    EvaluationResult,
)

logger = logging.getLogger(__name__)


class RmsWeightingChoice(Enum):
    """Choices for the --rms-weighting option"""

    NONE = "none"
    OCCURENCES = "occurences"
    OCCURENCES_BY_FILE = "occurences_by_file"


def parse_arguments():
    """Parse command-line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "bench_file",
        help="The benchmarks file to analyze, in pickled format",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
        help="Verbose output",
        default=logging.WARNING,
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="Debug output",
    )

    parser.add_argument(
        "--restrict-re",
        action="append",
        default=[],
        help="Restrict to benchmarks whose source matches this regex",
    )
    parser.add_argument(
        "--restrict",
        action="append",
        default=[],
        help="Restrict to benchmarks whose source contains this string",
    )
    parser.add_argument(
        "--restrict-neg",
        action="append",
        default=[],
        help="Restrict to benchmarks whose source does not contain this string",
    )

    parser.add_argument(
        "--print-rms",
        help="Print the RMS error of the respective benchers.",
        action="store_true",
    )
    parser.add_argument(
        "--rms-weighting",
        help="Weighting of the various benches in RMS error computation",
        choices=RmsWeightingChoice,
        type=RmsWeightingChoice,
        default=RmsWeightingChoice.OCCURENCES_BY_FILE,
    )
    parser.add_argument(
        "--rms-ref",
        help="Bencher acting as a reference for the others",
        default="native",
    )

    parser.add_argument(
        "--print-mape",
        help=(
            "Print the MAPE error of the respective benchers. MAPE = RMS without "
            "squaring (Mean Absolute Percentage Error), used by PMEvo paper."
        ),
        action="store_true",
    )
    parser.add_argument(
        "--mape-weighting",
        help="Weighting of the various benches in MAPE error computation",
        choices=RmsWeightingChoice,
        type=RmsWeightingChoice,
        default=RmsWeightingChoice.OCCURENCES_BY_FILE,
    )
    parser.add_argument(
        "--mape-ref",
        help="Bencher acting as a reference for the others",
        default="native",
    )

    parser.add_argument(
        "--tau",
        help="Computes Kendall's τ correlation coefficient for this bench series",
        action="store_true",
    )
    parser.add_argument(
        "--tau-ref",
        help="Bencher acting as a reference for the τ coefficient",
        default="native",
    )

    parser.add_argument(
        "--nb-worst-benchs",
        help="Number of worst basic blocks to record",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--worst-benchs-bencher",
        help="Use this bencher for worst BB output",
        default="palmed",
    )
    parser.add_argument(
        "--worst-benchs-file",
        help="File storing the worst predicted basic blocks",
        default="worst_benchs.yaml",
    )

    parser.add_argument(
        "--heatmap",
        help=(
            "Save heatmaps for each bencher vs. native, using this as a prefix for "
            "the saved files (eg. `--heatmap foo/bar` generates `foo/bar.palmed.png`)"
        ),
    )
    parser.add_argument(
        "--heatmap-unweighted",
        help="Is the heatmap weighted by basic block occurrences?",
        dest="heatmap_weighted",
        action="store_false",
    )
    parser.add_argument(
        "--heatmap-maxw",
        help="Max IPC to show for the reference (native).",
        default=6,
        type=int,
    )
    parser.add_argument(
        "--heatmap-maxh",
        help="Max IPC to show for the bencher. Do not pass for automatic choice.",
        default=0,
        type=int,
    )
    parser.add_argument(
        "--heatmap-noratio",
        help=(
            "Plot the heatmap as a ratio pred_IPC/native_IPC = f(native_IPC) "
            "(default), or as pred_IPC vs native_IPC."
        ),
        dest="heatmap_ratio",
        action="store_false",
    )
    parser.add_argument(
        "--heatmap-nolegend",
        help="Remove x/y axes legends on heatmaps",
        dest="heatmap_legend",
        action="store_false",
    )
    parser.add_argument(
        "--heatmap-in-cycles",
        help="Plot the heatmap in cycles instead of IPC.",
        action="store_true",
    )

    parser.add_argument(
        "--latex-tables",
        help="Show all tables in a LaTeX format.",
        action="store_true",
    )

    parser.add_argument(
        "--toolname", help="Name used to describe the tool", default="palmed"
    )
    argcomplete.autocomplete(parser)
    return parser.parse_args()


def get_export_format(path: str) -> str:
    EXTS = ["svg", "pdf", "png"]
    for ext in EXTS:
        if path.endswith(".{}".format(ext)):
            return ext
    raise Exception("Bad image extension for savefig: {}".format(path))


def pmevo_heatmap(
    bench_results: EvaluationResult,
    save_path: str,
    predictor: str,
    maxw: int,
    maxh: Optional[int] = None,
    predictor_name: Optional[str] = None,
    reference: str = "native",
    only_clean_rows: bool = True,
    weighted: bool = True,
    colorbar: bool = True,
    legends: bool = True,
    in_cycles: bool = False,
    horizontal_reference: bool = False,
):
    """Computes a heatmap "predicted IPC / reference IPC" "à la PMEvo" (PLDI'20 p10).
    Saves it as an image."""
    HORIZ_REF_HBOUND = 3
    MAX_IPC_H_ASPECT = 8
    HORIZ_REF_HRATIO = MAX_IPC_H_ASPECT / HORIZ_REF_HBOUND
    GRANULARITY = 5  # dots / IPC unit
    H_GRANULARITY = GRANULARITY * (HORIZ_REF_HRATIO if horizontal_reference else 1)
    W_GRANULARITY = GRANULARITY
    MAX_IPC_H = maxh or (HORIZ_REF_HBOUND if horizontal_reference else MAX_IPC_H_ASPECT)
    MAX_IPC_W = maxw
    MAX_IPC_MIN = min(MAX_IPC_H, MAX_IPC_W)
    width = int(math.ceil(MAX_IPC_W * W_GRANULARITY))
    height = int(math.ceil(MAX_IPC_H * H_GRANULARITY))
    EPSILON = 1e-6

    if predictor_name is None:
        predictor_name = predictor

    class OutOfBounds(Exception):
        pass

    def array_coord(ipc: float, bound: float, granularity: float) -> int:
        epsilonized_ipc = max(0.0, ipc - EPSILON)
        out = math.floor(epsilonized_ipc * granularity)
        if out >= bound:
            raise OutOfBounds
        return out

    def array_coord_h(ipc: float) -> int:
        return array_coord(ipc, height, H_GRANULARITY)

    def array_coord_w(ipc: float) -> int:
        return array_coord(ipc, width, W_GRANULARITY)

    heatmap_array = np.zeros((height, width))
    for bench in bench_results.benchs:
        if only_clean_rows and bench.has_error:
            continue
        for bencher in [predictor, reference]:
            if bench.results[bencher] is None:
                continue
            pred_ipc: float = bench.results[predictor]  # type:ignore
            ref_ipc: float = bench.results[reference]  # type:ignore

            if in_cycles:
                bench_instrs = bench.bench.benchmark.card()
                pred_ipc = bench_instrs / pred_ipc
                ref_ipc = bench_instrs / ref_ipc

            weight: int = bench.bench.occur if weighted else 1
            try:
                coord_h = array_coord_h(
                    pred_ipc / ref_ipc if horizontal_reference else pred_ipc
                )
                heatmap_array[coord_h, array_coord_w(ref_ipc)] += weight
            except OutOfBounds:
                pass

    vmax = len(bench_results.benchs)
    ipc_or_cycles = "cycles" if in_cycles else "IPC"
    if weighted:
        vmax = sum(map(lambda bench: bench.bench.occur, bench_results.benchs))
    plt.clf()
    plt.rcParams.update({"font.size": 25})
    if legends:
        if horizontal_reference:
            plt.ylabel("{}/native {} ratio".format(predictor_name, ipc_or_cycles))
        else:
            plt.ylabel("{}-predicted {}".format(predictor_name, ipc_or_cycles))
        plt.xlabel("{} {}".format(reference, ipc_or_cycles))
    plt.xticks(range(0, MAX_IPC_W + 1, 2))
    if horizontal_reference:
        plt.plot(
            [0, MAX_IPC_W],
            [1, 1],
            "r-",
            color="#ff000080",
            linewidth=2,
        )
    else:
        plt.plot(
            [0, MAX_IPC_MIN], [0, MAX_IPC_MIN], "r-", color="#ff000080", linewidth=1
        )

    heatmap_res = plt.imshow(
        heatmap_array,
        extent=(0, MAX_IPC_W, 0, MAX_IPC_H),
        # ^ Rectangle, in plotted coordinates. covered by the figure
        aspect=HORIZ_REF_HRATIO if horizontal_reference else "equal",
        # ^ Aspect ratio of a "pixel": height/width in plotted coordinates.
        # This value ensures the pixels in the final image are square.
        cmap="Blues",
        norm=LogNorm(vmax=vmax),
        origin="lower",
    )
    if colorbar:
        plt.colorbar(heatmap_res)
    plt.tight_layout()
    plt.savefig(save_path, format=get_export_format(save_path), bbox_inches="tight")

    return heatmap_res


def pmevo_heatmap_colorbar(colormap, save_path):
    _, ax = plt.subplots()
    plt.rcParams.update({"font.size": 20})
    plt.colorbar(colormap, ax=ax)
    ax.remove()
    plt.savefig(save_path, format=get_export_format(save_path), bbox_inches="tight")


def file_name_of_bench(bench: AnnotatedBench) -> str:
    out = bench.abbrv_source
    if ":" in out:
        return out[: out.rindex(":")]
    return out


def compute_errors_group(
    bench_results: EvaluationResult,
    rms_weighting: RmsWeightingChoice,
    reference: str,
    only_clean_rows: bool = True,
    ignored_benchs: List[BenchResult] = [],
) -> Dict[str, Dict[str, List[Tuple[float, float]]]]:
    """Cluster the `bench_results` into groups given by the `bench_to_group_selector`"""

    errors: Dict[str, Dict[str, List[Tuple[float, float]]]] = {}
    # bench_group -> {bencher -> (result, weight)}

    bench_to_group_selector: Dict[
        RmsWeightingChoice, Callable[[AnnotatedBench], Any]
    ] = {
        RmsWeightingChoice.NONE: (lambda x: None),
        RmsWeightingChoice.OCCURENCES: (lambda x: None),
        RmsWeightingChoice.OCCURENCES_BY_FILE: file_name_of_bench,
    }
    bench_to_group = bench_to_group_selector[rms_weighting]

    # Compute errors
    for bench in bench_results.benchs:
        if only_clean_rows and bench.has_error:
            continue
        if reference not in bench.results or bench.results[reference] is None:
            continue
        ref_measure: float = bench.results[reference]  # type: ignore
        # [type: ignore]: we check this two lines above, but mypy cannot see that

        bench_group = bench_to_group(bench.bench)
        if bench_group not in errors:
            errors[bench_group] = {
                bencher: [] for bencher in bench_results.selected_benchers
            }

        for bencher, measure in bench.results.items():
            if measure is None:
                continue
            error = (
                (abs(measure - ref_measure) / ref_measure)
                if bench not in ignored_benchs
                else 0
            )
            errors[bench_group][bencher].append((error, bench.bench.occur))
    return errors


def compute_rms(
    bench_results: EvaluationResult,
    rms_weighting: RmsWeightingChoice,
    reference: str,
    only_clean_rows: bool = True,
    ignored_benchs: List[BenchResult] = [],
) -> Dict[str, Tuple[int, float]]:
    """Computes the RMS error wrt. the `reference` bencher of each bencher, using the
    given weighting policy. If `only_clean_rows` is True, a bench row on which any
    bencher failed is excluded for every bencher.
    Returns a dict mapping each mapper to the number of rows on which it was measured,
    and its overall RMS error"""

    errors: Dict[str, Dict[str, List[Tuple[float, float]]]] = compute_errors_group(
        bench_results, rms_weighting, reference, only_clean_rows, ignored_benchs
    )

    # Aggregate results
    result: Dict[str, Tuple[int, float]] = {}
    for bencher in bench_results.selected_benchers:
        bencher_error: float = 0.0
        bencher_rows: int = 0
        filled_groups: int = 0
        for full_group in errors.values():
            group = full_group[bencher]
            error_sum: float = 0.0
            weight_sum: float = 0.0
            for error, weight in group:
                if weight <= 0:
                    continue
                weight_sum += weight
                error_sum += weight * (error**2)
                bencher_rows += 1
            if weight_sum > 0:
                bencher_error += error_sum / weight_sum
                filled_groups += 1
        result[bencher] = (bencher_rows, math.sqrt(bencher_error / filled_groups))
    return result


def compute_mape(
    bench_results: EvaluationResult,
    rms_weighting: RmsWeightingChoice,
    reference: str,
    only_clean_rows: bool = True,
    ignored_benchs: List[BenchResult] = [],
) -> Dict[str, Tuple[int, float]]:
    """Computes the MAPE error wrt. the `reference` bencher of each bencher, using the
    given weighting policy. If `only_clean_rows` is True, a bench row on which any
    bencher failed is excluded for every bencher.
    Returns a dict mapping each mapper to the number of rows on which it was measured,
    and its overall MAPE error"""

    errors: Dict[str, Dict[str, List[Tuple[float, float]]]] = compute_errors_group(
        bench_results, rms_weighting, reference, only_clean_rows, ignored_benchs
    )

    # Aggregate results
    result: Dict[str, Tuple[int, float]] = {}
    for bencher in bench_results.selected_benchers:
        bencher_error: float = 0.0
        bencher_rows: int = 0
        filled_groups: int = 0
        for full_group in errors.values():
            group = full_group[bencher]
            error_sum: float = 0.0
            weight_sum: float = 0.0
            for error, weight in group:
                if weight <= 0:
                    continue
                weight_sum += weight
                error_sum += weight * abs(error)
                bencher_rows += 1
            if weight_sum > 0:
                bencher_error += error_sum / weight_sum
                filled_groups += 1
        result[bencher] = (bencher_rows, bencher_error / filled_groups)
    return result


def compute_tau(
    bench_results: EvaluationResult,
    reference: str,
    only_clean_rows: bool = True,
) -> Dict[str, Tuple[int, float, float]]:
    """Computes Kendall's tau on results wrt. the `reference` bencher of each bencher.
    If `only_clean_rows` is True, a bench row on which any bencher failed is excluded
    for every bencher.
    Returns a dict mapping each mapper to the number of rows on which it was measured,
    its tau coefficient and its tau p-value."""

    def iter_benchs(bencher: str):
        for bench in bench_results.benchs:
            if only_clean_rows and any(
                [result is None for result in bench.results.values()]
            ):
                continue
            if bench.results[reference] is None or bench.results[bencher] is None:
                continue
            yield bench

    result: Dict[str, Tuple[int, float, float]] = {}

    for bencher in bench_results.selected_benchers:
        row_count = 0
        ref_values = []
        ben_values = []
        for bench in iter_benchs(bencher):
            row_count += 1
            ref_values.append(bench.results[reference])
            ben_values.append(bench.results[bencher])

        tau, p_val = kendalltau(ref_values, ben_values)
        result[bencher] = (row_count, tau, p_val)

    return result


def filter_bench(filter_func: Callable[[str], bool], benchs: List[BenchResult]):
    return list(filter(lambda bench: filter_func(bench.bench.source), benchs))


def print_tabular_data(
    widths: List[int],
    headers: List[List[str]],
    rows: Sequence[Iterable[Any]],
    align_right: Optional[List[bool]] = None,
    latex: bool = False,
):
    """Print a tabularized analysis in N columns.

    :param widths: An array of N elements, the respective widths in characters of the
        columns.
    :param header: A 2D array of N columns, each being a row of the header text of each
        column.
    :param rows: A 2D array of N columns, each being a row to be displayed.
    :param align_right: Optional. If passed, an array of N booleans. If the i-th
        element is True, the column will be right-aligned. Align every column left by
        default.
    :param latex: If true, data is presented in a LaTeX-compatible way.
    """

    if latex:
        fmt_line = " & ".join(["{}"] * len(widths)) + r"\\"
    else:
        fmt_chunks = itertools.starmap(
            lambda width, align: "{{:{}{}}}".format(">" if align else "<", width),
            zip(widths, align_right or [False] * len(widths)),
        )
        fmt_line = "│ " + " │ ".join(fmt_chunks) + " │"

    if latex:
        print(r"\toprule")

    for head in headers:
        print(fmt_line.format(*head))

    if latex:
        print(r"\midrule")
    else:
        print("├─" + "─┼─".join(["─" * width for width in widths]) + "─┤")

    for row in rows:
        print(fmt_line.format(*row))

    if latex:
        print(r"\bottomrule")


def main() -> None:
    """Entrypoint"""
    args = parse_arguments()

    setup_logging(args.loglevel)

    with open(args.bench_file, "rb") as handle:
        bench_results: EvaluationResult = pickle.load(handle)

    bench_list = bench_results.benchs
    initial_bench_count = len(bench_list)
    for cur_re in args.restrict_re:
        restrict_re = re.compile(cur_re)
        bench_list = filter_bench(
            lambda bench: restrict_re.search(bench) is not None, bench_list
        )
    for cur_restrict in args.restrict:
        bench_list = filter_bench(lambda bench: cur_restrict in bench, bench_list)
    for cur_restrict in args.restrict_neg:
        bench_list = filter_bench(lambda bench: cur_restrict not in bench, bench_list)
    bench_results = EvaluationResult(
        bench_list,
        bench_results.selected_benchers,
    )
    if args.restrict_re or args.restrict or args.restrict_neg:
        logger.info(
            "Various restrictions: restricted to %d/%d benchs [%d %%]",
            len(bench_list),
            initial_bench_count,
            len(bench_list) * 100 // initial_bench_count,
        )

    headers: List[List[str]]
    if args.print_rms:
        rms_results_clean = compute_rms(
            bench_results, args.rms_weighting, args.rms_ref, True
        )
        rms_results_all = compute_rms(
            bench_results, args.rms_weighting, args.rms_ref, False
        )
        nb_clean_rows = next(iter(rms_results_clean.values()))[0]

        widths = [20, 8, 12, 12]
        align_right = [False, True, True, True]
        headers = [
            ["Bencher", "# errors", "All rows", "Clean rows"],
            ["", "", "", str(nb_clean_rows)],
        ]
        rms_fmt = "{:.2f} %"
        rows = [
            (
                bencher,
                (len(bench_results.benchs) - rms_results_all[bencher][0]),
                rms_fmt.format(rms_results_all[bencher][1] * 100),
                rms_fmt.format(rms_results_clean[bencher][1] * 100),
            )
            for bencher in bench_results.selected_benchers
        ]

        print("=== RMS errors ===")
        print_tabular_data(widths, headers, rows, align_right, latex=args.latex_tables)

    if args.print_mape:
        mape_results_clean = compute_mape(
            bench_results, args.mape_weighting, args.mape_ref, True
        )
        mape_results_all = compute_mape(
            bench_results, args.mape_weighting, args.mape_ref, False
        )
        nb_clean_rows = next(iter(mape_results_clean.values()))[0]

        widths = [20, 8, 12, 12]
        align_right = [False, True, True, True]
        headers = [
            ["Bencher", "# errors", "All rows", "Clean rows"],
            ["", "", "", str(nb_clean_rows)],
        ]
        mape_fmt = "{:.2f} %"
        rows = [
            (
                bencher,
                (len(bench_results.benchs) - mape_results_all[bencher][0]),
                mape_fmt.format(mape_results_all[bencher][1] * 100),
                mape_fmt.format(mape_results_clean[bencher][1] * 100),
            )
            for bencher in bench_results.selected_benchers
        ]

        print("=== MAPE errors ===")
        print_tabular_data(widths, headers, rows, align_right, latex=args.latex_tables)

    if args.tau:
        tau_results_all = compute_tau(bench_results, args.tau_ref, False)
        tau_results_clean = compute_tau(bench_results, args.tau_ref, True)
        nb_clean_rows = next(iter(tau_results_clean.values()))[0]
        widths = [20, 8, 8, 8]
        align_right = [False, True, True, True]
        headers = [
            ["Bencher", "# errors", "All", "Clean"],
            ["", "", "", str(nb_clean_rows)],
        ]
        tau_fmt = "{:.2f}"
        rows = [
            (
                bencher,
                (len(bench_results.benchs) - tau_results_all[bencher][0]),
                tau_fmt.format(tau_results_all[bencher][1]),
                tau_fmt.format(tau_results_clean[bencher][1]),
            )
            for bencher in bench_results.selected_benchers
        ]

        print("=== Kendall's Tau ===")
        print_tabular_data(widths, headers, rows, align_right, latex=args.latex_tables)

    if args.heatmap:
        BENCHER_RENAME = {
            "palmed": args.toolname or "palmed",
            "llvm_mca": "llvm-mca",
        }
        for bencher in bench_results.selected_benchers:
            if bencher == "native":
                continue
            bencher_name = BENCHER_RENAME.get(bencher.lower(), bencher)
            colormap = pmevo_heatmap(
                bench_results,
                "{prefix}-{bencher}.png".format(prefix=args.heatmap, bencher=bencher),
                bencher,
                args.heatmap_maxw,
                maxh=args.heatmap_maxh or None,
                weighted=args.heatmap_weighted,
                colorbar=False,
                legends=args.heatmap_legend,
                predictor_name=bencher_name,
                horizontal_reference=args.heatmap_ratio,
                in_cycles=args.heatmap_in_cycles,
            )
            if bencher == "PMEvo":
                pmevo_heatmap_colorbar(
                    colormap,
                    "{prefix}-legend.png".format(prefix=args.heatmap),
                )

    if args.nb_worst_benchs > 0:
        assert args.worst_benchs_bencher in bench_results.selected_benchers
        assert args.rms_ref in bench_results.selected_benchers
        tot_occur = sum(
            map(lambda bench_res: bench_res.bench.occur, bench_results.benchs)
        )

        # Influence of the bench on the total RMS score
        rms_results_all_value = compute_rms(
            bench_results, args.rms_weighting, args.rms_ref, False
        )[args.worst_benchs_bencher][1]

        def get_worst_benchs_gap(res: Optional[BenchResult]) -> float:
            if res is None:
                return 0.0
            assert args.worst_benchs_bencher is not None
            assert args.rms_ref is not None
            ref = res.results[args.rms_ref]
            wor = res.results[args.worst_benchs_bencher]
            assert ref is not None
            assert wor is not None
            ret = (ref - wor) / ref
            return abs(ret)

        def get_worst_benchs_score(
            res: Optional[BenchResult],
        ) -> float:
            if res is None:
                return 0.0
            rms_results_worst = compute_rms(
                bench_results, args.rms_weighting, args.rms_ref, False, [res]
            )[args.worst_benchs_bencher][1]
            return rms_results_all_value - rms_results_worst

        errors: Dict[str, Dict[str, List[Tuple[float, float]]]] = compute_errors_group(
            bench_results,
            args.rms_weighting,
            args.rms_ref,
            False,
        )
        bench_to_group_selector: Dict[
            RmsWeightingChoice, Callable[[AnnotatedBench], Any]
        ] = {
            RmsWeightingChoice.NONE: (lambda x: None),
            RmsWeightingChoice.OCCURENCES: (lambda x: None),
            RmsWeightingChoice.OCCURENCES_BY_FILE: file_name_of_bench,
        }
        bench_to_group = bench_to_group_selector[args.rms_weighting]

        group_to_weight: Dict[str, float] = {}
        for group, bencher_to_error_list in errors.items():
            group_to_weight[group] = 0
            error_list = bencher_to_error_list[args.rms_ref]
            for _, weight in error_list:
                if weight < 0:
                    continue
                else:
                    group_to_weight[group] += weight

        contribution = lambda bench_res: (
            (
                (
                    bench_res.bench.occur
                    * (
                        (
                            (
                                bench_res.results[args.rms_ref]
                                - bench_res.results[args.worst_benchs_bencher]
                            )
                            / bench_res.results[args.rms_ref]
                        )
                        ** 2
                    )
                )
                / group_to_weight[bench_to_group(bench_res.bench)]
            )
            if bench_res.results[args.worst_benchs_bencher] is not None
            else 0.0
        )

        bench_results_sorted = sorted(
            bench_results.benchs,
            key=contribution,
            reverse=True,
        )

        to_write = yaml.comments.CommentedSeq()
        for idx, bench_res in enumerate(
            bench_results_sorted[: min(args.nb_worst_benchs, len(bench_results_sorted))]
        ):
            if bench_res is not None:
                to_write.append(bench_res.bench.benchmark.mingled_instructions)
                to_write.yaml_set_comment_before_after_key(
                    key=idx,
                    before=f"----- Worst Benchmark #{idx+1}, "
                    f"predicted {bench_res.results[args.worst_benchs_bencher]} / "
                    f"measured {bench_res.results[args.rms_ref]} "
                    "({:.01f} %, weight {:.01f} per myriad, "
                    "RMS weight "
                    "{:.01f} permille) -----".format(
                        get_worst_benchs_gap(bench_res) * 100,
                        bench_res.bench.occur / tot_occur * 10000,
                        get_worst_benchs_score(bench_res)
                        * 1000
                        / rms_results_all_value,
                    ),
                )

        with open(args.worst_benchs_file, "w") as f:
            yaml.dump(to_write, stream=f, Dumper=yaml.RoundTripDumper)
