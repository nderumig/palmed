""" Generates and runs benchmarks of translation blocks """

## \file entry.py
## \brief Generates and runs benchmarks of translation blocks

# PYTHON_ARGCOMPLETE_OK

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argcomplete, argparse
import matplotlib.pyplot as plt
import multiprocessing as mp
from math import sqrt
from functools import wraps
from pathlib import Path
from io import BytesIO
from os import path, mkdir
import logging
import sys
import signal
import random
import traceback
from typing import Dict, Optional, List, Tuple, Type, NamedTuple, Mapping, Any, cast
import yaml
import pickle
from palmed import load_kernel
from palmed.benchmarks import Benchmark
from palmed.config import Config
from palmed.pinned_pool import select_multiprocessing_pool
from palmed.messages import setup_logging
from palmed.pipedream_config import configure_pipedream, RemotePipedream
import palmed.instructions
from .generic import TranslationBlock, prune_unbenchable_instructions
from .methods.generic import MeasureFailed, IpcMeasure
from .methods.native import NativeIpcMeasure
from . import methods


class AnnotatedBench(NamedTuple):
    source_type: str
    source: str
    abbrv_source: str
    occur: int
    benchmark: Benchmark


class BenchResult(NamedTuple):
    bench: AnnotatedBench
    results: Dict[str, Optional[float]]
    raw_native_measures: Optional[List[float]]
    has_result: bool
    has_error: bool


class EvaluationResult(NamedTuple):
    benchs: List[BenchResult]
    selected_benchers: List[str]


BENCHERS: Dict[str, Type[IpcMeasure]] = {
    "gus": methods.gus.GusIpcMeasure,
    "IACA": methods.iaca.IacaIpcMeasure,
    "native": methods.native.NativeIpcMeasure,
    "UOPS": methods.uops.UopsIpcMeasure,
    "UOPS_4": methods.uops_capped.CappedUopsIpcMeasure,
    "LLVM_MCA": methods.llvm_mca.LlvmMcaIpcMeasure,
    "PMEvo": methods.pmevo.PmevoIpcMeasure,
    "palmed": methods.palmed.PalmedIpcMeasure,
    "palmed_front_DSlack": methods.palmed_front.PalmedFrontIpcMeasure_DispSlack,
    "palmed_front_DTense": methods.palmed_front.PalmedFrontIpcMeasure_DispTense,
    "palmed_front_ULin": methods.palmed_front.PalmedFrontIpcMeasure_UopLinear,
    "palmed_front_UNoX": methods.palmed_front.PalmedFrontIpcMeasure_UopNoCross,
}

DISPLAY_SOURCE_WIDTH = 40

logger = logging.getLogger(__name__)

pipedream_supported_instr: List[palmed.instructions.Instruction_Pipedream] = []


def setup_config(cl_args):
    """Setup palmed's Config"""
    Config.MU_ARCH = cl_args.arch
    Config.ISA = cl_args.isa
    Config.REMOTE_BENCH = cl_args.remote_bench
    Config.SELECTED_MACHINE = cl_args.machine or Config.SELECTED_MACHINE
    Config.NUM_CPU = cl_args.num_cpu
    if Config.REMOTE_BENCH:
        assert "machine" in cl_args and cl_args.machine
        Config.REMOTE_PIPEDREAM_HOST = cl_args.machine
        Config.REMOTE_PIPEDREAM_USER = (
            cl_args.remote_user or Config.REMOTE_PIPEDREAM_USER
        )
        Config.REMOTE_PIPEDREAM_PORT = (
            cl_args.remote_port or Config.REMOTE_PIPEDREAM_PORT
        )
        Config.REMOTE_PIPEDREAM_WRAPPER_PATH = (
            cl_args.pipedream_benchmark_wrapper or Config.REMOTE_PIPEDREAM_WRAPPER_PATH
        )
    if "perflib" in cl_args and cl_args.perflib:
        Config.PERFLIB = cl_args.perflib
    Config.init_pipedream()


def foreach_path(func):
    @wraps(func)
    def wrapper(paths, *args, **kwargs):
        out = []
        for path in paths:
            res = func(path, *args, **kwargs)
            if res is None:
                continue
            if isinstance(res, list):
                out += res
            else:
                out.append(res)
        return out

    return wrapper


def parse_tb_log(stream):
    """Parse a translation block log"""
    blocks = []
    in_block = False
    cur_block = None
    for line in stream:
        if line.strip() == "End of TB":
            if cur_block:
                blocks.append(cur_block)
                cur_block = None
            in_block = False

        else:
            if not in_block:
                if line[:2] == "TB":
                    cur_block = TranslationBlock(line)
                in_block = True
            else:
                if cur_block:
                    cur_block.parse_line(line)
    if cur_block:
        if isinstance(cur_block, list):
            blocks += cur_block
        else:
            blocks.append(cur_block)
    return blocks


@foreach_path
def bench_of_tbs(path, threshold):
    """Parses translation blocks into benchmarks"""
    out = []
    with open(path, "r") as handle:
        blocks = parse_tb_log(handle)
        logger.debug("Found %d TBs in %s", len(blocks), path)
        for block in blocks:
            if block.nb_exec < threshold:
                continue

            abbrv_path = Path(path).name
            abbrv_path_maxlen = DISPLAY_SOURCE_WIDTH - 7
            if len(abbrv_path) > abbrv_path_maxlen:
                abbrv_path = "{beg}…{end}".format(
                    beg=abbrv_path[: abbrv_path_maxlen // 3 * 2],
                    end=abbrv_path[-abbrv_path_maxlen // 3 :],
                )

            abbrv_source = "{path}:{addr:x}".format(path=abbrv_path, addr=block.addr)
            try:
                instrs = load_kernel.bytes_to_pipedream(b"".join(block.instr_raw), path)
            except load_kernel.LoadKernelException as exn:
                logger.warning(
                    "Ignoring tb:%s: failed to load: %s", abbrv_source, str(exn)
                )
                continue

            pruned = prune_unbenchable_instructions(instrs, pipedream_supported_instr)
            if not pruned:
                logger.warning(
                    "Ignoring tb:%s: no benchmarkable instruction", abbrv_source
                )
                continue
            out.append(
                AnnotatedBench(
                    "tb",
                    "{path}:{addr:x}".format(path=path, addr=block.addr),
                    abbrv_source,
                    block.nb_exec,
                    Benchmark(*pruned),
                )
            )
    logger.info("Kept %d TBs in %s", len(out), path)
    return out


def bench_of_tb_files(metapaths, threshold, sample_size=None):
    """Parses a file containing paths of TB files"""
    tb_paths = []
    for metapath in metapaths:
        with open(metapath, "r") as handle:
            for line in handle:
                tb_paths.append(path.join(path.dirname(metapath), line.strip()))
    if sample_size and len(tb_paths) > 1.5 * sample_size:
        tb_paths = random.sample(tb_paths, sample_size)
    return bench_of_tbs(tb_paths, threshold)


@foreach_path
def bench_of_kernels(path):
    """Parses kernels in yml format into benchmarks"""
    instrs = load_kernel.load_pipedream_kernel_description(path)
    pruned = prune_unbenchable_instructions(instrs, pipedream_supported_instr)
    if not pruned:
        logger.warning("Ignoring yml:%s: no benchmarkable instruction", Path(path).name)
        return None
    return AnnotatedBench("yml", path, Path(path).name, 1, Benchmark(*pruned))


@foreach_path
def bench_of_kernels_hex(path, label_of_path=None):
    """Parses kernels in hex asm instructions into benchmarks"""

    label = None
    if label_of_path is not None:
        label = label_of_path.get(path, None)
    if label is None:
        label = Path(path).name

    try:
        instrs = load_kernel.load_hex_kernel_description(path)
    except load_kernel.LoadKernelException as exn:
        logger.error("Cannot load hex:%s (label=%s): %s", path, label, exn)
        return None
    pruned = prune_unbenchable_instructions(instrs, pipedream_supported_instr)
    if not pruned:
        logger.warning("Ignoring hex:%s: no benchmarkable instruction", Path(path).name)
        return None
    return AnnotatedBench("hex", path, label, 1, Benchmark(*pruned))


def bench_of_kernels_hex_files(metapaths, sample_size=None):
    """Parses a file containing paths of kernel hex files"""
    khex_paths = []
    labels = {}
    for metapath in metapaths:
        with open(metapath, "r") as handle:
            for line in handle:
                cur_path = line
                label = None
                if "#" in line:
                    cur_path, label = line.split("#", maxsplit=1)
                    label = label.strip()
                full_path = path.join(path.dirname(metapath), cur_path.strip())
                if label:
                    labels[full_path] = label
                khex_paths.append(full_path)
    if sample_size and len(khex_paths) > 1.5 * sample_size:
        khex_paths = random.sample(khex_paths, sample_size)
    return bench_of_kernels_hex(khex_paths, label_of_path=labels)


@foreach_path
def bench_of_kernels_raw(path):
    """Parses kernels as raw binary instructions into benchmarks"""
    instrs = load_kernel.load_raw_kernel_description(path)
    pruned = prune_unbenchable_instructions(instrs, pipedream_supported_instr)
    if not pruned:
        logger.warning("Ignoring raw:%s: no benchmarkable instruction", Path(path).name)
        return None
    return AnnotatedBench("raw", path, Path(path).name, 1, Benchmark(*pruned))


def parse_arguments():
    """Parse command-line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--tb",
        action="append",
        help="Input files to parse looking for translation blocks",
        default=[],
    )
    parser.add_argument(
        "--tb-file",
        action="append",
        help=(
            "File listing paths of various TB files, one each line, that will be "
            "processed as if passed to `--tb`."
        ),
        default=[],
    )
    parser.add_argument(
        "--tb-threshold",
        help="minimum occurrences of a TB required to keep it",
        default=20,
        type=int,
    )
    parser.add_argument(
        "--kernel",
        action="append",
        help="Kernels to benchmark, read as a yaml file of pipedream instructions",
        default=[],
    )
    parser.add_argument(
        "--kernel-hex",
        action="append",
        help=(
            "Kernels to benchmark, read as a hexadecimal file of instructions, "
            "objdump-like"
        ),
        default=[],
    )
    parser.add_argument(
        "--kernel-hex-file",
        action="append",
        default=[],
        help=(
            "File listing paths of various kernels to benchmark, read as a "
            "hexadecimal file of instructions, objdump-like"
        ),
    )
    parser.add_argument(
        "--kernel-raw",
        action="append",
        help="Kernels to benchmark, read as a binary file of instructions",
        default=[],
    )
    parser.add_argument(
        "--sample",
        action="store_const",
        const=30,
        dest="sample_size",
        help="Take a random, small sample of the inputs.",
    )
    parser.add_argument(
        "--sample-size",
        type=int,
        dest="sample_size",
        help="Take a random sample of the given size of the inputs",
    )

    parser.add_argument(
        "--csv",
        dest="csv_savefile",
        help="File in which the result will be saved as csv, or '-' for stdout",
    )
    parser.add_argument(
        "--pickle",
        dest="pickle_savefile",
        help="File in which the result will be saved as python pickled data",
    )
    parser.add_argument(
        "--graph",
        dest="graph_savedir",
        help="Directory in which the IPC of every native run will be separatly "
        "plotted as a graph",
    )
    parser.add_argument(
        "--asm-dir",
        dest="asm_savedir",
        help="Directory in which assembly files corresponding to "
        "the microkernels are stored for native measurements",
    )
    parser.add_argument(
        "--perflib",
        help=(
            "Performance library to use, as used by pipedream's pyperfs. "
            "Can be 'auto'."
        ),
    )
    parser.add_argument(
        "-p",
        "--print-results",
        action="store_true",
        help=(
            "Pretty-prints the results as they are computed. Not suited for automatic "
            "processing, but nicer than CSV for a human."
        ),
    )
    parser.add_argument(
        "--instr-stats",
        action="store_true",
        help="Prints statistics on the instructions provided in all the input blocks",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
        help="Verbose output",
        default=logging.WARNING,
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="Debug output",
    )
    parser.add_argument(
        "--inspect-crashes",
        action="store_true",
        help="Print debug data for unhandled exceptions occurring during measures",
    )
    parser.add_argument(
        "--uops-mapping",
        help="File in which the uops mapping is stored",
        default="uops_mapping.py",
    )
    parser.add_argument(
        "--palmed-mapping",
        help="File in which the palmed mapping is stored",
    )
    parser.add_argument(
        "--llvm-mca-cpu",
        help="Argument to use for llvm-mca's --mcpu if benching remotely",
    )
    parser.add_argument(
        "--dump-uops-unsupported",
        help=(
            "Dump the list of the encountered pipedream instructions unsupported by "
            "UOPS in this file."
        ),
    )
    parser.add_argument(
        "--rms-ref",
        help="Use this bencher as a reference for the RMS error",
        default="native",
    )

    parser.add_argument("--arch", help="Name of the target architecture", required=True)
    parser.add_argument(
        "--isa",
        help="Name of the target ISA",
        type=str,
        default=Config.SUPPORTED_ISA.X86.value,
    )

    parser.add_argument(
        "-j",
        "--num-cpu",
        help="Parallelize on N cores",
        type=int,
        default=None,
    )

    parser.add_argument(
        "--IACA-log-dir",
        help="Directory used to store the complete IACA reports of the basic blocks",
    )

    # Remote bencher
    parser.add_argument(
        "--remote-bench", action="store_true", help="Benchmark on a remote machine."
    )
    parser.add_argument(
        "-m",
        "--machine",
        default=None,
        help="Benchmark on this machine if --remote-bench is given",
    )
    parser.add_argument(
        "--remote-user", default=None, help="Use this user for remote benchmarking."
    )
    parser.add_argument(
        "--remote-port", default=None, help="Use this port for remote benchmarking."
    )
    parser.add_argument(
        "--pipedream-benchmark-wrapper",
        default=None,
        help=(
            "Local path to the wrapper binary used to run benchmarks on the remote "
            "machine."
        ),
    )

    for bencher in BENCHERS:
        parser.add_argument(
            "--B" + bencher,
            action="store_true",
            dest="bench_" + bencher,
            help="Runs benchmarks with " + bencher,
        )
        parser.add_argument(
            "--Bno-" + bencher,
            action="store_false",
            dest="bench_" + bencher,
            help="Do not run benchmarks with " + bencher,
        )
    argcomplete.autocomplete(parser)
    return parser.parse_args()


def bench_one(
    benchmark: AnnotatedBench,
    selected_benchers: List[str],
    keep_raw_values: bool,
    asm_savedir: Optional[str],
    iaca_report_savedir: Optional[str],
    inspect_crashes: bool = False,
) -> BenchResult:
    """Compute a single benchmark, return its result. Multiprocess-safe."""

    logging.debug("Benching [%s] `%s`...", benchmark.source_type, benchmark.source)
    cur_bench: Dict[str, Optional[float]] = {}
    has_result = False
    has_error = False
    raw_measures = None
    for bencher in selected_benchers:
        logging.debug("\tWith bencher %s...", bencher)
        try:
            ipc_measure = BENCHERS[bencher](
                benchmark.benchmark, inspect_crashes, asm_savedir, iaca_report_savedir
            )
            res = ipc_measure.get_ipc()
            if res >= 0:
                has_result = True
            else:
                has_error = True
            cur_bench[bencher] = res
            if isinstance(ipc_measure, NativeIpcMeasure):
                raw_measures = ipc_measure.raw_values
        except MeasureFailed as exn:
            logger.info("Bencher %s: measure failed (%s).", bencher, exn)
            logger.debug("%s", "".join(traceback.format_exc()))
            cur_bench[bencher] = None
            has_error = True

    logging.debug("\tDone.")

    return BenchResult(benchmark, cur_bench, raw_measures, has_result, has_error)


def _bench_one_wrapper(args):
    return bench_one(*args)


def pool_init_all_benchers(arg_dict: Dict[str, Tuple], cl_args) -> None:
    setup_logging(cl_args.loglevel)
    ins, unsupported = palmed.instructions.load_pipedream()
    all_ins: List[palmed.instructions.Instruction_Pipedream] = []
    for iset in ins.values():
        all_ins += iset
    all_ins += unsupported

    if cl_args.llvm_mca_cpu:
        methods.llvm_mca.LlvmMcaIpcMeasure.MCPU = cl_args.llvm_mca_cpu

    if cl_args.bench_UOPS:
        palmed.instructions.load_uops(arch=cl_args.arch, prune_nores=False)
    if cl_args.bench_PMEvo:
        cast(methods.pmevo.PmevoIpcMeasure, BENCHERS["PMEvo"]).init_mapping(all_ins)
    for bencher, args in arg_dict.items():
        # Looks like mypy cannot see that `BENCHERS[bencher]` has some class methods
        BENCHERS[bencher].pool_initfunc(*args)  # type: ignore


def run_benchmarks(args, out_handle, benchmarks, selected_benchers) -> None:
    BENCH_HEADER_COLOR = "\33[1;36m"

    formatting_line = (
        "{src_type:<4} {src:<"
        + str(DISPLAY_SOURCE_WIDTH)
        + "} {bench_weight:>12} {fmt_start}| "
        + " | ".join(map(lambda bencher: "{" + bencher + ":>18}", selected_benchers))
        + "|\33[0m"
    )
    if args.print_results:
        headline_args = {x: x for x in selected_benchers}
        headline_args.update(
            {
                "src_type": "Type",
                "src": "Source",
                "bench_weight": "",
                "fmt_start": BENCH_HEADER_COLOR,
            }
        )
        print(formatting_line.format(**headline_args))

    if out_handle:
        out_handle.write("src_type,src," + ",".join(selected_benchers) + "\n")

    rms_ref: Optional[str] = None
    if args.rms_ref in selected_benchers:
        rms_ref = args.rms_ref

    bench_results: List[BenchResult] = []
    error_sq: Dict[str, List[Tuple[float, int]]] = {
        bencher: [] for bencher in selected_benchers
    }
    error_sq_noerror: Dict[str, List[Tuple[float, int]]] = {
        bencher: [] for bencher in selected_benchers
    }
    benched_count_noerror: int = 0
    benched_count: Dict[str, int] = {bencher: 0 for bencher in selected_benchers}
    initargs: Tuple = (
        {
            bencher: BENCHERS[bencher].pool_initargs()  # type: ignore
            for bencher in selected_benchers
        },
        args,
    )

    with select_multiprocessing_pool()(
        args.num_cpu,
        initargs=initargs,
        initializer=pool_init_all_benchers,
    ) as pool:
        for bench_res in pool.imap(
            _bench_one_wrapper,
            map(
                lambda bench: (
                    bench,
                    selected_benchers,
                    args.graph_savedir is not None,
                    args.asm_savedir,
                    args.IACA_log_dir,
                    args.inspect_crashes,
                ),
                benchmarks,
            ),
        ):
            bench_results.append(bench_res)
            bencher_error: Dict[str, float] = {}
            # Update RMS errors
            for bencher, res in bench_res.results.items():
                if res is not None and res >= 0:
                    if (
                        rms_ref
                        and bench_res.results[rms_ref] is not None
                        and bench_res.results[rms_ref] >= 0
                    ):
                        error = (res - bench_res.results[rms_ref]) / bench_res.results[
                            rms_ref
                        ]
                        bencher_error[bencher] = error
                        sq_error = error**2
                        error_sq[bencher].append((sq_error, bench_res.bench.occur))
                        if not bench_res.has_error:
                            error_sq_noerror[bencher].append(
                                (sq_error, bench_res.bench.occur)
                            )
                    benched_count[bencher] += 1
            if not bench_res.has_error:
                benched_count_noerror += 1

            if not bench_res.has_result:
                logger.warning(
                    "Bench %s:%s has no valid result, ignoring.",
                    bench_res.bench.source_type,
                    bench_res.bench.abbrv_source,
                )
                continue

            if args.print_results:
                row_args = {
                    "src_type": bench_res.bench.source_type,
                    "src": bench_res.bench.abbrv_source,
                    "bench_weight": "[W:{: 6}]".format(bench_res.bench.occur),
                    "fmt_start": "",
                }
                for bench, val in bench_res.results.items():
                    if val is None:
                        val = "ERR"
                    else:
                        val = "{:.3f}".format(val)

                    if bench in bencher_error:
                        val += " [{: 6.1f} %]".format(bencher_error[bench] * 100)
                    row_args[bench] = val
                print(formatting_line.format(**row_args))
            if out_handle:
                out_row = (
                    bench_res.bench.source_type
                    + ","
                    + bench_res.bench.abbrv_source
                    + ","
                )
                ordered_benchs = [
                    bench_res.results[bencher] if bench_res.results[bencher] else -1
                    for bencher in selected_benchers
                ]
                out_row += ",".join(map(str, ordered_benchs)) + "\n"
                out_handle.write(out_row)

    if args.graph_savedir:
        for bench_res in bench_results:
            if bench_res.raw_native_measures:
                plt.plot(bench_res.raw_native_measures)
                plt.title(bench_res.bench.benchmark.db_str())
                plt.xlabel("Run index")
                plt.ylabel("IPC")
                plt.savefig(
                    args.graph_savedir
                    + "/"
                    + bench_res.bench.benchmark.db_str()
                    + ".svg",
                )
                plt.clf()

    if args.print_results:
        print(formatting_line.format(**headline_args))

        row_args_count: Dict[str, Any] = {
            "src_type": "TOT",
            "src": "benchs passed (clean:",
            "bench_weight": "{})".format(benched_count_noerror),
            "fmt_start": BENCH_HEADER_COLOR,
        }  # Number of benchs that worked
        row_args_count.update(benched_count)
        print(formatting_line.format(**row_args_count))
        if rms_ref:

            def get_rms_error(bencher, results):
                # elt == (squared_error, bench_weight)
                nb_elts = sum(map(lambda elt: elt[1], results[bencher]))
                if nb_elts == 0:
                    return 0
                return (
                    sqrt(
                        sum(map(lambda elt: elt[0] * elt[1], results[bencher]))
                        / nb_elts
                    )
                    * 100
                )

            row_args_rms = {
                "src_type": "TOT",
                "src": "RMS error",
                "bench_weight": "",
                "fmt_start": BENCH_HEADER_COLOR,
            }  # RMS error
            row_args_rms_noerror = {
                "src_type": "TOT",
                "src": "RMS error/clean rows",
                "bench_weight": "",
                "fmt_start": BENCH_HEADER_COLOR,
            }  # RMS error -- only rows without error
            for bencher in selected_benchers:
                row_args_rms[bencher] = "{: 5.01f} %".format(
                    get_rms_error(bencher, error_sq)
                )
                row_args_rms_noerror[bencher] = "{: 5.01f} %".format(
                    get_rms_error(bencher, error_sq_noerror)
                )
            print(formatting_line.format(**row_args_rms))
            print(formatting_line.format(**row_args_rms_noerror))

    if args.pickle_savefile:
        with open(args.pickle_savefile, "wb") as pickle_handle:
            pickle.dump(
                EvaluationResult(bench_results, selected_benchers), pickle_handle
            )


def do_instr_stats(benchmarks, order_by_benchmult=False) -> None:
    """Compute and display stats on the instructions provided"""
    instr_reps: Dict[palmed.instructions.Instruction, int | float] = {}
    # ^^^ occurrences of a specific instruction across all the benchmarks
    instr_reps_benchmult: Dict[palmed.instructions.Instruction, int | float] = {}
    # ^^^ occurrences of a specific instruction across all the benchmarks, taking the
    # multiplicity of the benchmark into account

    def cumulative_update(to_update, key, increment=1):
        if key in to_update:
            to_update[key] += increment
        else:
            to_update[key] = increment

    for bench in benchmarks:
        cur_instr_mult = bench.benchmark.instrs_with_reps()
        for instr, reps in cur_instr_mult.items():
            cumulative_update(instr_reps, instr, reps)
            cumulative_update(instr_reps_benchmult, instr, reps * bench.occur)

    tot_occur: float = sum(instr_reps.values())
    tot_occur_benchmult: float = sum(instr_reps_benchmult.values())

    rows = [
        (instr, instr_reps[instr], instr_reps_benchmult[instr])
        for instr in instr_reps.keys()
    ]

    rows.sort(
        key=(lambda x: x[2]) if order_by_benchmult else (lambda x: x[1]), reverse=True
    )

    print("=== Benched Instructions Statistics ===")
    fields_width = {
        "instr": 30,
        "occur": 10,
        "occur_freq": 6,
        "occur_benchmult": 10,
        "occur_benchmult_freq": 6,
    }
    row_format = (
        "{{instr:<{instr}}} │ "
        + "{{occur:>{occur}}} ({{occur_freq:>{occur_freq}.1f}}%) │ "
        + "{{occur_benchmult:>{occur_benchmult}}} "
        + "({{occur_benchmult_freq:>{occur_benchmult_freq}.1f}}%)"
    ).format(**fields_width)
    head_row_format = (
        "{{instr:<{instr}}} │ "
        + "{{occur:<{occur}}}  {{occur_freq:<{occur_freq}}}  │ "
        + "{{occur_benchmult:<{occur_benchmult}}} "
        + " {{occur_benchmult_freq:<{occur_benchmult_freq}}} "
    ).format(**fields_width)
    headline = head_row_format.format(
        instr="Instruction",
        occur="Occurrences",
        occur_freq="(%)",
        occur_benchmult="With bench mult",
        occur_benchmult_freq="(%)",
    )
    print(headline)
    print("—" * len(headline))
    for instr, occur, occur_benchmult in rows:
        print(
            row_format.format(
                instr=instr.name(),
                occur=occur,
                occur_freq=(occur / tot_occur) * 100,
                occur_benchmult=occur_benchmult,
                occur_benchmult_freq=(occur_benchmult / tot_occur_benchmult) * 100,
            )
        )
    print("—" * len(headline))
    print(
        row_format.format(
            instr="TOTAL",
            occur=tot_occur,
            occur_freq=100,
            occur_benchmult=tot_occur_benchmult,
            occur_benchmult_freq=100,
        )
    )


def main() -> None:
    """Entrypoint"""
    global pipedream_supported_instr

    mp.set_start_method("forkserver")
    args = parse_arguments()

    setup_logging(args.loglevel)
    # The only checkpoint used here is `INS_IPC`, which must correspond with the
    # local machine as every Pipedream bench is executed locally
    # setting every variable so that loading of `INS_IPC` happens flawlessly
    setup_config(args)

    if args.llvm_mca_cpu:
        methods.llvm_mca.LlvmMcaIpcMeasure.MCPU = args.llvm_mca_cpu

    if args.bench_palmed:
        if not args.palmed_mapping:
            logging.error(
                "Palmed selected as a bencher, but no mapping file provided. Please "
                "provide one with --palmed-mapping."
            )
            sys.exit(1)
        logging.info("Reading palmed mapping %s", args.palmed_mapping)
        palmed_bencher = BENCHERS["palmed"]
        assert isinstance(palmed_bencher, methods.palmed.PalmedIpcMeasure)
        with open(args.palmed_mapping, "rb") as handle:
            palmed_bencher.read_mapping(handle)

    should_bench = (
        args.csv_savefile
        or args.pickle_savefile
        or args.print_results
        or args.graph_savedir
        or args.asm_savedir
    )
    should_read_blocks = should_bench or args.instr_stats

    if not should_read_blocks:
        logger.error(
            "Nothing to be done: neither --csv, --pickle, --print-results, --graph, "
            "--asm-dir,  nor --instr-stats specified. Aborting."
        )
        sys.exit(1)

    use_native_bench = args.graph_savedir or args.asm_savedir
    if use_native_bench and not args.bench_native:
        logger.error(
            "Native execution needed by --graph or --asm-dir, but native bencher "
            "(--Bnative) not specified. Aborting."
        )
        sys.exit(1)

    if args.IACA_log_dir and not args.bench_IACA:
        logger.error("IACA logs (--IACA-log-dir) requires IACA bencher (--BIACA)")
        sys.exit(1)

    dir_to_check: List[str] = []
    if args.graph_savedir:
        dir_to_check.append(args.graph_savedir)
    if args.asm_savedir:
        dir_to_check.append(args.asm_savedir)
    for dir_str in dir_to_check:
        direc = Path(dir_str)
        if not direc.exists():
            logger.info("Directory %s does not exists, creating it", direc)
            mkdir(direc.as_posix())
        elif not direc.is_dir():
            logger.error(
                "file %s exists and is not a directory, but must be one. Aborting.",
                direc.as_posix(),
            )
            sys.exit(1)

    configure_pipedream()
    RemotePipedream.init_remote_pipedream_bencher(core_id=1, noremote_ok=True)

    pipedream_supported_instr_isa, _ = palmed.instructions.load_pipedream()
    for instrs in pipedream_supported_instr_isa.values():
        pipedream_supported_instr += instrs
    if args.bench_UOPS:
        palmed.instructions.load_uops(arch=args.arch, prune_nores=False)

    benchmarks = []
    benchmarks += bench_of_tbs(args.tb, args.tb_threshold)
    benchmarks += bench_of_tb_files(args.tb_file, args.tb_threshold, args.sample_size)
    benchmarks += bench_of_kernels(args.kernel)
    benchmarks += bench_of_kernels_hex(args.kernel_hex)
    benchmarks += bench_of_kernels_hex_files(args.kernel_hex_file)
    benchmarks += bench_of_kernels_raw(args.kernel_raw)

    if not benchmarks:
        logger.error(
            "No extracted benchmarks; this would not compute anything. Aborting."
        )
        sys.exit(1)
    logging.info("Extracted %d benchmarks.", len(benchmarks))

    if args.sample_size is not None:
        logging.info("Restricting to %d samples", args.sample_size)
        benchmarks.sort(key=lambda x: x.occur, reverse=True)
        trim_limit = min(
            max(3 * args.sample_size, len(benchmarks) // 5), len(benchmarks)
        )
        benchmarks = random.sample(benchmarks[:trim_limit], args.sample_size)

    if args.instr_stats:
        do_instr_stats(benchmarks, True)

    if not should_bench:
        logger.warning(
            "Neither --csv, --pickle, --graph, --asm-dir nor --print-results specified; this would not "
            "output the results in any way. Aborting."
        )
        sys.exit(0)

    selected_benchers = []
    for bencher in BENCHERS:
        if vars(args)["bench_" + bencher]:
            selected_benchers.append(bencher)
    if not selected_benchers:
        logger.error("No selected benchers; this would not compute anything. Aborting.")
        sys.exit(1)

    logging.info("Selected benchers: %s", ", ".join(selected_benchers))

    if "LLVM_MCA" in selected_benchers:
        methods.llvm_mca.LlvmMcaIpcMeasure.check_configuration()

    if not args.csv_savefile:
        run_benchmarks(
            args,
            None,
            benchmarks,
            selected_benchers,
        )
    elif args.csv_savefile == "-":
        run_benchmarks(
            args,
            sys.stdout,
            benchmarks,
            selected_benchers,
        )
    else:
        with open(args.csv_savefile, "w") as handle:
            run_benchmarks(
                args,
                handle,
                benchmarks,
                selected_benchers,
            )

    if args.bench_UOPS:
        bencher_UOPS = BENCHERS["UOPS"]
        assert isinstance(bencher_UOPS, methods.uops.UopsIpcMeasure)
        unsupported = list(bencher_UOPS.unsupported_instr())
        if unsupported:
            dump_loc = (
                "These were not dumped because you didn't provide a "
                "--dump-uops-unsupported argument"
            )
            if args.dump_uops_unsupported:
                dump_loc = "These were dumped in {}".format(args.dump_uops_unsupported)
                with open(args.dump_uops_unsupported, "w") as handle:
                    yaml.dump(unsupported, handle)
            logger.warning(
                "UNSUPPORTED UOPS INSTR: {} unsupported instructions.  {}.".format(
                    len(unsupported), dump_loc
                )
            )
