## \file generic.py
## \brief Datastructures and functions that apply to all types of comparative benches

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import sys
import logging
from palmed.instructions import Instruction_Pipedream, InstructionNotFound

logger = logging.getLogger(__name__)


class TranslationBlock:
    """A translation block as parsed from input text files"""

    def __init__(self, first_line):
        # Expected format: "TB 0x1066 was executed 1 time(s)"
        line_split = first_line.strip().split()
        if len(line_split) != 6 or line_split[0] != "TB":
            raise Exception("Bad TB first line")

        self.addr = int(line_split[1], 0x10)
        self.nb_exec = int(line_split[4])
        self.instr_raw = []
        self.instr_asm = []

    def parse_line(self, line):
        """Parse a line for this TB from the text file"""
        # Expected format: "48 83 C0 08                   [[add rax, 8]]"
        hex_part = line.strip().split("[[")[0]
        line_split = hex_part.split()
        instr_bytes = b""
        asm_start = None
        for pos, elt in enumerate(line_split):
            if len(elt) != 2:
                asm_start = pos
                break
            try:
                instr_bytes += bytes.fromhex(elt)
            except ValueError:
                asm_start = pos
                break
        self.instr_raw.append(instr_bytes)
        self.instr_asm.append(" ".join(line_split[asm_start:]))

    @property
    def instr_raw_flat(self):
        """Raw binary instructions in this TB"""
        out = b""
        for elt in self.instr_raw:
            out += elt
        return out


def format_command(cmd):
    """Prints a shell command from a command list, as passed to subprocess.run"""
    return " ".join(map(lambda x: x.replace(" ", "\\ "), cmd))


def prune_unbenchable_instructions(instrs, valid_instr):
    """Returns the sub-list of `instrs` that Pipedream reports as benchable"""

    def can_benchmark(instr):
        try:
            if "DIV" in instr.name().split()[0]:
                return False
            if instr not in valid_instr:
                return False
            # May raise an `InstructionNotFound` exception if the instruction
            # is not benchmarkable at all
            Instruction_Pipedream(instr.name())
            return True
        except InstructionNotFound:
            return False

    return list(filter(can_benchmark, instrs))
