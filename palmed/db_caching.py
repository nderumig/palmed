## \file db_caching.py
## \brief Manage the prefetching / bulk queries to the db

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import logging
import gc
from collections import defaultdict
import pipedream.utils.abc as abc
import palmed.pipedream_benchmarks as palmedpipe
from palmed import db
from palmed.db import current_machine
from palmed.config import Config
from palmed.messages import ProgressBar
from typing import Sequence, List, NamedTuple, Optional, Iterable, Dict, Set

logger = logging.getLogger(__name__)


class NonBenchingMachine(Exception):
    def __init__(self, bench: "DBCached"):
        self.bench = bench

    def __str__(self):
        return "This machine cannot perform benchmarks. Tried to benchmark: {}".format(
            self.bench.db_str()
        )


class DBCached(object):
    __metaclass__ = abc.ABCMeta
    BENCHMARK_AGGREGATION_METHOD = (
        db.models.BenchmarkAggregationMethod.MEAN_25_UPPER_PERCENT
    )

    _align: bool
    _IPC_commitable: bool
    _IPC_bench_stats: Optional[palmedpipe.IPC_bench_stats]

    def __init__(self) -> None:
        self._align = False
        self._IPC_commitable = False
        self._IPC_bench_stats = None
        self.orig_IPC: Optional[float] = None
        super().__init__()

    @abc.abstractmethod
    def IPC(self, core: Optional[int] = None):
        pass

    @abc.abstractmethod
    def set_IPC(self, ipc, commitable=True):
        pass

    @abc.abstractmethod
    def has_IPC(self):
        pass

    @classmethod
    @abc.abstractmethod
    def from_db_entry(cls, db_entry: db.models.Benchmark):
        pass

    def _gen_IPC_pipedream_or_cached(self, core: Optional[int]):
        """Fill the IPC with an IPC value coming from, in this order (first
        present):
        * database measures;
        * a freshly made pipedream benchmark otherwise.
        """
        if self._populate_pipedream_IPC_from_db():
            return

        logger.debug("%s not found in database, benching.", self)
        self._gen_IPC_pipedream(core)
        self._cached_IPC_to_db()

    @abc.abstractmethod
    def _gen_IPC_pipedream(self, core: Optional[int]):
        pass

    @abc.abstractmethod
    def commit_benchmark(
        self, cpi, bench_stats, round_IPC_nearest: bool = True, db_commit=False
    ):
        pass

    def db_fetch_IPC(self) -> Sequence[db.models.BenchmarkMeasure]:
        """Fetch this IPC measure in database and return a list of measures, but do not
        set _IPC yet."""
        logger.debug("Trying db-cached version of %s…", self)

        session = db.Session()
        db_str = self.db_str()
        relevant_measures_query = (
            session.query(db.models.BenchmarkMeasure)
            .where(db.models.Benchmark.descr == db_str)
            .join(db.models.Benchmark.measures)
            .where(db.models.BenchmarkMeasure.machine_id == current_machine.get_pk())
        )
        measures = relevant_measures_query.all()

        if not measures:
            logger.debug("\tFound no measures")
            return []

        logger.debug("\tFound %d measures", len(measures))
        return measures

    @staticmethod
    def aggregate_IPC_measures(
        measures: Iterable[db.models.BenchmarkMeasure],
    ) -> Optional[float]:
        """Aggregate a list of IPC measures into an IPC value, or None if aggregation
        did not yield a satisfying result (high variance, …)"""
        # TODO better aggregation
        nb_measures = 0
        ipc: float = 0.0
        for measure in measures:
            nb_measures += 1
            ipc += measure.ipc

        if nb_measures == 0:
            return None

        ipc /= nb_measures
        return ipc

    def _populate_pipedream_IPC_from_db(self):
        """Fetch this IPC from DB and set _IPC"""
        if Config.DATABASE_IGNORE_CACHE:
            return False
        ipc_measures = self.db_fetch_IPC()
        if ipc_measures:
            ipc_aggr = self.aggregate_IPC_measures(ipc_measures)
            if ipc_aggr is not None:
                self.set_IPC(ipc_aggr)
                return True
        return False

    def is_IPC_unreliable(self):
        return not self._IPC_commitable

    def _cached_IPC_to_db(self, commit=True):
        """Commit a cached IPC measure to database"""
        assert self.has_IPC()
        assert self._IPC_bench_stats is not None
        assert self.orig_IPC is not None
        assert current_machine.is_benching()

        db_str = self.db_str()
        if self.is_IPC_unreliable():
            logger.warning(
                "Trying to commit an unreliable IPC to database for %s, aborting",
                db_str,
            )
            return

        session = db.Session()
        db_bench = self.get_or_create_db_entry()
        conditions = db.current_machine.current_experimental_conditions(
            self._IPC_bench_stats.core,
            parallel_cores=self._IPC_bench_stats.parallel_cores_used,
        )

        measure = db.models.BenchmarkMeasure(
            benchmark_id=db_bench.id,
            machine_id=db.current_machine.get_pk(),
            conditions=conditions,
            ipc=self.IPC(),
            orig_ipc=self.orig_IPC,
            variance=float(self._IPC_bench_stats.variance),
            date=datetime.datetime.now(),
            aggregation_mode=self.BENCHMARK_AGGREGATION_METHOD,
            warmup_repetitions=self._IPC_bench_stats.num_warmup_iterations,
            repetitions=self._IPC_bench_stats.num_iterations,
            instr_count=self._IPC_bench_stats.num_dynamic_instructions,
            inner_instr_count=self._IPC_bench_stats.unrolled_length,
            is_aligned=self._align,
            use_register_pools=self._IPC_bench_stats.use_register_pools,
        )

        session.add(conditions)
        session.add(measure)
        if commit:
            session.commit()

    @abc.abstractmethod
    def db_str(self) -> str:
        raise NotImplementedError()

    def get_db_entry(self) -> Optional[db.models.Benchmark]:
        """Get the database entry for this benchmark, or None if there is no such
        entry"""
        session = db.Session()
        entry = (
            session.query(db.models.Benchmark)
            .where(db.models.Benchmark.descr == self.db_str())
            .one_or_none()
        )
        return entry

    def get_or_create_db_entry(self) -> db.models.Benchmark:
        """Get the database entry for this benchmark. Creates the entry if it does not
        exist yet."""
        session = db.Session()
        db_entry = self.get_db_entry()
        if db_entry is not None:
            return db_entry
        # else, create it
        db_entry = db.models.Benchmark(descr=self.db_str())
        session.add(db_entry)
        session.commit()
        session.refresh(db_entry)
        return db_entry


def prefetch_benchs(
    bench_list: Sequence[Sequence[DBCached]],
):
    """Prefetch the relevant IPC measures from database for the provided benchmarks,
    using a huge SQL select request"""
    if Config.DATABASE_IGNORE_CACHE:
        return

    all_benchs: Set[DBCached] = {
        bench for bench_row in bench_list for bench in bench_row
    }
    logger.info(
        "Building query to prefetch %d benchmarks from database...", len(all_benchs)
    )
    session = db.Session()
    bench_query = (
        session.query(db.models.Benchmark, db.models.BenchmarkMeasure)
        .where(
            db.models.Benchmark.descr.in_(map(lambda bench: bench.db_str(), all_benchs))
        )
        .join(db.models.Benchmark.measures)
        .where(db.models.BenchmarkMeasure.machine_id == current_machine.get_pk())
    )
    logger.info("Prefetching %d benchmarks from database...", len(all_benchs))
    bench_measures = bench_query.all()

    # Map results back to their db_str
    measures_for_bench: Dict[str, List[db.models.BenchmarkMeasure]] = defaultdict(list)
    for benchmark, measure in bench_measures:
        measures_for_bench[benchmark.descr].append(measure)

    # Cache fetched measures
    for row in bench_list:
        for bench in row:
            ipc = bench.aggregate_IPC_measures(measures_for_bench[bench.db_str()])
            if ipc is not None:
                bench.set_IPC(ipc, commitable=False)

    # Clean up and garbage-collect all the mess we've made
    session.rollback()
    logger.info("Garbage collecting...")
    gc.collect()
