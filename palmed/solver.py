## \file solver.py
## \brief Launch gurobi in order to infer a mapping from diverse
## benchmarks

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import gurobipy as gp  # type: ignore
import logging
import pathlib
import subprocess
import sys
import tempfile
import palmed.benchmarks as bench
import palmed.heuristics as heu
import palmed.sol2output as s2o
from gurobipy import GRB
from palmed.config import Config
from palmed.instructions import Instruction
from palmed.messages import ProcessTitle
from palmed.utils import NoStdout
from typing import (
    Set,
    List,
    Dict,
    Tuple,
    IO,
    Optional,
    Union,
    Type,
    FrozenSet,
    cast,
    Sequence,
)


logger = logging.getLogger(__name__)


class GurobiRuntimeError(Exception):
    def __init__(self, cpexn: subprocess.CalledProcessError):
        super().__init__()
        self.cpexn = cpexn

    def __str__(self):
        return "Gurobi returned with exit code {}. Stderr: {}".format(
            self.cpexn.returncode, self.cpexn.stderr
        )


# Names of LP internal variables
def rho_a(var: Instruction, res: int) -> str:
    return "Rho_" + var.var_name() + "_R" + str(res)


def rho_k(k: int, res: int) -> str:
    return "Rho_K" + str(k) + "_R" + str(res)


def sat(k: int) -> str:
    return "Sat_K" + str(k)


def front_end(k: int) -> str:
    return f"FE_K{k}"


def exist_r(res: int) -> str:
    return "Exist_" + str(res)


def cumul_non_inst_res_clique(var: Instruction, res: int) -> str:
    return "Cumul_Not_" + var.var_name() + "_R" + str(res) + "_Clique"


def shared(k: int) -> str:
    return "Shared_K" + str(k)


def shared_r(k: int, res: int) -> str:
    return "Shared_K" + str(k) + "_R" + str(res)


def cumul_non_inst_res_k(var: Instruction, res: int, k: int) -> str:
    return "Cumul_Not_" + var.var_name() + "_R" + str(res) + "_K" + str(k)


def add_variables_and_constraints_integer(
    instructions: List[Instruction],
    max_cliques: List[List[Instruction]],
    min_order: List[Instruction],
    resources: List[int],
    benchmarks: Sequence[bench.AbstractBenchmark],
    m: gp.Model,
) -> Tuple[Dict[Tuple[Instruction, int], gp.Var], Dict[int, gp.Var]]:
    rho_a_vars: Dict[Tuple[Instruction, int], gp.Var] = {}
    exist_r_vars: Dict[int, gp.Var] = {}
    for res in resources:
        exists_args: List[gp.Var] = []
        for instr in instructions:
            rho_a_vars[instr, res] = m.addVar(
                vtype=GRB.BINARY,
                name=rho_a(instr, res),
                lb=0,
                ub=1,
                obj=0,
            )
    for res in resources:
        exist_r_vars[res] = m.addVar(
            vtype=GRB.BINARY,
            name=exist_r(res),
            lb=0,
            ub=1,
            obj=0,
        )
        # Denote the presence (or not) of a resource
        m.addGenConstrMax(
            exist_r_vars[res], [rho_a_vars[instr, res] for instr in instructions]
        )

    # Force maxclique insts to map to different resources
    zero = m.addVar(vtype=GRB.INTEGER, name="zero", lb=0, ub=0, obj=0)
    for mc in max_cliques:
        for inst in mc:
            logger.debug("Max clique %s", inst.name())
            # Sum of all edges from not(inst) to one resource
            sum_edge_noninst_vars: List[gp.Var] = []
            sum_edge_noninst: List[gp.Var]
            for res in resources:
                sum_edge_noninst = [
                    rho_a_vars[inst2, res] for inst2 in mc if inst2 != inst
                ] + [rho_a_vars[inst, res]]
                sum_edge_noninst_vars.append(
                    m.addVar(
                        vtype=GRB.INTEGER,
                        name=cumul_non_inst_res_clique(inst, res),
                        lb=0,
                        ub=len(mc),
                    )
                )
                sum_edge_noninst.append(sum_edge_noninst_vars[-1])
                m.addLConstr(
                    gp.LinExpr(
                        [1] * (len(mc) - 1) + [-1, -1],
                        sum_edge_noninst,
                    ),
                    GRB.EQUAL,
                    -1,
                )
            m.addGenConstrMin(zero, sum_edge_noninst_vars)

    # Force i \in min_order (cf heuristics.py for a complete explanation) to map
    # to one resource common with all a s.t a^{ipc{a}} i^{ipc{i}} \neq \ipc{a} + \ipc{i}
    for inst in min_order:
        logger.debug("Min order %s", inst.name())
        sum_conflicting_ins_vars: List[gp.Var] = []
        for res in resources:
            conflicting_ins_vars: List[gp.Var] = [rho_a_vars[inst, res]]
            for b in benchmarks:
                # Getting inst^{\ipc{inst}} a^{\ipc{a}} and there is some sharing between
                # inst and a
                if (
                    isinstance(b, bench.Benchmark)
                    and len(b.instrs_with_reps()) == 2
                    and inst in b.instrs_with_reps().keys()
                ):
                    other_inst = list(b.instrs_with_reps().keys())
                    other_inst.remove(inst)
                    if b.IPC() < b.max_IPC():
                        conflicting_ins_vars.append(rho_a_vars[other_inst[0], res])
            sum_conflicting_ins_vars.append(
                m.addVar(lb=0, ub=len(conflicting_ins_vars), obj=0)
            )
            m.addLConstr(
                gp.LinExpr(
                    [1] * len(conflicting_ins_vars) + [-1],
                    conflicting_ins_vars + [sum_conflicting_ins_vars[-1]],
                ),
                GRB.EQUAL,
                0,
            )
        m.addGenConstrMax(
            m.addVar(
                vtype=GRB.INTEGER,
                lb=len(conflicting_ins_vars),
                # Length = #{conflincting ins}, indep of `res`, so we can use only the
                # one from the latest `res`
                ub=len(conflicting_ins_vars),
                obj=len(conflicting_ins_vars),
            ),
            sum_conflicting_ins_vars,
        )

    for idx, b in enumerate(benchmarks):
        if isinstance(b, bench.Benchmark) and len(b.instrs_with_reps().items()) > 1:
            instrs = list(set(b.instrs()))
            # If there is some sharing somewhere (i.e., the IPC is not maximal)
            # then there exists a resource shared by every instruction (approx) of
            # the benchmark
            if b.IPC() < b.max_IPC() - Config.MAX_IPC_ERROR:
                logger.debug(
                    "Sharing (=> 1 common res) on %s (MAX IPC = %s", b, b.max_IPC()
                )

                shared_res_vars: Dict[int, gp.Var] = {}
                for res in resources:
                    # whether sharing occurs on `res`
                    shared_res_vars[res] = m.addVar(
                        vtype=GRB.INTEGER,
                        name=shared_r(idx, res),
                        lb=0,
                        obj=0,
                        ub=len(instrs),
                    )
                    # instructions sharing a resource
                    shared_var_instr: List[gp.Var] = []
                    for inst in instrs:
                        shared_var_instr.append(rho_a_vars[inst, res])
                    shared_var_instr.append(shared_res_vars[res])
                    # `shared_res_var[res] = sum(rho_a_var[instrs])`
                    m.addLConstr(
                        gp.LinExpr([1 for _ in instrs] + [-1], shared_var_instr),
                        GRB.EQUAL,
                        0,
                    )
                # "There must be some sharing" variable
                shared_var = m.addVar(
                    vtype=GRB.INTEGER, name=shared(idx), lb=len(instrs), obj=len(instrs)
                )
                m.addLConstr(shared_var, GRB.GREATER_EQUAL, len(instrs))
                m.addGenConstrMax(
                    shared_var, [shared_res_vars[res] for res in resources]
                )

                # If the sharing is not complete, i.e. the execution time is not the
                # sum of the individual exec times
                if b.IPC() > b.min_IPC() + Config.MAX_IPC_ERROR:
                    # Then there is at least 2 resources loaded by each instruction of
                    # b, i.e.
                    # \sum_{r} \rho_{i,r} \geq 2
                    for inst in b.instrs():
                        print(f"{inst} has 2 res")
                        m.addLConstr(
                            gp.LinExpr(
                                [1 for _ in resources],
                                [rho_a_vars[inst, r] for r in resources],
                            ),
                            GRB.GREATER_EQUAL,
                            2,
                        )
            else:
                # IPC is maximal:
                # There is no sharing on this bench -> different resources
                # i.e. for all instruction i that saturates (measured by their execution
                # times compared to the execution time of the microbenchmark),
                # i is mapped to at least one resource untouched by other instrs
                # i.e. for r resource and i instruction of the bench:
                # \min_r (\sum_{j\neq i} \rho_{j,r} + (1 - \rho_{i, r}) ) = 0
                # (same formulation as max_clique)
                logger.debug("No sharing (=> exist different res for sat int) on %s", b)
                cycles = sum([rep for rep in b.instrs_with_reps().values()]) / b.IPC()
                for inst, coef in b.instrs_with_reps().items():
                    # Sum of all edges from not(inst) to one resource
                    sum_edge_noninst_vars = []
                    if cycles - float(coef) / inst.IPC() < Config.MAX_IPC_ERROR:
                        # ^^^ It is a saturating instruction, i.e. its execution time
                        # is the same (or more) than the benchmark
                        for res in resources:
                            sum_edge_noninst = [
                                rho_a_vars[inst2, res]
                                for inst2 in instrs
                                if inst2 != inst
                            ] + [
                                rho_a_vars[inst, res]
                            ]  # \sum_{j \neq i} \rho_{j,r}
                            sum_edge_noninst_vars.append(
                                m.addVar(
                                    vtype=GRB.INTEGER,
                                    name=cumul_non_inst_res_k(inst, res, idx),
                                    lb=0,
                                    ub=len(instrs),
                                )
                            )
                            sum_edge_noninst.append(sum_edge_noninst_vars[-1])
                            m.addLConstr(
                                gp.LinExpr(
                                    [1] * (len(instrs) - 1) + [-1, -1],
                                    sum_edge_noninst,
                                ),
                                GRB.EQUAL,
                                -1,
                            )  # cumul_non_inst_res_k = \sum_{j\neq i} \rho_{j,r}
                            #                           + (1 - \rho_{i, r})
                        m.addGenConstrMin(zero, sum_edge_noninst_vars)
                        # min of all cumul_non_inst_res_k
    return rho_a_vars, exist_r_vars


# Add all necessary variables to the LP
def add_variables_and_constraints(
    instructions: List[Instruction],
    resources: List[int],
    benchmarks: Sequence[bench.AbstractBenchmark],
    frozen_edges: List[Tuple[Instruction, int, float]],
    m: gp.Model,
) -> Tuple[
    Dict[Tuple[Instruction, int], gp.Var],
    Dict[Tuple[bench.Benchmark, int], gp.Var],
    Dict[Tuple[bench.Benchmark, int], gp.Constr],
    Dict[bench.Benchmark, gp.Var],
]:
    rho_a_vars: Dict[Tuple[Instruction, int], gp.Var] = {}
    for instr in instructions:
        for res in resources:
            rho_a_vars[instr, res] = m.addVar(
                vtype=GRB.CONTINUOUS,
                name=rho_a(instr, res),
                lb=0,
                ub=1 / instr.IPC() + Config.FRAC_ERROR_RATE,
                obj=0,
            )

    for instr, res, value in frozen_edges:
        m.addLConstr(rho_a_vars[instr, res], GRB.EQUAL, value)

    rho_k_vars: Dict[Tuple[bench.Benchmark, int], gp.Var] = {}
    sat_vars: Dict[bench.Benchmark, gp.Var] = {}
    sat_constr: Dict[bench.Benchmark, gp.GenConstr] = {}
    rho_k_constr: Dict[Tuple[bench.Benchmark, int], gp.Constr] = {}
    for idx, ben in enumerate(benchmarks):
        if isinstance(ben, bench.DummyBenchmark):
            continue
        assert isinstance(ben, bench.Benchmark)
        var_ben_res: List[gp.Var] = []

        if sat_vars.get(ben) is not None:
            logger.warning(
                "Duplicate bench in constraint: %s, ignoring", ben.human_str()
            )
            logger.warning("Benchs tested")
            for b in benchmarks[:idx]:
                logger.warning("    %s", b.human_str())
            continue

        sat_vars[ben] = m.addVar(
            vtype=GRB.CONTINUOUS,
            name=sat(idx),
            lb=0 if Config.PIPEDREAM else 1,
            ub=1,
            obj=1,
        )
        for res in resources:
            rho_k_vars[ben, res] = m.addVar(
                vtype=GRB.CONTINUOUS,
                name=rho_k(idx, res),
                lb=0,
                ub=1,
                obj=0,
            )
            rho_k_constr[ben, res] = m.addLConstr(
                gp.LinExpr(
                    ben.coefs(),
                    [rho_a_vars[instr, res] for instr in ben.instrs()]
                    + [rho_k_vars[ben, res]],
                ),
                GRB.EQUAL,
                0,
            )
        # Load of the front-end, seen as a per-benchmark (fixed) offset
        fe_val = ben.max_front_end_ipc() if Config.PIPEDREAM else 0
        if fe_val != 0:
            fe_val = min(1, ben.IPC() / fe_val)
        front_end_var = m.addVar(
            vtype=GRB.CONTINUOUS, name=front_end(idx), lb=fe_val, ub=fe_val, obj=fe_val
        )
        sat_constr[ben] = m.addGenConstrMax(
            sat_vars[ben], [rho_k_vars[ben, res] for res in resources] + [front_end_var]
        )
    return rho_a_vars, rho_k_vars, rho_k_constr, sat_vars


# Fixes to zero edges that are not in the input mapping
def restrict_to_edges(
    m: gp.Model,
    available_edges: Dict[Instruction, List[Tuple[float, str]]],
    rho_a_vars: Dict[Tuple[Instruction, int], gp.Var],
) -> None:
    for (ins, res), var in rho_a_vars.items():
        for val, res2 in available_edges[ins]:
            if val > 0 and res2 == f"R{res}":
                break
        else:
            m.addLConstr(var, GRB.EQUAL, 0)


# On sucess, returns the mapping and the saturating benchmarks
# On failure, returns the benchmarks present in the IIS
def LP_gurobi(
    lp1: bool,
    instructions: List[Instruction],
    max_clique: List[List[Instruction]],
    min_order: List[Instruction],
    available_edges: Optional[Dict[Instruction, List[Tuple[float, str]]]],
    frozen_edges: List[Tuple[Instruction, int, float]],
    benchs: Sequence[bench.AbstractBenchmark],
    resources: List[int],
    epsilon_frac: float,
    time_factor: float,
    quiet: bool,
    quiet_IIS: bool,
) -> Union[
    Tuple[Dict[Instruction, List[Tuple[float, str]]], Dict[str, bench.Benchmark]],
    List[bench.Benchmark],
]:
    assert len(instructions) > 0
    assert len(benchs) > 0
    if quiet:
        with_content = NoStdout("gurobipy.gurobipy")
    else:
        with_content = NoStdout()

    with with_content:
        env: Dict = {}
        if Config.GUROBI_CSMANAGER is not None:
            env["CSManager"] = Config.GUROBI_CSMANAGER
            env["CSAPIAccessID"] = Config.GUROBI_ACCESSID
            env["CSAPISecret"] = Config.GUROBI_SECRET
        with gp.Env(params=env) as gb_env:
            with gp.Model(env=gb_env) as model:
                model.Params.LOG_TO_CONSOLE = int(not quiet)
                model.Params.TIME_LIMIT = time_factor * Config.GUROBI_TIME
                model.Params.LOG_FILE = Config.GUROBI_LOGFILE.as_posix()

                if Config.UOPS:
                    model.Params.PRESOLVE = 2
                    (
                        rho_a_vars,
                        rho_k_vars,
                        rho_k_constr,
                        sat_vars,
                    ) = add_variables_and_constraints(
                        instructions, resources, benchs, frozen_edges, model
                    )
                    model.setObjective(
                        gp.LinExpr(
                            [1.0] * (len(instructions) * len(resources)),
                            [
                                rho_a_vars[inst, res]
                                for inst in instructions
                                for res in resources
                            ],
                        ),
                        GRB.MINIMIZE,
                    )
                else:
                    if lp1:
                        (
                            rho_a_vars,
                            exist_r_vars,
                        ) = add_variables_and_constraints_integer(
                            instructions,
                            max_clique,
                            min_order,
                            resources,
                            benchs,
                            model,
                        )
                        # Default target is to minimize
                        model.setObjectiveN(
                            gp.LinExpr(
                                [1.0] * len(exist_r_vars),
                                exist_r_vars.values(),
                            ),
                            index=0,
                        )
                        model.setObjectiveN(
                            gp.LinExpr(
                                [1.0] * len(rho_a_vars.values()),
                                list(rho_a_vars.values()),
                            ),
                            index=1,
                            priority=0,
                        )
                    else:
                        (
                            rho_a_vars,
                            rho_k_vars,
                            rho_k_constr,
                            sat_vars,
                        ) = add_variables_and_constraints(
                            instructions, resources, benchs, frozen_edges, model
                        )
                        model.setObjective(
                            gp.LinExpr(
                                [1.0] * len(sat_vars.values()),
                                sat_vars.values(),
                            ),
                            GRB.MAXIMIZE,
                        )

                if available_edges is not None:
                    restrict_to_edges(model, available_edges, rho_a_vars)

                if Config.DUMP_LPS:
                    model.write("/tmp/base_system.lp")
                model.optimize()
                if model.status == GRB.INFEASIBLE:
                    if lp1:
                        logger.error("ILP found no solution, exiting")
                        if Config.DUMP_LPS:
                            iis = model.computeIIS()
                            model.write("/tmp/infeasible.ilp")
                        exit(1)

                    # Extract infos from the Irreductible Inconsistent Subsystem
                    ret: Set[bench.Benchmark] = set()
                    logger.error("No LP solution found")
                    logger.error("Tested instruction(s):")
                    for i in instructions:
                        logger.error("\t" + str(i))
                    # This may take a long time on non-integer mode
                    model.Params.TIME_LIMIT = 0.1 * Config.GUROBI_TIME
                    iis = model.computeIIS()
                    if Config.DUMP_LPS:
                        model.write("/tmp/infeasible.ilp")
                    for ben, var in sat_vars.items():
                        if var.IISLB or var.IISUB:
                            ret.add(ben)

                    if not quiet_IIS:
                        logger.info("Benchmark in the IIS:")
                        for ben in ret:
                            logger.info(f"\t{ben.human_str()}")
                            logger.info(ben)
                            logger.info(ben.min_IPC())
                            logger.info(ben.max_IPC())
                    return list(ret)
                elif model.SolCount == 0 and model.status == GRB.TIME_LIMIT:
                    if lp1:
                        logger.error("ILP found no solution, exiting")
                        exit(1)
                    logger.info(
                        "No solution found within time limit, updating error of all benchs"
                    )
                    all_benchs = []
                    for b in benchs:
                        if isinstance(b, bench.Benchmark) and len(b.instrs()) > 1:
                            all_benchs.append(b)
                    return all_benchs
                elif model.status not in {GRB.OPTIMAL, GRB.TIME_LIMIT}:
                    logger.error("Gurobi stopped because of unknown reason, aborting")
                    exit(1)
                if lp1:
                    return s2o.read_solution_integer(rho_a_vars)
                else:
                    return s2o.read_solution(
                        rho_a_vars, rho_k_vars, Config.FRAC_ERROR_RATE
                    )


def force_LP_kernel(
    lp1: bool,
    kernel: List[Instruction],
    max_clique: List[List[Instruction]],
    min_order: List[Instruction],
    available_edges: Optional[Dict[Instruction, List[Tuple[float, str]]]],
    kernel_benchs: Sequence[bench.AbstractBenchmark],
    time_factor: float = 1.0,
    quiet: bool = False,
    nb_max_res: Optional[int] = None,
    descr="",
) -> Tuple[Dict[Instruction, List[Tuple[float, str]]], Dict[str, bench.Benchmark]]:
    mapping: Dict[Instruction, List[Tuple[float, str]]] = {}
    sat_benchs: Dict[str, bench.Benchmark] = {}

    if nb_max_res is None:
        nb_max_res = Config.MAX_RES
    if not quiet:
        logger.info("Launching Gurobi..")
    with ProcessTitle(
        f"Gurobi {len(kernel)} instructions {len(kernel_benchs)} benchs"
        if not quiet
        else None
    ):
        ret_lp = LP_gurobi(
            lp1,
            kernel,
            max_clique,
            min_order,
            available_edges if Config.PIPEDREAM else None,
            [],
            kernel_benchs,
            list(range(nb_max_res)),
            Config.MIN_BENCH_COEF,
            time_factor,
            quiet,
            quiet,
        )
    if isinstance(ret_lp, tuple):
        mapping, sat_benchs = ret_lp
    else:
        if lp1:
            logger.error("No solution found for LP1, exiting...")
        else:
            logger.error("No solution found for LP, exiting...")
        exit(1)

    if len(mapping.items()) == 0:
        logger.error("Cannot map kernel: LP returned an empty mapping")
        exit(1)
    return mapping, sat_benchs


def LP_one_inst(
    instr_to_map: Instruction,
    frozen_edges: List[Tuple[Instruction, int, float]],
    benchs: List[bench.AbstractBenchmark],
    resources: List[int],
    epsilon_frac: float,
    quiet: bool = True,
) -> List[Tuple[float, str]]:
    assert len(frozen_edges) > 0
    all_instrs = {instr_to_map}
    for instr, _, _ in frozen_edges:
        all_instrs.add(instr)

    with ProcessTitle(f"Gurobi 1 instruction {len(benchs)} benchs"):
        ret_lp = LP_gurobi(
            False,
            list(all_instrs),
            [],
            [],
            None,
            frozen_edges,
            benchs,
            resources,
            epsilon_frac,
            time_factor=1,
            quiet=True,
            quiet_IIS=quiet,
        )
    if isinstance(ret_lp, tuple):
        mapping, sat_benchs = ret_lp
    else:
        list_invalid_bench = ret_lp
        if not quiet:
            logger.info("No mapping found for one-inst LP, exiting...")
        if len(list_invalid_bench) == 0:
            raise Exception("No saturating constraint in IIS (?!)")
        exit(1)
    return mapping[instr_to_map]
