## \file messages.py
## \brief Manage the output messages, which can be debug, info, warning or error

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import sys
import tqdm  # type: ignore
import setproctitle
import gzip
import shutil
from typing import TypeVar, Iterable, Optional, List
from pathlib import Path
import logging

_CEND = "\33[0m"
_CBOLD = "\33[1m"
_CITALIC = "\33[3m"
_CURL = "\33[4m"
_CBLINK = "\33[5m"
_CBLINK2 = "\33[6m"
_CSELECTED = "\33[7m"

_CBLACK = "\33[30m"
_CRED = "\33[31m"
_CGREEN = "\33[32m"
_CYELLOW = "\33[33m"
_CBLUE = "\33[34m"
_CVIOLET = "\33[35m"
_CBEIGE = "\33[36m"
_CWHITE = "\33[37m"

_CBLACKBG = "\33[40m"
_CREDBG = "\33[41m"
_CGREENBG = "\33[42m"
_CYELLOWBG = "\33[43m"
_CBLUEBG = "\33[44m"
_CVIOLETBG = "\33[45m"
_CBEIGEBG = "\33[46m"
_CWHITEBG = "\33[47m"

_CGREY = "\33[90m"
_CRED2 = "\33[91m"
_CGREEN2 = "\33[92m"
_CYELLOW2 = "\33[93m"
_CBLUE2 = "\33[94m"
_CVIOLET2 = "\33[95m"
_CBEIGE2 = "\33[96m"
_CWHITE2 = "\33[97m"

_CGREYBG = "\33[100m"
_CREDBG2 = "\33[101m"
_CGREENBG2 = "\33[102m"
_CYELLOWBG2 = "\33[103m"
_CBLUEBG2 = "\33[104m"
_CVIOLETBG2 = "\33[105m"
_CBEIGEBG2 = "\33[106m"
_CWHITEBG2 = "\33[107m"


class LoggingCustomFormatter(logging.Formatter):
    """Logging Formatter to add colours"""

    log_format = "[{relative_time}|%(levelname)s] (%(name)s) — %(message)s"

    FORMATS = {
        logging.DEBUG: _CBLUE2 + log_format + _CEND,
        logging.INFO: _CGREEN + log_format + _CEND,
        logging.WARNING: _CYELLOW2 + log_format + _CEND,
        logging.ERROR: _CRED2 + log_format + _CEND,
        logging.CRITICAL: _CRED2 + _CBOLD + log_format + _CEND,
    }

    def __init__(self):
        self._starttime = datetime.datetime.now()

    def format(self, record):
        delta_time = datetime.timedelta(
            seconds=int((datetime.datetime.now() - self._starttime).total_seconds())
        )
        log_fmt = self.FORMATS.get(record.levelno).format(relative_time=delta_time)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def setup_logging(loglevel=logging.INFO, quiet=False, log_tee: Optional[Path] = None):
    """Configure Python's logging module.

    If `log_tee` is not None, also logs everything at this path. If the file already
    exists, rotates it.
    """
    if quiet:
        loglevel = logging.CRITICAL

    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(LoggingCustomFormatter())
    handlers: List[logging.Handler] = [stdout_handler]

    if log_tee is not None:
        if log_tee.exists():
            logrotate(log_tee)
        log_tee.parent.mkdir(parents=True, exist_ok=True)
        file_handler = logging.FileHandler(log_tee, mode="w")
        file_handler.setFormatter(LoggingCustomFormatter())
        handlers.append(file_handler)

    logging.basicConfig(level=loglevel, handlers=handlers)
    if loglevel <= logging.DEBUG:
        logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)


def logrotate(base_path: Path, max_rotate=5):
    """Rotates log files, compressing old ones"""

    paths = [
        base_path.with_suffix(base_path.suffix + f".{rot}.gz")
        for rot in range(1, max_rotate)
    ]
    rot_paths = list(zip(paths, paths[1:]))[::-1]

    # Rotate older files
    for rot_from, rot_to in rot_paths:
        if rot_to.exists():
            rot_to.unlink()
        if rot_from.exists():
            rot_from.rename(rot_to.as_posix())

    # Rotate and compress most recent one
    postzip = base_path.with_suffix(base_path.suffix + ".1.gz")
    with base_path.open("rb") as f_in:
        with gzip.open(postzip.as_posix(), "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    base_path.unlink()


class ProcessTitle:
    """Class setting the process title in top/ps/htop to a custom value in a
    `with` block, then reseting to the original value. If `None` is provided, no
    change is made."""

    def __init__(self, name: Optional[str]):
        self._oldname = setproctitle.getproctitle()
        self._newname = f"palmed ({name})" if name is not None else None

    def __enter__(self):
        if self._newname is not None:
            setproctitle.setproctitle(self._newname)

    def __exit__(self, *args):
        if self._newname is not None:
            setproctitle.setproctitle(self._oldname)


# Type checking
T = TypeVar("T")


class ProgressBar(tqdm.tqdm):
    disabled = False

    def __init__(self, iterable: Optional[T] = None, *args, **kwargs):
        super().__init__(
            iterable=iterable,
            *args,
            bar_format=_CGREEN + "[INFO] {l_bar}{bar}{r_bar}" + _CEND,
            disable=self.disabled,
            **kwargs,
        )  # type: ignore
        self._oldprocname = setproctitle.getproctitle()
        # tqdm seems to keep internally references to past bars. Avoid restoring twice
        # on garbage collection or other misc events.
        self._name_restored = False
        if (not self.disabled) and self.desc:
            setproctitle.setproctitle(f"palmed ({self.desc})")

    def close(self, *args, **kwarg):
        super().close()
        if (not self.disabled) and self.desc and not self._name_restored:
            setproctitle.setproctitle(self._oldprocname)
            self._name_restored = True
