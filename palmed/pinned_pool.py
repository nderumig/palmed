## \file pinned_pool.py
## \brief As a standard pool, but ensure that every process is pinned to a different physical
## core

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import threading
import os
import math
import faulthandler
import queue
import signal
import sys
import logging
import traceback
import multiprocessing as mp
import threading
import traceback
import datetime
from multiprocessing import Pipe
from multiprocessing.pool import ExceptionWithTraceback  # type:ignore
from multiprocessing.pool import Pool
from setproctitle import setproctitle
from typing import Optional, Callable, List, Iterable, TypeVar, Type, Dict, Any
from palmed.messages import *
from palmed import db
from palmed.config import Config
from palmed.utils import BenchmarkScheduler
from palmed.pipedream_config import RemotePipedream, configure_pipedream


logger = logging.getLogger(__name__)


class CrashOnThreadExceptions:
    """If an exception occurs in some other thread while this context manager is live
    in the parent thread, it will crash the application."""

    def __init__(self):
        self._old_hook = threading.excepthook

    @staticmethod
    def _threading_excepthook_reraise(args):
        """An alternative `threading.excepthook`."""
        exn_typ, exn, tb, thread = args
        logger.error("Exception uncaught in thread %s:", thread)
        traceback.print_exception(exn_typ, exn, tb)
        os._exit(1)

    def enter(self):
        threading.excepthook = self._threading_excepthook_reraise

    def exit(self):
        threading.excepthook = self._old_hook


class PinnedProcess(mp.context.ForkServerProcess):
    pool_class: Type["CoreInitializedPool"]

    _args: list
    _kwargs: Dict[str, Any]

    def __init__(self, pool_class: Type["CoreInitializedPool"], *args, **kwargs):
        # This code runs in the parent process
        super().__init__(*args, **kwargs)

        # UGLY HACK ALERT
        # In order to pass the `core_queue` to our child process, we abuse the
        # `initargs` parameter: before calling the Pool's `__init__`, we transform
        #     initargs <- (core_queue, initargs)
        # then, right here, we unpack initargs and restore it to its legitimate value
        # even before the child process is spawned, passing it through `self._kwargs`,
        # which is more easily controllable and marginally cleaner.
        self.pool_class = pool_class
        core_queue, initargs = self._args[3]
        actual_args = list(self._args)
        actual_args[3] = initargs
        self._args = actual_args

        self._error_channel_read, _error_channel_write = Pipe(duplex=False)

        # Ugly hack to pass this end of the pipe to the child process -- since this
        # code runs in the parent process, putting it in `self._error_channel_write`
        # won't help passing it.
        self._kwargs["__error_channel_write"] = _error_channel_write
        # Same goes for the core queue: see comments above
        self._kwargs["__core_queue"] = core_queue
        self._kwargs["__config_dict"] = Config.get_options_dict(pickle_safe=True)

    def run(self, *args, **kwargs):
        # This code runs in the child process

        # This end of the pipe is popped from arguments -- this is transparent
        # to self._target
        _error_channel_write = self._kwargs.pop("__error_channel_write", None)
        _core_queue = self._kwargs.pop("__core_queue", None)
        _config_dict = self._kwargs.pop("__config_dict", {})

        Config.set_options_dict(_config_dict)

        try:
            self.pool_class._start_process(_core_queue)
            _error_channel_write.send(None)
            _error_channel_write.close()
        except Exception as exn:
            logger.error("An exception occurs during initalisation, exiting...")
            # ExceptionWithTraceback is some deep dark magic: it uses the __reduce__
            # function + the fact that interprocess communication is pickled/unpickled
            # to restore the exception to the original one (here, `exn`) on the parent
            # side of the communication, setting in passing the "cause" traceback.
            exn_tb = ExceptionWithTraceback(exn, exn.__traceback__)
            _error_channel_write.send(exn_tb)
            _error_channel_write.close()
            return

        super().run(*args, **kwargs)

    def start(self, *args, **kwargs) -> Any:
        # This code runs in the parent process
        out = super().start(*args, **kwargs)
        # See above: on this side, we recieve a plain `Exception`.
        maybe_exn: Optional[Exception] = self._error_channel_read.recv()
        if maybe_exn is not None:
            self._error_channel_read.close()
            raise maybe_exn
        self._error_channel_read.close()
        return out

    # make 'daemon' attribute always return False, so that ForkedProcess are
    # allowed to create threads (e.g. for `instruction.test()` )
    def _get_daemon(self):
        return False

    def _set_daemon(self, value):
        pass

    daemon = property(_get_daemon, _set_daemon)  # type:ignore


_A = TypeVar("_A")
_B = TypeVar("_B")


class SingleCorePool:
    """A mock CorePinnedPool, to be used as a drop-in replacement where single-core is
    expected"""

    def __init__(self, *args, initargs=None, initializer=None, **kwargs):
        if initializer is not None:
            initializer(*initargs)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        pass

    def imap(self, mapper: Callable[[_A], _B], objs: Iterable[_A]) -> Iterable[_B]:
        # We cannot return an actual Iterable or use `map` at it would
        # break thread pinning.
        ret = []
        with BenchmarkScheduler():
            for i in objs:
                ret.append(mapper(i))
        return ret

    def imap_unordered(
        self, mapper: Callable[[_A], _B], objs: Iterable[_A]
    ) -> Iterable[_B]:
        return self.imap(mapper, objs)


class CoreInitializedPool(Pool):
    """A multiprocessing pool where each process is initialized with a CPU core ID.
    Should be subclassed; each subclass can act on this core ID at will (eg. pin to
    core)."""

    class NotEnoughCores(Exception):
        """Raised when a pool is created with more cores than available"""

    class NestedError(Exception):
        """Raised when trying to use a nested CoreInitializedPool, which has no
        well-defined behaviour"""

    class BrokenPool(Exception):
        """Raised when the pool tries to respawn a worker after an error that cannot be
        recovered from"""

        def __str__(self):
            return (
                "This CoreInitializedPool is trapped inside an infinite respawn scheme, "
                "and cannot recover from this error"
            )

    class PinnedCore:
        def __init__(self, core_id, core_queue):
            self.core_id = core_id
            self.core_queue = core_queue

        def free(self):
            self.core_queue.put(self.core_id)

    current_parallel_processes: Optional[int] = None
    core_queue: mp.Queue
    _current_pool: Optional["CoreInitializedPool"] = None
    _init_time: datetime.datetime
    _ready_time: datetime.datetime

    @classmethod
    def Process(cls, ctx, *pargs, **pkwargs):
        """Hackish override of Pool.Process, which is hackish itself, in order to
        catch exceptions from CoreInitializedPool._start_process."""

        # Since `PinnedProcess` has to be picklable, it must be defined at top-level in
        # the module; hence `PinnedProcess` cannot inherit from `ctx.Process`. Here, we
        # assume we are in `forkserver` mode and inherit from `ForkServerProcess`; if
        # this is not the case, the pool will crash.
        assert ctx.Process == mp.context.ForkServerProcess
        return PinnedProcess(cls, *pargs, **pkwargs)

    @classmethod
    def current_process_count(cls) -> int:
        if cls.current_parallel_processes is None:
            return 1
        return cls.current_parallel_processes

    @classmethod
    def _start_process(cls, core_queue: mp.Queue) -> None:
        """NOTE: this function is called only from `PinnedProcess.run`, which gets the
        core_queue through a circumvoluted process. Changes made in the argument types
        or argument passing here should be very cautiously checked."""
        assert core_queue is not None
        try:
            core_id = core_queue.get_nowait()
        except queue.Empty as exn:
            logger.error("A worker died in PinnedPool, exiting...")
            raise cls.BrokenPool() from exn

        cls._init_with_core(core_id)

        core_resource = cls.PinnedCore(core_id, core_queue)
        mp.util.Finalize(
            core_resource,
            core_resource.free,
            exitpriority=10,
        )

    @classmethod
    def _init_with_core(cls, core_id: int) -> None:
        """Called from _start_process upon worker process start. Is responsible for
        using the core ID to do something (eg. pin to core)"""
        pass

    @classmethod
    def get_pool(cls) -> Optional["CoreInitializedPool"]:
        """Get the currently allocated pool, if any, or None"""
        return cls._current_pool

    @classmethod
    def has_pool(cls) -> bool:
        """Check whether a pool is currently running"""
        return cls._current_pool is not None

    def __new__(cls, processes: Optional[int] = None, *args, **kwargs):
        if processes == 1:
            # Use single-core pool instead
            return SingleCorePool(processes, *args, **kwargs)
        return super().__new__(cls)

    @classmethod
    def default_processes(cls) -> int:
        """Number of processes to use by default if the user doesn't supply a
        number"""
        cpu_count: Optional[int] = os.cpu_count()
        assert cpu_count is not None
        return cpu_count

    @classmethod
    def core_multiplicity(cls, processes: int) -> int:
        """How many processes are spawned with each core ID"""
        return 1

    @classmethod
    def core_range(cls, processes: int) -> Iterable[int]:
        return range(1, processes + 1)  # Don't use CPU 0

    def __init__(
        self,
        processes: Optional[int] = None,
        initargs=None,
        **kwargs,
    ):
        self._init_time = datetime.datetime.now()
        if processes is None:
            processes = self.default_processes()
        processes = processes - 1  # We can't use CPU 0 for benchs

        multiplicity = self.core_multiplicity(processes)
        self.core_queue = mp.Queue(2 * multiplicity * processes)
        for core in self.core_range(processes):
            for _ in range(multiplicity):
                self.core_queue.put(core)
        processes *= multiplicity

        # See comment in `PinnedProcess`
        forwarded_initargs = (self.core_queue, initargs)

        if self.__class__.current_parallel_processes is not None:
            raise self.NestedError("CoreInitializedPools cannot be nested")
        self.__class__.current_parallel_processes = processes

        self._thread_reraise = CrashOnThreadExceptions()
        self._thread_reraise.enter()
        try:
            super().__init__(
                processes,
                initargs=forwarded_initargs,
                **kwargs,
            )
        except Exception as exn:
            self._exit_cleanup()
            raise exn

        self._ready_time = datetime.datetime.now()
        self.__class__._current_pool = self

    def _exit_cleanup(self):
        self.__class__.current_parallel_processes = None
        self.core_queue = None
        self._thread_reraise.exit()

    def __exit__(self, *args, **kwargs):
        clean_time = datetime.datetime.now()
        super().__exit__(*args, **kwargs)
        self.__class__._current_pool = None
        self._exit_cleanup()
        done_time = datetime.datetime.now()

        logger.debug(
            "--- Pool profiling report ---\n"
            "Startup: %s\n"
            "Computation: %s\n"
            "Cleanup: %s",
            self._ready_time - self._init_time,
            clean_time - self._ready_time,
            done_time - clean_time,
        )


class CorePinnedPool(CoreInitializedPool):
    """A multiprocessing pool where each process is pinned to an individual, non-zero
    CPU"""

    @classmethod
    def _init_with_core(cls, core_id: int) -> None:
        setproctitle.setproctitle(f"palmed ({cls.__name__} core=#{core_id})")
        os.sched_setaffinity(0, [core_id])
        configure_pipedream()
        db.init_engine(force=True)


class RemotePipedreamPool(CoreInitializedPool):
    """A multiprocessing pool where each process is set to use an individual core on a
    remote bencher server"""

    class ProcessCountRequired(Exception):
        """Raised when creating a RemotePipedreamPool without specifying a process
        count"""

    @classmethod
    def default_processes(cls) -> int:
        """Number of processes to use by default if the user doesn't supply a
        number"""
        return len(RemotePipedream.get_online_cpus())

    @classmethod
    def core_range(cls, processes: int) -> Iterable[int]:
        cores = list(RemotePipedream.get_online_cpus()).copy()
        if 0 in cores:
            cores.remove(0)  # Do not use core 0
        cores = cores[:processes]
        if processes > len(cores):
            raise cls.NotEnoughCores(
                f"{cls.__name__} requested with {processes} cores, when remote host "
                f"has only {len(cores)} cores"
            )
        assert len(cores) == processes
        return cores

    @classmethod
    def core_multiplicity(cls, processes: int) -> int:
        """How many processes are spawned with each core ID"""
        # This setting is useful to tweak to better even out the load on the building
        # and benching machine.
        return min(
            math.ceil(
                1.5 * (Config.local_num_cpu() / processes)
            ),  # good ratio for matched machines
            6,  # Max multiplicity
        )

    @classmethod
    def _init_with_core(cls, core_id: int) -> None:
        configure_pipedream()
        RemotePipedream.init_remote_pipedream_bencher(core_id, noremote_ok=False)
        setproctitle.setproctitle(f"palmed ({cls.__name__} core=#{core_id})")
        db.init_engine(force=True)


def select_multiprocessing_pool() -> Type[CoreInitializedPool]:
    """Automatically select which kind of pool is needed, and return its class."""

    if RemotePipedream.is_remote:
        return RemotePipedreamPool
    return CorePinnedPool
