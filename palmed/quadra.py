## \file quadra.py
## \brief Implement heuristics and multi-htreading computation of the
## quadratic tables of benchmarks

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import math
import numpy as np
import os
from collections import defaultdict
import pickle
from pathlib import Path
import signal
import time
import logging
import itertools
import traceback
import palmed.instructions as ins
import palmed.heuristics as heu
import palmed.benchmarks as bench
from multiprocessing import Process
from scipy.cluster import hierarchy
from sklearn.cluster import KMeans  # type: ignore
from sklearn.metrics import silhouette_score  # type: ignore
from typing import Set, List, Dict, Tuple, Type, NamedTuple, Optional, cast
from palmed.messages import ProgressBar
from palmed.messages import *
from palmed.config import Config

logger = logging.getLogger(__name__)


def balanced_latin_squares(n: int) -> List[List[int]]:
    # Extracted from https://gist.github.com/nickdelgrosso/e7e82ed3a8a825274c5ff4bbaf5439ba
    l = [
        [((j // 2 + 1 if j % 2 else n - j // 2) + i) % n for j in range(n)]
        for i in range(n)
    ]
    return l


# Extract some slices from the latin squares
def sample_index(size: int, threshold: int = 3) -> List[List[bool]]:
    lat_sq = balanced_latin_squares(size)
    selected_values = [int(i * (size / threshold)) for i in range(threshold)]
    ret = [[(i in selected_values) for i in j] for j in lat_sq]
    return ret


def gen_subslice_worker(
    isn_list: List[ins.Instruction],
    core: int,
    save_file: str,
    index: int,
    pattern: List[bool],
    progress: ProgressBar,
):
    os.sched_setaffinity(0, [core])
    benchs_list: List[bench.Benchmark] = []
    logger.debug("Thread %d computing benchs for %s", core, str(isn_list[index]))
    for idx, instr in enumerate(isn_list):
        benchs_list += [
            bench.Benchmark(
                isn_list[index],
                isn_list[index].IPC(),
                instr,
                instr.IPC(),
            )
        ]

        if pattern[idx]:
            benchs_list[-1].IPC(core)
            progress.update()
    with open(save_file, "wb") as sav:
        pickle.dump(benchs_list, sav)
    return


def gen_subslice(
    isn_list: List[ins.Instruction],
    start_idx: int,
    end_idx: int,
    core: int,
    save_file: str,
):
    pattern = sample_index(len(isn_list), threshold=Config.NB_BENCH_PER_INSN)[
        start_idx:end_idx
    ]
    i = start_idx
    while i < end_idx:
        try:
            with open(save_file + f".{i}", "rb") as sav:
                _ = pickle.load(sav)
        except FileNotFoundError:
            break
        except pickle.UnpicklingError:
            break
        i += 1

    if end_idx != i:
        # No need for further subprocesses, as we use latin squares
        time.sleep(core / 100)  # good old sleepSort of threads for a nice output
        iterator = ProgressBar(
            total=(end_idx - i) * Config.NB_BENCH_PER_INSN,
            desc=f"Comput. Benchs. #{str(core).zfill(2)}",
            position=core - 1,
        )
        for idx in range(i, end_idx):
            gen_subslice_worker(
                isn_list,
                core,
                f"{save_file}.{idx}",
                idx,
                pattern[idx - i],
                iterator,
            )
        iterator.close()


def compute_benchs(
    isn_list: List[ins.Instruction],
    save_file: str,
    num_cpu: int,
) -> List[List[bench.Benchmark]]:
    """Launch the muti-threaded computation of the values designated by several
    latin square patterns.
    Returns a 2D-matrix of benchmarks, computing the IPCs of only a sample."""

    pattern = None
    if not Config.DENSE:
        pattern = sample_index(len(isn_list), threshold=Config.NB_BENCH_PER_INSN)
        logger.info(
            "Computing a sample of %d benchmarks per instruction...",
            Config.NB_BENCH_PER_INSN,
        )

    bench_list: List[List[bench.Benchmark]] = []
    for row_instr in ProgressBar(
        isn_list, desc="Generating quadratic benchmarks", total=len(isn_list)
    ):
        row_ipc = row_instr.IPC()

        bench_list.append(
            [
                bench.Benchmark(
                    row_instr,
                    row_ipc,
                    col_instr,
                    col_instr.IPC(),
                )
                for col_instr in isn_list
            ]
        )

    bench.compute_benchs(
        bench_list,
        save_file,
        num_cpu,
        sample_select=pattern,
        symmetric_benchs=True,
    )
    return bench_list


class DenseReconstructedCheckpoint(NamedTuple):
    """Type for the pickled checkpoint data"""

    reconstructed_matr: List[List[bench.Benchmark]]


def restore_dense_matrix_checkpoint(
    checkpoint_name: str, benchs: List[List[bench.Benchmark]]
) -> bool:
    checkpoint_path = Path(checkpoint_name)
    if not checkpoint_path.is_file():
        return False
    logger.info("Trying to restore the full matrix from %s...", checkpoint_name)
    try:
        with open(checkpoint_path, "rb") as handle:
            checkpoint: DenseReconstructedCheckpoint = pickle.load(handle)
    except (EOFError, FileNotFoundError, pickle.UnpicklingError):
        logger.error("Could not restore from %s:", checkpoint_name)
        traceback.print_exc()
        return False

    # Check that the saved matrix is conform
    for in_row, chk_row in zip(benchs, checkpoint.reconstructed_matr):
        for in_bench, chk_bench in zip(in_row, chk_row):
            if in_bench != chk_bench or not chk_bench.has_IPC():
                return False
    # Restore IPCs
    for in_row, chk_row in zip(benchs, checkpoint.reconstructed_matr):
        for in_bench, chk_bench in zip(in_row, chk_row):
            in_bench.set_IPC(chk_bench.IPC(), commitable=False)
    logger.info("Restored the full matrix from %s.", checkpoint_name)
    return True


def save_dense_matrix_checkpoint(
    checkpoint_name: str, benchs: List[List[bench.Benchmark]]
):
    logger.info("Saving the full matrix to %s", checkpoint_name)
    picklable_result = DenseReconstructedCheckpoint(reconstructed_matr=benchs)
    with open(checkpoint_name, "wb") as handle:
        pickle.dump(picklable_result, handle)


def infer_missing_IPC(benchs: List[List[bench.Benchmark]], checkpoint_name: str):
    """Reconstruct the mappping from the Latin Squares computed values"""

    # Trying to restore from `checkpoint_path`, if existing
    if restore_dense_matrix_checkpoint(checkpoint_name, benchs):
        return

    # Could not restore; computing it.
    logger.info("Symetrising matrix...")
    for i in range(len(benchs)):
        for j in range(len(benchs)):
            if benchs[i][j].has_IPC():
                benchs[j][i].set_IPC(benchs[i][j].IPC(), commitable=True)
    logger.info("Converting benchs to numpy representation")
    benchs_np = np.array(
        [
            [b.IPC() if b.has_IPC() else np.nan for b in bench_row]
            for bench_row in benchs
        ]
    )

    logger.info("Reconstructing matrix (can take a long time...)")
    # Should silence log of CUDA loading when the function is not called
    try:
        import fancyimpute as fi  # type: ignore
    except ImportError as exn:
        logger.critical(
            "Cannot reconstruct the matrix: missing optional requirement "
            "fancyimpute >= 0.55`. Please `pip install` it an run again."
        )
        raise Exception("Missing requirement: fancyimpute >= 0.55") from exn

    benchs_np = fi.IterativeSVD(
        min_value=Config.MIN_BENCH_COEF,
        convergence_threshold=1e-6,
        max_iters=500,
    ).fit_transform(benchs_np)

    bar = ProgressBar(total=len(benchs_np), desc="Copying coefs to global benchs")
    for bench_row_np, bench_row in zip(benchs_np, benchs):
        for ipc_b_np, b in zip(bench_row_np, bench_row):
            b.set_IPC(ipc_b_np, commitable=False)
        bar.update()
    bar.close()


def chose_and_apply_kmeans(
    array: np.ndarray, min_nb: int, max_nb: int
) -> Tuple[np.ndarray, np.ndarray, float]:
    minsil = 0.0
    min_idx = -1
    for i in ProgressBar(range(min_nb, max_nb + 1), desc="Computing K-Means"):
        kmeans = KMeans(n_clusters=i, random_state=0, max_iter=1000000).fit(array)
        sil = silhouette_score(array, kmeans.labels_)
        if sil < minsil or min_idx == -1:
            minsil = sil
            min_idx = i
            save_kmeans = kmeans
    return save_kmeans.labels_, save_kmeans.cluster_centers_, minsil


def centroids_from_clustering(
    ipc_matrix: np.ndarray,
    clustering: List[int],
) -> np.ndarray:
    """For each cluster defined in `clustering`, computes a centroid, and returns a
    matrix of those centroids. The representative instruction for the class will be the
    instruction closest to this centroid.
    For now, this computes the point-to-point average vector.
    Might be worth enhancing later."""

    classes: List[List[int]] = [[] for _ in range(len(clustering))]
    averages: List[float] = []
    for elt_id, cls_id in enumerate(clustering):
        classes[cls_id].append(elt_id)

    for elts in classes:
        if not elts:
            continue
        avg = sum(map(lambda x: ipc_matrix[x], elts)) / len(elts)
        averages.append(avg)

    return np.array(averages)


# This should be a local class, but mypy crashes when used that way


class PartialClustering(NamedTuple):
    """When building a clustering by iteratively merging two clusters, this class
    is used to record a partial clustering, in order to continue merging
    iteratively to get a more merged state from a less merged one."""

    parent_for: List[int]
    clustering_bound: int


def compute_hierarchical_clustering_classes(
    ipc_matrix: np.ndarray, min_classes: int, max_classes: int
) -> Tuple[List[int], np.ndarray, float]:
    """Clusters the instructions using hierarchical clustering, optimizing on the
    silhouette to find the best cut value"""

    linkage = hierarchy.linkage(ipc_matrix, method="ward")

    def clusters_for_cut_count(
        count: int, partial: Optional[PartialClustering] = None
    ) -> Tuple[List[int], PartialClustering]:
        start_from = 0
        if partial and partial.clustering_bound <= count:
            parent_for: List[int] = partial.parent_for
            start_from = partial.clustering_bound
        else:
            parent_for = [i for i in range(len(ipc_matrix) + len(linkage))]

        def union(i, j, merged):
            while parent_for[merged] != merged:
                merged = parent_for[merged]
            parent_for[i] = merged
            parent_for[j] = merged

        def find(i):
            if parent_for[i] == i:
                return i
            parent = find(parent_for[i])
            parent_for[i] = parent
            return parent

        for row_id, merge in enumerate(linkage[start_from:count]):
            merge_id = row_id + start_from + len(ipc_matrix)
            union(int(merge[0]), int(merge[1]), merge_id)

        label_for: List[int] = [find(i) for i in range(len(ipc_matrix))]
        labelset = list(set(label_for))
        labelset.sort()
        relabel = {old_label: new_label for new_label, old_label in enumerate(labelset)}
        for pos, old_label in enumerate(label_for):
            label_for[pos] = relabel[old_label]

        return label_for, PartialClustering(
            parent_for=parent_for, clustering_bound=count
        )

    min_cutid = max(1, len(ipc_matrix) - max_classes)
    max_cutid = len(ipc_matrix) - min_classes
    assert max_cutid > 1

    silhouettes: List[Optional[float]] = [None] * len(linkage)

    # First pass: compute one result out of three on the given range
    last_partial: Optional[PartialClustering] = None
    for pos in ProgressBar(
        range(min_cutid, max_cutid + 1, 3), desc="Optimizing silhouette (first pass)"
    ):
        clustering, last_partial = clusters_for_cut_count(pos, last_partial)
        silhouettes[pos] = silhouette_score(ipc_matrix, clustering)

    # Get the amplitude
    filled_silhouettes: List[float] = []
    for elt in silhouettes:
        if elt is not None:
            filled_silhouettes.append(elt)
    min_val = min(filled_silhouettes)
    max_val = max(filled_silhouettes)
    amplitude = max_val - min_val
    top_threshold = min_val + 0.8 * amplitude

    # Gather relevant ranges
    intervals = []
    cur_start = None
    nb_iter = 0
    for pos, elt in enumerate(silhouettes):
        if elt is not None:
            if cur_start is None and elt >= top_threshold:
                cur_start = pos
            elif cur_start is not None and elt < top_threshold:
                intervals.append(range(cur_start, pos + 1))
                nb_iter += len(intervals[-1])
                cur_start = None
    if cur_start is not None:
        intervals.append(range(cur_start, max_cutid))
        nb_iter += len(intervals[-1])

    # Fill the selected ranges
    def iterate_intervals(intervals):
        for relevant_range in intervals:
            for pos in relevant_range:
                yield pos

    last_partial = None
    for pos in ProgressBar(
        iterate_intervals(intervals),
        desc="Optimizing silhouette (second pass)",
        total=nb_iter,
    ):
        if silhouettes[pos] is None:
            clustering, last_partial = clusters_for_cut_count(pos, last_partial)
            silhouettes[pos] = silhouette_score(ipc_matrix, clustering)

    # Generating clustering
    max_id: Optional[int] = None
    for pos, val in enumerate(silhouettes):
        if val is not None:
            if max_id is None or val > cast(float, silhouettes[max_id]):
                # ^^^ cast is safe: silhouettes[max_id] is always non-None
                # there is no clean way that I see to make this cast-free
                max_id = pos
    assert max_id is not None

    clustering, _ = clusters_for_cut_count(max_id)
    centers = centroids_from_clustering(ipc_matrix, clustering)
    best_silhouette = silhouettes[max_id]
    assert best_silhouette is not None

    return (clustering, centers, best_silhouette)


# Use hierarchical clustering to infer classes from the benchs
def compute_classes(
    instrs: List[ins.Instruction],
    benchs: List[List[bench.Benchmark]],
    num_cpu: int,
) -> List[ins.Instruction]:
    logger.info("Converting benchs to numpy representation")
    benchs_np = np.array(
        [
            [b.IPC() if b.has_IPC() else np.nan for b in bench_row]
            for bench_row in benchs
        ]
    )
    clusters, centers, minsil = compute_hierarchical_clustering_classes(
        benchs_np, Config.MIN_CLASSES, Config.MAX_CLASSES
    )
    logger.info(
        "KMeans chosed %d classes, silhouhette %f",
        len(centers),
        minsil,
    )
    classes_idx: List[int] = []
    num_in_class: List[int] = []
    for point in centers:
        num_in_class += [0]
        min_dst = np.sum((benchs_np[0] - point) ** 2)
        nearest = 0
        for idx, potential in enumerate(benchs_np):
            pot_dst = np.sum((potential - point) ** 2)
            if pot_dst < min_dst:
                nearest = idx
                min_dst = pot_dst
            if clusters[idx] == len(classes_idx):
                num_in_class[-1] += 1
        classes_idx += [nearest]
    class_repr: List[ins.Instruction] = [instrs[i] for i in classes_idx]
    logger.info("Found %d classes", len(class_repr))
    return class_repr
