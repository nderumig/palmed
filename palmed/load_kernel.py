## \file load_kernel.py
## \brief Load a kernel of instructions from various formats. Used with `--override-kernel`.

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import sys
from pathlib import Path
import string
import subprocess
import logging
from functools import wraps
import yaml
from typing import Set, List, Dict, Tuple, Optional
import palmed.benchmarks as bench
from palmed import instructions
from .instructions import Instruction
from .instructions import Instruction_Pipedream
from .heuristics import add_instr_to_kernel
from .config import Config
import pipedream.asm.ir as pipeasm
from pipedream.disasm.capstone import utils as pipe_capstone
from pipedream.disasm.capstone import disassembler as pipe_capstone_ds


logger = logging.getLogger(__name__)


class LoadKernelException(Exception):
    def __init__(self, what):
        self.what = what

    def __str__(self):
        return "Cannot load kernel: {}".format(self.what)


class ExtractXEDCall:
    """Wrapper around a call to the external binary `extract-xed-instruction-database`"""

    EXTR_XED_PATH = (
        Path(os.path.dirname(os.path.abspath(__file__)))
        / "../pipedream/tools/extract-xed-instruction-database/extract-xed-instruction-database"
    )

    def __init__(self, path, omit_unbenchable=False):
        self.path = path
        self.omit_unbenchable = omit_unbenchable

    def call_stream(self, stream):
        """Call with a python stream object as stdin"""
        self.process_out = subprocess.run(
            [self.EXTR_XED_PATH.as_posix(), "disasm"],
            stdin=stream,
            stdout=subprocess.PIPE,
        )
        return self.result()

    def call_pipe(self, in_bytes):
        """Call by streaming python bytes to stdin"""
        self.process_out = subprocess.run(
            [self.EXTR_XED_PATH.as_posix(), "disasm"],
            input=in_bytes,
            stdout=subprocess.PIPE,
        )
        return self.result()

    def _instr_to_pipedream(self, instr_name: str) -> Optional[Instruction_Pipedream]:
        """Convert instr_name to its matching pipedream instruction. If it cannot be
        converted and `omit_unbenchable == True`, returns `None` instead."""
        if self.omit_unbenchable:
            try:
                return Instruction_Pipedream(instr_name)
            except instructions.InstructionNotFound:
                return None
        return Instruction_Pipedream(instr_name)

    def result(self):
        if self.process_out.returncode != 0:
            raise LoadKernelException(
                (
                    "extract-xed-instruction-database exited with non-zero return code "
                    "{retcode} while parsing input file {path}."
                ).format(
                    retcode=self.process_out.returncode,
                    path=self.path,
                )
            )
        decoded_raw = self.process_out.stdout.decode("utf-8").strip().split("\n")
        return list(
            filter(
                lambda instr: instr is not None,
                map(self._instr_to_pipedream, decoded_raw),
            )
        )
        # return [Instruction_Pipedream(instr_name) for instr_name in decoded_raw]


def cs_disasm_to_pipedream(
    insn_bytes: bytes, isa: Config.SUPPORTED_ISA
) -> List[Instruction_Pipedream]:
    """Converts raw instruction bytes to a list of Pipedream instructions, using
    Pipedream built-in capstone-based disassembler"""
    arch: pipeasm.Architecture = pipeasm.Architecture.for_name(isa.value)
    disas = pipe_capstone_ds.Disassembler(arch).get_binary_dis(insn_bytes, skip=True)
    out = []
    for insn in disas:
        pipe_name = pipe_capstone.cs_insn_class_pipedream(insn.cs_insn)
        if not pipe_name:
            logger.warning(
                "Cannot decode insn %s %s [%s]",
                insn.cs_insn.mnemonic,
                insn.cs_insn.op_str,
                insn.cs_insn.bytes.hex(),
            )
            continue
        out.append(Instruction_Pipedream(pipe_name))
    return out


def bytes_to_pipedream(
    insn_bytes: bytes, path: str, isa: Optional[Config.SUPPORTED_ISA] = None
) -> List[Instruction_Pipedream]:
    """Converts raw instruction bytes to a list of Pipedream instructions"""
    if isa is None:
        isa = Config.SUPPORTED_ISA(Config.ISA)

    if isa == Config.SUPPORTED_ISA.X86:
        return ExtractXEDCall(path, True).call_pipe(insn_bytes)
    if isa == Config.SUPPORTED_ISA.ARMv8a:
        return cs_disasm_to_pipedream(insn_bytes, isa)
    raise LoadKernelException(f"Unsupported ISA: {isa}")


def open_file(func):
    @wraps(func)
    def wrapper(path, *args, **kwargs):
        if path == "-":
            return func(sys.stdin, "stdin", *args, **kwargs)
        try:
            with open(path, "r") as handle:
                return func(handle, path, *args, **kwargs)
        except FileNotFoundError as exn:
            raise LoadKernelException("cannot open file: {}".format(exn))

    return wrapper


@open_file
def load_pipedream_kernel_description(handle, path: str) -> List[Instruction_Pipedream]:
    """Load a kernel composed of pipedream instruction names

    Yaml syntax example:
    - MOV_GPR64i64_GPR64i64
    - instr: MOV_GPR64i64_MEM64i64
      rep: 4
    yields one reg-reg MOV and four reg-mem MOV
    """
    yaml_data = yaml.safe_load(handle)
    if not isinstance(yaml_data, list):
        raise LoadKernelException(
            "bad yaml file. Your file must describe a list of Pipedream instructions."
        )
    out = []
    for entry in yaml_data:
        rep = 1
        if isinstance(entry, str):
            instr = entry
        else:
            instr = entry["instr"]
            rep = entry["rep"]

        pipedream_instr = Instruction_Pipedream(instr)
        out += [pipedream_instr for _ in range(rep)]
    return out


@open_file
def load_hex_kernel_description(handle, path: str) -> List[Instruction]:
    """Load a kernel from the hexadecimal representation of instructions"""
    hex_instrs = handle.read()

    raw_bytes = b""
    nibble = None
    for ch in hex_instrs:
        if ch in string.whitespace:
            continue
        if ch in string.hexdigits:
            if nibble:
                cur_byte = bytes.fromhex(nibble + ch)
                nibble = None
                raw_bytes += cur_byte
            else:
                nibble = ch
        else:
            raise LoadKernelException(
                "bad character in hex representation: {}".format(ch)
            )
    if nibble:
        raise LoadKernelException(
            "odd number of hex digits in hex representation, leaving unfinished nibble"
        )

    return ExtractXEDCall(path).call_pipe(raw_bytes)


@open_file
def load_raw_kernel_description(handle, path: str) -> List[Instruction]:
    """Load a kernel from the raw bytes representation of instructions"""
    return ExtractXEDCall(path).call_stream(handle)


def load_kernel_override(path: str, format_type: str) -> List[Instruction]:
    """
    Entrypoint to load a kernel of given instructions,
    in the given format
    """

    load_for_format = {
        "pipedream": load_pipedream_kernel_description,
        "hex": load_hex_kernel_description,
        "raw": load_raw_kernel_description,
    }
    if format_type not in load_for_format:
        logger.error("Invalid kernel override format: %s", format_type)
        raise LoadKernelException("bad kernel override type '{}'".format(format_type))
    kernel: List[Instruction] = load_for_format[format_type](path)

    logger.info("Kernel is composed of %d instructions", len(kernel))
    logger.info("Kernel:")
    for instr in kernel:
        logger.info("\t%s (%s, IPC = %f)", instr.name(), instr._ports_str, instr.IPC())

    return kernel
