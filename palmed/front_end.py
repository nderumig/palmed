## \file fonrt-end.py
## \brief Simulates benchmarks when interleaving a variable number of instructions
"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2023  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import pipedream.utils.abc as abc
from typing import (
    Any,
    Callable,
    Dict,
    Set,
    List,
    Dict,
    Tuple,
    Optional,
    Union,
    Iterable,
    Iterator,
    NamedTuple,
    Sequence,
    cast,
)

import a72_frontend as a72_fe

logger = logging.getLogger(__name__)


class FrontEndError(Exception):
    """An error occurring during the computation of the front-end IPC"""

    def __init__(self, what):
        super().__init__()
        self.what = what

    def __str__(self):
        return self.what


class DummyFrontEnd:

    def __init__(self, mingled_instructions: List[str]):
        self._mingled_instr = mingled_instructions

    @abc.abstractmethod
    def IPC(self) -> float:
        return 0


class NoFrontEnd(DummyFrontEnd):

    @abc.override
    def IPC(self) -> float:
        return 0


class FixedFrontEndClass(DummyFrontEnd):
    INST_PER_CYCLE: int

    @abc.override
    def IPC(self) -> float:
        return float(len(self._mingled_instr)) / self.__class__.INST_PER_CYCLE


def FixedFrontEnd(inst_per_cycle: int) -> type[FixedFrontEndClass]:
    FixedFrontEndClass.INST_PER_CYCLE = inst_per_cycle
    return FixedFrontEndClass


class A72FrontEnd(DummyFrontEnd):
    FRONTEND_MODEL: type[a72_fe.generic.BaseFrontend]

    @abc.abstractmethod
    def FrontendCarac(self) -> float:
        try:
            frontend_carac = self.__class__.FRONTEND_MODEL(self._mingled_instr)
            frontend_cycles = frontend_carac.cycles()
        except a72_fe.generic.InstructionNotFound as exn:
            logger.info("Palmed frontend cannot bench instruction %s", exn)
            raise FrontEndError(
                f"Palmed frontend error: cannot bench instruction {exn}"
            ) from exn
        return float(frontend_cycles)

    @abc.override
    def IPC(self) -> float:
        ret = 0.0
        try:
            ret = len(self._mingled_instr) / self.FrontendCarac(self._mingled_instr)
        except:
            pass
        return ret


class A72DispTenseFrontEnd(A72FrontEnd):
    FRONTEND_MODEL = a72_fe.dispatch_model.TenseDispatchModel


class A72DispSlackFrontEnd(A72FrontEnd):
    FRONTEND_MODEL = a72_fe.dispatch_model.SlackDispatchModel


class A72UopLinearFrontEnd(A72FrontEnd):
    FRONTEND_MODEL = a72_fe.uop_nocross.PureLinearModel


class A72UopNoCrossFrontEnd(A72FrontEnd):
    FRONTEND_MODEL = a72_fe.uop_nocross.UopNoCrossModel
