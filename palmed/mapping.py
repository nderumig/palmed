## \file mapping.py
## \brief Infer a resource mapping from the instructions: first clusters the instructions
## into the equivalence class depending on their behaviour, then use gurobi
## to infer a resource mapping between a "small" set of instructions.
## then fix the nember of resources that were found, and run gurobi again one time per
## other remaining instruction to deduce their own mapping

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import pickle
import subprocess
import sys
import logging
import palmed.instructions as ins
import palmed.benchmarks as bench
import palmed.export2gus as e2g
import palmed.heuristics as heu
import palmed.sol2output as s2o
import palmed.solver as sol
import palmed.quadra as qu
from typing import (
    Any,
    Set,
    List,
    Dict,
    Tuple,
    Optional,
    Union,
    ItemsView,
    Iterable,
    TypeVar,
    cast,
    DefaultDict,
)
from palmed.messages import *
from palmed import load_kernel
from palmed.config import Config, ISA_Extension
from palmed.checkpoints import PalmedStep, checkpoint
from palmed.pinned_pool import select_multiprocessing_pool, SingleCorePool
from palmed.pipedream_config import RemotePipedream, configure_pipedream
from palmed import db

logger = logging.getLogger(__name__)


def split_ipc(
    instr_list: List[ins.Instruction],
) -> Tuple[List[ins.Instruction], List[ins.Instruction]]:
    high_ipc: List[ins.Instruction] = []
    low_ipc: List[ins.Instruction] = []
    to_del: List[ins.Instruction] = []
    for instr in instr_list:
        if instr.IPC() >= 1.0 - Config.CLASSES_COEF * Config.MIN_BENCH_COEF:
            high_ipc.append(instr)
        elif instr.IPC() >= Config.CLASSES_COEF * Config.MIN_BENCH_COEF:
            low_ipc.append(instr)
        else:
            to_del.append(instr)
    for instr in to_del:
        instr_list.remove(instr)
    return (high_ipc, low_ipc)


@checkpoint(PalmedStep.HIGH_IPC_CLASSES)
def get_high_ipc_classes(
    instructions_isa: Dict[ISA_Extension, List[ins.Instruction]],
) -> Tuple[Dict[ISA_Extension, List[ins.Instruction]], List[ins.Instruction]]:
    assert ins.ISA_BASE in instructions_isa
    all_instructions: List[ins.Instruction] = []
    classes_high_ipc: Dict[ISA_Extension, List[ins.Instruction]] = {}

    for isa, instructions in instructions_isa.items():
        benchs_high_ipc: List[List[bench.Benchmark]] = []
        all_instructions += instructions
        high_ipc, _ = split_ipc(instructions)
        logger.info("%s: found %d instructions with IPC >= 1", isa.value, len(high_ipc))
        if len(instructions) == 0:
            continue
        if Config.PIPEDREAM:
            benchs_high_ipc = qu.compute_benchs(
                high_ipc,
                Config.CHECKPOINT.get_save_file(PalmedStep.QUADRA_SPARSE)
                + f".{isa.value}",
                Config.effective_num_cpu(),
            )
            if not Config.DENSE:
                qu.infer_missing_IPC(
                    benchs_high_ipc,
                    Config.CHECKPOINT.get_save_file(PalmedStep.QUADRA_DENSE),
                )
            qu.save_dense_matrix_checkpoint(
                Config.CHECKPOINT.get_save_file(PalmedStep.QUADRA_DENSE)
                + f".{isa.value}",
                benchs_high_ipc,
            )

            classes_high_ipc[isa] = qu.compute_classes(
                high_ipc,
                benchs_high_ipc,
                Config.effective_num_cpu(),
            )
        else:
            benchs_high_ipc = bench.gen_benchs(high_ipc)
            bench.compute_benchs(
                benchs_high_ipc,
                Config.CHECKPOINT.get_save_file(PalmedStep.QUADRA_DENSE) + f".{isa}",
                Config.effective_num_cpu(),
                symmetric_benchs=True,
            )
            classes_high_ipc[isa], _ = heu.compute_classes(high_ipc, benchs_high_ipc)
            logger.info(
                "%s: Found %d classes from %d instructions",
                isa,
                len(classes_high_ipc[isa]),
                len(high_ipc),
            )
    return classes_high_ipc, all_instructions


def generate_benchs_and_classes_from_sat(
    sat_benchs: Dict[str, bench.Benchmark],
    instructions: List[ins.Instruction],
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]],
) -> Tuple[
    Dict[ins.Instruction, List[bench.Benchmark]],
    Dict[ins.Instruction, List[ins.Instruction]],
]:
    kernel_instr = mapping.keys()
    instr_to_bench: Dict[ins.Instruction, List[bench.Benchmark]] = {}
    all_benchs: List[List[bench.Benchmark]] = [[] for _ in instructions]
    for i, instr in ProgressBar(
        enumerate(instructions),
        total=len(instructions),
        desc="Generate non-kernel benchs",
    ):
        assert len(all_benchs[i]) == 0
        add_sat_bench(
            [instr], all_benchs[i], sat_benchs, verify=False, add_dummies=True
        )
        all_benchs[i].append(bench.Benchmark(instr, IPC=instr.IPC(), lcm=1))
        instr_to_bench[instr] = all_benchs[i]
    bench.compute_benchs(
        all_benchs,
        Config.CHECKPOINT.get_save_file(PalmedStep.SATURATED_BENCHS),
        Config.effective_num_cpu(),
    )
    (classes, class_to_instrs) = heu.compute_classes(
        instructions, all_benchs, kernel_instr
    )

    # Remove doubles, i.e. instructions part of the original mapping grouped into
    # the same class
    to_del: List[ins.Instruction] = []
    for instr in kernel_instr:
        # `instr` is not in the class representants: then it has been gathered in
        # another class, thus using its own representant (=> different from itself) in
        # `mapping`.
        if instr not in classes:
            to_del.append(instr)
    for instr in to_del:
        mapping.pop(instr)

    assert mapping.keys() <= set(classes)

    classes_to_benchs: Dict[ins.Instruction, List[bench.Benchmark]] = {}
    to_compute: List[List[bench.Benchmark]] = []
    for instr in classes:
        to_compute.append(instr_to_bench[instr])
    bench.compute_benchs(
        to_compute,
        Config.CHECKPOINT.get_save_file(PalmedStep.SATURATED_BENCH_REDUCED),
        Config.effective_num_cpu(),
    )
    for instr, benchs in zip(classes, to_compute):
        classes_to_benchs[instr] = benchs
    return (classes_to_benchs, class_to_instrs)


# Generate benchs when the kernel has been manually specified
@checkpoint(PalmedStep.KERNEL_BENCHS)
def gen_override_kernel_benchs(kernel: List[ins.Instruction]) -> List[bench.Benchmark]:
    kernel_benchs: List[bench.Benchmark] = []
    for instr in ProgressBar(kernel, desc="Generating IPC for kernel's intructions"):
        instr.IPC()
    bar = ProgressBar(
        total=int(len(kernel) * (len(kernel) - 1) / 2),
        desc="Generating overriden kernel's benchmarks",
    )
    kernel_tmp: List[ins.Instruction] = []
    with select_multiprocessing_pool()(Config.effective_num_cpu()) if Config.PIPEDREAM else SingleCorePool():  # type: ignore
        for instr in kernel:
            heu.add_instr_to_kernel(instr, kernel_tmp, kernel_benchs, progress_bar=bar)
    bar.close()

    if Config.ISA == Config.SUPPORTED_ISA.X86.value:
        for ben in kernel_benchs:
            try:
                if (abs(ben.IPC() - ben.IPC_UOPS_from_pipedream()) > 0.01) and (
                    Config.UOPS or (ben.IPC() != ben.max_IPC())
                ):
                    logger.warning("Benchmark differs with uops.info's result:")
                    logger.warning("%s", ben.human_str())
                    logger.warning("Measured (rounded) IPC: %f", ben.IPC())
                    logger.warning(
                        "uops.info's IPC:        %s", ben.IPC_UOPS_from_pipedream()
                    )
                    logger.warning("Max IPC:                %f", ben.max_IPC())
            except ins.InstructionNotFound:
                logger.info(
                    "Instruction %s not found in uops.info's results, "
                    "cannot test benchs...",
                    instr.name(),
                )
    return kernel_benchs


def coalesce_mapping(
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]],
    class_to_instrs: Dict[ins.Instruction, List[ins.Instruction]],
):
    """Given the final mapping, coalesce classes that have the same edges."""
    # We are supposed to have only a few classes, hence quadratic is acceptable.

    logger.info("Coalescing classes...")

    mapping_dict: Dict[ins.Instruction, Dict[str, float]] = {
        instr: {resource: value for (value, resource) in edges}
        for (instr, edges) in mapping.items()
    }

    instrs: List[ins.Instruction] = list(mapping.keys())
    coalesce_list: List[Tuple[ins.Instruction, ins.Instruction]] = []
    merged: Set[ins.Instruction] = set()

    for pos1, instr1 in enumerate(instrs):
        if instr1 in merged:
            continue

        for instr2 in instrs[pos1 + 1 :]:
            if instr2 not in merged and mapping_dict[instr1] == mapping_dict[instr2]:
                merged.add(instr2)
                coalesce_list.append((instr1, instr2))

    for instr1, instr2 in coalesce_list:
        class_to_instrs[instr1].append(instr2)
        class_to_instrs[instr1] += class_to_instrs[instr2]
        class_to_instrs.pop(instr2)
        mapping.pop(instr2)

    logger.info("Coalesced %d classes.", len(coalesce_list))


# For each resource, add the bench of every instruction that uses this resource,
# repeated a number of times equal to their IPC
def add_common_resource_benchs(
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]],
    list_benchs: List[bench.Benchmark],
) -> List[bench.Benchmark]:
    res_to_instrs = s2o.mapping_to_res_to_instrs(mapping)
    new_benchs: List[bench.Benchmark] = []

    # We want to add benchmarks of instructions using this resources, but not when
    # several different extensions are used simultaneously. Therefore, we add
    # `ISA_BASE` + ext for every ext used and ISA_BASE instrs alone.

    # First, splitting the instructions depending on their extension
    ext_to_kernel_instr: DefaultDict[ISA_Extension, List[ins.Instruction]] = (
        DefaultDict(list)
    )
    for res, list_instr in res_to_instrs.items():
        # extension -> (future) benchmark
        # initialisation: first, the benchs contains nothing
        ext_to_benchs: Dict[ISA_Extension, List[Union[float, ins.Instruction]]] = {}
        for instr in mapping.keys():
            if instr.extension != ins.ISA_BASE:
                ext_to_benchs[instr.extension] = []
        base_bench: List[Union[float, ins.Instruction]] = []

        for instr in list_instr:
            ext = instr.extension
            # if it is a base instruction, adding it to every possible benchs
            if ext == ins.ISA_BASE:
                base_bench.append(instr)
                base_bench.append(instr.IPC())
                for ext in ext_to_kernel_instr.keys():
                    ext_to_benchs[ext].append(instr)
                    ext_to_benchs[ext].append(instr.IPC())
            else:
                # else, adding it to just the bench of its extension
                ext_to_benchs[ext].append(instr)
                ext_to_benchs[ext].append(instr.IPC())

        if len(base_bench) != 0:
            new_bench = bench.Benchmark(*base_bench)
            new_benchs.append(new_bench)
        for args in ext_to_benchs.values():
            if len(args) != 0:
                new_bench = bench.Benchmark(*args)
                new_benchs.append(new_bench)

    # Remove doubles
    new_benchs = list(set(new_benchs))

    # Do not add twice the same constraints
    to_remove: List[int] = []
    logger.debug("Adding common resource bench:")
    for idx, new_bench in enumerate(new_benchs):
        is_new = True
        for old_bench in list_benchs:
            if new_bench == old_bench:
                is_new = False
                break
        else:
            logger.debug("\t%s", new_bench.human_str())

        if is_new is False:
            to_remove = [idx] + to_remove
    for idx in to_remove:
        del new_benchs[idx]
    if len(new_benchs) == 0:
        logger.info("No further common benchmarks to add, executing next step...")
        return []

    if Config.ISA == Config.SUPPORTED_ISA.X86.value:
        for ben in ProgressBar(
            new_benchs, desc="Computing IPC for common resource benchs"
        ):
            ben.IPC()
            try:
                if (abs(ben.IPC() - ben.IPC_UOPS_from_pipedream()) > 0.01) and (
                    Config.UOPS or (ben.IPC() != ben.max_IPC())
                ):
                    logger.warning("Benchmark differs with uops.info's result:")
                    logger.warning("%s", ben.human_str())
                    logger.warning("Measured (rounded) IPC: %f", ben.IPC())
                    logger.warning(
                        "uops.info's IPC:        %f", ben.IPC_UOPS_from_pipedream()
                    )
                    logger.warning("Max IPC:                %f", ben.max_IPC())
            except ins.InstructionNotFound:
                # `instr` is not always responsible for the error
                if instr._ports_str is None:
                    logger.info(
                        "Instruction %s not found in uops.info's results, "
                        "cannot test benchs...",
                        instr.name(),
                    )

    list_benchs += new_benchs
    return new_benchs


# Add benchs composed of:
# - an already existing bench saturating a resource, repeated 4 times
# - one single instruction of the kernel that is not already in the previous
# benchs
def add_sat_bench(
    kernel: List[ins.Instruction],
    benchs: List[bench.AbstractBenchmark],
    sat_benchs: Dict[str, bench.Benchmark],
    progress=False,
    verify=True,
    add_dummies=False,
):
    logger.debug("Adding saturating benchmarks:")
    iterator: Union[ProgressBar, ItemsView[str, bench.Benchmark]]
    if progress:
        iterator = ProgressBar(sat_benchs.items(), desc="Adding new benchmarks")
    else:
        iterator = sat_benchs.items()
    nb_zero = 0
    for instr in kernel:
        if instr.IPC() == 0:
            nb_zero += 1
            continue
        for res, ben in iterator:
            new_bench: bench.AbstractBenchmark = bench.DummyBenchmark()
            if all(
                map(
                    lambda sat_instr: instr.is_compatible_with(sat_instr),
                    ben.instrs(),
                )
            ):
                cur_bench: bench.Benchmark = (ben * (4 * ben.IPC() / instr.IPC())).add(
                    instr
                )
                cur_bench.cap_IPC_high(ben.IPC() + instr.IPC() / (4 * ben.card()))
                new_bench = cur_bench
                """
  cap_IPC_high bound is obtained this way:
                                                                 _               _
  _   |Bs| + 1  _  |Bs| + 1   |Bs|      1     _     1     _      I         _     I
  B = --------  <  -------- = ----- + ----- = S + ----- = S + --------  =  S + -----
        C(B)        C(Bs)     C(Bs)   C(Bs)       C(Bs)         _              4 |S|
                                                              4 S C(S)

  where:
  S  = `ben` (saturating benchmark)
  I  = `instr`
  Bs = S^(4 * IPC(B)/IPC(I))
  B  = Bs + I
  _
  x  = IPC(x)
  C(x) = cycles used by x
                """

                if new_bench in benchs:
                    continue  # we won't add it twice
            elif not add_dummies:
                continue

            assert isinstance(new_bench, bench.Benchmark)
            new_bench.IPC()
            benchs.append(new_bench)

            logger.debug("\t%s", new_bench.human_str())

            try:
                if (
                    Config.ISA == Config.SUPPORTED_ISA.X86.value
                    and verify
                    and isinstance(new_bench, bench.Benchmark)
                    and (
                        abs(new_bench.IPC() - new_bench.IPC_UOPS_from_pipedream())
                        > 0.01
                    )
                    and (Config.UOPS or (new_bench.IPC() != new_bench.max_IPC()))
                ):
                    logger.warning("Benchmark differs with uops.info's result:")
                    logger.warning("%s", new_bench.human_str())
                    logger.warning("Measured (rounded) IPC: %f", new_bench.IPC())
                    logger.warning(
                        "uops.info's IPC:        %f",
                        new_bench.IPC_UOPS_from_pipedream(),
                    )
                    logger.warning("Max IPC:                %f", new_bench.max_IPC())
            except ins.InstructionNotFound:
                # `instr` is not always responsible for the error
                if instr._ports_str is None:
                    logger.info(
                        "Instruction %s not found in uops.info's results,"
                        "cannot test benchs...",
                        instr.name(),
                    )
        if nb_zero > 0:
            logger.error("Found %d instructions with an IPC of 0", nb_zero)
            sys.exit(1)


# Run the LPs to deduce the resources and their corresponding saturating
# benchamrks. WARNING: this may alter kernel and kernel_benchs
@checkpoint(PalmedStep.SATURATED)
def deduce_sat_benchs_from_kernel(
    kernel: List[ins.Instruction],
    max_cliques: List[List[ins.Instruction]],
    min_order: List[ins.Instruction],
    kernel_benchs: List[bench.Benchmark],
) -> Tuple[
    Dict[str, bench.Benchmark],
    Dict[ins.Instruction, List[Tuple[float, str]]],
    Dict[str, str],
]:
    sat_benchs: Dict[str, bench.Benchmark] = {}
    label: Dict[str, str] = {}
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]] = {}
    logger.debug("Kernel benchs:")
    for ben in kernel_benchs:
        logger.debug("    %s", ben.human_str())
    cur_iter = 1
    while True:
        if cur_iter > Config.MAX_NUM_LP1:
            logger.warning("Too many LP1 run, switching to LP2")
            break

        message = f"Running first LPs for kernel (iteration #{cur_iter})"
        logger.info(message)
        # Use ILP as LP1 for the PIPEDREAM (i.e. inexact) mode, else use
        # the exact version (UOPS mode)
        mapping, sat_benchs = sol.force_LP_kernel(
            Config.PIPEDREAM,
            kernel,
            max_cliques,
            min_order,
            None,
            kernel_benchs,
            descr=message,
        )
        logger.info("Successfully read Gurobi's first solution for kernel")

        # For debugging purposes: export the current shape of the mapping
        label = s2o.label_mapping(mapping)
        graph, _ = s2o.check_edges(mapping, label, Config.UOPS, integer=True)
        s2o.export_dot(graph, Config.MIN_BENCH_COEF)
        s2o.run_dot()

        new_benchs = add_common_resource_benchs(mapping, kernel_benchs)
        if len(new_benchs) == 0:
            break
        cur_iter += 1

    message = "Running LP2 for saturating benchs"
    logger.info(message)
    kernel_benchs_casted = cast(List[bench.AbstractBenchmark], kernel_benchs)
    add_sat_bench(kernel, kernel_benchs_casted, sat_benchs, progress=True)
    mapping, sat_benchs = sol.force_LP_kernel(
        False,
        kernel,
        [],
        [],
        mapping,
        kernel_benchs,
        time_factor=2,
        descr=message,
    )

    if Config.UOPS:
        # Should be untill stabilisation of the mapping, experimentally 3 times
        # is sufficient
        add_sat_bench(kernel, kernel_benchs_casted, sat_benchs, progress=True)
        message = "Running LP2 #2 for saturating benchs"
        logger.info(message)
        mapping, sat_benchs = sol.force_LP_kernel(
            False,
            kernel,
            [],
            [],
            mapping,
            kernel_benchs,
            time_factor=2,
            descr=message,
        )
        add_sat_bench(kernel, kernel_benchs_casted, sat_benchs, progress=True)
        message = "Running LP2 #3 for saturating benchs"
        logger.info(message)
        mapping, sat_benchs = sol.force_LP_kernel(
            False,
            kernel,
            [],
            [],
            mapping,
            kernel_benchs,
            time_factor=2,
            descr=message,
        )

    label = s2o.label_mapping(mapping)
    graph, _ = s2o.check_edges(mapping, label, Config.UOPS)

    if Config.DOT_FILE is not None:
        s2o.export_dot(graph)
        logger.info("Kernel solution successfully exported as %s", Config.DOT_FILE)
    if Config.PDF_FILE is not None:
        s2o.run_dot()
        logger.info("Kernel solution successfully converted to %s", Config.PDF_FILE)
    if Config.DEBUG:
        logger.info("Mapping found for kernel:")
        s2o.print_mapping(mapping, label, Config.MIN_BENCH_COEF)
    return sat_benchs, mapping, label


# Convert the dictionnary to re-usable structure for the individual LPs
def format_kernel_mapping(
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]],
    sat_benchs: Dict[str, bench.Benchmark],
) -> Tuple[List[Tuple[ins.Instruction, int, float]], List[int]]:
    # Instruction -> resource
    frozen_edges: List[Tuple[ins.Instruction, int, float]] = []
    resources: Set[int] = set()
    for instr, res_list in mapping.items():
        for value, res in res_list:
            resources.add(int(res[1:]))

    for instr, res_list in mapping.items():
        if bench.instr_is_in_benchs_dict(instr, sat_benchs):
            unused_res = resources.copy()
            for value, res in res_list:
                frozen_edges.append((instr, int(res[1:]), value))
                unused_res.remove(int(res[1:]))
            # Add zero edges
            for res_int in unused_res:
                frozen_edges.append((instr, res_int, 0.0))
    list_res = list(resources)
    list_res.sort()  # bonus for debug
    return (frozen_edges, list_res)


def map_all_instr_from_sat_benchs(
    sat_benchs: Dict[str, bench.Benchmark],
    instructions: List[ins.Instruction],
    sat_mapping: Dict[ins.Instruction, List[Tuple[float, str]]],
) -> Tuple[
    Dict[ins.Instruction, List[Tuple[float, str]]],
    Dict[ins.Instruction, List[ins.Instruction]],
    List[ins.Instruction],
]:
    tot_instrs = 0
    label: Dict[str, str] = {}
    logger.info("Generating mapping for non-kernel instructions")
    frozen_edges, list_res = format_kernel_mapping(sat_mapping, sat_benchs)
    mapping: Dict[ins.Instruction, List[Tuple[float, str]]] = {}
    for instr, res_int, edge in frozen_edges:
        if mapping.get(instr) is not None:
            mapping[instr].append((edge, f"R{res_int}"))
        else:
            mapping[instr] = [(edge, f"R{res_int}")]
    (classes_to_benchs, class_to_instrs) = generate_benchs_and_classes_from_sat(
        sat_benchs, instructions, mapping
    )
    iterator = ProgressBar(classes_to_benchs.items(), desc="Aux. LP")
    not_mapped = 0
    ret: List[ins.Instruction] = []
    for instr, benchs in iterator:
        tot_instrs += len(class_to_instrs[instr])
        if bench.instr_is_in_benchs_dict(instr, sat_benchs):
            logger.info(
                "Instruction %s is already in sat_benchs, not computing its mapping",
                instr.name(),
            )
            continue
        mapping[instr] = sol.LP_one_inst(
            instr, frozen_edges, benchs, list_res, Config.MIN_BENCH_COEF
        )
        if len(mapping[instr]) == 0:
            # Instruction cannot be mapped, adding it to the return list
            not_mapped += len(class_to_instrs[instr])
            logger.warning(
                "No mapping found for instruction %s using %s (IPC=%f) (representing "
                "%d instructions)",
                instr.name(),
                instr._ports_str,
                instr.IPC(),
                len(class_to_instrs[instr]),
            )
            ret.append(instr)
        else:
            logger.debug(
                "Instruction %s using %s successfuly mapped representing "
                "%d instructions",
                instr.name(),
                instr._ports_str,
                len(class_to_instrs[instr]),
            )

    assert tot_instrs == len(instructions)
    logger.info(
        "Intructions successfully mapped: %d/%d",
        len(instructions) - not_mapped,
        tot_instrs,
    )
    tot_instrs = 0
    for instrs in class_to_instrs.values():
        tot_instrs += len(instrs)

    return mapping, class_to_instrs, ret


def main(args=None) -> None:
    Config.init_from_cli_args(args)
    assert not (Config.PIPEDREAM and Config.UOPS)
    db.init_engine()
    if Config.REMOTE_BENCH:
        # Check that the machine is registered in DB
        assert db.current_machine.get_pk() is not None

    configure_pipedream()
    RemotePipedream.init_remote_pipedream_bencher(core_id=1, noremote_ok=True)

    logger.info("Parsing intructions, please wait...")
    instructions: Dict[str, List[ins.Instruction]] = {}
    ins_uops = ins.load_uops(Config.MU_ARCH, cache_IPC=True)
    if Config.PIPEDREAM:
        ins_pipe, ins_unmapped = ins.load_pipedream()
        instructions = cast(Dict[str, List[ins.Instruction]], ins_pipe)
        logger.info("Found %d unsupported pipedream instruction(s)", len(ins_unmapped))
    if Config.UOPS:
        instructions = cast(Dict[str, List[ins.Instruction]], ins_uops)
        ins_unmapped = []
    nb_instr = 0
    for _, instrs in instructions.items():
        nb_instr += len(instrs)
    logger.info("Found %d instructions", nb_instr)

    if "repl" in args and args.repl:
        try:
            logger.info("Dropping to REPL…")
            import IPython  # Do not force the user to install IPython in general

            IPython.embed(colors="neutral")
        except ImportError:
            logger.critical("Cannot import module IPython to drop to REPL.")
            sys.exit(1)
        sys.exit(0)

    kernel: List[ins.Instruction]
    kernel_benchs: List[List[bench.Benchmark]]
    max_cliques: List[List[ins.Instruction]] = []
    min_order: List[ins.Instruction] = []
    classes_high_ipc, all_instructions = get_high_ipc_classes(instructions)
    if not Config.OVERRIDE_KERNEL:
        if len(classes_high_ipc[ins.ISA_BASE]) == 0:
            logger.error("No classes found for base ISA")
            exit(1)
        kernel, kernel_benchs, max_cliques, min_order = heu.select_kernel(
            classes_high_ipc
        )
    else:
        kernel = load_kernel.load_kernel_override(
            Config.OVERRIDE_KERNEL, Config.OVERRIDE_KERNEL_FORMAT
        )
        kernel_benchs = gen_override_kernel_benchs(kernel)
    logger.info("Kernel is composed of %d instructions:", len(kernel))
    for instr in kernel:
        logger.info(
            "  %s (IPC = %f) : %s",
            instr.name(),
            instr.IPC(),
            instr._ports_str,
        )
    sat_benchs, sat_mapping, label = deduce_sat_benchs_from_kernel(
        kernel, max_cliques, min_order, kernel_benchs
    )

    def resource_key(res_name: str) -> str:
        """Supposedley fail-safe `key` method to sort resources"""
        if res_name.startswith("R"):
            try:
                res_num = int(res_name[1:])
                return f"{res_num:03d}"
            except ValueError:
                pass
        return res_name

    logger.info("Saturating Benchmarks:")
    seen: Dict[bench.Benchmark, str] = {}
    sat_benchs_res: List[str] = list(sat_benchs.keys())
    sat_benchs_res.sort(key=resource_key)

    for res in sat_benchs_res:
        b = sat_benchs[res]
        logger.info(
            "  %s: %s", label.get(res, "[NO LABEL: {}]".format(res)), b.human_str()
        )
        if seen.get(b) is None:
            seen[b] = res
        else:
            logger.warning(
                "Bench %s saturates several resources: %s and %s", b, seen[b], res
            )

    mapping, class_to_instrs, instrs_failed = map_all_instr_from_sat_benchs(
        sat_benchs, all_instructions, sat_mapping
    )
    assert mapping.keys() == class_to_instrs.keys()
    coalesce_mapping(mapping, class_to_instrs)

    logger.info("Mapping found!")
    if Config.DEBUG:
        s2o.print_mapping(mapping, label, Config.MIN_BENCH_COEF, class_to_instrs)
    graph, unseen_res = s2o.check_edges(mapping, label)
    if len(instrs_failed + ins_unmapped) > 0:
        logger.info("Adding unmapped instructions")
        s2o.add_failed_instructions(
            mapping, label, class_to_instrs, instrs_failed + ins_unmapped
        )
    if Config.UOPS:
        s2o.print_errors(
            graph, Config.MIN_BENCH_COEF, class_to_instrs, unseen_res, instructions
        )

    s2o.save_mapping(mapping, label, class_to_instrs, Config.MAPPING_FILE)
    logger.info("Finished")
