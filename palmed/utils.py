## \file utils.py
## \brief Various standalone functions usefull in diverse scripts

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import posix
import sys
import logging
import contextlib
import datetime
import math
from functools import wraps
from fractions import Fraction
from typing import Set, List, Dict, Tuple, Optional, Type, Iterable
from palmed.config import Config
from palmed.pipedream_config import RemotePipedream

logger = logging.getLogger(__name__)


## Minimum error rate, to avoid infinite loops
MIN_FRAC_ERROR_RATE = 1e-7


def get_frac(
    number: float, max_denum: Optional[int] = None, precision: Optional[float] = None
) -> Tuple[int, int]:
    """Returns a tuple `(num, den)` describing a fraction `num/den` that best
    approximates `number`. If `max_denum` is provided, `den` will be at most
    `max_denum`. If `precision` is provided, `abs(number - num/den)` will be at most
    `precision`."""
    frac = Fraction(number)
    denum_limits = []
    if max_denum:
        denum_limits.append(max_denum)
    if precision:
        # Z/(1/(2*precision)) has a number every 2*precision, thus the set of fractions
        # with a denominator at most 1/(2*precision) has at least a member
        # `precision`-close to `number`
        denum_limits.append(math.ceil(1 / (2 * precision)))
    if denum_limits:
        frac = frac.limit_denominator(min(denum_limits))
    return frac.as_integer_ratio()


def get_frac_str(
    number: float,
    max_denum=int(1 / Config.FRAC_ERROR_RATE),
    precision: Optional[float] = None,
) -> str:
    if precision is None:
        precision = Config.FRAC_ERROR_RATE
    else:
        precision = float(precision)
    num, denum = get_frac(number, max_denum)
    if num == 0:
        return "0"
    elif denum == 1:
        return str(num)
    else:
        return f"{num}/{denum}"


class NoStdout:
    """Class redirecting temporarily sys.stdout to /dev/null, optionnally
    disabling logging from a module"""

    def __init__(self, module_name: Optional[str] = None):
        self._logger = None
        if module_name is not None:
            self._logger = logging.getLogger(module_name)

    def __enter__(self):
        self._back_stdout = sys.stdout
        sys.stdout = open("/dev/null", "w")
        if self._logger is not None:
            self._oldlevel = logger.level
            self._logger.setLevel(logging.WARNING)

    def __exit__(self, *args):
        sys.stdout.close()
        sys.stdout = self._back_stdout
        if self._logger is not None:
            self._logger.setLevel(self._oldlevel)


class BenchmarkScheduler:
    """Class to be used in a `with` block to handle `set_scheduler_params` and
    `release_sched`"""

    def __init__(self, core: Optional[int] = None, nop: bool = False):
        """If `nop` is True, this will behave as a NOP, not touching the scheduler at
        all.
        Will pin this process to the given :core:, until released. If left to None
        (default), will pin to core 2 or the last core available for CPUs that have less
        that 4 cores, unless already pinned to a single, non-zero core. If -1, will
        not pin anything.
        """

        _remote = RemotePipedream.is_remote
        self.nop: bool = nop or _remote
        desired_affinity = self.get_specified_affinity(core)
        assert _remote or len(desired_affinity) == 1
        self._exec_core = next(iter(desired_affinity))
        if _remote:
            assert RemotePipedream.remote_core is not None
            self._exec_core = RemotePipedream.remote_core
        self.core = self._exec_core if self._exec_core >= 0 else -1
        self.initial_sched_param: Optional[posix.sched_param] = None
        self.initial_policy = None
        self.initial_affinity: Optional[Set[int]] = None

    @staticmethod
    def get_specified_affinity(core: Optional[int]) -> Set[int]:
        if core is None:
            current_affinity = os.sched_getaffinity(0)
            if len(current_affinity) == 1:
                return current_affinity
            max_cpu = os.cpu_count()
            assert isinstance(max_cpu, int)
            return set([min(2, max_cpu - 1)])
        elif core < 0:
            return os.sched_getaffinity(0)
        else:
            return set([core])

    def __enter__(self):
        if not self.nop:
            self.set_scheduler_params()
        return self

    def __exit__(self, *args):
        if not self.nop:
            self.release_sched()

    def get_exec_core(self):
        """The core on which this code section is actually pinned"""
        return self._exec_core

    def set_scheduler_params(self):
        self.initial_affinity = os.sched_getaffinity(0)
        self.initial_policy = os.sched_getscheduler(0)
        self.initial_sched_param = os.sched_getparam(0)

        if not self.core < 0:
            os.sched_setaffinity(0, [self.core])
            assert 0 not in os.sched_getaffinity(0) or os.cpu_count() == 1
        try:
            os.nice(-40)
            max_priority = os.sched_param(
                os.sched_get_priority_max(os.SCHED_FIFO)
            )  # type:ignore
            os.sched_setscheduler(0, os.SCHED_FIFO, max_priority)
            os.sched_yield()
        except PermissionError as e:
            logger.warning(
                "Could not change scheduler arguments (%s); "
                "ensure %s has CAP_SYS_NICE permission",
                e,
                sys.executable,
            )

    def release_sched(self):
        if not self.core < 0:
            assert 0 not in os.sched_getaffinity(0) or os.cpu_count() < 2
            os.sched_setaffinity(0, self.initial_affinity)
        try:
            os.sched_setscheduler(0, self.initial_policy, self.initial_sched_param)
            os.sched_yield()
            os.nice(20)
        except PermissionError as e:
            logger.warning(
                "Could not change scheduler arguments (%s); "
                "ensure %s has CAP_SYS_NICE permission",
                e,
                sys.executable,
            )


class TimedBlock:
    _nest_count: int = 0

    def __init__(
        self,
        name: str,
        log: bool = False,
        log_on: Optional[logging.Logger] = None,
        min_time: Optional[datetime.timedelta] = None,
        indent: bool = True,
    ):
        self.name = name
        self.log = log
        self.logger = log_on or logger
        self.min_time = min_time or datetime.timedelta(seconds=0)
        self.indent = indent

    def _gen_indent(self) -> str:
        if not self.indent:
            return ""
        return " " * self.__class__._nest_count

    def __enter__(self):
        self.start_time = datetime.datetime.now()
        if self.log:
            self.logger.debug("%s> %s", self._gen_indent(), self.name)
        self.__class__._nest_count += 1

    def __exit__(self, *args):
        delta_time = datetime.datetime.now() - self.start_time
        self.__class__._nest_count -= 1
        if delta_time >= self.min_time:
            self.logger.debug(
                "%s< %s [%s]", self._gen_indent(), self.name, str(delta_time)
            )


def retry_on(times: int, exn_types: Tuple[Type[Exception]]):
    """If the wrapped function fails by throwing one of the exceptions in :exn_types:,
    retries up to :times: times. Raises the last exception if all retries fail. Lets
    through all the other exceptions."""

    def wrapper(fun):
        @wraps(fun)
        def wrapped_fun(*args, **kwargs):
            for retry in range(times - 1):
                try:
                    return fun(*args, **kwargs)
                except exn_types as exn:
                    logger.warning(
                        "Function %s failed [%d/%d times]: %s. Retrying…",
                        fun.__name__,
                        retry + 1,
                        times,
                        str(exn),
                    )
            try:
                return fun(*args, **kwargs)
            except exn_types as exn:
                raise exn

        return wrapped_fun

    return wrapper
