## \file checkpoints.py
## \brief Centralise all save files and management for discarding several steps

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import functools
import logging
import os
import pickle
import traceback
import palmed.messages as mes
from enum import Enum, unique
from pathlib import Path
from shutil import rmtree
from typing import Dict, Tuple, Generic, TypeVarTuple

logger = logging.getLogger(__name__)


# Step of the palmed algorithm
# MUST define the corresponding description in PalmedStep_Descr
@unique
class PalmedStep(Enum):
    INIT = 0
    LIST_TEST_INS = 5
    INS_IPC = 10
    QUADRA_SPARSE = 20
    QUADRA_DENSE = 30
    HIGH_IPC_CLASSES = 40
    HIGH_IPC_QUADRA = 50
    KERNEL_BENCHS = 60
    SATURATED = 70
    SATURATED_BENCHS = 80
    SATURATED_BENCH_REDUCED = 90


PalmedStep_Descr = {
    0: "Nothing has been done",
    5: "Acquired the ISA's instructions list and filtered out unsupported instructions",
    10: "IPC of every inidividual instruction has been computed",
    20: "The quadratic sparse matrix of the benchs' IPC has been computed",
    30: "The quadratic dense matrix after reduction by quadra.py",
    40: "Equivalence classes computed from quadra",
    50: "The quadratic dense matrix of the HIGH_IPC_CLASSES has been computed",
    60: (
        "The kernel has been found or sideloaded and the derived benchs needed for "
        "the very first LP has been computed"
    ),
    70: "The saturated benchmarks has been computed",
    80: "The combinations of all instructions + sat benchs have been computed",
    90: "The combinations of all instructions + sat_benchs have been computed "
    "and clustered into classes",
}


def get_check_txt_descr() -> str:
    return "\n\t" + "\n\t".join(
        [f"{step.name}: {PalmedStep_Descr[step.value]}" for step in PalmedStep]
    )


T0 = TypeVarTuple("T0")


class CheckpointResult(Generic[*T0]):
    """Base class for the result of a checkpoint function.
    Must be pickle-able.
    Stores the passed arguments as `self.ret_val`.
    If the checkpoint was just actually computed, the decorator `checkpoint` will
    return `CheckpointResult.just_computed()`, forwarding the returned values.
    If the checkpoint was just restored, the decorator will return
    `CheckpointResult.just_restored()`, which calls `state_restore` by default.
    """

    def __init__(self, *args: *T0) -> None:
        self.ret_val: Tuple[*T0] = args

    def just_computed(self) -> Tuple[*T0]:
        """Called when the result was just effectively computed. Should return the
        computed result, and may perform some other actions in passing.
        The object is saved with `pickle` *after* this method is called."""
        return self.ret_val

    def state_restore(self):
        """Called by default by `apply_loaded_result`; should perform any necessary
        stateful restoration based on `self.ret_val`. Does nothing by default."""

    def just_restored(self) -> Tuple[*T0]:
        """Called when the result was restored from savefile. Can perform any state
        restoration, calls `state_restore` by default.
        Should return the previously computed result."""
        self.state_restore()
        return self.ret_val


class Checkpoint:
    ROOT_DIR = Path("checkpoints")
    SAVE_DIR: Dict[PalmedStep, Path] = dict()
    SAVE_DIR_BASENAME = {
        PalmedStep.LIST_TEST_INS: Path(
            f"{PalmedStep.LIST_TEST_INS.value:02d}_list_test_instructions"
        ),
        PalmedStep.INS_IPC: Path(f"{PalmedStep.INS_IPC.value}_instruction_ipc"),
        PalmedStep.QUADRA_SPARSE: Path(
            f"{PalmedStep.QUADRA_SPARSE.value}_quadra_sparse_benchmarks"
        ),
        PalmedStep.QUADRA_DENSE: Path(
            f"{PalmedStep.QUADRA_DENSE.value}_quadra_dense_benchmarks"
        ),
        PalmedStep.HIGH_IPC_CLASSES: Path(
            f"{PalmedStep.HIGH_IPC_CLASSES.value}_high_ipc_classes"
        ),
        PalmedStep.HIGH_IPC_QUADRA: Path(
            f"{PalmedStep.HIGH_IPC_QUADRA.value}_high_ipc_quadra"
        ),
        PalmedStep.KERNEL_BENCHS: Path(
            f"{PalmedStep.KERNEL_BENCHS.value}_kernel_benchmarks"
        ),
        PalmedStep.SATURATED: Path(
            f"{PalmedStep.SATURATED.value}_saturating_benchs_list"
        ),
        PalmedStep.SATURATED_BENCHS: Path(
            f"{PalmedStep.SATURATED_BENCHS.value}_saturating_benchs_applied"
        ),
        PalmedStep.SATURATED_BENCH_REDUCED: Path(
            f"{PalmedStep.SATURATED_BENCH_REDUCED.value}"
            "_saturated_benchs_applied_reduced"
        ),
    }
    # Files are saved if ROOT_DIR/SAVE_DIR[checkpoint]/SAVE_NAME
    # either as a single file (for example when using @checkpoint)
    # or as the basename of several smaller saves (for example for .blk files)
    SAVE_NAME = Path("palmed")

    def __getstate__(self):
        class_state = {
            "ROOT_DIR": self.__class__.ROOT_DIR,
            "SAVE_DIR": self.__class__.SAVE_DIR,
            "SAVE_NAME": self.__class__.SAVE_NAME,
        }
        return {
            "class": class_state,
            "object": self.__dict__,
        }

    def __setstate__(self, state):
        assert {"class", "object"} == set(state.keys())
        self.__dict__ = state["object"]
        for attr, val in state["class"].items():
            setattr(self.__class__, attr, val)

    @classmethod
    def set_suffix(cls, suffix: str) -> None:
        for chkp, path in cls.SAVE_DIR_BASENAME.items():
            cls.SAVE_DIR[chkp] = path.with_name(path.name + suffix)

    def __init__(self):
        self._step = PalmedStep.INIT
        super().__init__()

    @classmethod
    def create_directories(cls):
        for _, directory in cls.SAVE_DIR.items():
            (cls.ROOT_DIR / directory).mkdir(parents=True, exist_ok=True)

    def set_and_reset_step_after(self, step: PalmedStep, pipedream: bool) -> None:
        self._step = step
        msg = ""
        for check, directory in Checkpoint.SAVE_DIR.items():
            # Pipdream mode should not delete `INS_IPC`
            if pipedream or check != PalmedStep.INS_IPC:
                # bla ...and the directory exists, and is not empty
                if (
                    check.value > step.value
                    and (Checkpoint.ROOT_DIR / directory).is_dir()
                    and any((Checkpoint.ROOT_DIR / directory).iterdir())
                ):
                    msg += (
                        f"{mes._CBOLD}{mes._CRED}\t"
                        f"{(Checkpoint.ROOT_DIR / directory).as_posix()}{mes._CEND}\n"
                    )

        if msg != "":
            print(
                f"{mes._CBOLD}{mes._CRED}[palmed] WARNING: the following directory·ies "
                f"will be DELETED!{mes._CEND}\n{msg}",
                end="",
            )
        else:
            Checkpoint.create_directories()
            return

        ret = ""
        while ret.lower() not in ["y", "n"]:
            ret = input(f"{mes._CBOLD}{mes._CRED}Are you sure (Y/N)?{mes._CEND} ")
        if ret.lower() == "n":
            logger.error("User stated not to delete files, exiting...")
            exit(1)
        logger.info("User authorized checkpoint deletion starting from %s", step.name)
        for check, directory in Checkpoint.SAVE_DIR.items():
            direc = Checkpoint.ROOT_DIR / directory
            if check.value > step.value:
                if direc.is_dir():
                    rmtree(direc.as_posix())
                elif direc.exists():
                    logger.error(
                        "file %s exists and is not a directory, but must be one"
                        "for checkpointing.",
                        direc.as_posix(),
                    )
        Checkpoint.create_directories()

    @staticmethod
    def _get_save_file_static(step: PalmedStep) -> str:
        ret = (
            Checkpoint.ROOT_DIR / Checkpoint.SAVE_DIR[step] / Checkpoint.SAVE_NAME
        ).as_posix()

        return ret

    def get_save_file(self, step: PalmedStep, update=True) -> str:
        if update and step.value > self._step.value:
            self._step = step
        return Checkpoint._get_save_file_static(step)


class WrongCheckpointLoaded(Exception):
    pass


def checkpoint(step: PalmedStep, ignore_verifs=False):
    def inner(function):
        @functools.wraps(function)
        def new_func(*args, **kwargs):
            # sorry Python, you're giving me no choice here
            from palmed.config import Config

            savefile_ctx = Config.CHECKPOINT.get_save_file(step, update=False) + ".ctx"
            savefile_value = Config.CHECKPOINT.get_save_file(step, update=False)
            try:
                with open(savefile_ctx, "rb") as sav:
                    loaded = pickle.load(sav)
                if loaded["fun"] != function.__name__:
                    raise WrongCheckpointLoaded()
                if not ignore_verifs:
                    if loaded["arch"] != Config.MU_ARCH:
                        raise WrongCheckpointLoaded()
                    if loaded["uops"] != Config.UOPS:
                        raise WrongCheckpointLoaded()
                if loaded["args"] != args:
                    raise WrongCheckpointLoaded()
                if loaded["kwargs"] != kwargs:
                    raise WrongCheckpointLoaded()
                with open(savefile_value, "rb") as sav:
                    ret = pickle.load(sav)

                logger.info(
                    "Successfully read checkpoint %s from %s", step.name, savefile_value
                )
                if isinstance(ret, CheckpointResult):
                    return ret.just_restored()
                return ret

            except (
                WrongCheckpointLoaded,
                KeyError,
                pickle.UnpicklingError,
                EOFError,
            ):
                logger.error("Tried and failed to restore checkpoint %s:", step.name)
                traceback.print_exc()
            except FileNotFoundError:
                if step.value < Config.CHECKPOINT._step.value:
                    logger.error(
                        "Checkpoint %s was supposed to be found, but was not:",
                        step.name,
                    )
                    traceback.print_exc()

            logger.info(
                "Checkpoint %s was not found or could not be restored. Recomputing.",
                step.name,
            )

            with open(savefile_ctx, "wb") as sav:
                pickle.dump(
                    {
                        "fun": function.__name__,
                        "arch": Config.MU_ARCH,
                        "uops": Config.UOPS,
                        "args": args,
                        "kwargs": kwargs,
                    },
                    sav,
                )
            ret = function(*args, **kwargs)
            if step.value > Config.CHECKPOINT._step.value:
                Config.CHECKPOINT._step = step
            logger.info("Saving checkpoint %s to %s", step.name, savefile_value)
            out_value = ret
            if isinstance(ret, CheckpointResult):
                out_value = ret.just_computed()
            with open(savefile_value, "wb") as sav:
                pickle.dump(ret, sav)
            return out_value

        return new_func

    return inner
