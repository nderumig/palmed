## \file instructions.py
## \brief Load pipedream instruction description and uops measures from the instructions.xml
## file.

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import pickle
import re
import subprocess
import logging
import datetime
import multiprocessing as mp
import xml.sax
import pipedream.asm.x86 as pipex86
import pipedream.asm.armv8a as pipearmv8
import pipedream.asm.x86.instructions_xed as xed
import pipedream.benchmark as pipebench
import pipedream.asm.ir as pipeasm
import pipedream.utils.abc as abc
import palmed.ports as pt
import palmed.pipedream_benchmarks as palmedpipe
from collections import defaultdict
from enum import Enum
from typing import (
    Any,
    Union,
    Tuple,
    Set,
    List,
    Dict,
    Iterable,
    Optional,
    TypeVar,
    FrozenSet,
)
import typing as t
from palmed.checkpoints import PalmedStep, checkpoint, CheckpointResult
from palmed.convert import *
from palmed.config import Config, ISA_Extension
from palmed import db
from palmed.db import current_machine
from palmed.db_caching import DBCached, NonBenchingMachine
from palmed.utils import get_frac
from palmed.messages import ProgressBar
from palmed.pinned_pool import select_multiprocessing_pool

logger = logging.getLogger(__name__)


UNSUPPORTED_INSTR: Set[str] = {
    "DIV_GPR8i8",
    "DIV_GPR16i16",
    "DIV_GPR32i32",
    "DIV_GPR64i64",
    "DIVSS_VR128f64_MEM64f64",
    "DIVSS_VR128f64_VR128f64",
    "DIVSD_VR128f64_MEM64f64",
    "DIVSD_VR128f64_VR128f64",
    "DIVPS_VR128f32x4_MEM64f32x4",
    "DIVPS_VR128f32x4_VR128f32x4",
    "DIVPD_VR128f64x2_VR128f64x2",
    "DIVPD_VR128f64x2_MEM64f64x2",
    "IDIV_GPR8i8",
    "IDIV_GPR16i16",
    "IDIV_GPR32i32",
    "IDIV_GPR64i64",
    "LEAVE",
    "LFENCE",
    "SFENCE",
    "MFENCE",
    "RET_NEAR",
    "WBINVD",
}


class ISA_Base(ISA_Extension):
    BASE = "BASE"


ISA_BASE = ISA_Base.BASE
UNSUPPORTED_TAGS: FrozenSet[str] = frozenset(["AVX512", "SVE", "SVE2"])
UNSUPPORTED_EXTENSIONS: FrozenSet[str] = frozenset(
    [
        xed.ISA.ADOX_ADCX,
        xed.ISA.X87,
        xed.ISA.CLFLUSHOPT,
        xed.ISA.CLFSH,
        xed.ISA.SSE3X87,
        xed.ISA.PREFETCHW,
        xed.ISA.PREFETCHWT1,
        xed.ISA.PREFETCH_NOP,
    ]
)


class InstructionNotFound(Exception):
    def __init__(self, what):
        super().__init__()
        self.instr_name = what

    def __str__(self):
        return f"Instruction {self.instr_name} not found"


# Generic class, for type checking
class Instruction(object):
    __metaclass__ = abc.ABCMeta

    CACHED_IPC: Dict["Instruction", float] = {}
    CACHED_LAT: Dict["Instruction", float] = {}

    # Extension of the base instruction set
    _ext: Optional[ISA_Extension]

    @abc.abstractmethod
    def __init__(self, IPC: Optional[float] = None, lat: Optional[float] = None):
        if "_name" not in self.__dict__:
            self._name: str = ""
        if IPC is not None:
            self.set_IPC(IPC)
        if lat is not None:
            self.set_latency(lat)
        self._ext = None
        super().__init__()

    @classmethod
    def reset_cache(cls):
        cls.CACHED_IPC.clear()
        cls.CACHED_LAT.clear()

    @property
    def _ports_str(self):
        """Compatibility with Instruction_UOPS"""
        return None

    @property
    def extension(self) -> ISA_Extension:
        assert self._ext is not None
        return self._ext

    @abc.abstractmethod
    def name(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def _get_IPC(self, core: Optional[int]) -> float:
        raise NotImplementedError()

    def is_compatible_with(self, other: "Instruction") -> bool:
        assert isinstance(other, type(self))
        return True

    @abc.abstractmethod
    def _get_lat(self, core: Optional[int]) -> float:
        raise NotImplementedError()

    def latency(self, core: Optional[int] = None) -> float:
        try:
            return self.__class__.CACHED_LAT[self]
        except KeyError:
            lat = self._get_lat(core)
            self.__class__.CACHED_LAT[self] = lat
            return lat

    def has_latency(self) -> bool:
        """Does this instruction have its IPC computed and cached yet?"""
        return self in self.__class__.CACHED_LAT

    def set_latency(self, lat):
        """Sets the cached IPC value for this instruction"""
        self.__class__.CACHED_LAT[self] = lat

    def IPC(self, core: Optional[int] = None) -> float:
        try:
            return self.__class__.CACHED_IPC[self]
        except KeyError:
            ipc = self._get_IPC(core)
            return ipc

    def has_IPC(self) -> bool:
        """Does this instruction have its IPC computed and cached yet?"""
        return self in self.__class__.CACHED_IPC

    def set_IPC(self, ipc, **kwargs):
        """Sets the cached IPC value for this instruction"""
        self.__class__.CACHED_IPC[self] = ipc

    def set_ext(self, ext: ISA_Extension):
        self._ext = ext

    @abc.abstractmethod
    def __repr__(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def var_name(self) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    def closure(self):
        raise NotImplementedError()

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.name() == other.name()

    @abc.abstractmethod
    def __hash__(self) -> int:
        raise NotImplementedError()


def _instruction_pipedream_test_inner(inst_name):
    bench = palmedpipe.PipedreamKernel([inst_name])
    try:
        bench.measure_CPI(
            num_dynamic_instructions=100,
            unrolled_length=20,
            num_iterations=10,
            kind=pipebench.Benchmark_Kind.THROUGHPUT,
            core=-1,
        )
    except subprocess.CalledProcessError:
        exit(1)
    exit(0)


class Instruction_Pipedream(Instruction, DBCached):
    """
    _name: name of the ASM instruction
    """

    _ext: Optional[ISA_Extension]

    def __init__(
        self,
        name: str,
        IPC: Optional[float] = None,
        lat: Optional[float] = None,
        ext: Optional[ISA_Extension] = None,
    ):
        # Assert that the instruction is valid
        if (
            name not in self.__class__.CACHED_IPC
            and not palmedpipe.is_valid_instruction(name)
        ):
            raise InstructionNotFound(name)
        self._name = name
        self._ext = ext
        super().__init__(IPC, lat)

    @abc.override
    def IPC(self, core: Optional[int] = None):
        return Instruction.IPC(self, core)

    @abc.override
    def set_IPC(self, ipc, commitable=True):
        self._IPC_commitable = commitable
        return Instruction.set_IPC(self, ipc)

    @abc.override
    def has_IPC(self):
        return Instruction.has_IPC(self)

    @classmethod
    @abc.override
    def from_db_entry(cls, db_entry: db.models.Benchmark) -> "Instruction_Pipedream":
        """Generate a benchmark from a database entry. Does not populate IPC from DB."""
        descr = db_entry.descr
        chunks = descr.strip().split(";")
        assert len(chunks) == 1
        chunk = chunks[0]
        rep, instr_str = chunk.split("*")
        rep = int(rep)
        assert rep == 1
        return Instruction_Pipedream(instr_str)

    @abc.override
    def commit_benchmark(
        self, cpi, bench_stats, round_IPC_nearest: bool = True, db_commit=False
    ):
        """Save a previous measure (supposedly made through `palmedpipe`) and
        commit it to database if `db_commit`."""
        ipc = 1 / cpi
        self.orig_IPC = ipc
        if round_IPC_nearest:
            num, denum = get_frac(ipc, precision=Config.MIN_BENCH_COEF)
            ipc = num / denum
        self.set_IPC(ipc)
        self._IPC_bench_stats = bench_stats
        if db_commit:
            self._cached_IPC_to_db()

    # For compatilibilty and debug only
    @property
    def _ports_str(self):
        try:
            return instr_to_uops(self)._ports_str
        except InstructionNotFound:
            logger.debug("Instruction %s not found", self.name())
            return None

    @abc.override
    def name(self) -> str:
        return self._name

    @abc.override
    def _gen_IPC_pipedream(self, core: Optional[int]):
        if not current_machine.is_benching():
            raise NonBenchingMachine(self)
        # Quick test in order to check whether the IPC is 0 or not
        bench = palmedpipe.PipedreamKernel([self.name()])
        cpi, _, conditions = bench.measure_CPI(
            num_dynamic_instructions=Config.NUM_DYN_INST // 100,
            unrolled_length=500,
            num_iterations=20,
            kind=pipebench.Benchmark_Kind.THROUGHPUT,
            core=core,
        )
        if 1 / cpi < Config.MIN_IPC_PIPE_INSTR:
            self.commit_benchmark(
                cpi, conditions, round_IPC_nearest=False, db_commit=True
            )
            return
        # More precise measure
        cpi, _, conditions = bench.measure_CPI(
            num_dynamic_instructions=Config.NUM_DYN_INST,
            unrolled_length=500,
            num_iterations=50,
            kind=pipebench.Benchmark_Kind.THROUGHPUT,
            core=core,
        )
        self.commit_benchmark(cpi, conditions, round_IPC_nearest=True, db_commit=True)

    @abc.override
    def _get_IPC(self, core: Optional[int]) -> float:
        # Calls `self._gen_IPC_pipedream()` which populate the IPC cache
        self._gen_IPC_pipedream_or_cached(core)
        # Fetches from the IPC cache
        return self.IPC(core)

    # Return True when we are sure that self and other may be benchmarked together
    # i.e. one of them is `ISA_BASE` or both belongs to the same exention
    @abc.override
    def is_compatible_with(self, other: Instruction) -> bool:
        super().is_compatible_with(other)
        my_isa = self._ext
        their_isa = other._ext
        if my_isa is None or their_isa is None:
            logger.warning(
                "%s is not guaranteed to be compatible with %s (respective known "
                "extension of %s and %s), "
                "trying anyway",
                self,
                other,
                my_isa,
                their_isa,
            )
            return False
        return ISA_BASE in (my_isa, their_isa) or my_isa == their_isa

    def _get_cached_lat(self) -> Optional[float]:
        session = db.Session()
        lat_measures = (
            session.query(db.models.BenchmarkLatencyMeasure)
            .join(db.models.Benchmark)
            .where(
                db.models.Benchmark.descr == self.db_str(),
                db.models.BenchmarkLatencyMeasure.machine_id
                == current_machine.get_pk(),
            )
            .all()
        )
        if lat_measures:
            return sum(map(lambda x: x.latency, lat_measures)) / len(lat_measures)
        return None

    def _lat_to_db(
        self, latency: float, bench_stats: palmedpipe.IPC_bench_stats, commit=True
    ):
        """Commit a computed latency to database"""
        session = db.Session()
        db_bench = self.get_or_create_db_entry()
        conditions = db.current_machine.current_experimental_conditions(
            bench_stats.core,
            bench_stats.parallel_cores_used,
        )
        measure = db.models.BenchmarkLatencyMeasure(
            benchmark_id=db_bench.id,
            machine_id=db.current_machine.get_pk(),
            conditions=conditions,
            latency=latency,
            variance=bench_stats.variance,
            date=datetime.datetime.now(),
            aggregation_mode=self.BENCHMARK_AGGREGATION_METHOD,
            warmup_repetitions=bench_stats.num_warmup_iterations,
            repetitions=bench_stats.num_iterations,
            instr_count=bench_stats.num_dynamic_instructions,
            inner_instr_count=bench_stats.unrolled_length,
            is_aligned=False,
        )
        session.add(conditions)
        session.add(measure)
        if commit:
            session.commit()

    @abc.override
    def _get_lat(self, core: Optional[int]) -> float:
        lat = self._get_cached_lat()
        if lat is None:
            bench = palmedpipe.PipedreamKernel([self.name()])
            cpi, _, conditions = bench.measure_CPI(
                num_dynamic_instructions=50_000,
                unrolled_length=500,
                num_iterations=20,
                kind=pipebench.Benchmark_Kind.LATENCY,
                with_mapping=False,
                core=core,
            )
            assert cpi != 0
            lat = cpi
            self._lat_to_db(lat, conditions)

        self._latency = lat
        return self._latency

    def test(self) -> bool:
        """Test whether pipedream crashes or not when trying to measure `self`'s
        IPC"""
        p = mp.get_context("fork").Process(
            target=_instruction_pipedream_test_inner, args=(self.name(),)
        )
        p.start()
        p.join()
        return p.exitcode == 0

    @abc.override
    def __repr__(self):
        optional = ""
        if self.has_IPC():
            optional += ", IPC={}".format(self.IPC())
        if self.has_latency():
            optional += ", lat={}".format(self.latency())

        return "ins.{cls}('{name}'{optional})".format(
            cls=self.__class__.__name__, name=self.name(), optional=optional
        )

    @abc.override
    def var_name(self):
        return self._name

    # Stub for compatibility
    @abc.override
    def closure(self):
        raise NotImplementedError()

    @abc.override
    def db_str(self):
        """A string serialization of the current benchmark, to be stored in the
        database"""
        return f"1*{self.name()}"

    @abc.override
    def __hash__(self) -> int:
        return hash("__PIPEDREAM__" + self.name())


class Instruction_UOPS(Instruction):
    """
    _name: name of the ASM instruction
    _ports_str: string of the uops.info coding of ports
    _ports: list of tuple (multiplicity, port) representing the used ports
    """

    CACHED_PORTS: Dict[
        "Instruction_UOPS", Dict[str, Optional[Union[bool, pt.Port, str]]]
    ] = {}

    # Parse 1*p01+2*P0156+...
    @abc.override
    def __init__(
        self,
        name: str,
        ports: Optional[str] = None,
        IPC: Optional[float] = None,
        lat: Optional[float] = None,
    ):
        self._name = name
        super().__init__(IPC, lat)
        self._ext = ISA_BASE
        if (self not in self.__class__.CACHED_PORTS) and (ports is None):
            raise InstructionNotFound(name)
        if ports is not None:
            self._set_ports(ports, pt.parse(ports), False)

    @property
    def _ports(self):
        return self.__class__.CACHED_PORTS[self]["parsed"]

    @property
    def _ports_str(self):
        return self.__class__.CACHED_PORTS[self]["str"]

    @property
    def _closed(self):
        return self.__class__.CACHED_PORTS[self]["closed"]

    def _set_ports(self, ports_str=None, ports=None, closed=None):
        """Updates with the non-None values"""
        new_ports = {
            "str": ports_str if ports_str is not None else self._ports_str,
            "parsed": ports if ports is not None else self._ports,
            "closed": closed if closed is not None else self._closed,
        }
        self.__class__.CACHED_PORTS[self] = new_ports

    @abc.override
    def __repr__(self):
        optional = ""
        if self.has_IPC():
            optional += ", IPC={}".format(self.IPC())
        if self.has_latency():
            optional += ", lat={}".format(self.latency())

        return "ins.{cls}('{name}', '{ports}'{optional})".format(
            cls=self.__class__.__name__,
            name=self.name(),
            ports=self._ports_str,
            optional=optional,
        )

    @abc.override
    def _get_IPC(self, core: Optional[int]) -> float:
        resource_alloc: Dict[pt.Port, float] = {}
        for mult, port in self._ports:
            if resource_alloc.get(port) == None:
                resource_alloc[port] = mult
            else:
                resource_alloc[port] += mult

        time = -1.0
        for port, load in resource_alloc.items():
            loc_time = load / float(len(port))
            if time < loc_time:
                time = loc_time
        ipc = 1.0 / time
        self.set_IPC(ipc)
        return ipc

    @abc.override
    def closure(self) -> None:
        if self._closed:
            raise Exception(
                f"Trying get the closure of an already closed instruction: {str(self)}"
            )
        to_add = []
        ports = self._ports

        # O(N^2) probably suboptimal, but len(ports) and len(all_ports()) are small
        for mult, port in ports:
            for res in pt.Port.all_ports():
                if port < res:
                    to_add.append((mult, res))
        ports += to_add
        # Merge doubles
        seen: Dict[pt.Port, int] = defaultdict(int)
        for mult, port in ports:
            seen[port] += mult

        out_ports = []
        for port, mult in seen.items():
            out_ports.append((mult, port))
        self._set_ports(ports=out_ports, closed=True)

    def ports(self) -> List[Tuple[int, pt.Port]]:
        return self._ports

    @abc.override
    def name(self) -> str:
        return self._name

    @abc.override
    def var_name(self) -> str:
        return re.sub("[(]", "_", re.sub("[*\\{\\}\\s,)]", "", self._name))

    @abc.override
    def __hash__(self) -> int:
        return hash("__UOPS__" + self.name())


    @abc.override
    def _get_lat(self, core: Optional[int]) -> float:
        raise ValueError("Latency is unknown")


class InstructionHandler(xml.sax.ContentHandler):  # type: ignore
    def __init__(self, archi: str, prune: bool):
        self._archi = archi
        self._prune = prune
        self._in_instruction = False
        self._extension: Optional[str] = None
        self._in_operand = False
        self._instruction_name = ""
        self._instruction_ports = None
        self._operand_list: List[Tuple[str, str, str]] = []
        self._in_archi = False
        self._IPC: Optional[float] = None
        self._latency = float("inf")
        self._norex = False
        self._ISA: List[Instruction_UOPS] = []

    def _instr_needs_u8(self, attr_name, width) -> bool:
        """These instructions use unsigned in pipedream and int in uops"""
        need_IMMu8 = [
            "AESKEYGENASSIST",
            "BLENDPS",
            "BLENDPD",
            "BT",
            "BTR",
            "BTC",
            "BTS",
            "CMPSS",
            "CMPSD",
            "CMPSD_XMM",
            "CMPPS",
            "CMPPD",
            "DPPS",
            "DPPD",
            "EXTRACTPS",
            "INSERTPS",
            "MPSADBW",
            "PALIGNR",
            "PBLENDW",
            "PCLMULQDQ",
            "PEXTRB",
            "PEXTRW",
            "PEXTRD",
            "PEXTRQ",
            "PINSRB",
            "PINSRW",
            "PINSRD",
            "PINSRQ",
            "PSHUFW",
            "PSHUFD",
            "PSHUFHW",
            "PSHUFLW",
            "PSLLDQ",
            "PSRAD",
            "PSRAW",
            "PSRLD",
            "PSRLDQ",
            "PSRLW",
            "PSRLQ",
            "PSLLW",
            "PSLLD",
            "PSLLQ",
            "SAR",
            "SHL",
            "SHLD",
            "SHR",
            "SHRD",
            "SHUFPS",
            "SHUFPD",
            "VCMPSD",
            "ROUNDSS",
            "ROUNDSD",
            "ROUNDPS",
            "ROUNDPD",
            "RCL",
            "RCR",
            "ROR",
            "RORX",
            "ROL",
        ]
        return (
            (
                self._instruction_name in need_IMMu8
                or (
                    self._instruction_name in ["XOR", "ADD", "AND"]
                    and len(self._operand_list) > 0
                    and self._operand_list[-1][0] == "GPR"
                    and "8" in self._operand_list[-1][1]
                )
                or (
                    self._instruction_name in ["AND"]
                    and len(self._operand_list) > 0
                    and self._operand_list[-1] == ("MEM", "64", "u8")
                )
            )
            and attr_name == "IMM"
            and width == "8"
        )

    def startElement(self, tag, attributes):
        if tag == "extension":
            extension_raw = attributes.get("name", ISA_BASE)
            for ext in Config.EXTENSIONS:
                if extension_raw.startswith(ext.name):
                    self._extension = ext
                else:
                    self._extension = ISA_BASE

        elif tag == "instruction":
            self._in_instruction = True
            self._instruction_name = attributes["iclass"]
            self._operand_list = []
            self._IPC = None
            self._latency = float("inf")
            self._norex = "NOREX" in attributes["string"]

            if self._instruction_name.endswith("_XMM"):
                self._instruction_name = self._instruction_name[:-4]

        elif (
            self._in_instruction
            and tag == "architecture"
            and attributes["name"] == self._archi
        ):
            self._in_archi = True

        elif self._in_archi and tag == "latency":
            if attributes.get("cycles"):
                self._latency = min(float(attributes["cycles"]), self._latency)
            if attributes.get("max_cycles"):
                self._latency = min(float(attributes["max_cycles"]), self._latency)
            if attributes.get("cycles_addr"):
                self._latency = min(float(attributes["cycles_addr"]), self._latency)
            if attributes.get("cycles_mem"):
                self._latency = min(float(attributes["cycles_mem"]), self._latency)
            if attributes.get("max_cycles_addr"):
                self._latency = min(float(attributes["max_cycles_addr"]), self._latency)

        elif self._in_archi and tag == "measurement":
            if "ports" in attributes:
                self._instruction_ports = attributes["ports"]
            # Hacky way to discriminate between "no ports" and "no measures"
            elif not self._prune:
                if "uops" in attributes:
                    self._instruction_ports = ""
                if "TP_ports" in attributes:
                    self._IPC = 1 / float(attributes["TP_ports"])
                # TP should be used for IPC computation when no port is given
                # As of now I only have NOP on SKL+ in mind
                elif "TP" in attributes:
                    self._IPC = 1 / float(attributes["TP"])
                elif "TP_loop" in attributes:
                    self._IPC = 1 / float(attributes["TP_loop"])

        elif tag == "operand":
            self._in_operand = True
            ignored_types = ["flags"]
            if attributes["type"] in ignored_types or "suppressed" in attributes:
                return

            # Set att_name: reg or memory
            corres_types = {"reg": "GPR", "agen": "ADDR"}
            if attributes.get("width") in ["128", "256", "512"]:
                corres_types["reg"] = "VRX" + attributes["width"]
            attr_name = corres_types.get(attributes["type"], attributes["type"].upper())

            # Set width
            if "width" in attributes:
                # Assuming 64-bit adressing mode
                if attr_name == "IMM":
                    width = ""
                else:
                    width = attributes["width"]
            else:
                # Default to a 64-bit, as we run on a 64-bit machine
                width = "64"

            # Set xtype
            if "xtype" in attributes:
                if attributes["xtype"] == "int":
                    xtype = "i" + attributes["width"]
                else:
                    xtype = attributes["xtype"]
            elif "width" in attributes:
                if self._instr_needs_u8(attr_name, attributes["width"]):
                    xtype = "u8"
                else:
                    xtype = "i" + attributes["width"]
            else:
                # Default to integer 64-bit, as we run on a 64-bit machine
                xtype = "i64"

            ## Post-process
            # bfloat & vectors
            if xtype[:2] == "bf" or xtype[:2] == "2f":
                offset = 2
            else:
                offset = 1
            if attr_name == "GPR":
                if int(width) > int(xtype[offset:]):
                    xtype += "x" + str(int(int(width) / int(xtype[offset:])))
                    # Force VR128 type even when less than 64 bits are used
                    # if int(width) <= 64:
                    #    width = "128"
            # Memory: width is alway 64
            elif attr_name == "MEM":
                if xtype[offset:].isnumeric() and int(width) != int(xtype[offset:]):
                    xtype += "x" + str(int(int(width) / int(xtype[offset:])))
                width = "64"

            # SHL special treatment
            if (
                self._instruction_name
                in [
                    "SHL",
                    "SHLD",
                ]
                and attr_name == "GPR"
                and len(self._operand_list) > 2
            ):
                attr_name = "CL"
                width = ""
                xtype = "i8"
            if (
                self._instruction_name in ["MOV", "XOR"]
                and len(self._operand_list) > 0
                and self._operand_list[-1]
                in [("GPR", "8", "i8"), ("GPR", "8NOREX", "i8"), ("MEM", "64", "u8")]
            ) and attr_name == "IMM":
                xtype = "u8"

            # Add NOREX
            if self._norex and attr_name == "GPR":
                width += "NOREX"

            self._operand_list.append((attr_name, width, xtype))

    def characters(self, content):
        if self._in_operand:
            if content == "1":
                self._operand_list[-1] = ("ONE", "", "")
            # Replace "GPR" by "VR"
            if len(self._operand_list) > 0 and self._operand_list[-1][0] == "GPR":
                if "XMM" in content:
                    self._operand_list[-1] = ("VR", "128", self._operand_list[-1][2])
                elif "YMM" in content:
                    self._operand_list[-1] = ("VR", "256", self._operand_list[-1][2])
                elif "MM" in content:
                    self._operand_list[-1] = ("VR", "64", self._operand_list[-1][2])

    def endElement(self, tag):
        if tag == "instruction":
            self._in_instruction = False
        elif self._in_instruction and tag == "architecture" and self._in_archi:
            self._in_archi = False
            # Do not add instructions with no measures
            if self._instruction_ports is not None:
                name = self._instruction_name + "".join(
                    [f"_{op[0]+op[1]+op[2]}" for op in self._operand_list]
                )
                try:
                    if not self._prune:
                        new_ins = Instruction_UOPS(
                            name,
                            self._instruction_ports,
                            IPC=self._IPC,
                            lat=self._latency,
                        )
                    else:
                        new_ins = Instruction_UOPS(
                            name, self._instruction_ports, lat=self._latency
                        )
                    new_ins.set_ext(self._extension)
                    if name in UNSUPPORTED_INSTR:
                        logger.debug("Dropping unsupported %s instruction", name)
                    else:
                        self._ISA.append(new_ins)
                except pt.UnsupportedPort:
                    logger.debug(
                        "Dropping %s as it contains at least unsupported port (cf "
                        "config file): %s",
                        name,
                        self._instruction_ports,
                    )

                # Some instructions does not have any ports reported, we do not want to
                # keep the previous value
                self._instruction_ports = None
                self._IPC = None
            elif self._IPC is not None:
                logger.warning(
                    "Found no port information for instruction %s but an IPC of %f",
                    self._instruction_name,
                    self._IPC,
                )
        elif self._in_operand and tag == "operand":
            self._in_operand = False

    def ISA(self) -> List[Instruction_UOPS]:
        return self._ISA


def pipedream_to_uops(instr: Instruction_Pipedream) -> Instruction_UOPS:
    return Instruction_UOPS(instr.name())


def instr_to_uops(instr: Instruction) -> Instruction_UOPS:
    if isinstance(instr, Instruction_UOPS):
        return instr
    elif isinstance(instr, Instruction_Pipedream):
        return pipedream_to_uops(instr)
    else:
        raise InstructionNotFound(instr.name())


def load_uops(
    arch: str,
    prune_nores: bool = True,
    filename: str = "instructions.xml",
    cache_IPC=False,
) -> Dict[ISA_Extension, List[Instruction_UOPS]]:
    logger.info("Loading uops.info's mapping...")
    pt.Port.reset()
    parser = xml.sax.make_parser()
    Handler = InstructionHandler(arch, prune_nores)
    parser.setContentHandler(Handler)
    parser.parse(filename)
    # Remove doubles
    isa = list(set(Handler.ISA()))
    isa.sort(key=lambda i: i.name())
    # Computing closure
    pt.Port.compute_closure()
    for instr in isa:
        instr.closure()
    if cache_IPC:
        for instr in isa:
            instr.IPC()
    instructions = defaultdict(list)
    for instr in isa:
        instructions[instr.extension].append(instr)
    return dict(instructions)


def _map_instr_ipc_and_latency(
    instr: Instruction_Pipedream,
) -> Optional[Tuple[Instruction_Pipedream, float, float, bool]]:
    try:
        ipc = instr.IPC(core=-1)
        lat = instr.latency(core=-1)
        return (instr, ipc, lat, ipc > Config.MIN_BENCH_COEF / 2)
    except (ValueError, subprocess.CalledProcessError):
        logger.warning("Cannot bench instruction %s, ignoring it...", instr)
        return None


T0 = TypeVar("T0")


class LoadPipedreamCheckpointResult(
    CheckpointResult[
        Dict[ISA_Extension, List[Instruction_Pipedream]],
        List[Instruction_Pipedream],
    ]
):
    """Return type for `load_pipedream`, restoring the instructions' state upon load
    from checkpoint file"""

    instr_IPCs: Dict[Instruction_Pipedream, float]
    instr_latencies: Dict[Instruction_Pipedream, float]

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.instr_IPCs = {}
        self.instr_latencies = {}

    def state_restore(self) -> None:
        dict_instrs, instrs_unsupported = self.ret_val
        all_instrs: List[Instruction_Pipedream] = []
        for instrs in dict_instrs.values():
            all_instrs += instrs
        for instr_list in [all_instrs, instrs_unsupported]:
            for instr in instr_list:
                if instr in self.instr_IPCs:
                    instr.set_IPC(self.instr_IPCs[instr], commitable=False)
                    instr.set_latency(self.instr_latencies[instr])

    def just_computed(
        self,
    ) -> Any:
        dict_instrs, instrs_unsupported = self.ret_val
        all_instrs: List[Instruction_Pipedream] = []
        for instrs in dict_instrs.values():
            all_instrs += instrs
        for instr_list in [all_instrs, instrs_unsupported]:
            for instr in instr_list:
                if instr.has_IPC():
                    self.instr_IPCs[instr] = instr.IPC()
                    self.instr_latencies[instr] = instr.latency()
        return super().just_computed()


def sideload_pipedream(instrs: Dict[str, Tuple[float, float]]):
    """Sideload a dict of pipedream instructions, avoiding a call to `load_pipedream`
    when impossible or inconvenient, allowing to provide directly IPC and latency
    values"""
    for instr_name, (ipc, lat) in instrs.items():
        instr = Instruction_Pipedream(instr_name)
        instr.set_IPC(ipc, commitable=False)
        instr.set_latency(lat)


def iter_inst_test(ins_list: List[pipeasm.Instruction], test: bool):
    for i in ins_list:
        if (
            i.isa_set in UNSUPPORTED_EXTENSIONS
            or len(set(i.tags) & UNSUPPORTED_TAGS) > 0
            or i.name in UNSUPPORTED_INSTR
        ):
            logger.warning("Not supporting %s", i.name)
            yield None
            continue

        ext: ISA_Extension
        for extension in Config.EXTENSIONS:
            if extension.value in i.tags:
                ext = extension
                break
        else:
            ext = ISA_BASE

        # Ignore instruction which operates on a single register
        for op in i.operands:
            if isinstance(op, pipeasm.Register_Operand):
                if len(op.register_class) == 1:
                    instr_palmed = Instruction_Pipedream(i.name)
                    yield (instr_palmed, test, 0, ext)
                    break
        else:
            instr_palmed = Instruction_Pipedream(i.name)
            yield (instr_palmed, test, 1, ext)


# Returns the instruction and its category:
# -1 -> not supported
#  0 -> not benchable with other instructions (`selected_unsupported`)
#  1 -> supported
def _test_one_instr(
    args: Tuple[Instruction_Pipedream, bool, int, ISA_Extension]
) -> Tuple[Optional[int], Union[Instruction_Pipedream, str]]:
    if args is None:
        return (None, "")
    instr_palmed = args[0]
    test = args[1]
    ret_int = args[2]
    extension = args[3]
    if test and not instr_palmed.test():
        return (-1, instr_palmed.name())
    instr_palmed.set_ext(extension)
    return (ret_int, instr_palmed)


@checkpoint(PalmedStep.LIST_TEST_INS)
def get_benchable_pipedream_instructions(
    test=True,
) -> Tuple[
    Dict[ISA_Extension, List[Instruction_Pipedream]], List[Instruction_Pipedream]
]:
    """Load every Pipedream-Supported instructions.
    If :test: is true, perform a test benchmark to filter out
    instructions that cannot be benchmarked with Pipedream (crashes, …)"""
    if Config.ISA == Config.SUPPORTED_ISA.X86.value:
        pipeisa = pipex86.X86_Instruction_Set()
    elif Config.ISA == Config.SUPPORTED_ISA.ARMv8a.value:
        pipeisa = pipearmv8.ARMv8_Instruction_Set()
    else:
        logger.error("Invalid ISA")
        exit(1)

    isa_instr = pipeisa.benchmark_instructions()

    selected: Dict[ISA_Extension, List[Instruction_Pipedream]] = {
        ext: [] for ext in Config.EXTENSIONS
    }
    selected[ISA_BASE] = []
    selected_unsupported: List[Instruction_Pipedream] = []

    with select_multiprocessing_pool()(Config.effective_num_cpu()) as pool:
        for support, palmed_instr in ProgressBar(
            pool.imap_unordered(_test_one_instr, iter_inst_test(isa_instr, test)),
            desc="Listing & testing instr.",
            total=len(isa_instr),
        ):
            if support is not None:
                if support == 0:
                    assert isinstance(palmed_instr, Instruction_Pipedream)
                    selected_unsupported.append(palmed_instr)
                elif support == 1:
                    assert isinstance(palmed_instr, Instruction_Pipedream)
                    selected[palmed_instr.extension].append(palmed_instr)
                else:
                    assert isinstance(palmed_instr, str)
                    logger.warning(
                        "Cannot bench instruction %s, ignoring it...", palmed_instr
                    )

    return selected, selected_unsupported


@checkpoint(PalmedStep.INS_IPC, ignore_verifs=True)
def load_pipedream(
    bench: bool = True,
) -> LoadPipedreamCheckpointResult:
    ret: Dict[ISA_Extension, List[Instruction_Pipedream]] = {
        ext: [] for ext in Config.EXTENSIONS
    }
    ret[ISA_BASE] = []
    ret_unsupported: List[Instruction_Pipedream] = []

    selected, selected_unsupported = get_benchable_pipedream_instructions()

    if bench:
        with select_multiprocessing_pool()(Config.effective_num_cpu()) as pool:
            for ext, list_instr in selected.items():
                for result in ProgressBar(
                    pool.imap_unordered(
                        _map_instr_ipc_and_latency,
                        list_instr,
                    ),
                    desc=f"Bench. IPC ({ext})",
                    total=len(list_instr),
                ):
                    if result is None:
                        continue
                    instr_palmed, ipc, lat, supported = result
                    instr_palmed.set_IPC(ipc, commitable=False)
                    instr_palmed.set_latency(lat)
                    # ^^ computed in another process -- this won't be automatically set
                    # and we do not want it (again) in the DB
                    if supported:
                        ret[ext].append(instr_palmed)
                    else:
                        ret_unsupported.append(instr_palmed)
            for result in ProgressBar(
                pool.imap_unordered(
                    _map_instr_ipc_and_latency,
                    selected_unsupported,
                ),
                desc="Bench. IPC (unsupported instr.)",
                total=len(selected_unsupported),
            ):
                if result is None:
                    continue
                instr_palmed, ipc, lat, _ = result
                instr_palmed.set_IPC(ipc, commitable=False)
                instr_palmed.set_latency(lat)
                # ^^ computed in another process -- this won't be automatically set
                ret_unsupported.append(instr_palmed)
        return LoadPipedreamCheckpointResult(ret, ret_unsupported)
    else:
        return LoadPipedreamCheckpointResult(selected, selected_unsupported)
