## \file config.py
## \brief Contains all configuration variables and default values used by palmed
"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import io
from abc import ABC
import logging
import palmed.front_end as fe
from palmed.messages import *
from palmed.checkpoints import PalmedStep, Checkpoint
from pathlib import Path
from typing import Optional, Set, Type, Dict, Any
from enum import Enum

logger = logging.getLogger(__name__)


class ISA_Extension(Enum): ...


class IncompleteConfiguration(Exception):
    """Raised when the configuration is not complete enough for the given state"""


class Config:

    class X86_Extension(ISA_Extension):
        SSE = "SSE"
        AVX = "AVX"

    class ARMv8a_Extension(ISA_Extension):
        SVE = "SVE"
        SIMD = "SIMD"
        SVE2 = "SVE2"

    class SUPPORTED_ISA(Enum):
        X86 = "x86"
        ARMv8a = "ARMv8a"

    ## Default command-line options
    EXTENSIONS: Type[ISA_Extension] = X86_Extension
    MU_ARCH = ""  # µArch
    ISA = SUPPORTED_ISA.X86.value
    DEBUG = False
    QUIET = False
    UOPS: bool
    PIPEDREAM: bool
    NUM_CPU: Optional[int] = None
    DENSE = True
    OVERRIDE_KERNEL = None
    OVERRIDE_KERNEL_FORMAT = None

    ## Logging
    GUROBI_LOG_BASEFILE = Path("gurobi.log")
    GUROBI_LOGFILE: Path
    GUROBI_CSMANAGER: Optional[str] = None
    GUROBI_ACCESSID: Optional[str] = None
    GUROBI_SECRET: Optional[str] = None

    # Write LP in `/tmp/base_system.lp` and IIS (if computed) in `/tmp/infeasible.ilp`
    DUMP_LPS = True

    ## Outputs
    OUT_FOLDER = Path("out")
    DOT_FILE_BASEFILE = Path("mapping.dot")
    DOT_FILE: Path
    PDF_FILE_BASEFILE = Path("mapping.pdf")
    PDF_FILE: Path

    LOGFILE: Optional[Path] = OUT_FOLDER / "palmed.log"

    MAPPING_FILE = Path("mapping")

    ## Checkpoints
    CHECKPOINT = Checkpoint()

    ## Debug only:
    # List of ports supported for this run of Palmed, i.e. instructions that uses
    # anything other than these ports (at parse time) are ignored.
    # Not taken into account if set to `None`.
    UOPS_SUPPORTED_PORTS: Optional[Set[str]]
    UOPS_SUPPORTED_PORTS = None
    # Example: ignore anything other than p0, p1, p6
    # UOPS_SUPPORTED_PORTS = {
    #    "0",
    #    "1",
    #    "6",
    # }

    # Benchmarks
    PIPEDREAM_NO_REGISTER_POOLS = False
    DATABASE_IGNORE_CACHE = False  # Redo all measurements, do not use DB cached values

    ## Benchmark-specific variables
    # Error rate
    MIN_BENCH_COEF = 0.03  # overriden by `init_palmed()` or `init_pipedream()`
    # Threshold from which Pipedream's benchmarks fall back to a
    # "quick" mode and mark instructions as "unsupported" (i.e. maps them
    # to a single resources as these instructions are latency-bound)
    MIN_IPC_PIPE_INSTR = 0.03
    # Multiplicative factor for error-sensitive selection (bound with integer benchmark
    # rounding)
    CLASSES_COEF = 2
    # Maximum denominator when calculating lcm for benchmark
    # coefficients
    MAX_DENUM_BENCH = 10  # overriden by `init_palmed()` or `init_pipedream()`
    # Threshold: if the benchmark contains more instructions, then
    # we add padding inside the microbenchmark's loop body
    NB_INST_START_PADDING = 20

    # Model of the front-end (decoder) to use, defined in `palmed/front_end.py`
    # can be [ fe.NoFrontEnd, fe.fixedFrontEnd(VALUE), fe.A72DispTEnseFrontEnd, fe.A72DispSlackFrontEnd,
    # A72UopLinearFrontEnd, A72UopNoCrossFrontEnd ]
    FRONT_END_MODEL = fe.NoFrontEnd

    ## Heuristics-specific variables
    # Coefficient for a^{k}b and a^{2k}b benchmarks generation
    K_BENCH = 4
    # Number of times a max clique is extracted
    NB_MAX_CLIQUE = 1
    # Number of instructions for min order
    NB_INST = 10
    # Number of instructions executed per benchmark
    NUM_DYN_INST = 500_000

    ## Quadra-specific variables
    NB_BENCH_PER_INSN = 250
    MIN_CLASSES = 50
    MAX_CLASSES = 200

    ## Utils-specific variables
    FRAC_ERROR_RATE = 1e-2

    ## Solver-specific variables
    MAX_RES = 25
    MAX_ERROR_DO_BENCH = 0.1
    MAX_ERROR_DO_NOT_BENCH = 0.5
    # If the IPC of a bench is this close to the max_IPC,
    # the we consider the bench to reach its max IPC
    MAX_IPC_ERROR = 1e-4
    GUROBI_TIME = 300
    GUROBI_LOG = sys.stdout
    GUROBI_ERR = sys.stderr
    MAX_NUM_LP1 = 7

    # Performance library to use
    # This will be passed directly to pipedream.utils.pyperfs.set_perf_lib, unless
    # 'auto' is specified. In this case,
    # * if benching locally, 'auto' will be passed as-is,
    # * else, the benchmarking library will be selected based on the ISA
    PERFLIB: str = "auto"

    # Remote pipedream
    REMOTE_BENCH: bool = False
    REMOTE_PIPEDREAM_USER: Optional[str] = None
    REMOTE_PIPEDREAM_HOST: Optional[str] = None
    REMOTE_PIPEDREAM_PORT: int = 22
    REMOTE_PIPEDREAM_WRAPPER_PATH: Optional[str] = None

    ## Database
    DATABASE_URL = "postgresql+psycopg2:///palmed?host=/var/run/postgresql"

    ## Benching machine
    SELECTED_MACHINE = "localhost"

    @classmethod
    def get_options_dict(cls, pickle_safe=False):
        return {
            k: v
            for k, v in cls.__dict__.items()
            if (
                not k.startswith("_")
                and k.upper() == k
                and (not pickle_safe or not isinstance(v, io.IOBase))
            )
        }

    @classmethod
    def set_options_dict(cls, opt_dict):
        for k, v in opt_dict.items():
            if not k.startswith("_") and k.upper() == k and hasattr(cls, k):
                setattr(cls, k, v)

    @classmethod
    def remote_pipedream_args(cls) -> Optional[Dict[str, Any]]:
        """Arguments required to setup a remote pipedream bencher, or None if the
        remote bencher is not configured"""

        if not cls.REMOTE_BENCH:
            return None
        return {
            "host": cls.REMOTE_PIPEDREAM_HOST,
            "user": cls.REMOTE_PIPEDREAM_USER,
            "port": cls.REMOTE_PIPEDREAM_PORT,
            "wrapper": cls.REMOTE_PIPEDREAM_WRAPPER_PATH,
        }

    @classmethod
    def local_num_cpu(cls) -> int:
        num_cpu = os.cpu_count()
        assert num_cpu is not None
        return num_cpu

    @classmethod
    def default_num_cpu(cls) -> Optional[int]:
        """Get the default number of cores to use"""
        if cls.REMOTE_BENCH:
            return None
        else:
            return cls.local_num_cpu()

    @classmethod
    def effective_num_cpu(cls) -> int:
        """Numbers of CPUs that should effectively be used"""
        if cls.NUM_CPU is not None:
            return cls.NUM_CPU
        raise IncompleteConfiguration(
            "Could not determine automatically the number of CPUs to use. "
            "Please provide a --num_cpu argument. If you are using a remote bencher, "
            "please initialize RemotePipedream before using num_cpu."
        )

    @classmethod
    def init_pipedream(cls):
        cls.PIPEDREAM = True
        cls.UOPS = False
        suffix = f".pipedream.{cls.SELECTED_MACHINE}"
        cls.MIN_BENCH_COEF = 0.03
        cls.MAX_DENUM_BENCH = 10
        Checkpoint.set_suffix(suffix)
        cls.MAPPING_FILE = cls.OUT_FOLDER / Path(cls.MAPPING_FILE.with_suffix(suffix))

    @classmethod
    def init_uops(cls):
        cls.PIPEDREAM = False
        cls.UOPS = True
        suffix = f".uops.{cls.MU_ARCH}"
        Checkpoint.set_suffix(suffix)
        cls.MIN_BENCH_COEF = 0.0
        cls.MAX_DENUM_BENCH = 0.0
        cls.MAPPING_FILE = cls.OUT_FOLDER / Path(cls.MAPPING_FILE.with_suffix(suffix))

    @classmethod
    def init_from_cli_args(cls, args):
        if args is not None:
            setup_logging(args.LOGLEVEL, args.QUIET, cls.LOGFILE)
            cls.DEBUG = args.LOGLEVEL <= logging.DEBUG
            if args.QUIET:
                ProgressBar.disabled = True
            cls.OUT_FOLDER.mkdir(exist_ok=True)
            cls.DOT_FILE = cls.OUT_FOLDER / Path(args.DOT_FILE)
            cls.PDF_FILE = cls.OUT_FOLDER / Path(args.PDF_FILE)
            cls.GUROBI_LOGFILE = cls.OUT_FOLDER / cls.GUROBI_LOG_BASEFILE
            cls.DENSE = args.DENSE
            if not args.ISA in [e.value for e in Config.SUPPORTED_ISA]:
                logger.critical("Unsupported ISA")
                exit(2)
            cls.ISA = args.ISA
            if cls.ISA == cls.SUPPORTED_ISA.X86.value:
                cls.EXTENSIONS = cls.X86_Extension
            elif cls.ISA == cls.SUPPORTED_ISA.ARMv8a.value:
                cls.EXTENSIONS = cls.ARMv8a_Extension
            else:
                raise NotImplementedError("Unsupported ISA:", Config.ISA)
            cls.MU_ARCH = args.ARCH
            cls.OVERRIDE_KERNEL = args.OVERRIDE_KERNEL
            cls.OVERRIDE_KERNEL_FORMAT = args.OVERRIDE_KERNEL_FORMAT
            cls.DATABASE_URL = args.DATABASE_URL
            cls.SELECTED_MACHINE = args.SELECTED_MACHINE
            cls.REMOTE_BENCH = args.REMOTE_BENCH
            if cls.REMOTE_BENCH:
                cls.REMOTE_PIPEDREAM_HOST = (
                    args.REMOTE_PIPEDREAM_HOST or cls.SELECTED_MACHINE
                )
                cls.REMOTE_PIPEDREAM_USER = (
                    args.REMOTE_PIPEDREAM_USER or cls.REMOTE_PIPEDREAM_USER
                )
                cls.REMOTE_PIPEDREAM_PORT = (
                    args.REMOTE_PIPEDREAM_PORT or cls.REMOTE_PIPEDREAM_PORT
                )
                cls.REMOTE_PIPEDREAM_WRAPPER_PATH = (
                    args.REMOTE_PIPEDREAM_WRAPPER_PATH
                    or cls.REMOTE_PIPEDREAM_WRAPPER_PATH
                )

            cls.PERFLIB = args.PERFLIB or cls.PERFLIB

            # Configure NUM_CPU only after remote bencher has been set
            if "NUM_CPU" in args and args.NUM_CPU:
                cls.NUM_CPU = args.NUM_CPU
            else:
                cls.NUM_CPU = cls.default_num_cpu()

            if args.UOPS:
                cls.init_uops()
            else:
                cls.init_pipedream()
            if args.CHECKPOINT is not None:
                cls.CHECKPOINT.set_and_reset_step_after(
                    PalmedStep[args.CHECKPOINT], cls.PIPEDREAM
                )
        cls.CHECKPOINT.create_directories()
