""" Get details on the machine currently selected for benchmarks """

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

## \file current_machine.py
## \brief Gather information on the machine that ran the benchmarks used in this run

from typing import Optional
from functools import lru_cache
import sqlalchemy as sa
from palmed.db import models, this_machine
from palmed.config import Config
from palmed.pipedream_config import RemotePipedream
import palmed.db


class NoSuchMachine(Exception):
    """Raised when the required machine is not found in database"""

    def __init__(self, hostname):
        self.hostname = hostname

    def __str__(self):
        return (
            "Required machine `{}` not found in database. Maybe you forgot to add it "
            "using `palmed-machines`?"
        ).format(self.hostname)


def get_hostname() -> str:
    """Get the selected machine's hostname"""
    selected_machine = Config.SELECTED_MACHINE
    if is_local():
        return this_machine.get_hostname()
    return selected_machine


@lru_cache
def get_pk(hostname: Optional[str] = None) -> int:
    """Get the public key of the given machine in DB"""

    if hostname is None:
        return get_pk(get_hostname())  # abuse cache mechanism

    with palmed.db.Session() as session:
        try:
            return (
                session.query(models.Machine.id)
                .where(models.Machine.hostname == hostname)
                .one()[0]
            )
        except sa.orm.exc.NoResultFound as exn:
            raise NoSuchMachine(hostname) from exn


def is_local() -> bool:
    """Check whether the current machine is the local machine"""
    selected_machine = Config.SELECTED_MACHINE
    return not selected_machine or selected_machine == "localhost"


def is_benching() -> bool:
    """Check whether the current machine is able to run benchmarks for the current
    run"""
    return is_local() or Config.REMOTE_BENCH


def current_experimental_conditions(
    core: Optional[int] = None,
    parallel_cores: int = 1,
) -> models.ExperimentalConditions:
    if is_local():
        return this_machine.current_experimental_conditions()

    assert Config.REMOTE_BENCH
    if core is None or core < 0:
        core = RemotePipedream.remote_core
    assert core is not None and core >= 0

    out = models.ExperimentalConditions(
        num_cpu_booted=None,
        num_cpu_online=Config.effective_num_cpu(),
        num_cpu_used=parallel_cores,
        cpu_id=core,
        sched_is_fifo=True,
    )
    return out
