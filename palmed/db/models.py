"""Database models

Defines the various database models through SQLAlchemy's ORM"""

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

## \file models.py
## \brief Database models

from typing import Any
import enum
import sqlalchemy as sa
from sqlalchemy import orm

Base: Any = orm.declarative_base()


class BenchmarkAggregationMethod(enum.Enum):
    """Various methods to aggregate benchmarks into a final IPC value"""

    UNDEF = 0
    MAX_VAL = 1
    MEAN_25_UPPER_PERCENT = 2


class Machine(Base):
    """A physical machine from the pool of benching machines"""

    __tablename__ = "machine"

    id = sa.Column(sa.Integer, primary_key=True)
    hostname = sa.Column(
        sa.String(256),
        unique=True,
        index=True,
        nullable=False,
        doc="Hostname of the machine",
    )
    location = sa.Column(
        sa.String(256), doc="Human-readable physical location of the machine"
    )
    isa = sa.Column(
        sa.String(32), nullable=False, doc="ISA of the processor; eg. 'ARM'"
    )
    microarchitecture = sa.Column(
        sa.String(32),
        nullable=False,
        doc="Microarchitecture of the processor; eg 'SKX' (Skylake X)",
    )
    cpu_model_name = sa.Column(sa.String(64), nullable=False)
    cpu_model_id = sa.Column(
        sa.Integer, nullable=False, doc="As reported by eg.  `lscpu`"
    )
    sockets = sa.Column(sa.Integer, nullable=False)
    cores_per_socket = sa.Column(sa.Integer, nullable=False)
    threads_per_core = sa.Column(sa.Integer, nullable=False)

    measures = orm.relationship(
        "BenchmarkMeasure", cascade="all, delete-orphan", back_populates="machine"
    )
    latency_measures = orm.relationship(
        "BenchmarkLatencyMeasure",
        cascade="all, delete-orphan",
        back_populates="machine",
    )

    def __str__(self):
        return "<Machine '{hostname}' [{id}]>".format(
            hostname=self.hostname, id=self.id
        )


class Instruction(Base):
    """An instruction in the ISA"""

    __tablename__ = "instruction"

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(
        sa.String(128),
        unique=True,
        index=True,
        nullable=False,
        doc="Instruction name as represented by Pipedream",
    )

    def __str__(self):
        return str(self.name)


class Benchmark(Base):
    """A benchmark formal description"""

    __tablename__ = "benchmark"

    id = sa.Column(sa.Integer, primary_key=True)
    descr = sa.Column(
        sa.Text,
        unique=True,
        nullable=False,
        index=True,
        doc="Formal representation of the benchmark",
    )
    measures = orm.relationship(
        "BenchmarkMeasure", cascade="all, delete-orphan", back_populates="benchmark"
    )
    latency_measures = orm.relationship(
        "BenchmarkLatencyMeasure",
        cascade="all, delete-orphan",
        back_populates="benchmark",
    )

    def __str__(self):
        return str(self.descr)


class BenchmarkLatencyMeasure(Base):
    """A point of data for the latency measure of a given benchmark"""

    __tablename__ = "benchmark_latency_measure"

    id = sa.Column(sa.Integer, primary_key=True)
    benchmark_id = sa.Column(sa.Integer, sa.ForeignKey("benchmark.id"), index=True)
    latency = sa.Column(sa.Float)
    variance = sa.Column(sa.Float, doc="Variance on the latency measure")
    date = sa.Column(sa.DateTime)
    machine_id = sa.Column(sa.Integer, sa.ForeignKey("machine.id"))
    conditions_id = sa.Column(sa.Integer, sa.ForeignKey("experimental_conditions.id"))
    aggregation_mode = sa.Column(
        sa.Enum(
            BenchmarkAggregationMethod,
            name="latency_benchmarkaggregationmethod",
        ),
        doc="How were the results aggregated",
    )
    warmup_repetitions = sa.Column(
        sa.Integer, doc="Number of discarded outer loop repetitions"
    )
    repetitions = sa.Column(
        sa.Integer, doc="Number of kept and aggregated outer loop repetitions"
    )
    instr_count = sa.Column(
        sa.BigInteger, doc="Total number of instructions per outer loop"
    )
    inner_instr_count = sa.Column(
        sa.Integer, doc="Number of instructions per inner loop"
    )
    is_aligned = sa.Column(sa.Boolean, doc="Whether the benchmark was NOP-padded")

    benchmark = orm.relationship("Benchmark", back_populates="latency_measures")
    machine = orm.relationship("Machine", back_populates="latency_measures")
    conditions = orm.relationship(
        "ExperimentalConditions",
        cascade="all, delete-orphan",
        back_populates="latency_measure",
        single_parent=True,
    )

    def __str__(self):
        return "<BenchmarkLatencyMeasure for {id}: {latency}>".format(
            id=self.benchmark_id, latency=self.latency
        )

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return int(self.id)  # no need for hash()


class BenchmarkMeasure(Base):
    """A point of data for the throughput (IPC) measure of a given benchmark"""

    __tablename__ = "benchmark_measure"

    id = sa.Column(sa.Integer, primary_key=True)
    benchmark_id = sa.Column(sa.Integer, sa.ForeignKey("benchmark.id"), index=True)
    ipc = sa.Column(sa.Float)
    orig_ipc = sa.Column(sa.Float)
    variance = sa.Column(sa.Float, doc="Variance on the IPC measure")
    date = sa.Column(sa.DateTime)
    machine_id = sa.Column(sa.Integer, sa.ForeignKey("machine.id"))
    conditions_id = sa.Column(sa.Integer, sa.ForeignKey("experimental_conditions.id"))
    aggregation_mode = sa.Column(
        sa.Enum(
            BenchmarkAggregationMethod,
            name="throughput_benchmarkaggregationmethod",
        ),
        doc="How were the results aggregated",
    )
    warmup_repetitions = sa.Column(
        sa.Integer, doc="Number of discarded outer loop repetitions"
    )
    repetitions = sa.Column(
        sa.Integer, doc="Number of kept and aggregated outer loop repetitions"
    )
    instr_count = sa.Column(
        sa.BigInteger, doc="Total number of instructions per outer loop"
    )
    inner_instr_count = sa.Column(
        sa.Integer, doc="Number of instructions per inner loop"
    )
    is_aligned = sa.Column(sa.Boolean, doc="Whether the benchmark was NOP-padded")
    use_register_pools = sa.Column(
        sa.Boolean,
        doc="Whether `pipebench.Benchmark_Spec` was called with a `register_pools`",
        nullable=False,
    )

    benchmark = orm.relationship("Benchmark", back_populates="measures")
    machine = orm.relationship("Machine", back_populates="measures")
    conditions = orm.relationship(
        "ExperimentalConditions",
        cascade="all, delete-orphan",
        back_populates="measure",
        single_parent=True,
    )

    def __str__(self):
        return "<BenchmarkMeasure for {id}: {ipc}>".format(
            id=self.benchmark_id, ipc=self.ipc
        )

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return int(self.id)  # no need for hash()


class ExperimentalConditions(Base):
    """A summary of the conditions in which a benchmark was conducted"""

    __tablename__ = "experimental_conditions"

    id = sa.Column(sa.Integer, primary_key=True)
    num_cpu_booted = sa.Column(sa.Integer, doc="Number of CPUs available to the OS")
    num_cpu_online = sa.Column(sa.Integer, doc="Number of CPUs available and online")
    num_cpu_used = sa.Column(
        sa.Integer,
        doc="Number of CPUs used for benchmarks when this experiment was conducted",
    )
    cpu_id = sa.Column(sa.Integer, doc="CPU on which this measure was conducted")
    # cpu_temp = sa.Column(sa.Float, doc="Temperature (in celsius) of this CPU core")
    sched_is_fifo = sa.Column(sa.Boolean, doc="Was the CPU in FIFO scheduling mode?")

    measure = orm.relationship(
        "BenchmarkMeasure",
        back_populates="conditions",
        uselist=False,
    )
    latency_measure = orm.relationship(
        "BenchmarkLatencyMeasure",
        back_populates="conditions",
        uselist=False,
    )

    def __str__(self):
        return "<ExperimentalConditions: CPU {cpu}/{num_cpu}>".format(
            cpu=self.cpu_id, num_cpu=self.cpu_online
        )
