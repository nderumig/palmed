""" Entrypoint to the database part of palmed """

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import logging
import os
import uuid
import threading
from typing import Optional
from contextlib import contextmanager
import sqlalchemy as sa
from . import models, this_machine, current_machine
from palmed.config import Config

engine: Optional[sa.engine.Engine] = None
session_scope: Optional[sa.orm.Session] = None

logger = logging.getLogger(__name__)


class DatabaseException(Exception):
    def __init__(self, what):
        super().__init__()
        self.what = what

    def __str__(self):
        return self.what


def Session(*args, **kwargs):
    if session_scope is None:
        raise DatabaseException("Database is not initialized")
    return session_scope(*args, **kwargs)


@contextmanager
def ensure_session(session: Optional[sa.orm.Session] = None):
    """context manager taking a possibly-None session as argument, opening one only if
    necessary, and closing it only if it opened it itself."""
    if session is None:
        session = Session()
        with session as inner:
            yield inner
    else:
        yield session


def init_engine(db_engine: Optional[sa.engine.Engine] = None, force: bool = False):
    global engine, session_scope

    if engine is not None and not force:
        raise DatabaseException("Database is already initialized")
    if db_engine is None:
        db_engine = sa.create_engine(Config.DATABASE_URL)
    engine = db_engine
    session_scope = sa.orm.scoped_session(sa.orm.sessionmaker(bind=engine))
