""" Various entrypoints for easy access of the database models """

## \file entry.py
## \brief Entrypoint functions for the DB submodule

# PYTHON_ARGCOMPLETE_OK

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import argcomplete, argparse
import logging
from typing import Callable, Dict, Any, Optional
import sys
import sqlalchemy as sa
from palmed.config import Config
from palmed import messages
from palmed.db import models, this_machine
from palmed import db

logger = logging.getLogger(__name__)


def setup_base_parser(*args, **kwargs) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(*args, **kwargs)
    parser.add_argument("--db-url", default=Config.DATABASE_URL)
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_const",
        const=logging.INFO,
        default=logging.WARNING,
        help="configure logger to show information messages",
        dest="loglevel",
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        const=logging.DEBUG,
        default=logging.WARNING,
        help="configure logger to show debug messages",
        dest="loglevel",
    )

    return parser


def list_machines():
    """List the existing machines"""
    row_format = (
        "│ {hostname:<25} │ {location:<15} │ {isa:<6} │ {microarch:<5} │ {cpu:<50} │ "
        "{sockets:>5} │ {cores:>5} │"
    )
    head_row = row_format.format(
        hostname="Hostname",
        location="Location",
        isa="ISA",
        microarch="μarch",
        cpu="CPU model",
        sockets="#sock",
        cores="#core",
    )
    print(head_row)
    print("─" * len(head_row))

    row_count = 0
    with db.Session() as session:
        for row in session.execute(sa.select(models.Machine)):
            row_count += 1
            machine = row[0]
            print(
                row_format.format(
                    hostname=machine.hostname,
                    location=machine.location,
                    isa=machine.isa,
                    microarch=machine.microarchitecture,
                    cpu=machine.cpu_model_name,
                    sockets=machine.sockets,
                    cores=machine.sockets * machine.cores_per_socket,
                )
            )
    print("({} rows)".format(row_count))


def yesno_prompt(msg, default_yes=False):
    print("{} [{}] ".format(msg, "Y/n" if default_yes else "y/N"))
    is_fine = input()
    nondefault_option = "n" if default_yes else "y"
    if is_fine.lower() != nondefault_option:
        return default_yes
    return not default_yes


class BadPrefill(Exception):
    pass


def prompt_machine(
    prefill: Callable[[str], Dict[str, Any]], default_hostname: Optional[str] = None
) -> Dict[str, Any]:
    """Prompts the user to fill in a machine's details. Prefills the fields with
    `prefill`, based on the hostname."""

    hostname: str = ""
    details: Dict[str, Any] = {}
    filled_once = False

    while True:
        if not filled_once:
            if hostname:
                default_hostname = hostname
                hostname = ""
            while not hostname:
                print("Hostname [default={}]:".format(default_hostname), end=" ")
                hostname = input() or default_hostname or ""
            try:
                details = prefill(hostname)
            except BadPrefill:
                continue

        for field, ftype in [
            ("location", str),
            ("isa", str),
            ("microarchitecture", str),
            ("cpu_model_name", str),
            ("cpu_model_id", int),
            ("sockets", int),
            ("cores_per_socket", int),
            ("threads_per_core", int),
        ]:
            while True:
                default = details.get(field, None)
                print("{} [default={}]:".format(field, default), end=" ")
                try:
                    val = input()
                    details[field] = ftype(val or default)
                    if not details[field]:
                        continue
                except ValueError:
                    continue
                break
        filled_once = True

        print(
            "\nAdding machine <{}>:\n{}".format(
                hostname,
                "\n".join(
                    map(lambda row: "{}: {}".format(row[0], row[1]), details.items())
                ),
            )
        )
        if yesno_prompt("Is this fine?"):
            break

    details["hostname"] = hostname
    return details


def add_machine():
    """Add a new machine"""

    default_hostname = this_machine.get_hostname()

    def prefill(hostname: str):
        with db.Session() as session:
            matching_count = (
                session.query(models.Machine.hostname)
                .where(models.Machine.hostname == hostname)
                .count()
            )
            if matching_count > 0:
                logging.error("There is already a machine with this hostname")
                raise BadPrefill

        if hostname != default_hostname:
            return {}
        autofill = {}
        cpu_details = this_machine.get_cpu_details()
        if cpu_details is None:
            return {}
        autofill["isa"] = cpu_details.get("Architecture", None)
        autofill["cpu_model_name"] = cpu_details.get("Model name", None)
        autofill["cpu_model_id"] = cpu_details.get("Model", None)
        autofill["sockets"] = cpu_details.get("Socket(s)", None)
        autofill["cores_per_socket"] = cpu_details.get("Core(s) per socket", None)
        autofill["threads_per_core"] = cpu_details.get("Thread(s) per core", None)
        return autofill

    details = prompt_machine(prefill, default_hostname)
    machine = models.Machine(**details)
    with db.Session() as session:
        session.add(machine)
        session.commit()


def edit_machine():
    """Edit an existing machine"""
    default_hostname = this_machine.get_hostname()

    def prefill(hostname: str):
        with db.Session() as session:
            try:
                machine = (
                    session.query(models.Machine)
                    .where(models.Machine.hostname == hostname)
                    .one()
                )
            except sa.exc.NoResultFound as exn:
                logger.error("Machine with hostname <%s> not found.", hostname)
                raise BadPrefill from exn
            except sa.exc.MultipleResultsFound as exn:
                logger.error("Multiple machines with hostname <%s> found.", hostname)
                raise BadPrefill from exn

        autofill = {}
        autofill["location"] = machine.location
        autofill["isa"] = machine.isa
        autofill["microarchitecture"] = machine.microarchitecture
        autofill["cpu_model_name"] = machine.cpu_model_name
        autofill["cpu_model_id"] = machine.cpu_model_id
        autofill["sockets"] = machine.sockets
        autofill["cores_per_socket"] = machine.cores_per_socket
        autofill["threads_per_core"] = machine.threads_per_core
        return autofill

    details = prompt_machine(prefill, default_hostname)
    with db.Session() as session:
        machine = (
            session.query(models.Machine)
            .where(models.Machine.hostname == details["hostname"])
            .one()
        )
        for attr, val in details.items():
            sa.orm.attributes.set_attribute(machine, attr, val)
        session.commit()


def delete_machine():
    """Delete an existing machine"""
    print("Hostname: ", end="")
    hostname = input().strip()
    with db.Session() as session:
        try:
            query = session.query(models.Machine).where(
                models.Machine.hostname == hostname
            )
            machine = query.one()
        except sa.exc.NoResultFound:
            logger.error("Machine with hostname <%s> not found.", hostname)
            sys.exit(1)
        print(
            "Deleting <{}> would delete {} experiments as well. ".format(
                hostname, len(machine.measures)
            ),
            end="",
        )
        if yesno_prompt("Is this fine?"):
            query.delete()
            session.commit()
            logger.info("Recursively deleted <%s> and its experiments", hostname)
        else:
            logger.info("Nothing deleted, aborting.")


def entry_palmed_machines():
    """Entrypoint for listing/editing machines"""
    parser = setup_base_parser(description="List or edit the benching machines")
    parser.add_argument(
        "action",
        nargs="?",
        default="list",
        choices=["list", "add", "edit", "delete"],
        help=(
            "add:\tbe prompted to add a new machine.\n"
            "edit:\tbe prompted to edit an existing machine.\n"
            "delete:\tbe prompted for a machine to delete from the database.\n"
            "list:\t[default] list existing machines.\n"
        ),
    )
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    messages.setup_logging(args.loglevel)

    db.init_engine(sa.create_engine(args.db_url))
    action_endpoints = {
        "list": list_machines,
        "add": add_machine,
        "edit": edit_machine,
        "delete": delete_machine,
    }
    action_endpoints[args.action]()
