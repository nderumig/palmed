""" Get the local machine, and default values based on system informations """

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

## \file this_machine.py
## \brief Gather information on the machine on which palmed is running

import socket
import subprocess
import os
import re
from pathlib import Path
from functools import lru_cache
from typing import Optional, Dict, Tuple
from palmed.db import models
from palmed.utils import BenchmarkScheduler
import palmed.db


def get_hostname():
    """Get the local machine's hostname"""
    return socket.gethostname()


@lru_cache
def get_pk():
    """Get the local machine's primary key in the database"""
    return palmed.db.current_machine.get_pk(hostname=get_hostname())


def get_cpu_details() -> Optional[Dict[str, str]]:
    """Get the local machine's CPU details"""

    def get_lscpu():
        try:
            return subprocess.run(
                ["lscpu"], capture_output=True, check=True, encoding="utf8"
            ).stdout
        except subprocess.CalledProcessError:
            return None

    lscpu_text = get_lscpu()
    if not lscpu_text:
        return None

    details = {}
    for line in lscpu_text.split("\n"):
        try:
            key, val = list(map(lambda x: x.strip(), line.split(":")))
        except ValueError:
            continue
        details[key] = val
    return details


@lru_cache(maxsize=None)
def get_core_counts() -> Tuple[int, int]:
    """Returns the number of online and booted cores
    Online: available for processing at the moment
        (/sys/devices/system/cpu/cpuN/online)
    Booted: seen by the system at all (not BIOS-disabled)"""

    is_cpuN = re.compile("^cpu[0-9]+$")

    nb_online = 0
    nb_booted = 0

    basepath = Path("/sys/devices/system/cpu/")
    for child_dir in basepath.iterdir():
        if not child_dir.is_dir():
            continue
        if not is_cpuN.match(child_dir.name):
            continue

        nb_booted += 1
        online_path = child_dir / "online"
        if not online_path.is_file():  # assume CPU cannot be set offline -- thus online
            nb_online += 1
        else:
            with online_path.open("r") as handle:
                nb_online += handle.read().strip() == "1"
    return nb_booted, nb_online


def current_experimental_conditions(
    core: Optional[int] = None,
    parallel_cores: int = 1,
) -> models.ExperimentalConditions:
    if core is None or core < 0:
        affinity = BenchmarkScheduler.get_specified_affinity(core)
        assert len(affinity) == 1
        core = affinity.pop()
    sched_fifo = os.sched_getscheduler(0) == os.SCHED_FIFO
    core_booted, core_online = get_core_counts()

    out = models.ExperimentalConditions(
        num_cpu_booted=core_booted,
        num_cpu_online=core_online,
        num_cpu_used=parallel_cores,
        cpu_id=core,
        sched_is_fifo=sched_fifo,
    )
    return out
