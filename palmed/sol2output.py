## \file sol2output.py
## \brief Convert the sol file output by gurobi to a dot graph or on of Python's internal
## structures

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import gurobipy as gp
import logging
import pickle
import re
import subprocess
import palmed.benchmarks as bench
from collections import defaultdict
from pathlib import Path
from palmed import ports as pt
from typing import Set, List, Dict, Tuple, Optional, Type, DefaultDict, Union, FrozenSet
from palmed.config import Config
from palmed.utils import get_frac, get_frac_str
from palmed.instructions import Instruction
from palmed.instructions import (
    Instruction_UOPS,
    pipedream_to_uops,
    instr_to_uops,
    InstructionNotFound,
)

logger = logging.getLogger(__name__)


def read_solution_integer(
    rho_a_vars: Dict[Tuple[Instruction, int], gp.Var]
) -> Tuple[Dict[Instruction, List[Tuple[float, str]]], Dict[str, bench.Benchmark]]:
    mapping: Dict[Instruction, List[Tuple[float, str]]] = {}
    for (ins, res), var in rho_a_vars.items():
        if var.x == 1:
            if mapping.get(ins) is None:
                mapping[ins] = []
            mapping[ins].append((1, f"R{res}"))
    return mapping, {}


# Warning, this does not ensure that all resources are present in
# the saturating_bench returned
def read_solution(
    rho_a_vars: Dict[Tuple[Instruction, int], gp.Var],
    rho_k_vars: Dict[Tuple[bench.Benchmark, int], gp.Var],
    epsilon_frac: float,
) -> Tuple[Dict[Instruction, List[Tuple[float, str]]], Dict[str, bench.Benchmark]]:
    mapping: Dict[Instruction, List[Tuple[float, str]]] = {}
    for (ins, res), var in rho_a_vars.items():
        if var.x > Config.FRAC_ERROR_RATE / 2:
            if mapping.get(ins) is None:
                mapping[ins] = []
            num, denum = get_frac(var.x, precision=epsilon_frac)
            mapping[ins].append((num / denum, f"R{res}"))

    bench_to_sat: DefaultDict[bench.Benchmark, float] = defaultdict(float)
    bench_to_sat_res: DefaultDict[bench.Benchmark, List[int]] = defaultdict(list)
    for (ben, res), var in rho_k_vars.items():
        bench_to_sat[ben] += var.x
        # Do not forget the error rate!
        error_rate = Config.FRAC_ERROR_RATE / 2
        if var.x >= 1.0 - error_rate:
            bench_to_sat_res[ben].append(res)

    saturating_bench: Dict[str, bench.Benchmark] = {}
    for ben, sat in bench_to_sat_res.items():
        for res in sat:
            res_name = f"R{res}"
            if saturating_bench.get(res_name) is None:
                saturating_bench[res_name] = ben
            elif bench_to_sat[ben] < bench_to_sat[saturating_bench[res_name]]:
                saturating_bench[res_name] = ben
            elif bench_to_sat[ben] == bench_to_sat[saturating_bench[res_name]]:
                if ben.card() < saturating_bench[res_name].card():
                    saturating_bench[res_name] = ben
    return mapping, saturating_bench


def mapping_to_res_to_instrs(
    mapping: Dict[Instruction, List[Tuple[float, str]]]
) -> Dict[str, List[Instruction]]:
    res_to_instrs: Dict[str, List[Instruction]] = {}
    for _, list_edges in mapping.items():
        for _, res in list_edges:
            res_to_instrs[res] = []
    for instr, list_edges in mapping.items():
        for _, res in list_edges:
            res_to_instrs[res] += [instr]
    return res_to_instrs


# Attribute port names to abstract resources
def label_mapping(
    mapping: Dict[Instruction, List[Tuple[float, str]]]
) -> Dict[str, str]:
    res_to_port: Dict[str, Set[pt.Port]] = {}
    for _, resources in mapping.items():
        for _, res in resources:
            res_to_port[res] = pt.Port.all_ports()

    for instr, resources in mapping.items():
        for val, res in resources:
            used_ports: Set[pt.Port] = set()
            try:
                instr_casted = instr_to_uops(instr)
                for _, port in instr_casted.ports():
                    used_ports.add(port)
                res_to_port[res] = res_to_port[res] & used_ports
            except InstructionNotFound:
                pass

    label: Dict[str, str] = {}
    seen: Set[str] = set()
    for res, set_res in res_to_port.items():
        mini: Optional[pt.Port] = None
        for port in set_res:
            if not (isinstance(mini, pt.Port)) or len(port) < len(mini):
                mini = port
        if isinstance(mini, pt.Port):
            for port in set_res:
                if len(port) == len(mini) and port != mini:
                    # Determinist: chose lowest number
                    chosen = (
                        mini if int(port.name()[1:]) > int(mini.name()[1:]) else port
                    )
                    logger.warning(
                        "Two ports (%s, %s) may correspond to "
                        "the following resource %s, choosing %s",
                        mini.name(),
                        port.name(),
                        res,
                        chosen.name(),
                    )
                    mini = chosen
            if mini.name() in seen:
                logger.warning(
                    "Duplicated resource %s, second detection on %s, ignoring",
                    mini.name(),
                    res,
                )
            else:
                label[res] = mini.name()
                seen.add(mini.name())
                logger.info("%s -> %s", res, mini.name())
        else:
            logger.error("No resource name found for %s", res)

    # No resource has been found
    for instr, ports in mapping.items():
        for _, res in ports:
            if label.get(res) == None:
                label[res] = res
    return label


# Simple edge generator, no labeling
def gen_edges(
    mapping: Dict[Instruction, List[Tuple[float, str]]]
) -> List[Tuple[Instruction, str, float]]:
    ret: List[Tuple[Instruction, str, float]] = []
    for instr, resources in mapping.items():
        for res in resources:
            ret += [(instr, res[1], res[0])]
    return ret


# Verify that the edge are valid, i.e. that
# - the value is correct
# - no edges are missing
# Returns a list of (instruction_name, res, val) annotated with their correctness:
# val == 0 => correct
# val == -1 => missing
# val == other => incorrect value (reported)
# Also returns a set of undetected resources
def check_edges(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    add_missing_edges: bool = True,
    integer: bool = False,
) -> Tuple[List[Tuple[Instruction, str, float]], Set[str]]:
    epsilon: float = Config.FRAC_ERROR_RATE
    out_graph: List[Tuple[Instruction, str, float]] = []
    set_res: Set[str] = set()
    for _, resources in mapping.items():
        for _, res in resources:
            set_res.add(label[res])

    for instr, resources in mapping.items():
        seen: Dict[str, int] = {}
        try:
            instr_casted = instr_to_uops(instr)
            for mult, port in instr_casted.ports():
                seen[port.name()] = mult
        except InstructionNotFound:
            pass

        for edge, res in resources:
            if seen.get(label[res]) == None:
                # The edge does not correspond to anything
                out_graph += [(instr, label[res], edge)]
            elif (
                integer
                or abs(
                    (seen[label[res]] - edge * (len(label[res][1:]))) / seen[label[res]]
                )
                <= epsilon
            ):
                # len(label[res][1:]) corresponds to the number of ports in the combined port
                # label[res], i. e. len("0156") = 4 for p0156
                # The edge is correct
                out_graph += [(instr, label[res], 0)]
                seen[label[res]] = 0
            else:
                # Wrong value
                out_graph += [(instr, label[res], edge)]
                seen[label[res]] = 0
        if add_missing_edges:
            for port_str, mult in seen.items():
                # missing
                if mult != 0 and (port_str in set_res):
                    out_graph += [(instr, port_str, -1)]
    out_graph.sort(key=lambda triplet: triplet[0].name())
    unseen_res = set(map(lambda x: x.name(), pt.Port.all_ports())) - set_res
    return out_graph, unseen_res


def export_dot(
    graph: List[Tuple[Instruction, str, float]], epsilon: Optional[float] = None
):
    dot_file = open(Config.DOT_FILE, "w")
    dot_file.write("graph bipartite {\n")
    for instr, res, value in graph:
        dot_file.write(
            f'\t"{instr.name()} \\n\
{instr._ports_str}\\n(Real IPC = {instr.IPC()})" -- '
        )

        if value == -1:
            # Missing edge
            dot_file.write(f'"{res}" [color=red];\n')
        elif value == 0:
            dot_file.write(f'"{res}" [style=dashed];\n')
        elif value > 0:
            dot_file.write(
                f'"{res}" [label="\
{get_frac_str(value, precision = epsilon)}", penwidth = 3];\n'
            )
    dot_file.write("}")
    dot_file.close()


def print_errors(
    graph: List[Tuple[Instruction, str, float]],
    epsilon: Optional[float],
    instr_to_classes: Dict[Instruction, List[Instruction]],
    unseen_res: Set[str],
    instructions: Dict[str, List[Instruction]],
) -> None:
    err_str: List[str] = []
    old_instr: Optional[Instruction_UOPS] = None
    nb_incorrect = 0
    nb_instr = 0
    for instr_unknown, res, value in graph:
        try:
            instr = instr_to_uops(instr_unknown)
        except InstructionNotFound:
            logger.info(
                "Instruction %s not found in UOPS database, cannot compare",
                instr_unknown.name(),
            )
            continue
        if old_instr != instr and err_str != [] and old_instr is not None:
            logger.warning(
                "%s not correctly mapped (representing %d instructions)",
                old_instr.name(),
                len(instr_to_classes[old_instr]),
            )
            nb_incorrect += len(instr_to_classes[old_instr])
            logger.warning(
                "%s (%s) IPC: %f",
                old_instr.name(),
                old_instr._ports_str,
                old_instr.IPC(),
            )
            for msg in err_str:
                logger.warning(msg)
            err_str = []
        if value == -1:
            # Missing edge
            err_str += [f"    Missing edge: {res}, value should be "]
            for mult, port in instr.ports():
                if port.name() == res:
                    err_str[-1] += f"{mult}"
                    break

        if value > 0:
            # Wrong edge
            err_str += [
                f"    Wrong value for edge {instr.name()} -> "
                f"{res}: got {get_frac_str(value, precision=epsilon)} instead of "
            ]
            found = False
            for mult, port in instr.ports():
                if port.name() == res:
                    err_str[-1] += f"{mult}"
                    found = True
                    break
            if not (found):
                err_str[-1] += "0"
        if instr != old_instr:
            old_instr = instr
            nb_instr += len(instr_to_classes[old_instr])
    logger.info("Found %d/%d incorrectly mapped instructions", nb_incorrect, nb_instr)

    all_instructions: List[Instruction] = []
    for instrs in instructions.values():
        all_instructions += instrs

    if len(unseen_res) > 0:
        logger.warning("Missed %d resource(s):", len(unseen_res))
    for res_str in unseen_res:
        logger.warning("    %s", res_str)
        maskable_candidates = set(pt.Port.all_ports())
        carac_bench: List[Instruction_UOPS] = []
        for instruction in all_instructions:
            uops_ins = instr_to_uops(instruction)
            amount = -1
            unseen_res_port: pt.Port
            for amount_test, port_test in uops_ins.ports():
                if port_test.name() == res_str:
                    amount = amount_test
                    unseen_res_port = port_test
                    break

            if amount == -1:
                continue

            min_amount = 0
            # Look for the biggest resource lower than the undetected one
            biggest_p: Union[FrozenSet, pt.Port] = frozenset({})
            for port in pt.Port.all_ports():
                if biggest_p < port and port < unseen_res_port:
                    biggest_p = port

            # If we found one, then we need to detect a usage of the
            # unseen port bigger than what its throughput.
            # i.e. if pXY is not detected but pX is, then we need at least
            # 2 pXY to detect pXY (as 1pXY alone can just go to pY)
            if biggest_p != frozenset({}):
                min_amount = len(unseen_res_port) - len(biggest_p)

            loc_maskable = set()
            unseen_res_amount = 0
            for amount_test, port_test in uops_ins.ports():
                # Port is a frozenset, so < and > are relative to inclusion
                # if the unseen res is pYX, then count usage(pXY) as
                # usage(pXY) - usage(pX) (direct contribution)
                unseen_res_amount = amount
                if port_test < unseen_res_port:
                    unseen_res_amount -= amount_test

                # No real amount, only back-edges induced one or undetectable ones
                if unseen_res_amount <= min_amount:
                    break

                # The load of the port **excluding the load induced by the the shadowed
                # resource** must be bigger than the load on the shadowed resource
                # (definition of a shadowed resource)
                if unseen_res_amount / len(unseen_res_port) < amount_test / len(
                    port_test
                ):
                    loc_maskable.add(port_test)
            # Not a detectable usage of `unseen_res_port`, continuing
            if unseen_res_amount <= min_amount:
                continue

            if len(maskable_candidates) > 0:
                if len(maskable_candidates & loc_maskable) < len(maskable_candidates):
                    carac_bench.append(uops_ins)
                maskable_candidates = maskable_candidates & loc_maskable
                if len(maskable_candidates) == 0:
                    break

        if len(maskable_candidates) > 0:
            logger.warning(
                "        (but %s is masked by %s)", res_str, maskable_candidates
            )
            if len(carac_bench) > 0:
                logger.warning("        Instructions worth looking into:")
                for uops_ins in carac_bench:
                    logger.warning(
                        "            %s (%s)", uops_ins.name(), uops_ins._ports_str
                    )
        else:
            logger.warning("        Can be detected using the following instructions:")
            for uops_ins in carac_bench:
                logger.warning(
                    "            %s (%s)", uops_ins.name(), uops_ins._ports_str
                )


def run_dot():
    subprocess.run(["dot", "-Tpdf", "-o", Config.PDF_FILE, Config.DOT_FILE])


def print_mapping(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Optional[Dict[str, str]],
    epsilon: Optional[float] = None,
    class_to_instrs: Optional[Dict[Instruction, List[Instruction]]] = None,
):
    for instr, list_res in mapping.items():
        to_print = ""
        if isinstance(label, Dict):
            for res in list_res:
                to_print += (
                    f" {get_frac_str(res[0], precision=epsilon)}*{label[res[1]]}"
                )
            logger.info(
                "    %s (%s):%s",
                instr.name(),
                instr._ports_str,
                to_print,
            )
            if class_to_instrs is not None:
                for instr_of_class in class_to_instrs[instr]:
                    logger.info("        %s", instr_of_class.name())
        else:
            for res in list_res:
                to_print += f" {get_frac_str(res[0], precision=epsilon)}*{res[1]}"
            logger.info("    %s :%s", instr.name(), to_print)
            if class_to_instrs is not None:
                for instr_of_class in class_to_instrs[instr]:
                    logger.info("        %s", instr_of_class.name())


# Map all instructions to a single resource
def add_failed_instructions(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    class_to_instrs: Dict[Instruction, List[Instruction]],
    instructions: List[Instruction],
):
    res_name = "R_Other"
    label[res_name] = res_name

    IPC_to_inst: Dict[float, Instruction] = {}
    for instr in instructions:
        if IPC_to_inst.get(instr.IPC()) == None:
            if instr.IPC() == 0:
                logger.warning(
                    "Instruction %s has a very low IPC, cannot fall back: ignoring",
                    instr.name(),
                )
                continue
            IPC_to_inst[instr.IPC()] = instr
            class_to_instrs[instr] = [instr]
            mapping[instr] = [(1 / instr.IPC(), res_name)]
        else:
            class_to_instrs[IPC_to_inst[instr.IPC()]] += [instr]


def save_mapping(
    mapping: Dict[Instruction, List[Tuple[float, str]]],
    label: Dict[str, str],
    class_to_instrs: Dict[Instruction, List[Instruction]],
    filename: Path,
):
    savedict = {
        "label": label,
        "class_to_instrs": class_to_instrs,
        "mapping": mapping,
        "ISA": Config.ISA,
        "MU_ARCH": Config.MU_ARCH,
    }
    with open(filename, "wb") as savefile:
        pickle.dump(savedict, savefile)


def read_mapping(
    filename: str,
) -> Tuple[
    Dict[Instruction, List[Tuple[float, str]]],
    Dict[str, str],
    Dict[Instruction, List[Instruction]],
]:
    with open(filename, "rb") as savefile:
        savedict = pickle.load(savefile)

    return (
        savedict["mapping"],
        savedict["label"],
        savedict["class_to_instrs"],
    )
