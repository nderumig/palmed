## \file heuristics.py
## \brief Give heuristics to find a set of instruction big enough to discover all resources
## and all links between each instructions and their used resources

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import pickle
import logging
import palmed.benchmarks as bench
import palmed.instructions as ins
import palmed.solver as sol
from typing import Set, List, Dict, Tuple, Type, Iterable, Optional
from palmed.config import Config, ISA_Extension
from palmed.messages import ProgressBar
from palmed.checkpoints import PalmedStep, checkpoint
from palmed.pinned_pool import select_multiprocessing_pool, SingleCorePool

logger = logging.getLogger(__name__)


def _compute_IPC(bench):
    return bench.IPC()


# Returns True if the instructions are compatible, i.e. when the two
# instructions kernel[i] and instr can be mapped without error
def add_instr_to_kernel(
    instr: ins.Instruction,
    kernel: List[ins.Instruction],
    benchs: List[bench.Benchmark],
    enforced=False,
    progress_bar: Optional[ProgressBar] = None,
):
    assert instr not in kernel
    pool = (
        select_multiprocessing_pool().get_pool()
    )  # might be None if no pool is running

    benchs.append(bench.Benchmark(instr))

    local_benchs: List[bench.Benchmark] = []

    for instr2 in kernel:
        if not instr.is_compatible_with(instr2):
            continue
        # Generating benchmarks
        new_benchs: List[bench.Benchmark] = []
        new_benchs.append(bench.Benchmark(instr, instr.IPC(), instr2, instr2.IPC()))
        if Config.UOPS:
            new_benchs.append(
                bench.Benchmark(
                    instr,
                    Config.K_BENCH * instr.IPC() + 1,
                    instr2,
                    Config.K_BENCH * instr2.IPC(),
                )
            )
            new_benchs.append(
                bench.Benchmark(
                    instr,
                    Config.K_BENCH * instr.IPC(),
                    instr2,
                    Config.K_BENCH * instr2.IPC() + 1,
                )
            )
            new_benchs.append(bench.Benchmark(instr, Config.K_BENCH, instr2, 1))
            new_benchs.append(bench.Benchmark(instr, 1, instr2, Config.K_BENCH))

        local_benchs += new_benchs

    # Remove potential doubles
    old_length = len(local_benchs)
    local_benchs = list(set(local_benchs))
    skipped_iters = old_length - len(local_benchs)

    progress_update_step = 5 if Config.UOPS else 1

    def update_bar(idx: int, do_update: bool = True) -> None:
        if progress_bar and do_update and (idx + 1) % progress_update_step == 0:
            progress_bar.update()

    # Benchmarking IPC
    if pool:
        ipcs = pool.imap(_compute_IPC, local_benchs)
        for pos, ipc in enumerate(ipcs):
            local_benchs[pos].set_IPC(ipc)
            update_bar(pos)
    else:
        for idx, cur_bench in enumerate(local_benchs):
            cur_bench.IPC()
            update_bar(idx)

    # Update the skipped iters (due to doubles in the gen. benchs.)
    if progress_bar:
        progress_bar.update(skipped_iters)

    benchs += local_benchs

    kernel.append(instr)


## HEURISTIC Nico
# Extract all the possible sub-clique of size > SIZE
def largest_clique(
    kernel: List[ins.Instruction],
    potential_instructions: List[ins.Instruction],
    benchs: List[List[bench.Benchmark]],
):
    # Sort by (degree, IPC)
    dep = [
        [0 for i in range(len(potential_instructions))]
        for j in range(len(potential_instructions))
    ]
    for i, instr1 in enumerate(potential_instructions):
        for j, instr2 in enumerate(potential_instructions):
            if (
                abs(benchs[i][j].IPC() - instr1.IPC() - instr2.IPC())
                > Config.MIN_BENCH_COEF
            ):
                dep[i][j] = 1

    nb_dep: Dict[int, int] = {}
    for i in range(len(potential_instructions)):
        ret = 0
        for j in range(len(potential_instructions)):
            if dep[i][j]:
                ret += 1
        nb_dep[i] = ret

    # Sort by increasing neighbour, decreasing IPC
    potential_instruction_id = list(range(len(potential_instructions)))
    potential_instruction_id.sort(
        key=lambda x: (nb_dep[x], -potential_instructions[x].IPC()), reverse=False
    )
    to_add: List[int] = []
    nb_clique = 0
    while nb_clique < Config.NB_MAX_CLIQUE:
        for i in to_add:
            potential_instruction_id.remove(i)
        to_add = []
        added = True
        while added:
            added = False
            for i in potential_instruction_id:
                added = True
                for j in to_add:
                    if dep[i][j]:
                        added = False
                if added:
                    break
            if added:
                to_add.append(i)
        for i in to_add:
            kernel.append(potential_instructions[i])
        nb_clique += 1

    logger.info("Largest Clique is composed of %d instructions", len(kernel))
    logger.info("Largest Clique:")
    for instr in kernel:
        logger.info("\t%s (%s, IPC = %f)", instr.name(), instr._ports_str, instr.IPC())


## HEURISTIC FAB
# Take one after the others the minima of the order (solve ties by taking the highest
# individual IPC):
# CAREFUL: read attentivly the meaning of \f$\preceq\f$
# n is smaller than m when the IPC of np is *greater* than the one of mp
# \f$n\preceq m \Leftrightarrow \forall p, \bar{n^{\bar{n}}p^{\bar{p}}} > \bar{m^{\bar{m}}p^{\bar{p}}}\f$
# Returns the first set of minima.
def minima_order_increasing_ipc(
    kernel: List[ins.Instruction],
    potential_instructions: List[ins.Instruction],
    benchs: List[List[bench.Benchmark]],
) -> List[ins.Instruction]:
    potential_instruction_id = list(range(len(potential_instructions)))
    V: Set[int] = set()
    E: Set[Tuple[int, int]] = set()
    for n in potential_instruction_id:
        V.add(n)
        for m in potential_instruction_id:
            lower = True
            for p in potential_instruction_id:
                if benchs[n][p].IPC() < benchs[m][p].IPC():
                    lower = False
                    break
            if lower:
                E.add((n, m))

    # Degrees of each vertex
    deg: Dict[int, int] = {}
    for n in V:
        deg[n] = 0
    for n in V:
        for m in V:
            if (n, m) in E:
                deg[n] += 1

    if Config.DEBUG:
        logger.debug("Edges found:")
        for n in V:
            logger.debug(
                "%s %s (%d):",
                potential_instructions[n].name(),
                potential_instructions[n]._ports_str,
                deg[n],
            )
            for m in V:
                if (n, m) in E:
                    logger.debug(
                        "     %s (%d, %s)",
                        potential_instructions[m].name(),
                        deg[m],
                        potential_instructions[m]._ports_str,
                    )

    # Set of the selected nodes
    S: List[int] = []
    while len(S) < Config.NB_INST and len(V) != 0:
        mins: Set[int] = set()
        for n in V:
            is_min = True
            for p in V:
                if p != n and (p, n) in E:
                    is_min = False
                    break
            if is_min:
                mins.add(n)
        max_deg = list(mins)[0]
        for n in mins:
            if deg[n] > deg[max_deg]:
                max_deg = n

        S.append(max_deg)
        V.remove(max_deg)
        mins.remove(max_deg)

    ret: List[ins.Instruction] = []
    logger.info("Min Order:")
    for i in S:
        kernel.append(potential_instructions[i])
        ret.append(potential_instructions[i])
        logger.info(
            "\t%s (%s, IPC = %f)",
            potential_instructions[i].name(),
            potential_instructions[i]._ports_str,
            potential_instructions[i].IPC(),
        )
    return ret


def compute_classes(
    instr_list: List[ins.Instruction],
    benchs: List[List[bench.Benchmark]],
    saturating_instrs: Iterable[ins.Instruction] = [],
    epsilon: Optional[float] = None,
) -> Tuple[List[ins.Instruction], Dict[ins.Instruction, List[ins.Instruction]]]:
    class_to_instrs: Dict[ins.Instruction, List[ins.Instruction]] = dict()
    classes: Dict[ins.Instruction, List[bench.Benchmark]] = dict()

    def compare_list(l1: List[bench.Benchmark], l2: List[bench.Benchmark]) -> bool:
        # This case may happen, for example when we compare the benchmarks of
        # instructions in the sat. bench. is compared with another one.
        if len(l1) != len(l2):
            return False
        else:
            if epsilon is not None:
                err = epsilon
            else:
                err = Config.MIN_BENCH_COEF
            return all(
                [
                    (
                        isinstance(i, bench.DummyBenchmark)
                        and isinstance(j, bench.DummyBenchmark)
                    )
                    or (
                        isinstance(i, bench.Benchmark)
                        and isinstance(j, bench.Benchmark)
                        and (abs(i.IPC() - j.IPC()) <= err * max(i.IPC(), j.IPC()))
                    )
                    for i, j in zip(l1, l2)
                ]
            )

    for candidate, cand_benchs in ProgressBar(
        zip(instr_list, benchs), total=len(instr_list), desc="Gen. Classes"
    ):
        for potential, pot_benchs in classes.items():
            if abs(
                candidate.IPC() - potential.IPC()
            ) <= potential.IPC() * Config.MIN_BENCH_COEF and compare_list(
                cand_benchs, pot_benchs
            ):
                if (
                    Config.UOPS
                    and candidate._ports_str != potential._ports_str
                    and len(cand_benchs) == len(pot_benchs)
                ):
                    # If the len are not equal, then one of the instructions
                    # have a special handeling
                    logger.debug("Cannot distinguish:")
                    logger.debug("    %s", candidate)
                    logger.debug("    %s", potential)
                class_to_instrs[potential].append(candidate)
                break
            elif (
                Config.UOPS
                and candidate._ports_str == potential._ports_str
                and len(cand_benchs) == len(pot_benchs)
            ):
                logger.error("Invalid distinction:")
                logger.error("    %s", candidate)
                logger.error("    %s", potential)
        else:
            class_to_instrs[candidate] = [candidate]
            classes[candidate] = cand_benchs

    # Post-pass to ensure that saturating instructions are representents of their
    # class
    changes: Dict[ins.Instruction, ins.Instruction] = {}
    # instr -> sat_instr (first encountered)
    for instr, class_members in class_to_instrs.items():
        for sat_instr in saturating_instrs:
            if sat_instr in class_members and instr != sat_instr:
                changes[instr] = sat_instr
                break

    for instr, sat_instr in changes.items():
        class_to_instrs[sat_instr] = class_to_instrs[instr]
        class_to_instrs.pop(instr)
    return (list(class_to_instrs.keys()), class_to_instrs)


@checkpoint(PalmedStep.KERNEL_BENCHS)
def select_kernel(
    high_ipc_isa: Dict[ISA_Extension, List[ins.Instruction]],
) -> Tuple[
    List[ins.Instruction],
    List[bench.Benchmark],
    List[List[ins.Instruction]],
    List[ins.Instruction],
]:
    new_benchs: List[bench.Benchmark] = []
    new_kernel: List[ins.Instruction] = []
    max_cliques: List[List[ins.Instruction]] = []
    min_order: List[ins.Instruction] = []

    for isa, high_ipc in high_ipc_isa.items():
        kernel: List[ins.Instruction] = []
        benchs = bench.gen_benchs(high_ipc)
        if Config.PIPEDREAM:
            bench.compute_benchs(
                benchs,
                Config.CHECKPOINT.get_save_file(PalmedStep.HIGH_IPC_QUADRA)
                + f".{isa.value}",
                Config.effective_num_cpu(),
                symmetric_benchs=True,
            )
        logger.info(f"Computing largest clique ({isa.value})")
        largest_clique(kernel, high_ipc, benchs)
        max_cliques.append(list(set(kernel)))  # copy and avoid duplicates
        if isa == ins.ISA_BASE:
            logger.info(f"Computing min order ({isa.value})")
            min_order = minima_order_increasing_ipc(kernel, high_ipc, benchs)

        for instr in kernel:
            if instr not in new_kernel:
                new_kernel.append(instr)

    kernel = []
    bar = ProgressBar(
        total=int(len(new_kernel) * (len(new_kernel) - 1) / 2),
        desc=f"Generating kernel benchmarks ({isa.value})",
    )
    with select_multiprocessing_pool()(Config.effective_num_cpu()) if Config.PIPEDREAM else SingleCorePool():  # type: ignore
        for instr in new_kernel:
            add_instr_to_kernel(instr, kernel, new_benchs, progress_bar=bar)
    bar.close()

    for mc in max_cliques:
        new_benchs.append(bench.Benchmark(*mc))

    # If we have several cliques, add also the bench of all instructions
    # (i.e. uops mode and whole base ISA)
    if len(max_cliques) != 1:
        all_cliques: List[ins.Instruction] = []
        for mc in max_cliques:
            all_cliques += mc
        all_cliques = list(set(all_cliques))
        new_benchs.append(bench.Benchmark(*all_cliques))

    return kernel, new_benchs, max_cliques, min_order
