## \file ports.py
## \brief Defines ports and contains helper function such as all needed resources
## to model all the combined port, i.e. the intesections of all combined ports used

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import itertools
from palmed.config import Config
from typing import Dict, Tuple, Set, List, FrozenSet


class UnsupportedPort(Exception):
    def __init__(self, port):
        self._port = port

    def __str__(self):
        return f"Port {self._port} was flagged as ignored in configuration file"


class Port(frozenset):
    class PortsClosed(Exception):
        def __str__(self):
            return (
                "The closure of the ports has already been computed; cannot add any "
                "new port now. Call `reset()` to reset the ports' state"
            )

    class PortsNotClosed(Exception):
        def __str__(self):
            return "The closure of the ports has not been computed yet."

    _all_ports: Set["Port"] = set()
    _closed: bool = False

    def __new__(cls, *args, **kwargs):
        if cls._closed:
            raise cls.PortsClosed
        out = super().__new__(cls, *args, **kwargs)
        if Config.UOPS_SUPPORTED_PORTS and not out <= Config.UOPS_SUPPORTED_PORTS:
            raise UnsupportedPort(*args)
        cls._all_ports.add(out)
        return out

    @classmethod
    def reset(cls):
        """Reset the ports state. This must be called before loading a new
        architecture"""
        cls._all_ports = set()
        cls._closed = False

    @classmethod
    def compute_closure(cls):
        """Close the set of existing ports by union of ports with a non-empty
        intersection. This actually computes the set of abstract resources."""

        to_add = set()
        while True:
            for pt1, pt2 in itertools.combinations(cls._all_ports, 2):
                if pt1 & pt2:
                    union = cls(pt1 | pt2)
                    if union not in cls._all_ports:
                        to_add.add(union)

            if to_add:
                cls._all_ports.update(to_add)
                to_add = set()
            else:
                break

        cls._closed = True

    @classmethod
    def all_ports(cls) -> Set["Port"]:
        """Return the set of all detected abstract resources (combined ports)"""
        if not cls._closed:
            raise cls.PortsNotClosed()
        return cls._all_ports

    def __repr__(self):
        ret = "pt.Port({"
        for i in self:
            ret += f"{repr(i)},"
        ret = ret[:-1] + "})"
        return ret

    def name(self):
        port_list = list(self)
        port_list.sort()
        return "p" + "".join(port_list)


def parse(ports: str) -> List[Tuple[int, Port]]:
    if ports == "":
        return []
    list_ports_with_num = ports.split("+")
    mult = 1
    list_ports: List[Tuple[int, Port]] = []
    for p in list_ports_with_num:
        struct = p.split("*")
        mult = int(struct[0])
        # [1:] to remove the "p" (and thanks Dr A.F.F. D. for the tuple....)
        list_ports += [(mult, Port(struct[1][1:]))]
    return list_ports
