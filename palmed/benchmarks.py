## \brief Simulates benchmarks when interleaving a variable number of instructions
"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import copy
import logging
import math
import multiprocessing
import multiprocessing.util  # type:ignore
import os
import pickle
import sqlalchemy as sa
import time
import pipedream.utils.abc as abc
import palmed.ports as pt
import palmed.instructions as ins
import palmed.pipedream_benchmarks as palmedpipe
from collections import defaultdict
from concurrent import futures
from functools import cached_property, reduce
from multiprocessing import Process
from typing import (
    Set,
    List,
    Dict,
    Tuple,
    Dict,
    Optional,
    Iterable,
    Iterator,
    Any,
    Callable,
    NamedTuple,
    Sequence,
    cast,
)
from pipedream import benchmark as pipebench  # type:ignore
from palmed.config import Config
from palmed.convert import *
from palmed.instructions import (
    Instruction,
    Instruction_UOPS,
    Instruction_Pipedream,
    pipedream_to_uops,
)
from palmed.messages import ProgressBar
from palmed.utils import get_frac
from palmed.pinned_pool import select_multiprocessing_pool
from palmed import db
from palmed.db import current_machine
from palmed.db_caching import (
    DBCached,
    prefetch_benchs,
    NonBenchingMachine,
)

logger = logging.getLogger(__name__)


class BadBenchmark(Exception):
    pass


class AbstractBenchmark:

    @abc.abstractmethod
    def human_str(self) -> str:
        raise NotImplementedError()

    def is_uops(self) -> bool:
        return False

    @abc.abstractmethod
    def max_front_end_ipc(self) -> float:
        return -1


class DummyBenchmark(AbstractBenchmark):

    def __eq__(self, other):
        if isinstance(other, DummyBenchmark):
            return True
        elif isinstance(other, Benchmark):
            return False
        else:
            raise Exception("Comparison between DummyBenchmark and non-benchmark type")

    @abc.override
    def human_str(self) -> str:
        return "Dummy Benchmark"

    @abc.override
    def max_front_end_ipc(self) -> float:
        raise NotImplementedError("Dummy benchmark has no front-end")


class Benchmark(DBCached, AbstractBenchmark):
    _instrs: List[Instruction]
    # Automatically added with a *fixed* rep of 1
    _one_rep: List[Instruction]
    _reps: List[int | float]
    _coefs: List[float]
    # LCM is used to round non-integer coefficients when asking a non-integer rep
    # in pipedream mode. We execute on the machine `rep = round(float*lcm)`, but
    # display on `human_str` only the "asked" reps, `rep/lcm`.
    # Might be non-integer in some special cases, such as $a^{1.5}b^3$ "rounded" to
    # $a^1b^2$
    _lcm: float
    _nb_inst: float
    _IPC: float
    _raw_values: Optional[List[float]]
    _align: bool

    _lower_cap_IPC: Optional[float]
    _upper_cap_IPC: Optional[float]

    def __init__(
        self,
        *args,
        IPC: float = -1.0,
        lcm: int = 1,
    ):
        super().__init__()
        self._instrs: List[Instruction] = []
        # Automatically added with a *fixed* rep of 1
        self._one_rep: List[Instruction] = []
        self._reps: List[int | float] = []
        self._coefs: List[float] = []
        self._lcm = lcm
        self._nb_inst = 0.0
        self.add(*args)
        self._align = self._nb_inst > Config.NB_INST_START_PADDING
        self._IPC = float(IPC)

        self._lower_cap_IPC = None
        self._upper_cap_IPC = None

    @classmethod
    @abc.override
    def from_db_entry(cls, db_entry: db.models.Benchmark) -> "Benchmark":
        """Generate a benchmark from a database entry. Does not populate IPC from DB."""
        descr = db_entry.descr
        chunks = descr.strip().split(";")
        args = []
        for chunk in chunks:
            rep, instr_str = chunk.split("*")
            rep = int(rep)
            instr = ins.Instruction_Pipedream(instr_str)
            args.append(instr)
            args.append(rep)

        return cls(*args)

    def _gen_IPC_UOPS(self):
        # UOPS calculations should not be multithreaded (call to class
        # attributes conflicts with fork_server)
        assert not select_multiprocessing_pool().has_pool()
        num, denum = self.IPC_with_mapper_func(
            lambda inst: inst.ports(), lambda ports: len(ports)
        )
        self._IPC = num / denum
        self._IPC_frac = num, denum

    @cached_property
    def mingled_instructions(self) -> List[str]:
        """List of instructions of this benchmark (with multiplicities), mingled."""

        multiplicity: Dict[Instruction, int] = {}
        delta: Dict[Instruction, float] = {}

        for instr, mult in zip(self._instrs, self._reps):
            assert int(mult) == mult, f"instr={instr}, mult={mult}"
            if instr in multiplicity:
                multiplicity[instr] += int(mult)
            else:
                multiplicity[instr] = int(mult)
        for instr in self._one_rep:
            if instr in multiplicity:
                multiplicity[instr] += 1
            else:
                multiplicity[instr] = 1

        for instr in multiplicity:
            delta[instr] = multiplicity[instr] / len(multiplicity)

        instrs_out: List[str] = []

        for iter_id in range(len(multiplicity)):
            for instr in multiplicity:
                nb_elt = min(
                    (
                        round((iter_id + 1) * delta[instr])
                        - round(iter_id * delta[instr])
                    ),
                    multiplicity[instr],
                )
                instrs_out += [instr.name() for _ in range(nb_elt)]
                multiplicity[instr] -= nb_elt
        for instr in multiplicity:
            if multiplicity[instr] > 0:
                instrs_out += [instr.name() for _ in range(multiplicity[instr])]

        return instrs_out

    def IPC_with_mapper_func(
        self,
        instr_to_ports: Callable[[Instruction], List[Tuple[float, Any]]],
        resource_weight: Callable[[Any], float],
    ) -> Tuple[float, float]:
        resource_alloc: Dict[Any, float] = {}
        for inst, rep in zip(self._instrs, self._reps):
            instr_ports = instr_to_ports(inst)
            for port_mult in instr_ports:
                if resource_alloc.get(port_mult[1]) is None:
                    resource_alloc[port_mult[1]] = rep * port_mult[0]
                else:
                    resource_alloc[port_mult[1]] += rep * port_mult[0]
        for inst in self._one_rep:
            for port_mult in instr_to_ports(inst):
                if resource_alloc.get(port_mult[1]) is None:
                    resource_alloc[port_mult[1]] = port_mult[0]
                else:
                    resource_alloc[port_mult[1]] += port_mult[0]

        time_bound = -1.0
        time_bound_num = -1.0
        time_bound_denum = -1.0
        for port, load in resource_alloc.items():
            loc_time = load / resource_weight(port)
            if time_bound < loc_time:
                time_bound = loc_time
                time_bound_num = load
                time_bound_denum = resource_weight(port)
        return self._nb_inst * time_bound_denum, time_bound_num

    def to_pipedream_benchmark(
        self,
        with_mapping=True,
        num_dynamic_instructions=Config.NUM_DYN_INST,
        unrolled_length=500,
    ) -> pipebench.Benchmark_Spec:
        """Returns the pipedream version of this benchmark"""

        if not self._instrs:
            raise Exception("Empty benchmark: no instructions to be benchmarked.")

        bench = palmedpipe.PipedreamKernel(self.mingled_instructions)

        return bench.get_pipedream_benchmark(
            with_mapping,
            num_dynamic_instructions=num_dynamic_instructions,
            unrolled_length=unrolled_length,
            kind=pipebench.Benchmark_Kind.THROUGHPUT,
        )

    def IPC_UOPS_from_pipedream(self):
        # Special handler for one instruction alone
        # as ports() may be empty (e.g. for NOP)
        if len(self._instrs) == 1 and self._reps[0] == 1 and len(self._one_rep) == 0:
            return pipedream_to_uops(self._instrs[0]).IPC()

        num, denum = self.IPC_with_mapper_func(
            lambda inst: pipedream_to_uops(inst).ports(),
            lambda ports: float(len(ports)),
        )
        return num / denum

    def IPC_with_mapping(
        self,
        mapping: Dict[Instruction, List[Tuple[float, str]]],
        instr_to_class: Dict[Instruction, Instruction],
    ):

        def mapping_fct(instr: Instruction) -> List[Tuple[float, str]]:
            try:
                return mapping[instr_to_class[instr]]
            except KeyError as exn:
                raise ins.InstructionNotFound(str(exn)) from exn

        num, denum = self.IPC_with_mapper_func(mapping_fct, lambda x: 1)
        return num / denum

    def _instr_in_bench(self) -> Dict[Instruction, float]:
        instr_in_bench: Dict[Instruction, float] = {}
        for instr, coef in zip(self._instrs, self._reps):
            if instr_in_bench.get(instr):
                instr_in_bench[instr] += coef
            else:
                instr_in_bench[instr] = coef

        for instr in self._one_rep:
            if instr_in_bench.get(instr):
                instr_in_bench[instr] += 1
            else:
                instr_in_bench[instr] = 1
        return instr_in_bench

    def max_IPC(self) -> float:
        instr_in_bench = self._instr_in_bench()
        # The IPC cannot exceed all instructions passing
        # in parallel -> the minimum number of cycle is
        # given by the slowest sort of instruction in the bench
        max_cycle = 0.0
        for instr, rep in instr_in_bench.items():
            max_cycle = max(max_cycle, rep / instr.IPC())
        max_IPC: float = self._nb_inst / max_cycle if max_cycle > 0 else 0.0
        return max_IPC

    def min_IPC(self) -> float:
        instr_in_bench = self._instr_in_bench()
        # The IPC cannot exceed all instructions passing
        # in parallel -> the maximum number of cycle is
        # given by the sum of all the individual execution
        # time of the instructions in the bench
        max_cycle = 0.0
        for instr, rep in instr_in_bench.items():
            max_cycle += rep / instr.IPC()
        min_IPC: float = self._nb_inst / max_cycle if max_cycle > 0 else 0.0
        return min_IPC

    def cap_IPC_low(self, cap: float):
        """Put a lower cap on the possible IPC of this benchmark.
        **BEWARE!** This cap is reset whenever `add` is called.
        If the measure has already been committed to database, this **does not**
        update it."""
        assert cap >= 0
        self._lower_cap_IPC = max(cap, self._lower_cap_IPC or cap)
        if self._IPC >= 0:
            self._IPC = max(self._IPC, self._lower_cap_IPC)

    def cap_IPC_high(self, cap: float):
        """Put an upper cap on the possible IPC of this benchmark.
        **BEWARE!** This cap is reset whenever `add` is called.
        If the measure has already been committed to database, this **does not**
        update it."""
        assert cap >= 0
        self._upper_cap_IPC = min(cap, self._upper_cap_IPC or cap)

        if self._IPC >= 0:
            self._IPC = min(self._IPC, self._upper_cap_IPC)

    def _apply_IPC_cap(self, measure: float, is_cpi=False) -> float:
        """Apply the lower and upper IPC cap to a given measure and returns it. If
        `is_cpi` is True, `measure` will be treated as CPI and be capped accordingly.
        """

        if is_cpi:

            def transform(x: Optional[float]) -> float:
                if x:
                    return 1 / x
                return measure

            # Caps are swapped by the 1/x
            upper_cap: float = transform(self._lower_cap_IPC)
            lower_cap: float = transform(self._upper_cap_IPC)
        else:
            upper_cap = self._upper_cap_IPC or measure
            lower_cap = self._lower_cap_IPC or measure

        measure = min(measure, upper_cap)  # Upper-cap
        measure = max(measure, lower_cap)  # Lower-cap
        return measure

    def _nearest_benchmark_ipc(self, cpi: float) -> float:
        """Finds the closest feasible combination of execution times of the
        instructions of this benchmark that could have produced the measure :cpi:, in
        order to facilitate the job of the solver. Also bottom- and top-caps the value.

        Formally, let B a benchmark (a_i^{r_i}). We want to find a measure m_approx
        minimizing `abs(m_approx - cpi)`, such that `m_approx` is of the form

        m_approx = (1/P) (Σ λ_i r_i)

        where λ_i is a non-negative integer, and P is an integer such that

        1 ≤ P < :Config.MAX_DENUM_BENCH:

        Note that this **does not** enforce that λ_i must not be larger than
        `1/IPC(a_i)`
        """

        instr_in_bench_raw = self._instr_in_bench()
        assert all([isinstance(i, int) for i in instr_in_bench_raw.values()])
        instr_in_bench = cast(Dict[Instruction, int], instr_in_bench_raw)
        measure = cpi * self._nb_inst

        # Only one instruction: do not bother
        if len(instr_in_bench.items()) == 1:
            num, denum = get_frac(measure, max_denum=Config.MAX_DENUM_BENCH)
            if num == 0:
                return 0
            return denum * self._nb_inst / num

        # Find the closest linear combination of bench instructions' coefs to the
        # measure as possible.
        sorted_coefs = list(set(instr_in_bench.values()))
        sorted_coefs.sort(reverse=True)
        assert sorted_coefs

        coefs_gcd = reduce(math.gcd, sorted_coefs)
        normalized_coefs = [coef // coefs_gcd for coef in sorted_coefs]
        _feasability: List[bool] = [True] + [False] * (
            (math.ceil(measure * Config.MAX_DENUM_BENCH) + sorted_coefs[-1])
            // coefs_gcd
            + 10
            # +10: ensure we face no IndexError due to an off-by-one in my
            # pen-and-paper bound computation
        )

        for pos, cur_value in enumerate(_feasability):
            if cur_value:
                for coef in normalized_coefs:
                    if pos + coef < len(_feasability):
                        _feasability[pos + coef] = True

        def is_feasable(value):
            if value % coefs_gcd != 0:
                return False
            norm_value = value // coefs_gcd
            assert norm_value < len(_feasability)
            return _feasability[norm_value]

        optimal = 0.0
        optimal_dist = sorted_coefs[-1] * 10.0  # we can always do better than this
        for precision in range(1, Config.MAX_DENUM_BENCH + 1):
            # We actually aim for a linear combination as close to
            # `measure * precision` as possible, since equations are equivalent.
            objective = measure * precision

            # We search for integer values `approx_objective` that can be expressed as
            # non-negative integer linear combinations of `sorted_coefs`, closest to
            # `objective` as possible. For this, we iterate from closest to farthest
            # to `objective` integers, going as far as the smallest coeff (since there
            # is at least a multiple of the smallest coeff in this interval, hence at
            # least one expressible such number in this interval).
            for approx_range in [
                range(
                    math.floor(objective),
                    max(0, math.floor(objective) - sorted_coefs[-1]),  # smallest coeff
                    -1,
                ),  # under-approximate
                range(
                    math.ceil(objective),
                    math.ceil(objective) + sorted_coefs[-1],  # smallest coeff
                ),  # over-approximate
            ]:
                for approx_objective in approx_range:
                    dist = abs(objective - approx_objective) / precision
                    if dist >= optimal_dist:
                        # If we already found a better approximation, no need to
                        # continue
                        break
                    feasable = is_feasable(approx_objective)
                    if feasable:
                        optimal = approx_objective / precision
                        optimal_dist = dist
                        break

        if optimal == 0:
            return 0
        return self._nb_inst / optimal

    @abc.override
    def commit_benchmark(
        self, cpi, bench_stats, round_IPC_nearest: bool = True, db_commit=False
    ):
        """Save a previous measure (supposedly made through `palmedpipe` and
        commit it to database if `db_commit`."""
        self.orig_IPC = 1 / cpi
        ipc = self.orig_IPC
        if round_IPC_nearest:
            ipc = self._nearest_benchmark_ipc(cpi)
        self.set_IPC(ipc)
        self._IPC_bench_stats = bench_stats
        if db_commit:
            self._cached_IPC_to_db()

    def _gen_IPC_from_local_CPI(
        self,
        core: Optional[int],
        keep_raw_values: bool,
        asm_copyfile: Optional[str],
        add_to_db: bool,
    ):
        if not current_machine.is_benching():
            raise NonBenchingMachine(self)
        bench = palmedpipe.PipedreamKernel(self.mingled_instructions)
        cpi, raw_values, bench_stats = bench.measure_CPI(
            num_dynamic_instructions=Config.NUM_DYN_INST,
            unrolled_length=500,
            num_iterations=50,
            kind=pipebench.Benchmark_Kind.THROUGHPUT,
            keep_raw_values=keep_raw_values,
            asm_copyfile=asm_copyfile,
            core=core,
        )
        cpi = self._apply_IPC_cap(cpi, is_cpi=True)
        if add_to_db:
            self.commit_benchmark(
                cpi, bench_stats, round_IPC_nearest=True, db_commit=False
            )
        else:
            ipc = self._nearest_benchmark_ipc(cpi)
            self.set_IPC(ipc)
            self._raw_values = raw_values

    @abc.override
    def _gen_IPC_pipedream(
        self,
        core: Optional[int],
    ):
        self._gen_IPC_from_local_CPI(core, False, None, True)

    def _gen_IPC(self, core):
        if len(self.instrs()) == 1:
            self._IPC = self.instrs()[0].IPC()
            return
        if len(self._instrs) > 0:
            if self.is_uops():
                self._gen_IPC_UOPS()
            else:
                self._gen_IPC_pipedream_or_cached(core)
        elif len(self._one_rep) > 0:
            if self.is_uops():
                self._gen_IPC_UOPS()
            else:
                self._gen_IPC_pipedream_or_cached(core)
        else:
            raise Exception("Trying to get the IPC of an empty bench!")

    def _gen_coefs(self):
        coefs = self._reps.copy()
        for i in self._one_rep:
            coefs.append(1.0)
        if hasattr(self, "_IPC_frac"):
            num, denum = self._IPC_frac
            coefs.append(num)
            # Convert to coef of the constraints
            sum_coefs = 0.0
            for k in range(len(coefs) - 1):
                sum_coefs += coefs[k]
                coefs[k] *= coefs[-1]
            coefs[-1] = -sum_coefs * denum
        else:
            coefs.append(self._IPC)
            # Convert to coef of the constraints
            sum_coefs = 0.0
            for k in range(len(coefs) - 1):
                sum_coefs += coefs[k]
                coefs[k] *= coefs[-1]
            coefs[-1] = -sum_coefs
        self._coefs = coefs

    def IPC_no_db(
        self,
        keep_raw_values=False,
        asm_copyfile: Optional[str] = None,
        core: Optional[int] = None,
    ) -> Tuple[float, Optional[List[float]]]:
        self._gen_IPC_from_local_CPI(core, keep_raw_values, asm_copyfile, False)
        return self._IPC, self._raw_values

    @abc.override
    def IPC(self, core: Optional[int] = None) -> float:
        if self._IPC < 0:
            self._gen_IPC(core)
        return self._IPC

    @abc.override
    def set_IPC(self, IPC: float, commitable=True):
        self._IPC = IPC
        self._IPC_commitable = commitable

    @abc.override
    def has_IPC(self) -> bool:
        return self._IPC > 0

    def coefs(self) -> List[float]:
        # Gen coefs if this was never done before
        self.IPC()
        self._gen_coefs()
        return self._coefs

    def instrs(self) -> List[Instruction]:
        return self._instrs + self._one_rep

    def instrs_with_reps(self) -> Dict[Instruction, int | float]:
        """Dictionary of instructions with their repetitions"""
        out: Dict[Instruction, float] = defaultdict(float)
        out.update({instr: rep for instr, rep in zip(self._instrs, self._reps)})
        for instr in self._one_rep:
            out[instr] += 1
        return out

    def card(self) -> float:
        """Returns the cardinal of this benchmark, that is, the number of instructions
        (including reps)."""
        return self._nb_inst

    def is_uops(self) -> bool:
        """Check whether this benchmark is a UOPS benchmark

        Returns True if the benchmark contains no instruction at all
        """
        # Assume the benchmark is consistently UOPS or Pipedream
        return all(map(lambda x: isinstance(x, ins.Instruction_UOPS), self.instrs()))

    def copy(self) -> "Benchmark":
        new = Benchmark()
        new._instrs = copy.copy(self._instrs)
        new._reps = copy.copy(self._reps)
        new._one_rep = copy.copy(self._one_rep)
        new._coefs = copy.copy(self._coefs)
        new._IPC = self._IPC
        new._lcm = self._lcm
        new._nb_inst = self._nb_inst
        return new

    # `add()` adds instructions to `self` with sseveral behaviour depending on the
    # structure of the arguments given:
    # - `self.add(instr:Instruction, coef:float)` with `coef` > 0
    # will add `instr` with the coef `coef`
    # - `self.add(instr:Instruction)` is equivalent to `self.add(instr, 1)`
    # - `self.add(instr:Instruction, coef:float)` with `coef` < 0 will force `instr`
    # to be executed only one time per microkernel on the machine, no matter which
    # other operations are done later on the benchmarks (especially multiplications).
    # - `self.add(instr:Instruction, 0)` is invalid
    # For example, "1*ADD, 2*SUB".add(SHL, 3) results in "1*ADD, 2*SUB, 3*SHL"
    # and "1*ADD, 2*SUB".add(SHL, -1) results in "1*ADD, 2*SUB, 1*SHL", with a fixed
    # coef on "SHL"
    # Note that "1*ADD, 2*SUB".add(ADD, -1)` will result in a bench
    # `b` = "1*ADD + 2*SUB + 1*ADD" with 4*`b` = `4*ADD + 8*SUB + 1*ADD`.
    def add(self, *args) -> "Benchmark":
        # invalidate previous IPC and mappability
        self._IPC = -1.0

        # Invalidate upper and lower cap
        self._lower_cap_IPC = None
        self._upper_cap_IPC = None

        # Parse arguments
        prev_instr: Optional[Instruction] = None
        instr_pairs: List[Tuple[Instruction, float]] = []
        for arg in args:
            if isinstance(arg, ins.Instruction):
                if prev_instr is not None:
                    instr_pairs.append((prev_instr, 1.0))
                prev_instr = arg
            elif isinstance(arg, (float, int)):
                if prev_instr is None:
                    raise BadBenchmark(
                        "Bad argument format: repetition number qualifying no "
                        "instruction."
                    )
                # Do not forget to weight by `self._lcm`: the real benchmark (measured
                # on the machine) uses `reps*lcm` rel reps.
                instr_pairs.append((prev_instr, float(arg * self._lcm)))
                prev_instr = None
            else:
                raise BadBenchmark(
                    "Benchmark: expecting either a number or an instruction, but got "
                    f"{arg}, which is of type {type(arg)}."
                )
        if prev_instr is not None:
            instr_pairs.append((prev_instr, 1.0))

        # Coalescing duplicates -- not sorting: we want to preserve order
        instr_idx: Dict[Instruction, int] = {}
        coalesced: List[Tuple[Instruction, float]] = []
        for instr, reps in instr_pairs:
            if reps < 0:  # Preserving one-instr as is
                coalesced.append((instr, reps))
            if instr in instr_idx:
                coalesced[instr_idx[instr]] = (
                    instr,
                    coalesced[instr_idx[instr]][1] + reps,
                )
            else:
                instr_idx[instr] = len(coalesced)
                coalesced.append((instr, reps))
        instr_pairs = coalesced

        if len(instr_pairs) != 0:
            lcm = self._lcm
            one_rep: Set[int] = set()
            for idx, (_, coef) in enumerate(instr_pairs):
                # Negative numbers force a coef of 1
                if coef <= 0:
                    one_rep.add(idx)
                    continue
                if Config.PIPEDREAM:
                    # Relaxed condition for denum
                    epsilon = Config.CLASSES_COEF * Config.MIN_BENCH_COEF
                    if abs(int(coef) - coef) <= epsilon:  # coef is integer enough
                        int_coef = round(coef)

                        # The asked coef is too small to lead to the instruction being
                        # in the bench, putting it in `one_rep` instead
                        if int_coef == 0:
                            one_rep.add(idx)
                            continue

                    # If the coef was not an integer, we then need the LCM to increase
                    # the reps of all coefs to try to match the proportion betweend the
                    # asked coef
                    else:
                        # Python 3.8 does not have the `math.lcm()` function,
                        # computing it with the method:
                        # a*b = gcd*lcm
                        # so lcm = (a*b)/gcd
                        # with a = denum and b = lcm

                        # LCM can be float, so arithmetics are computed on the numerator,
                        # then re-divided.
                        coef_num, coef_denum = get_frac(
                            coef, max_denum=Config.MAX_DENUM_BENCH
                        )
                        lcm_num, lcm_denum = get_frac(
                            lcm, max_denum=Config.MAX_DENUM_BENCH
                        )
                        lcm_num_f = lcm_num * coef_denum / math.gcd(lcm_num, coef_denum)
                        lcm = min(lcm_num_f / lcm_denum, Config.MAX_DENUM_BENCH)

            if self._lcm != lcm:
                assert lcm != 0
                # LCM changed, we have to propagate it to the coefs already present
                for idx in range(len(self._reps)):
                    self._reps[idx] = round((self._reps[idx] / self._lcm) * lcm)
                    assert self._reps[idx] > 0

            self._lcm = lcm
            for idx, (new_instr, coef) in enumerate(instr_pairs):
                rep_float = coef * lcm
                if idx in one_rep:
                    self._one_rep.append(new_instr)
                elif new_instr in self._instrs:
                    self._reps[self._instrs.index(new_instr)] += coef
                else:
                    self._reps.append(rep_float)
                    self._instrs.append(new_instr)

        if Config.PIPEDREAM:
            for pos, rep_float in enumerate(self._reps):
                self._reps[pos] = min(round(rep_float), Config.MAX_DENUM_BENCH)

        # Simplify benchmark when possible
        if len(self._one_rep) == 0 and len(self._reps) > 0 and Config.PIPEDREAM:
            # All reps are int now
            gcd = int(self._reps[0])
            for i in self._reps:
                gcd = math.gcd(gcd, int(i))
            if gcd != 1:
                # LCM might be float:
                # `a^3.add(b^1.5)` should be `a^2 b^1`
                if self._lcm != 1:
                    self._lcm /= gcd
                assert 0 not in self._reps
                assert self._lcm != 0
                # self._lcm / gcd should be integer as self._lcm has previsouly
                # been updated with the latest coefs
            for i in range(len(self._reps)):
                self._reps[i] = self._reps[i] // gcd

        assert len(self._instrs) == len(self._reps)

        self._nb_inst = sum(self._reps) + len(self._one_rep)
        if self._nb_inst > 1e4 and Config.PIPEDREAM:
            logger.warning("%s has too much instructions", str(self))

        if Config.PIPEDREAM:
            # Update default cap
            self.cap_IPC_low(self.min_IPC())
            self.cap_IPC_high(self.max_IPC())

        return self

    def __eq__(self, other):
        # Do not compare `_lcm` as different `_lcm` will end up to be the same benchmark
        # Not that this *may* be problematic as we assume a^2b^2 == ab
        if (
            isinstance(other, Benchmark)
            and set(self._instrs) == set(other._instrs)
            and set(self._one_rep) == set(other._one_rep)
            and self._nb_inst == other._nb_inst
            and self._align == other._align
        ):
            other_with_reps = other.instrs_with_reps()
            for inst, rep in self.instrs_with_reps().items():
                if other_with_reps[inst] != rep:
                    break
            else:
                return True
        return False

    def __lshift__(self, instr: Instruction):
        ret = self.copy()
        ret._IPC = -1.0
        ret._instrs += [instr]
        ret._reps += [1]
        return ret

    def __add__(self, other):
        ret = self.copy()
        ret._IPC = -1.0
        for instr, rep in zip(other._instrs, other._reps):
            ret._reps += [rep]
            ret._instrs += [instr]
        return ret

    def __mul__(self, coef: float):
        ret = self.copy()
        if Config.PIPEDREAM:
            # We may exactly want to update LCM, i.e. do more repetitions
            # of every instructions (currently `reps` for `reps/lcm` asked)
            # if LCM was used, then we now want to do `coef*reps` times the instruction,
            # i.e. `coef*reps/lcm`, i.e. `new_lcm = lcm/coef`
            if self._lcm == 1.0 or coef > self._lcm:
                # Keep LCM as it, update only weights
                denum = get_frac(coef, max_denum=Config.MAX_DENUM_BENCH)[1]
                coef_int = max(round(coef * denum), 1)
                # Brutally capping out-of-range coefs.
                # TODO: we may need something conserving proportions here?
                ret._reps = list(
                    map(
                        lambda x: max(1, min(coef_int * x, Config.MAX_DENUM_BENCH)),
                        ret._reps,
                    )
                )
            else:  # coef =< lcm:
                self._lcm = round(self._lcm / coef)
        else:
            ret._reps = list(map(lambda x: coef * x, ret._reps))

        return ret

    @abc.override
    def human_str(self) -> str:
        if len(self._instrs) == 0 and len(self._one_rep) == 0:
            return "Empty bench"
        ret = ""
        for instr, rep in zip(self._instrs, self._reps):
            ret += f"{instr.name()} [{instr._ports_str}] (IPC={instr.IPC()}) * {rep}, "
        for instr in self._one_rep:
            ret += f"{instr.name()} [{instr._ports_str}] (IPC={instr.IPC()}) * 1, "
        ret = ret[:-2]
        ret += f" IPC = {self._IPC}"
        return ret

    def uops_str(self):
        if len(self._instrs) == 0 and len(self._one_rep) == 0:
            return "Empty bench"
        ret = ""
        for instr, rep in zip(self._instrs, self._reps):
            ret += f"[{pipedream_to_uops(instr)}] * {rep}, "
        for instr in self._one_rep:
            ret += f"[{pipedream_to_uops(instr)}]* 1, "
        return ret[:-2]

    def db_str(self) -> str:
        """A string serialization of the current benchmark, to be stored in the
        database"""
        instrs: List[Tuple[Instruction, int | float]] = list(
            self.instrs_with_reps().items()
        )
        instrs.sort(key=lambda x: x[0].name())  # lex. sort on instruction name
        return ";".join(
            map(
                lambda row: "{rep}*{instr}".format(rep=row[1], instr=row[0].name()),
                instrs,
            )
        )

    def __hash__(self) -> int:
        return hash(self.db_str())

    def __repr__(self):
        if len(self._instrs) == 0 and len(self._one_rep) == 0:
            return ""
        ret = ""
        for instr, rep in zip(self._instrs, self._reps):
            ret += f"{str(instr)}, {rep/self._lcm}, "
        for instr in self._one_rep:
            ret += f"{str(instr)}, -1, "
        ret = f"bench.Benchmark({ret[:-2]}, IPC = {self._IPC}, lcm = {self._lcm})"
        return ret

    @abc.override
    def max_front_end_ipc(self) -> float:
        return Config.FRONT_END_MODEL(self.mingled_instructions).IPC()


def gen_benchs(instr_list: List[Instruction]) -> List[List[Benchmark]]:
    n = len(instr_list)
    benchs: List[List[Optional[Benchmark]]] = [
        [None for i in range(n)] for j in range(n)
    ]

    pbar = ProgressBar(total=int(n * (n + 1) / 2), desc="Gen. Benchs")
    for i, instr1 in enumerate(instr_list):
        for j, instr2 in enumerate(instr_list[: i + 1]):
            to_add = Benchmark(instr1, instr1.IPC(), instr2, instr2.IPC())
            benchs[i][j] = to_add
            benchs[j][i] = to_add
        pbar.update(i + 1)
    pbar.close()
    # There should be no None after these loops
    return benchs  # type:ignore


def _compute_one_bench(
    args: Tuple[int, List[Benchmark], List[bool]]
) -> Tuple[int, List[Benchmark]]:
    bench_id, bench_row, sample_select = args
    for bench, selected in zip(bench_row, sample_select):
        if selected and isinstance(bench, Benchmark):
            try:
                bench.IPC(core=-1)
            except Exception as e:
                logger.error("Error occured in bench", bench)
                raise e
    return (bench_id, bench_row)


def compute_benchs(
    bench_list: List[List[Benchmark]],
    save_file: str,
    num_cpu: int,
    sample_select: Optional[List[List[bool]]] = None,
    symmetric_benchs: bool = False,
):
    """Computes the provided benches in parallel on `num_cpu - 1` (from 1 to
    `num_cpu`). If a `sample_select` is provided, only computes benchmarks whose flag
    is True, leaving others untouched.
    If `symmetric_benchs` is True, the lower part of the matrix (below the diagonal)
    will be left out and replicated in the end. In this case, `sample_select`'s upper
    part will be ORed against its lower part, to ensure that all the samples that
    should be computed are indeed selected, up to symmetry."""

    def block_save_file(block_id):
        return "{base}.blk{blk:04}".format(base=save_file, blk=block_id)

    def get_cached_block(uncached: List[Benchmark], block_id: int) -> List[Benchmark]:
        try:
            with open(block_save_file(block_id), "rb") as block_cache_handle:
                cached = pickle.load(block_cache_handle)
        except FileNotFoundError:
            return uncached
        except EOFError:
            return uncached
        except pickle.UnpicklingError:
            return uncached

        if len(uncached) != len(cached):
            return uncached

        out: List[Benchmark] = []
        for sub_bench, sub_bench_cached in zip(uncached, cached):
            if sub_bench == sub_bench_cached:
                out.append(sub_bench_cached)
            else:
                out.append(sub_bench)
        return out

    def sym_aware_select(is_sym: bool, row: int, select: List[bool]) -> List[bool]:
        if not is_sym:
            return select
        return [cur_select and col >= row for col, cur_select in enumerate(select)]

    def bench_list_iterator(
        bench_list: List[List[Benchmark]],
        sample_select: Optional[List[List[bool]]],
        is_sym: bool,
    ) -> Iterator[Tuple[int, List[Benchmark], List[bool]]]:
        if sample_select:
            for bench_id, (bench, select) in enumerate(zip(bench_list, sample_select)):
                yield (
                    bench_id,
                    get_cached_block(bench, bench_id),
                    sym_aware_select(is_sym, bench_id, select),
                )
        else:
            for bench_id, bench in enumerate(bench_list):
                block = get_cached_block(bench, bench_id)
                yield (
                    bench_id,
                    block,
                    sym_aware_select(is_sym, bench_id, [True] * len(block)),
                )

    if symmetric_benchs and sample_select is not None:
        for row in range(len(sample_select)):
            for col in range(len(sample_select)):
                if col < row:
                    continue
                sample_select[row][col] = (
                    sample_select[row][col] or sample_select[col][row]
                )
    if bench_list and bench_list[0] and not bench_list[0][0].is_uops():
        # Assume the `bench_list` is consistent (only either uops or pipedream benchs)
        bench_list_casted: List[List[Benchmark]] = []
        for benchs in bench_list:
            bench_list_casted.append([])
            for bench in benchs:
                if isinstance(bench, DBCached):
                    bench_list_casted[-1].append(bench)
        prefetch_benchs(bench_list_casted)
    out: List[List[Benchmark]] = [[] for _ in range(len(bench_list))]
    if Config.PIPEDREAM:
        with select_multiprocessing_pool()(
            num_cpu,
            maxtasksperchild=10,
        ) as pool:
            for pos, benchs in ProgressBar(
                pool.imap_unordered(
                    _compute_one_bench,
                    bench_list_iterator(bench_list, sample_select, symmetric_benchs),
                ),
                desc=f"Computing benchmarks",
                total=len(bench_list),
            ):
                out[pos] = benchs
                with open(block_save_file(pos), "wb") as handle:
                    pickle.dump(benchs, handle)

        bench_list.clear()
        for benchs in out:
            bench_list.append(benchs)
    else:
        for ben in ProgressBar(
            bench_list_iterator(bench_list, sample_select, symmetric_benchs),
            desc=f"Computing benchmark (UOPS mode)",
            total=len(bench_list),
        ):
            _compute_one_bench(ben)

    # Propagate symmetry
    if symmetric_benchs:
        for row in range(len(bench_list)):
            for col in range(len(bench_list)):
                if col < row:
                    continue
                if (
                    isinstance(bench_list[row][col], Benchmark)
                    and bench_list[row][col].has_IPC()
                ):
                    bench_list[col][row].set_IPC(bench_list[row][col].IPC())


def instr_is_in_benchs_dict(instr: Instruction, bench_dict: Dict[Any, Benchmark]):
    used_instr: Set[Instruction] = set()
    for _, bench in bench_dict.items():
        for inst in bench.instrs():
            used_instr.add(inst)
    return instr in used_instr
