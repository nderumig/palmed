#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

## \file palmed.py
## \brief Wrapper used to call the palmed library for automatized mapping detection

"""
Palmed -- Throughput Characterization for Superscalar Architectures
Copyright (C) 2021  INRIA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argcomplete, argparse
import sys
import logging
import multiprocessing
from palmed import mapping
from palmed.config import Config
from palmed.checkpoints import PalmedStep, get_check_txt_descr

MULTIPROCESSING_METHOD = "forkserver"


def init_multiprocessing():
    multiprocessing.set_forkserver_preload(
        [
            # Palmed
            "palmed.config",
            "palmed.instructions",
            "palmed.benchmarks",
            # Pipedream
            "pipedream.asm",
            "pipedream.asm.x86",
            "pipedream.benchmarks",
        ]
    )
    multiprocessing.set_start_method(MULTIPROCESSING_METHOD)


def init_parser():
    parser = argparse.ArgumentParser(
        __name__,
        description="Infer a port mapping from a given architechture",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "--arch", "-a", dest="ARCH", help="Architecture to run", required=True
    )

    parser.add_argument(
        "--isa",
        "-i",
        dest="ISA",
        help="Targetted ISA",
        default=Config.SUPPORTED_ISA.X86.value,
    )

    parser.add_argument(
        "--prefix",
        "-p",
        dest="PREFIX",
        default=Config.OUT_FOLDER,
        help="Default folder for file generation",
    )
    parser.add_argument(
        "--quiet",
        "-q",
        dest="QUIET",
        action="store_true",
        help="Do not print any output",
    )
    parser.add_argument(
        "--no-info",
        action="store_const",
        dest="LOGLEVEL",
        default=logging.INFO,
        const=logging.WARNING,
        help="Do not print info output",
    )
    parser.add_argument(
        "-g",
        "--debug",
        action="store_const",
        dest="LOGLEVEL",
        default=logging.INFO,
        const=logging.DEBUG,
        help="Print debug output",
    )
    parser.add_argument(
        "--dot",
        "-d",
        dest="DOT_FILE",
        default=Config.DOT_FILE_BASEFILE,
        help="Name of the dot file corresponding to the mapping",
    )
    parser.add_argument(
        "--pdf",
        dest="PDF_FILE",
        default=Config.PDF_FILE_BASEFILE,
        help="Name of the pdf file corresponding to the mapping",
    )
    parser.add_argument(
        "--uops",
        dest="UOPS",
        action="store_true",
        default=False,
        help="Use a variation of PALMED's LPs to reverse-engineer uops' mappings",
    )
    parser.add_argument(
        "--num_cpu",
        dest="NUM_CPU",
        type=int,
        help=(
            "Number of processes used when parallelising measures of microbenchmarks. "
            "Defaults to using all the available cores but core 0."
        ),
    )
    parser.add_argument(
        "--dense",
        dest="DENSE",
        default=Config.DENSE,
        action="store_true",
        help=(
            "Compute the full quadratic benchmark matrix, instead of computing a "
            "sparse matrix and reconstructing it."
        ),
    )
    parser.add_argument(
        "--sparse",
        dest="DENSE",
        default=not Config.DENSE,
        action="store_false",
        help=(
            "Compute a sparse benchmark matrix and reconstructs it, instead of "
            "computing the full dense quadratic benchmarks matrix"
        ),
    )
    parser.add_argument(
        "--override_kernel",
        dest="OVERRIDE_KERNEL",
        default=Config.OVERRIDE_KERNEL,
        help=(
            "Use a custom kernel provided at this path instead of using heuristics. "
            "If `-`, reads from the standard input."
        ),
    )
    parser.add_argument(
        "--override_kernel_format",
        dest="OVERRIDE_KERNEL_FORMAT",
        default=Config.OVERRIDE_KERNEL_FORMAT,
        help="Parse the custom kernel according to the given format",
        choices=["pipedream", "hex", "raw"],
    )
    parser.add_argument(
        "--checkpoint",
        dest="CHECKPOINT",
        default=None,
        help="Step from which palmed will resume its computation. /!\\ All "
        "following checkpointing files WILL BE LOST. Available choices for "
        "<CHECKPOINT> are:" + get_check_txt_descr(),
        choices=list(map(lambda step: step.name, PalmedStep)),
        metavar=" [--checkpoint <CHECKPOINT>]",
    )
    parser.add_argument(
        "--db-url",
        default=Config.DATABASE_URL,
        dest="DATABASE_URL",
        help="SQLAlchemy URL used to connect to the database",
    )
    parser.add_argument(
        "-m",
        "--machine",
        default=Config.SELECTED_MACHINE,
        dest="SELECTED_MACHINE",
        help=(
            "Machine to use as a source of benchmarks. If 'localhost', the local "
            "machine will be used and benchmarks will be computed on the go; else, "
            "results will only be fetched from DB, and the program will fail if "
            "benches are missing."
        ),
    )
    parser.add_argument(
        "--perflib",
        default=Config.PERFLIB,
        dest="PERFLIB",
        help=(
            "Performance library to use, as used by pipedream's pyperfs. "
            "Can be 'auto'."
        ),
    )
    parser.add_argument(
        "--remote-bench",
        action="store_true",
        dest="REMOTE_BENCH",
        help=(
            "Executes the benchmarks remotely on the selected machine (see --machine)."
        ),
    )
    parser.add_argument(
        "--pipedream-bencher",
        dest="REMOTE_PIPEDREAM_HOST",
        default=None,
        help=(
            "This option is only useful if --remote-bench is selected. "
            "Override the hostname used to connect to the remote pipedream bencher, "
            "which should normally be selected with the --machine parameter."
            "This is useful to eg. use a machine name defined in your ssh config."
        ),
    )
    parser.add_argument(
        "--pipedream-bencher-user",
        dest="REMOTE_PIPEDREAM_USER",
        default=Config.REMOTE_PIPEDREAM_USER,
        help="User used to connect to the remote pipedream bencher",
    )
    parser.add_argument(
        "--pipedream-bencher-port",
        dest="REMOTE_PIPEDREAM_PORT",
        default=Config.REMOTE_PIPEDREAM_PORT,
        type=int,
        help="Port used to connect to the remote pipedream bencher",
    )
    parser.add_argument(
        "--pipedream-bencher-wrapper",
        dest="REMOTE_PIPEDREAM_WRAPPER_PATH",
        default=Config.REMOTE_PIPEDREAM_WRAPPER_PATH,
        help=(
            "Local path to the wrapper binary used to run benchmarks on the remote "
            "machine"
        ),
    )

    parser.add_argument(
        "--repl",
        action="store_true",
        help=(
            "Initialize all necessary configuration options according to command "
            "line switches, then drop to an IPython REPL"
        ),
    )
    return parser


def main():
    init_multiprocessing()
    parser = init_parser()
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    mapping.main(args)


if __name__ == "__main__":
    main()
