#!/bin/bash
rm instructions.xml
wget https://uops.info/instructions.xml

pushd pipedream/tools/extract-xed-instruction-database
cmake .
make -j
./extract-xed-instruction-database print-instr-db > ../../src/pipedream/asm/x86/instructions_xed.py
popd

pushd pipedream/tools/extract-binutils-instruction-database
virtualenv -p python3 venv
source venv/bin/activate
pip install .
extract-arm-instructions
mv instructions_binutils.py ../../src/pipedream/asm/armv8a/instructions_binutils.py
deactivate
popd
