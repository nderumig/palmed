import random
import pytest
import palmed.instructions as ins
import palmed.benchmarks as bench
from tests.init_palmed import *


def bench_rand5_instructions():
    ins_, _ = ins.get_benchable_pipedream_instructions(False)
    lst_ins = list(ins_[ins.ISA_BASE])
    random.shuffle(lst_ins)
    ret = random.sample(lst_ins, 5)
    for i in ret:
        try:
            assert i.IPC() > 0
            assert i.latency() > 0
        except Exception as e:
            pytest.fail(f"Failed to benchmark {i}: {e}.")
    return ret


def test_pipedream_load_aarch64(init_palmed, in_tmp_dir, init_palmed_armv8a):
    Config = init_palmed_armv8a
    ins_classes, _ = ins.get_benchable_pipedream_instructions(False)
    assert len(ins_classes[ins.ISA_BASE]) > 0
    assert len(ins_classes[Config.ARMv8a_Extension.SIMD]) > 0


def test_pipedream_load_x86(init_palmed, in_tmp_dir, init_palmed_x86):
    Config = init_palmed_x86
    ins_classes, _ = ins.get_benchable_pipedream_instructions(False)
    assert len(ins_classes[ins.ISA_BASE]) > 0
    assert len(ins_classes[Config.X86_Extension.SSE]) > 0
    assert len(ins_classes[Config.X86_Extension.AVX]) > 0


@x86_only
def test_bench_rand5_ins_x86(init_palmed, in_tmp_dir, init_palmed_x86):
    bench_rand5_instructions()


@aarch64_only
def test_bench_rand5_ins_aarch64(init_palmed, in_tmp_dir, init_palmed_armv8a):
    bench_rand5_instructions()


@x86_only
def test_bench_mingled(init_palmed, in_tmp_dir, init_palmed_x86):
    b = bench.Benchmark(
        ins.Instruction_Pipedream("SMSW_GPR64i64"),
        1 / 3,
        ins.Instruction_Pipedream("CWD"),
        1,
    )
    b._IPC = -1  # force recomputation of the benchmark
    assert b.IPC() > 0
    assert b._lcm == 3
