import os
import tempfile
import platform
import pytest
import palmed.palmed as pmd
import palmed.db as db
import palmed.checkpoints as chk

from palmed.config import Config
from palmed.ports import Port
from palmed.instructions import Instruction

PMD_IS_INIT = False


@pytest.fixture(scope="session")
def init_palmed():
    global PMD_IS_INIT
    if not PMD_IS_INIT:
        pmd.init_multiprocessing()
        db.init_engine()
        PMD_IS_INIT = True


HOST = platform.uname().machine
NB_CPU = os.cpu_count()
assert NB_CPU is not None
if HOST == "x86_64":
    NB_THREAD = int(NB_CPU / 2) - 1
else:
    NB_THREAD = max(NB_CPU - 1, 1)
assert NB_THREAD > 0


def reset_ins():
    Port.reset()
    Instruction.reset_cache()


@pytest.fixture
def in_tmp_dir(monkeypatch):
    with tempfile.TemporaryDirectory(prefix="palmed-test-", dir="/tmp") as tmpdir:
        os.mkdir(tmpdir + os.path.sep + chk.Checkpoint.ROOT_DIR.as_posix())
        monkeypatch.chdir(tmpdir)
        yield


@pytest.fixture
def init_palmed_x86():
    parser = pmd.init_parser()
    args_parsed = parser.parse_args(["--arch", "EXPL", "--quiet", "--isa", "x86"])
    reset_ins()
    Config.init_from_cli_args(args_parsed)
    assert Config.PIPEDREAM
    yield Config


@pytest.fixture
def init_palmed_armv8a():
    parser = pmd.init_parser()
    args_parsed = parser.parse_args(["--arch", "EXPL", "--quiet", "--isa", "ARMv8a"])
    reset_ins()
    Config.init_from_cli_args(args_parsed)
    assert Config.PIPEDREAM
    yield Config


@pytest.fixture
def init_palmed_localarch():
    parser = pmd.init_parser()
    if HOST == "x86_64":
        isa = "x86"
    elif HOST == "aarch64":
        isa = "ARMv8a"
    args_parsed = parser.parse_args(["--arch", "EXPL", "--quiet", "--isa", isa])
    reset_ins()
    Config.init_from_cli_args(args_parsed)
    assert Config.PIPEDREAM
    yield Config


def x86_only(fun):
    return pytest.mark.skipif(HOST != "x86_64", reason="x86 host required")(fun)


def aarch64_only(fun):
    return pytest.mark.skipif(HOST != "aarch64", reason="ARMv8a host required")(fun)
