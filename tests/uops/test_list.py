import random
import pytest
import palmed.instructions as ins
from tests.init_palmed import *


def test_load_skl_uops(init_palmed_x86):
    _ = ins.load_uops("SKL")


def test_load_aarch64_uops(init_palmed_armv8a):
    _ = ins.load_uops("ARMv8a")  # Will not load any instruction
