import random
import pytest
import palmed.instructions as ins
import palmed.mapping as map
import palmed.heuristics as heu
import palmed.sol2output as s2o
from tests.init_palmed import *


def select_kernel(arch: str):
    ins_uops = ins.load_uops(arch)
    classes_hipc, instr = map.get_high_ipc_classes(ins_uops)
    assert len(classes_hipc[ins.ISA_BASE]) > 0
    kernel, kernel_benchs, max_clique, min_order = heu.select_kernel(classes_hipc)
    assert kernel
    assert kernel_benchs
    assert max_clique
    assert min_order
    return kernel, kernel_benchs, max_clique, min_order


def test_select_kernel_SKL(init_palmed_localarch):
    Config = init_palmed_localarch
    Config.MU_ARCH = "SKL"
    Config.init_uops()
    select_kernel(Config.MU_ARCH)


def test_select_kernel_CON(init_palmed_localarch):
    Config = init_palmed_localarch
    Config.MU_ARCH = "CON"
    Config.init_uops()
    select_kernel(Config.MU_ARCH)
