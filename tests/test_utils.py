import os
from palmed.utils import BenchmarkScheduler
from tests.init_palmed import *


def test_sched_nop(in_tmp_dir, init_palmed_localarch):
    before = os.sched_getaffinity(0)
    with BenchmarkScheduler(nop=True) as scheduler:
        cur_affinity = os.sched_getaffinity(0)
        assert before == cur_affinity


@pytest.mark.skipif(os.cpu_count() == 1, reason="More that one core is required")
def test_sched_minus1_nopin(in_tmp_dir, init_palmed_localarch):
    with pytest.raises(AssertionError):
        with BenchmarkScheduler(core=-1) as scheduler:
            pass


@pytest.mark.skipif(os.cpu_count() == 1, reason="More that one core is required")
def test_sched_minus1_pinned(in_tmp_dir, init_palmed_localarch):
    pinedcore = os.cpu_count() - 1
    with BenchmarkScheduler(core=pinedcore):
        with BenchmarkScheduler(core=-1) as scheduler:
            cur_affinity = os.sched_getaffinity(0)
            assert cur_affinity == set([pinedcore])


def test_sched_none(in_tmp_dir, init_palmed_localarch):
    with BenchmarkScheduler(core=None) as scheduler:
        cur_affinity = os.sched_getaffinity(0)
        assert len(cur_affinity) == 1 and (2 in cur_affinity or (os.cpu_count() < 3))


def test_sched_last(in_tmp_dir, init_palmed_localarch):
    avail = list(os.sched_getaffinity(0))
    avail.sort(reverse=True)
    top = avail[0]
    with BenchmarkScheduler(core=top) as scheduler:
        cur_affinity = os.sched_getaffinity(0)
        assert list(cur_affinity) == [top]
