import pytest
import palmed.sol2output as s2o
import palmed.instructions as ins

from tests.init_palmed import *


def init_graph():
    i0 = ins.Instruction_UOPS(
        "TestP0",
        IPC=1,
        lat=1,
        ports="1*p0",
    )
    i1 = ins.Instruction_UOPS("TestP1", IPC=1, lat=1, ports="1*p1")
    i01 = ins.Instruction_UOPS("TestP01", IPC=2, lat=1, ports="1*p01")
    ret = {i0: [(1, "R0"), (0.5, "R2")], i1: [(1, "R1"), (0.5, "R2")], i01: [(1, "R2")]}
    label = {"R0": "P0", "R1": "P1", "R2": "P01"}
    instr_to_classes = {i: [i] for i in ret.keys()}
    Port.compute_closure()
    return ret, label, instr_to_classes


def test_export_dot(in_tmp_dir, init_palmed_localarch):
    ins_graph, label, _ = init_graph()
    checked_graph, _ = s2o.check_edges(ins_graph, label)
    s2o.export_dot(checked_graph)
    s2o.run_dot()


def test_print_mapping(init_palmed_localarch, capfd):
    ins_graph, label, instr_to_classes = init_graph()
    s2o.print_mapping(ins_graph, label)
    out, err = capfd.readouterr()
    checked_graph, missed_res = s2o.check_edges(ins_graph, label)
    s2o.print_errors(
        checked_graph,
        None,
        instr_to_classes,
        missed_res,
        {"BASE_ISA": list(ins_graph.keys())},
    )
    out, err = capfd.readouterr()
    assert out == ""


def test_save_mapping(in_tmp_dir, init_palmed_localarch):
    mapping, label, instr_to_classes = init_graph()
    filename = "test_save_mapping.tmp"
    s2o.save_mapping(mapping, label, instr_to_classes, filename)
    mapping_read, label_read, class_to_instrs_read = s2o.read_mapping(filename)
    assert mapping_read == mapping
    assert label_read == label
    assert class_to_instrs_read == instr_to_classes
