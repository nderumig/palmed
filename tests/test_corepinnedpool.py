import os
import pytest
from tests.init_palmed import *
from tests.init_palmed import NB_THREAD


def fun_affinity(_):
    cur_affinity = os.sched_getaffinity(0)
    assert len(cur_affinity) == 1, cur_affinity
    return 0


class _TestException(Exception):
    pass


def fun_pyerror(i):
    assert i == 42
    raise _TestException()
    return 0


def fun_zero(_):
    return 0


def test_corepinnedpool_noerror(init_palmed):
    from palmed.pinned_pool import CorePinnedPool

    ret = []
    excp = False
    with CorePinnedPool(NB_THREAD) as pool:
        for ret in pool.imap_unordered(fun_zero, [42 for i in range(10)]):
            assert ret == 0


def test_pool_pyerror(init_palmed):
    from multiprocessing import Pool

    ret = []
    excp = False
    try:
        with Pool(NB_THREAD) as pool:
            for ret in pool.imap_unordered(fun_pyerror, [42 for i in range(10)]):
                assert False
    except _TestException as te:
        excp = True
    assert excp, "Exception was supposed to happen but never arrived"


def test_corepinnedpool_pyerror(init_palmed):
    from palmed.pinned_pool import CorePinnedPool

    ret = []
    try:
        with CorePinnedPool(NB_THREAD) as pool:
            for ret in pool.imap_unordered(fun_pyerror, [42 for i in range(10)]):
                assert False
    except _TestException as te:
        pass


def test_corepinnedpool_aff1(init_palmed):
    from palmed.pinned_pool import CorePinnedPool

    with CorePinnedPool(1) as pool:
        for ret in pool.imap_unordered(fun_affinity, range(10)):
            assert ret == 0


def test_corepinnedpool_aff(init_palmed):
    from palmed.pinned_pool import CorePinnedPool

    with CorePinnedPool(NB_THREAD) as pool:
        for ret in pool.imap_unordered(fun_affinity, range(10)):
            assert ret == 0
