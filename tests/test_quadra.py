import random
import pytest
import os

from tests.init_palmed import *
from tests.init_palmed import NB_THREAD
from tests.test_bench_instructions import bench_rand5_instructions
from palmed.utils import BenchmarkScheduler
import palmed.instructions as ins
import palmed.quadra as qu


def test_bench_rand5_quadra_ins_1core(init_palmed, in_tmp_dir, init_palmed_localarch):
    ins5 = bench_rand5_instructions()
    qu.compute_benchs(ins5, "example.chk", 1)


@pytest.mark.skipif(NB_THREAD <= 2, reason="Multicore requested to test compute_bench")
def test_bench_rand5_quadra_multicore(init_palmed, in_tmp_dir, init_palmed_localarch):
    ins5 = bench_rand5_instructions()
    ret = qu.compute_benchs(ins5, "example.chk", NB_THREAD)
    for row in ret:
        for b in row:
            assert b.IPC() > 0


def test_bench_empty_quadra(init_palmed, in_tmp_dir, init_palmed_localarch):
    qu.compute_benchs([], "example.chk", 1)
