"""Enrich BenchmarkAggregationMethod

Revision ID: f70587cf7d3a
Revises: e6c365630cc2
Create Date: 2021-01-05 16:31:29.607247

"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "f70587cf7d3a"
down_revision = "e6c365630cc2"
branch_labels = None
depends_on = None

enum_name = "benchmarkaggregationmethod"
tmp_enum_name = "tmp_" + enum_name
table_name = "benchmark_measure"
column_name = "aggregation_mode"

old_options = ("UNDEF", "MAX_VAL")
new_options = old_options + ("MEAN_25_UPPER_PERCENT",)

# Create enum fields
old_type = sa.Enum(*old_options, name=enum_name)
new_type = sa.Enum(*new_options, name=enum_name)


def upgrade():
    op.execute("ALTER TYPE " + enum_name + " RENAME TO " + tmp_enum_name)
    new_type.create(op.get_bind())
    op.execute(
        (
            "ALTER TABLE {table} ALTER COLUMN {column} TYPE {enum_name} USING "
            "{column}::text::{enum_name}"
        ).format(table=table_name, column=column_name, enum_name=enum_name)
    )
    op.execute("DROP TYPE " + tmp_enum_name)


def downgrade():
    table = sa.sql.table(table_name, sa.Column(column_name, new_type, nullable=False))
    op.execute(
        table.update()
        .where(table.c.aggregation_mode.in_(new_options))
        .values(aggregation_mode="UNDEF")
    )
    op.execute("ALTER TYPE " + enum_name + " RENAME TO " + tmp_enum_name)
    old_type.create(op.get_bind())
    op.execute(
        (
            "ALTER TABLE {table} ALTER COLUMN {column} TYPE {enum_name} USING "
            "{column}::text::{enum_name}"
        ).format(table=table_name, column=column_name, enum_name=enum_name)
    )
    op.execute("DROP TYPE " + tmp_enum_name)
