"""Rename enumeration {,throughput_}benchmarkaggregationmethod

Revision ID: dc1493d48e12
Revises: 5468f00fe36f
Create Date: 2021-01-05 17:29:37.472660

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "dc1493d48e12"
down_revision = "5468f00fe36f"
branch_labels = None
depends_on = None

enum_name = "benchmarkaggregationmethod"
new_enum_name = "throughput_" + enum_name
# table_name = "benchmark_measure"
# column_name = "aggregation_mode"


def upgrade():
    op.execute("ALTER TYPE " + enum_name + " RENAME TO " + new_enum_name)


def downgrade():
    op.execute("ALTER TYPE " + new_enum_name + " RENAME TO " + enum_name)
